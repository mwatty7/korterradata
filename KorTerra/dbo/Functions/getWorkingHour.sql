﻿Create function [dbo].[getWorkingHour](@Tstartdate datetime,@Tenddate datetime)
returns int
as
begin
	  
--    declare @Tstartdate datetime,@Tenddate datetime
--    set @Tstartdate='08/01/2011 08:30:40'
--    set @Tenddate='08/02/2011 09:30:40'
      declare @startdate varchar(10),@enddate varchar(10),@daydiff int
      declare @minCount int
      set @startdate=convert(varchar,cast(@Tstartdate as datetime),101)
      set @enddate=convert(varchar,cast(@Tenddate as datetime),101)
      set @daydiff = datediff(dd,@startdate,@enddate)
      set @minCount=0
      if @daydiff=0 
		begin
            -- Checks whether starttime and endtime is within working hour
            if (@Tstartdate between cast(@startdate + ' ' + '08:00' as datetime) and cast(@startdate + ' ' + '16:00' as datetime)) 
                  AND (@Tenddate between cast(@startdate + ' ' + '08:00' as datetime) and cast(@startdate + ' ' + '16:00' as datetime)) 
            begin
                  set @minCount=@minCount + datediff(n,@Tstartdate,@Tenddate)
            end
            else 
            begin
                  if @Tstartdate < cast(@startdate + ' ' + '08:00' as datetime) AND @Tenddate > cast(@startdate + ' ' + '16:00' as datetime)
                  begin
                        set @minCount=@minCount + 480  -- add complete one working day in min
                  end
                  else
                  begin
                        if @Tstartdate < cast(@startdate + ' ' + '08:00' as datetime) and @Tenddate between cast(@startdate + ' ' + '08:00' as datetime) and cast(@startdate + ' ' + '16:00' as datetime)
                        begin
                              set @minCount=@minCount + datediff(n,cast(@startdate + ' ' + '08:00' as datetime),@Tenddate)
                        end
                        if @Tenddate > cast(@startdate + ' ' + '16:00' as datetime) and @Tstartdate between cast(@startdate + ' ' + '08:00' as datetime) and cast(@startdate + ' ' + '16:00' as datetime)
                        begin
                              set @minCount=@minCount + datediff(n,@Tstartdate,cast(@startdate + ' ' + '16:00' as datetime))
                        end
                  end
            end
		end -- end of daydiff=0
		else
		begin
           declare @date datetime
           set @date=cast(@startdate as datetime)
           while @date <= @enddate
           begin 
				--Checking Sat and Sun
				if  datepart(dw,@date)<>1  and datepart(dw,@date)<>7 --and datepart(dd,@date)<>25 and datepart(mm,@date)<>12 and datepart(dd,@date)<>1 and datepart(mm,@date)<>1
				begin
                  --calculating min for start day
                  if datediff(d,@date,@startdate)=0 
                  begin
                        if (@Tstartdate between cast(@startdate + ' ' + '08:00' as datetime) and cast(@startdate + ' ' + '16:00' as datetime)) 
                        begin
                              set @minCount=@minCount + datediff(n,@Tstartdate,cast(@startdate + ' ' + '16:00' as datetime))
                        end
                        else
                        begin
								if @Tstartdate < cast(@startdate + ' ' + '08:00' as datetime)  set @minCount=@minCount + 480
                        end
                  end
                  --calculating min for days between start and end day
                  if datediff(d,@date,@startdate) <> 0 and datediff(d,@date,@enddate)<> 0 set @minCount=@minCount + 480
				  --calculating min for end day
			      if datediff(d,@date,@enddate)=0 
					  begin
							if (@Tenddate between cast(@enddate + ' ' + '08:00' as datetime) and cast(@enddate + ' ' + '16:00' as datetime)) 
							begin
								  set @minCount=@minCount + datediff(n,cast(@enddate + ' ' + '08:00' as datetime),@Tenddate)
							end
							else
							begin
								  if @Tenddate > cast(@enddate + ' ' + '16:00' as datetime) set @minCount=@minCount + 480
							end
					   end
                  end   --- this is if end part
				  set @date=dateadd(d,1,@date)
            end -- while end
	end
	return @minCount
end