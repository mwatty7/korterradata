﻿



CREATE FUNCTION [dbo].[howLateByDueDate] (@thisdate datetime, @duedate datetime, @firstcompletiondate datetime)

RETURNS INT
AS
    BEGIN
		RETURN
			CASE
			   WHEN @firstcompletiondate IS NULL THEN DATEDIFF(MINUTE, @duedate, @thisdate)
			   ELSE DATEDIFF(MINUTE, @duedate, @firstcompletiondate)
			END
	END