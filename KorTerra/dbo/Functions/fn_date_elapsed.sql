﻿CREATE FUNCTION [dbo].[fn_date_elapsed](
	@minDate DATETIME,
	@maxDate DATETIME
)
RETURNS VARCHAR(20) AS
BEGIN
RETURN CAST(
        (CAST(CAST(@maxDate AS FLOAT) - CAST(@minDate AS FLOAT) AS INT) * 24) /* hours over 24 */
        + DATEPART(HH, @maxDate - @minDate) /* hours */
        AS VARCHAR(10))
    + ':' + RIGHT('0' + CAST(DATEPART(MI, @maxDate - @minDate) AS VARCHAR(2)), 2) /* minutes */
    + ':' + RIGHT('0' + CAST(DATEPART(SS, @maxDate - @minDate) AS VARCHAR(2)), 2) /* seconds */
    + '.' + RIGHT('00' + CAST(DATEPART(MS, @maxDate - @minDate) AS VARCHAR(3)), 3) /* milliseconds */
END