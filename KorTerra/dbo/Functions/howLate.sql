﻿


CREATE FUNCTION [dbo].[howLate] ( @datetype char(1), @thisdate datetime, @duedate datetime, @wtbdate datetime, @firstcompletiondate datetime)

RETURNS INT
AS
    BEGIN
			RETURN
			  CASE WHEN @datetype <> '1' 
   			    THEN 
					CASE
					   WHEN @firstcompletiondate IS NULL THEN DATEDIFF(MINUTE, @duedate, @thisdate)
					   ELSE DATEDIFF(MINUTE, @duedate, @firstcompletiondate)
					END
				ELSE
					CASE
					   WHEN @firstcompletiondate IS NULL THEN DATEDIFF(MINUTE, @wtbdate, @thisdate)
					   ELSE DATEDIFF(MINUTE, @wtbdate, @firstcompletiondate)
					END
				END
	END