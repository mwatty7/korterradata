﻿CREATE FUNCTION [dbo].[GetUTC] ( @date AS datetime, @timezone as varchar(16), @direction int)
RETURNS DATETIME
AS
    BEGIN
        DECLARE
			@StartOfNovember DATETIME ,
            @DstEnd DATETIME,
			@StartOfMarch DATETIME ,
            @DstStart DATETIME 
	        SET @StartOfMarch = DATEADD(MONTH, 2,
                                    DATEADD(YEAR,  DATEPART(year, @date) - 1900, 0))
		    SET @DstStart = DATEADD(HOUR, 2,
                                DATEADD(day,
                                        ( ( 15 - DATEPART(dw,
                                                          @StartOfMarch) )
                                          % 7 ) + 7, @StartOfMarch))
 
			SET @StartOfNovember = DATEADD(MONTH, 10,
                                       DATEADD(YEAR,  DATEPART(year, @date) - 1900, 0))
		    SET @DstEnd = DATEADD(HOUR, 2,
                              DATEADD(day,
                                      ( ( 8 - DATEPART(dw,
                                                       @StartOfNovember) )
		                                    % 7 ), @StartOfNovember))
			RETURN
				CASE
				   WHEN (@date between dbo.GetDstStart(YEAR(@date)) AND  dbo.GetDstEnd(YEAR(@date))) THEN
					   CASE
						   WHEN @timezone = 'CENTRAL' THEN DATEADD(hh, -5 * @direction, (@date)) 
						   WHEN @timezone = 'ALASKAN' THEN DATEADD(hh, -8 * @direction, (@date)) 
						   WHEN @timezone = 'EASTERN' THEN DATEADD(hh, -4 * @direction, (@date)) 
						   WHEN @timezone = 'MOUNTAIN' THEN DATEADD(hh,-6 * @direction, (@date)) 
						   WHEN @timezone = 'PACIFIC' THEN DATEADD(hh, -7 * @direction, (@date)) 
						   ELSE DATEADD(hh, -5 * @direction, (@date)) 
					   END
					   ELSE
					   CASE
						   WHEN @timezone = 'CENTRAL' THEN DATEADD(hh, -6 * @direction, (@date)) 
						   WHEN @timezone = 'ALASKAN' THEN DATEADD(hh, -9 * @direction, (@date)) 
						   WHEN @timezone = 'EASTERN' THEN DATEADD(hh, -5 * @direction, (@date)) 
						   WHEN @timezone = 'MOUNTAIN' THEN DATEADD(hh,-7 * @direction, (@date)) 
						   WHEN @timezone = 'PACIFIC' THEN DATEADD(hh, -8 * @direction, (@date)) 
						   ELSE DATEADD(hh, -6  * @direction, (@date))
					   END
					END 
		
				END