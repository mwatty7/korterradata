﻿


CREATE FUNCTION [dbo].[isLateByDueDate] (@fromdate AS datetime, @throughdate datetime, @duedate datetime, @firstcompletiondate datetime)

RETURNS INT
AS
    BEGIN
		RETURN
			CASE
			   WHEN (@firstcompletiondate is NULL AND @duedate < GetDate()) THEN 1 
			   WHEN @duedate < @firstcompletiondate THEN 1
			   ELSE 0
			END
	END