﻿


CREATE FUNCTION [dbo].[isLate] ( @datetype char(1), @fromdate AS datetime, @throughdate datetime, @duedate datetime, @wtbdate datetime, @firstcompletiondate datetime)

RETURNS INT
AS
    BEGIN
			RETURN
			  CASE WHEN @datetype <> '1' 
   			    THEN 
					CASE
					   WHEN (@duedate NOT between @fromdate and @throughdate  + ' 23:59:59') THEN 0
					   WHEN @firstcompletiondate is NULL THEN 1
					   WHEN @duedate < @firstcompletiondate THEN 1
					   ELSE 0
					END
				ELSE
					CASE
					   WHEN (@wtbdate NOT between @fromdate and @throughdate + ' 23:59:59') THEN 0
					   WHEN @firstcompletiondate is NULL THEN 1
					   WHEN @wtbdate < @firstcompletiondate THEN 1
					   ELSE 0
					END
				END
	END