﻿CREATE FUNCTION dbo.fn_Business_Days_Between_Dates
(
@DateStart DATETIME,
@DateEnd DATETIME
)
returns int
--AS
BEGIN

DECLARE @BusinessDays INT --OUTPUT
DECLARE @RAWDAYS INT

IF DATEPART(dw,@DateEnd) = 1 
SET @DateEnd = DATEADD(day, -2, @DateEnd)
ELSE IF DATEPART(dw,@DateEnd) = 7
SET @DateEnd = DATEADD(day, -1, @DateEnd)

----The SET DATEFIRST was part of the original StorProc.
----I moved it outside (see usage) to allow me to wrap this script within a function
--SET DATEFIRST 1

SELECT @RAWDAYS = DATEDIFF(day, @DateStart, @DateEnd ) -
( 2 * DATEDIFF( week, @DateStart, @DateEnd ) ) -
CASE WHEN DATEPART( weekday, @DateStart + @@DATEFIRST ) = 1 THEN 1 ELSE 0 END -
CASE WHEN DATEPART( weekday, @DateEnd + @@DATEFIRST ) = 1 THEN 1 ELSE 0 END

--SELECT @BusinessDays = @RAWDAYS - COUNT(*)
--FROM holiday
--WHERE Holiday_Date BETWEEN @DateStart AND @DateEnd

return @BusinessDays;
END