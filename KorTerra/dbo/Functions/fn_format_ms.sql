﻿CREATE FUNCTION [dbo].[fn_format_ms](
	@total BIGINT
)
RETURNS VARCHAR(20) AS
BEGIN
	DECLARE @remaining BIGINT = @total
	DECLARE @ms INT = @remaining % 1000
	SET @remaining = @remaining / 1000
	DECLARE @secs INT = @remaining % 60
	SET @remaining = @remaining / 60
	DECLARE @mins INT = @remaining % 60
	SET @remaining = @remaining / 60
	RETURN CAST(@remaining AS VARCHAR) + ':' 
		+ CAST(@mins AS VARCHAR(2)) + ':' 
		+ CAST(@secs AS VARCHAR(2)) + '.' 
		+ CAST(@ms AS VARCHAR(3))
END