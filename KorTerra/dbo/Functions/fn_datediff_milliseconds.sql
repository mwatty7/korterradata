﻿CREATE FUNCTION [dbo].[fn_datediff_milliseconds](
	@fromDate DATETIME2,
	@toDate DATETIME2
)
RETURNS BIGINT AS
BEGIN
	DECLARE @days BIGINT = DATEDIFF(DAY, @fromDate, @toDate)
	DECLARE @ms BIGINT = DATEDIFF(MS, DATEADD(DAY, @days, @fromDate), @toDate)
	DECLARE @sign INT = IIF(SIGN(@days) >= 0, 1, -1)
	SET @days = ABS(@days)
	DECLARE @diff BIGINT = @sign * (@days * 24 * 60 * 60 * 1000 + @ms)
	RETURN @diff
END