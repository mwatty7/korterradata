﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbyoperatoridv3]
	@customerid varchar(32),
	@operatorid varchar(32),
	@regionid varchar(32),
	@districtid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32),
	@summary bit = 0,
	@detail bit = 1,
	@statusCounts bit = 1,
	@offset int = 0
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @operator varchar(32) = @operatorid
DECLARE @region varchar(32) = @regionid
DECLARE @district varchar(32) = @districtid
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_summary bit = @summary
DECLARE @_statusCounts bit = @statusCounts
DECLARE @nowUtc DATETIME = SYSUTCDATETIME()
create table #opcounts (
	customerid varchar(32),
	operatorid varchar(32),
	jobcount bigint
)
create table #summary(
	customerid varchar(32),
	LateCount bigint, 
	CompletedLate bigint,
	OpenLate bigint,
	avgHrs bigint,
	avgMins bigint,
	operatorid varchar(32),
	operatordescription varchar (128),
	TotalTicketCount bigint
)
BEGIN
 if @date = '1' 
	SET @through = dateAdd(dy, 1, @throughDate)
 BEGIN
	SET @from = dbo.DateOnly(dateAdd(MINUTE, @offset,  @from))
	SET @through = dbo.DateOnly(dateAdd(MINUTE, @offset, @through))
 END
 if @_detail = 1
   BEGIN 
	 if @output = 'detailed'
		WITH detailed (customerid,jobid, mobileid, mobiledescription, worktype, Transmit, Wtb, duedate, origpriority, address, street, city, county, state, isproject, status, membercode, regionid, districtid, lateHours, lateMinutes, firstcompletiondt,  CompletedLate, OpenLate, AllLate, operatorid, operatordescription )
		AS
		(SELECT TOP 1000 
			job.customerid as customerid,
			job.jobid as jobid,
			job.mobileid as mobileid,
			mobile.description as mobiledescription,
			job.worktype as worktype,
			CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(job.duedateutc,occ.timezone, 1) as duedate,
			job.origpriority as origpriority,
			job.address as address, 
			job.street as street,
			job.city as city, 
			job.county as county, 
			job.state as state,
			job.isproject as isproject,
			--job.county as county,
			job.status as status,
			request.membercode as membercode,
			mobile.regionid as regionid,
			mobile.districtid as districtid,
			(DATEDIFF(minute,job.duedateutc, ISNULL(request.firstcompletiondateutc, @nowUtc)) / 60) as lateHours,
			(DATEDIFF(minute, job.duedateutc, ISNULL(request.firstcompletiondateutc, @nowUtc)) % 60) AS lateMinutes,
			dbo.GetUTC(request.firstcompletiondateutc, occ.occid, 1) AS firstcompletiondt,  
			CASE when request.firstcompletiondtdate is not null
				THEN 1
				ELSE 0
			END as CompletedLate,
			CASE when request.firstcompletiondtdate is null
				THEN 1
				ELSE 0
			END as OpenLate,
			1 as AllLate,
			vc.operatorid as operatorid,
			operator.description as operatordescription
		FROM job WITH(NOLOCK)
			-- Rule I: Tickets with multi member codes, Late ticket will count once in each member code.
			JOIN request WITH(NOLOCK) on request.customerid = job.customerid AND request.jobid = job.jobid
			JOIN occ on request.customerid = occ.customerid AND occ.occid = request.xmitsource
			LEFT JOIN mobile WITH(NOLOCK) on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			OUTER APPLY (
				SELECT TOP 1 operatorid FROM visitcommon WITH(NOLOCK)
				WHERE visitcommon.customerid = job.customerid AND visitcommon.jobid = job.jobid AND visitcommon.membercode = request.membercode AND visitcommon.completiondateutc = request.firstcompletiondateutc
			) as vc
			LEFT JOIN operator on operator.customerid = job.customerid AND operator.operatorid = vc.operatorid
		WHERE
			-- Filter by customerid always
	
			job.customerid = @customerid AND
			-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
			((@dateType = '1' AND (job.duedateutc) BETWEEN @from AND @through) OR
			(@dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through))) AND
		-- Rule G: Filter out all cancelled / nlr tickets
			job.status NOT IN ('R', 'X') AND
			-- Rule C: Filter out updates without completiondt
			(job.status = 'U' AND request.firstcompletiondtdate IS NOT NULL OR job.status != 'U') AND 
			-- Rule B: Late is always determined by WTB date 
			-- Rule H: If One facility for a membercode is on time, count it as On Time. ONLY LOOK AT FIRST COMPLETION PER MEMBERCODE
			((request.firstcompletiondateutc >  job.duedateutc) OR
			-- Rule D: Membercodes still open are late if "right now" is late.
			(request.firstcompletiondateutc IS NULL AND @nowUtc > job.duedateutc))
			-- These rules are inherited because we are not looking at completion levels
			-- Rule E: Reopened are treated like partials - if one complete was on time, we count it as on time.
			-- Rule F: Partial completes count as complete.
			-- Filter Emergencies and Projects by user criteria
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
			--Filter By membercode and memberid by user criteria
			AND ((@operator = '0' AND operator.operatorid IS NULL) OR (@operator = '0' OR vc.operatorid = @operator))
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)--)
			)
			SELECT * FROM detailed WITH(NOLOCK) ORDER BY jobid DESC
		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WITH(NOLOCK) WHERE customerid = @customer
	
END
IF @_summary = 1
	BEGIN
	IF @output = 'summary' 
		   insert into #opcounts
		   
				SELECT 
					jb.customerid, operatorid, COUNT(DISTINCT jb.jobid)
				FROM job jb
					JOIN request r on r.customerid = jb.customerid AND r.jobid = jb.jobid
					LEFT JOIN mobile m on m.customerid = jb.customerid AND m.mobileid = jb.mobileid
					OUTER APPLY (
						SELECT TOP 1 operatorid FROM visitcommon
						WHERE visitcommon.customerid = jb.customerid AND visitcommon.jobid = jb.jobid AND visitcommon.membercode = r.membercode AND visitcommon.completiondateutc = r.firstcompletiondateutc
						ORDER by r.firstcompletiondateutc, visitcommon.creationdt
					) as v
				WHERE
					jb.customerid = @customerid AND
					jb.status NOT IN ('R', 'X') AND
					((@dateType = '1' AND (jb.duedateutc) BETWEEN @from AND @through) OR
					(@dateType = '2' AND ((jb.latestxmitdtdate BETWEEN @from AND @through)))) --AND
					AND (@emergencies = 'Yes' OR jb.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR jb.isproject = '0')
					AND (@district = '0' OR m.districtid = @district)
					AND (@region = '0' OR m.regionid = @region)
				group by jb.customerid, operatorid
				--select * from #opcounts
				update #opcounts set operatorid ='N/A' where operatorid is null;
END			
	 
IF @_summary = 1
	BEGIN
	IF @output = 'summary' 
		 insert into #summary (customerid,  LateCount, CompletedLate, OpenLate, avgHrs, avgMins, operatorid, operatordescription)
			SELECT
			job.customerid,
			CAST(COUNT_BIG(job.jobid) as bigint) LateCount,
			SUM(CASE when request.firstcompletiondtdate is not null
				THEN 1
				ELSE 0
			END) as CompletedLate,
			SUM (CASE when request.firstcompletiondtdate is null
				THEN 1
				ELSE 0
			END) as OpenLate,
			((SUM(CAST(DATEDIFF(minute, job.duedateutc, ISNULL(request.firstcompletiondateutc, @nowUtc))as bigint)) / COUNT_BIG(job.jobid)) / 60) as avgHrs,
			((SUM(CAST(DATEDIFF(minute, job.duedateutc, ISNULL(request.firstcompletiondateutc, @nowUtc))as bigint)) / COUNT_BIG(job.jobid)) % 60) as avgMins,
			vc.operatorid,
			operator.description as operatordescription
		
		FROM job WITH(NOLOCK)
		-- Rule I: Tickets with multi member codes, Late ticket will count once in each member code.
			JOIN request WITH(NOLOCK) on request.customerid = @customerid AND request.jobid = job.jobid
			LEFT JOIN mobile WITH(NOLOCK) on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			OUTER APPLY (
				SELECT TOP 1 operatorid FROM visitcommon WITH(NOLOCK)
				WHERE visitcommon.customerid = job.customerid AND visitcommon.jobid = job.jobid AND visitcommon.membercode = request.membercode AND visitcommon.completiondateutc = request.firstcompletiondateutc
			) as vc
			LEFT JOIN operator on operator.customerid = @customerid AND operator.operatorid = vc.operatorid
		WHERE
			-- Filter by customerid always
			job.customerid = @customerid AND
			-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
   		   ((@dateType = '1' AND duedateutc BETWEEN @from AND @through) OR
			@dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through)) AND
			-- Rule G: Filter out all cancelled / nlr tickets
			(job.status NOT IN ('R', 'X') AND
			-- Rule C: Filter out updates without completiondt
			(job.status = 'U' AND request.firstcompletiondtdate IS NOT NULL OR job.status != 'U')) AND 
			-- Rule B: Late is always determined by UTC duedate
			-- Rule H: If One facility for a membercode is on time, count it as On Time. ONLY LOOK AT FIRST COMPLETION PER MEMBERCODE
			((request.firstcompletiondateutc >  job.duedateutc OR
			-- Rule D: Membercodes still open are late if "right now" is late.
			(request.firstcompletiondateutc IS NULL AND @nowUtc > job.duedateutc))
			-- These rules are inherited because we are not looking at completion levels
			-- Rule E: Reopened are treated like partials - if one complete was on time, we count it as on time.
			-- Rule F: Partial completes count as complete.
			-- Filter Emergencies and Projects by user criteria
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
			--Filter By membercode and memberid by user criteria
			AND ((@operator = '0' AND operator.operatorid IS NULL) OR (@operator = '0' OR vc.operatorid = @operator))
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region))
		GROUP BY job.customerid, vc.operatorid, operator.description
		--ORDER BY vc.operatorid 
		
		--Set null operatorids to 'N/A'
		UPDATE #summary set operatorid ='N/A' where operatorid is null;
		--Set operator desc fields 
		UPDATE #summary 
		set operatordescription = operator.description
		FROM #summary
		join operator on #summary.operatorid = operator.operatorid and operator.customerid = @customerid
		UPDATE #summary
		set TotalTicketCount = #opcounts.jobcount
		FROM #summary
		join #opcounts on #opcounts.operatorid = #summary.operatorid and #opcounts.customerid = #summary.customerid
		
		SELECT * from  #summary	
		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WITH(NOLOCK) WHERE customerid = @customer
	END
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'sp_kt_reports_lateticketsbymobileidv3')
BEGIN
	EXEC('CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymobileidv3] AS BEGIN SET NOCOUNT ON; END')
END
/*********************************************************************************************************************/
/****** Object:  StoredProcedure [dbo].[sp_kt_reports_lateticketsbymobileidv3]    Script Date: 6/7/2018 1:12:22 PM ******/
SET ANSI_NULLS ON