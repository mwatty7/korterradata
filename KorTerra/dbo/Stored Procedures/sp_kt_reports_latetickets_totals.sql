﻿CREATE PROCEDURE [dbo].[sp_kt_reports_latetickets_totals]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@operatorid varchar(32),
	@mobileid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeNLR varchar(16),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
				SELECT
					job.jobid,
					job.worktype,
					CONVERT(VARCHAR, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
					request.firstcompletiondt, 
					job.origpriority,
					job.status
				FROM 
					JOB
					OUTER APPLY 
					(
						SELECT TOP 1
							request.jobid,
							CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
						FROM 
							request
						WHERE 
							request.customerid = job.customerid 
							AND request.jobid = job.jobid
					) request
					OUTER APPLY
					(
						SELECT TOP 1
							mobileid,
							districtid,
							regionid
						FROM
							mobile
						WHERE
							mobile.customerid = job.customerid
							AND mobile.mobileid = job.mobileid
					) as mobile
					OUTER APPLY
					(
						SELECT TOP 1 
							operatorid,
							mobileid
						FROM
							visitcommon
						WHERE
							visitcommon.customerid = job.customerid
							AND visitcommon.jobid = job.jobid
							AND visitcommon.mobileid = job.mobileid
							AND visitcommon.membercode = job.sendto
							AND visitcommon.completiondt = request.firstcompletiondt
					) as visitcommon
					WHERE
						job.customerid = ''' + @customerId + '''
	'
	-- Report Options
	If( @includeNLR = 'No')
		Set @Command = @Command + ' AND job.status NOT IN (''R'', ''X'', ''U'')'
	If( @includeEmergencies = 'No')
		Set @Command = @Command + '  AND job.priority <> ''EMERGENCY'''
	if( @includeProjects = 'No' )
		Set @Command = @Command + '  AND job.isproject = 0'
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(request.firstcompletiondt as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(request.firstcompletiondt as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND (CAST(job.wtbdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.wtbdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(job.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	-- Member / Member Code
	If( @regionid <> '0' )
	   Set @Command = @Command + ' AND (mobile.regionid = ''' + @regionid + ''') '
	If( @districtid <> '0' )
	   Set @Command = @Command + ' AND (mobile.districtid = ''' + @districtid + ''') '
	If( @mobileid <> '0' )
	   Set @Command = @Command + ' AND (visitcommon.mobileid= ''' + @mobileid + ''') '
	If( @operatorid <> '0' )
	   Set @Command = @Command + ' AND (visitcommon.operatorid= ''' + @operatorid + ''') '
	-- End Member / Member Code
	Set @Command = @Command + ' ORDER BY mobile.regionid, mobile.districtid, job.mobileid, visitcommon.operatorid, job.jobid, Wtb'
	Execute SP_ExecuteSQL  @Command
END
--End of Late Tickets Totals