﻿CREATE PROCEDURE [dbo].[sp_remove_customer]
    @customerid VARCHAR(32) = NULL,
    @server VARCHAR(100) = NULL,
    @database VARCHAR(50) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @return Int;
    select @return = count(*)
    from sys.servers
    where name = @server;
    IF @return = 0
   BEGIN
        PRINT 'Error! Source Server Must be linked'
        RETURN -1
    END
    BEGIN
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.AAattachmentexif WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.AAattachment WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.AAattachmentsize WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.AAwidgetAvailable WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.AAwidgetInUse WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.actionaudit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.activity WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.addmembers WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.archsettings WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.area WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.areapoint WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.areatypes WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.attachment WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.attachmentbackward WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.attachmentext WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.audititm WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.auditsend WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.autodept WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.autosendrule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.batch WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.bbillstatus WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.bonusbatch WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.bonusledger WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.bonusperiod WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.c1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.c2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.c3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.c4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.c5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.childdept WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.citycounty WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.company WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.comppref WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.contact WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.contrem WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.currentmobloc WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customactcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customerattribute WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customfaccode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customreport WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customreportsecurity WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.custsearchdtl WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.custsearchhdr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.custwindowfld WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.defaultvisit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.deptbill WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.dptgp WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.dptgpmbr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.duedaterules WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.dupaddr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e6visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e7visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e8visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.e9visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.editaddr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.emailnotify WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.emailtrigger WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.emergencycontact WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.emergencygroup WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_addmembers WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_completion WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_comprem WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_extjob WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_locpoints WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_order WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_remarks WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.exp_ticket WHERE k_customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.expcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.expdetl WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.extjob WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.extjobcol WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visrflag WHERE jobid IN (SELECT jobid FROM [' + @server + '].'  + @database + '.dbo.job WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.f1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.filteraudititm WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.formvalidation WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.g6visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.groupoperator WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.herald WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.heraldmessenger WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.holiday WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.calendar WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMattachment WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMaudit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMconfig WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMcontractor WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMfilter WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMincident WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMincidentContent WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMincidentContributingFactor WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMinvestigationremark WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMpayableclaim WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMreceivableclaim WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMsequence WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMtracelog WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.IMwitness WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.invoice WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.invrule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.j_sortedjob WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobmobilehistory WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobstatus WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.job WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobidsuffix WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.joblock WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobsend WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobsendcompatible WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		--Queue Table -> jobsendcompatibleforward
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobsendcompatibleforward WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobsort WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ledger WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ledgrem WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.locpoints WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.manrequest WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mapannotationpoint where shapeid IN (SELECT shapeid FROM [' + @server + '].'  + @database + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mapannotation WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.masterdept WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mdsisends WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.member WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.membrem WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.membzone WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.memcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.memcodemap WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.menu WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mobdest WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mobilenotification WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mobstat WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.msg WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.msgdata WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.msgview WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		--Queue Table -> msgqueue
		        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.msgqueue WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.nlr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.nlrcmpny WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.nlrdone4 WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.notes WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.occattribute WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.opermember WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.opertype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p6visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p7visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.p8visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.partialprojectreason WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.paymastr WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.payrate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.pigeonhl WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.pmpacket WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.preferences WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.price WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prioritymap WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsaudit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsauditattachment WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsonecalls WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsreasonlist WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsregion WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prssends WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prssetup WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.r1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rawaudit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rawticket WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rdsecurity WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reason WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reasonlist WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.remarks WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END	
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reminderrecipient WHERE reminderid IN 
			(select reminderid FROM [' + @server + '].'  + @database + '.dbo.remindercommon WHERE customerid = ''' + @customerid + ''' )';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.remindernotification WHERE reminderid IN 
			(select reminderid FROM [' + @server + '].'  + @database + '.dbo.remindercommon WHERE customerid = ''' + @customerid + ''' )';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reminderdata WHERE reminderid IN 
			(select reminderid FROM [' + @server + '].'  + @database + '.dbo.remindercommon WHERE customerid = ''' + @customerid + ''' )';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.remindercommon WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		--Queue Table -> reportquerysecurity
		        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.request WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.requesttype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.requesttypetable WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rptdetail WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rptheader WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rptsched WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rptsched_print WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rpttemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rqmemcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rqvaldpt WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rqvalidphone WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.rqvalqty WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.revisedduedatereason WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.s1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.s2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.savedrpttemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.savedrpttemplateline WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		--Queue Table -> screeningqueue
		        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.screeningqueue WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        --ScreeningRuleassignment
        --ScreeningRuleset 
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.security WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.securitygroup WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.seqid WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.servicearea WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.statcols WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.suffixrequesttypes WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.t1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.t2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketentrytype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketflag WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketflagremarks WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketstatus WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tagvalue WHERE tagfieldid IN (SELECT tagfieldid FROM [' + @server + '].'  + @database + '.dbo.tagfield WHERE tagversionid IN (SELECT tagversionid FROM [' + @server + '].'  + @database + '.dbo.tagversion WHERE customerid = ''' + @customerid + '''))';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tagfield WHERE tagversionid IN (SELECT tagversionid FROM [' + @server + '].'  + @database + '.dbo.tagversion WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tagemail WHERE tagversionid IN (SELECT tagversionid FROM [' + @server + '].'  + @database + '.dbo.tagversion WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.emailrecipient WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END	
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.scheduledetail WHERE scheduleversionid IN (SELECT scheduleversionid FROM [' + @server + '].'  + @database + '.dbo.scheduleversion WHERE scheduleid IN (SELECT scheduleid FROM [' + @server + '].'  + @database + '.dbo.schedule WHERE customerid = ''' + @customerid + '''))';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.scheduleversion WHERE scheduleid IN (SELECT scheduleid FROM [' + @server + '].'  + @database + '.dbo.schedule WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.schedule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.taggeometry WHERE tagversionid IN (SELECT tagversionid FROM [' + @server + '].'  + @database + '.dbo.tagversion WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tagjob WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tagversion WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tag WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketupdate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.tplformat WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.u1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.updatetype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.userinfo WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.userquery WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.validdpt WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.validphone WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.validqty WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.validrule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.validterritory WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.valruledpt WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.valruleitm WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitbackward WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitcommon WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitdata WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		--Queue Table -> visitqueue		
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitqueue WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitrem WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitreq WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vmrequesttype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqcustomactcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqcustomfaccode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqreason WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqreasonlist WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqvalidrule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqvalidterritory WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqvalruledpt WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.vrqvalruleitm WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.w1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.w2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.w3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.w4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.w5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.watch WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.wojobid WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.workdetl WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.worktype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.woworktype WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x1visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x2visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x3visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x4visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x5visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x6visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.x7visit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.xmlcompletion WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXconfig WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXemailhistory WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXemaildata WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXsessionduration WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXqueue WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXqueuehistory WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXgenericnotes WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXpreference WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXtermsaccept WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.XXtracelog WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.zone WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.zoneinterface WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        /************Auto Increment, new and FK Tables****/
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.jobdigpoint WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.messageactivity WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.messagecustomer WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reason WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reasonoccpriority WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.reasontemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketpriorityoccpriority WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketpriority WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitruleaction where visitruleid in (select visitruleid FROM [' + @server + '].'  + @database + '.dbo.visitrule WHERE customerid = ''' + @customerid + ''') AND visitactionid in 
			(select visitactionid FROM [' + @server + '].'  + @database + '.dbo.visitaction WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitaction WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitrule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitprsaudit WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitprsqueue WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.xxtemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.routingrulepoints WHERE rrvalueid IN
			(select rrvalueid FROM [' + @server + '].'  + @database + '.dbo.routingrulevalue WHERE rrtypeid IN 
			(select rrtypeid FROM [' + @server + '].'  + @database + '.dbo.routingruletype WHERE rrversionid IN
			(select rrversionid FROM [' + @server + '].'  + @database + '.dbo.routingruleversion WHERE customerid = ''' + @customerid + ''')))';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.routingrulevalue WHERE rrtypeid IN
			(select rrtypeid FROM [' + @server + '].'  + @database + '.dbo.routingruletype WHERE rrversionid IN
			(select rrversionid FROM [' + @server + '].'  + @database + '.dbo.routingruleversion WHERE customerid = ''' + @customerid + '''))';		
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.routingruletype WHERE rrversionid IN
			(select rrversionid FROM [' + @server + '].'  + @database + '.dbo.routingruleversion WHERE customerid = ''' + @customerid + ''')';		
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END		
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.routingruleversion WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ticketsort WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.duedatestatus WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyEmails WHERE hnst_id IN (SELECT hnst_id FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyScheduleTemplate hnst JOIN [' + @server + '].'  + @database + '.dbo.HeraldNotifyGroup hng on hng.hng_id = hnst.hng_id WHERE hng.customerid = ''' + @customerid + ''')'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyScheduleTemplate WHERE hng_id IN (select hng_id FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + ''')'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.heraldnotifyemails WHERE hnst_id IN (SELECT hnst_id FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyScheduleTemplate hnst JOIN [' + @server + '].'  + @database + '.dbo.HeraldNotifyGroup hng on hng.hng_id = hnst.hng_id WHERE hng.customerid = ''' + @customerid + ''')'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyScheduleTemplate WHERE hng_id IN (select hng_id FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + ''')'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + ''''
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.PAaudittrail where contactid in (select contactid FROM [' + @server + '].'  + @database + '.dbo.PAcontact WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.PAcontactdata where contactid in (select contactid FROM [' + @server + '].'  + @database + '.dbo.PAcontact WHERE customerid = ''' + @customerid + ''') AND maintid in 
			(select maintid FROM [' + @server + '].'  + @database + '.dbo.PAmaint WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.PAcontact WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.PAmaint WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.PAtemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mapannotationpoint where shapeid in (select shapeid FROM [' + @server + '].'  + @database + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mapannotation WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.screeningdecisiondata where screeningid in (select screeningid FROM [' + @server + '].'  + @database + '.dbo.screeningdecisioncommon WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.screeningdecisioncommon WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ScreeningRulevalue 
        WHERE ruleid IN (SELECT ruleid FROM [' + @server + '].'  + @database + '.dbo.ScreeningRule sr JOIN  [' + @server + '].'  + @database + '.dbo.ScreeningRuleset srs ON srs.rulesetid = sr.rulesetid WHERE srs.customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.ScreeningRule WHERE rulesetid IN (SELECT rulesetid FROM [' + @server + '].'  + @database + '.dbo.ScreeningRuleset WHERE customerid = ''' + @customerid + ''')  AND ruletypeid IN (SELECT ruletypeid FROM [' + @server + '].'  + @database + '.dbo.ScreeningRuletype)'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.screeningruleassignment WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.screeningruleset WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.scheduletemplatetimes where cst_id in (select cst_id FROM [' + @server + '].'  + @database + '.dbo.customerscheduletemplates WHERE customerid = ''' + @customerid + ''')';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customerscheduletemplates WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.msggroup WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.blacklist WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.FirstAlertPricingSchedule WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.prsmemcode WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.mobile WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.district WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.region WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.operator WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.occpriority WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.occ WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.customer WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        --REDO VisitBackward for any trigger creations: 
        SET @SQLCommand = 'DELETE FROM [' + @server + '].'  + @database + '.dbo.visitbackward WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            PRINT @SQLCommand + ' FAILED!'
            RETURN -1
        END
        BEGIN
            PRINT 'Congratulations! Customer data has been successfully removed!'
        END
    END
END