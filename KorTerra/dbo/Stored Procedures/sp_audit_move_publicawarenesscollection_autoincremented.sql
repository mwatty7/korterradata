﻿-- =============================================
-- Description:	Audit the Public Awareness Collection Row Count from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_audit_move_publicawarenesscollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @errMsg NVARCHAR(MAX);
	DECLARE @tableName VARCHAR(30);
	DECLARE @ParmDefinition nvarchar(500);
	DECLARE @CountSQLQuery varchar(30);
	DECLARE @SQLCommand NVARCHAR(MAX);
	SET @tableName = 'PAmaint';
	EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	SET @tableName = 'PAcontact';
	EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	SET @tableName = 'PAtemplate';
	EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS PAcontactdataCount
				FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAcontactdata AS PACD
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAcontact AS PAC ON PACD.contactid = PAC.contactid
			WHERE PAC.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS PAcontactdataCount
				FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.PAcontactdata AS PACD
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.PAcontact AS PAC ON PACD.contactid = PAC.contactid
			WHERE PAC.customerid = ''' + @customerId + ''')
					
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: PAcontactdata FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'PAcontactdata Records Match'
	END
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS PAcontactdataCount
				FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAaudittrail AS PAT
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAcontact AS PAC ON PAT.contactid = PAC.contactid
			WHERE PAC.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS PAcontactdataCount
				FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.PAaudittrail AS PAT
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.PAcontact AS PAC ON PAT.contactid = PAC.contactid
			WHERE PAC.customerid = ''' + @customerId + ''')
					
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: PAaudittrail FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'PAaudittrail Records Match'
	END
END