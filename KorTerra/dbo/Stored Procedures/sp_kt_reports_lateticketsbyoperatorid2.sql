﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbyoperatorid2]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@operatorid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32),

	@summary bit = 0,
	@detail bit = 0,
	@statusCounts bit = 0,
	@lateOperatorCounts bit = 0,
	@grandTotal bit = 0
AS

DECLARE @customer varchar(32) = @customerId
DECLARE @region varchar(16) = @regionid
DECLARE @district varchar(16) = @districtid
DECLARE @operator varchar(32) = @operatorid
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_statusCounts bit = @statusCounts
DECLARE @_lateOperatorCounts bit = @lateOperatorCounts
DECLARE @_grandTotal bit = @grandTotal
DECLARE @_summary bit = @summary

-- Detail Report - By CompletionDt
IF @_detail = 1 AND @date = 0
	BEGIN
		IF @output = 'detailed'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					jobid,
					ROW_NUMBER() over(partition by vc.customerid, jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
				operator.operatorid = vc.operatorid and
				operator.customerid = vc.customerid
					
				WHERE 
					vc.customerid = @customer AND
					vc.completiondt >= @from AND vc.completiondt < DATEADD(DAY, 1, @through)

			)  
			SELECT TOP 1000
				job.customerid,
				mobile.regionid,
				mobile.districtid,
				job.sendto,
				job.mobileid,
				vs.operatorid,
				vs.description,
				job.jobid,
				job.worktype,
				CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
					vs.completiondt,
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) / 60) as lateHours,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) % 60) AS lateMinutes,
				CASE 
					WHEN vs.completiondt IS NULL then 'Open / Late'
					WHEN vs.completiondt IS NOT NULL then 'Complete / Late'
				END AS completionStatus
			FROM
				job
			inner join visitsequence as vs on
			vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1

			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				-- Filter critera by mobile, district, region, emergencies and projects
			WHERE
			 ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))

			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)



		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Operator Graph - By CompletionDt
IF @_lateOperatorCounts = 1 AND @date = 0
BEGIN	
	WITH visitsequence as (
	SELECT 
			vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					jobid,
					ROW_NUMBER() over(partition by vc.customerid, jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
				operator.operatorid = vc.operatorid and
				operator.customerid = vc.customerid
					
				WHERE 
					vc.customerid = @customer AND
					vc.completiondt >= @from AND vc.completiondt < DATEADD(DAY, 1, @through)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				TOP 5 
				vs.operatorid, count(*) as totalLate
				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid
				order by count(*) desc 


	END

-- Late Ticket Summary - By Completion Dt
IF @_summary = 1 AND @date = 0
	BEGIN
	IF @output = 'summary'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					jobid,
					ROW_NUMBER() over(partition by vc.customerid, jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				WHERE 
					vc.customerid = @customer AND
					vc.completiondt >= @from AND vc.completiondt < DATEADD(DAY, 1, @through)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null or vc.operatorid = '')
			)  
			SELECT 
				vs.operatorid, vs.description, count(*) as latecount,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) / 60) as avgHrs,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) % 60) as avgMins


				from job

				inner join visitsequence as vs on
				vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1
				LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				where  ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid, vs.description
				order by latecount DESC,vs.operatorid 



		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Ticket @_statusCounts - By Completion Date
IF @_statusCounts = 1 AND @date = 0
	BEGIN
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				WHERE 
					vc.customerid = @customer AND
					vc.completiondt >= @from AND vc.completiondt < DATEADD(DAY, 1, @through)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				count(*) as grandtotal

				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)


				
	END
-- Detail Report - By Latest WTB Date
IF @_detail = 1 AND @date = 1
	BEGIN
		IF @output = 'detailed'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					vc.completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				join job on 
					vc.jobid = job.jobid and
					vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.wtbdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE) 
					--and	 (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)

			)  
			SELECT TOP 1000
				job.customerid,
				mobile.regionid,
				mobile.districtid,
				job.sendto,
				job.mobileid,
				vs.operatorid,
				vs.description,
				job.jobid,
				job.worktype,
				CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
					vs.completiondt,
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) / 60) as lateHours,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) % 60) AS lateMinutes,
				CASE 
					WHEN vs.completiondt IS NULL then 'Open / Late'
					WHEN vs.completiondt IS NOT NULL then 'Complete / Late'
				END AS completionStatus
			FROM
				job
			inner join visitsequence as vs on
			vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1

			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				-- Filter critera by mobile, district, region, emergencies and projects
			WHERE
			 ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))

			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)




		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Operator Graph - By WTB Dt
IF @_lateOperatorCounts = 1 AND @date = 1
BEGIN	
	WITH visitsequence as (
	SELECT 
			vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				join job on 
					vc.jobid = job.jobid and
					vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.wtbdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				TOP 5 
				vs.operatorid, count(*) as totalLate
				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid
				order by count(*) desc 


	END

-- Late Ticket Summary - By WTB Dt
IF @_summary = 1 AND @date = 1
	BEGIN
	IF @output = 'summary'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
						operator.operatorid = vc.operatorid and
						operator.customerid = vc.customerid
					join job on 
						vc.jobid = job.jobid and
						vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.wtbdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE) 
					--and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				vs.operatorid, vs.description, count(*) as latecount,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) / 60) as avgHrs,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) % 60) as avgMins


				from job

				inner join visitsequence as vs on
				vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1
				LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				where  ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid, vs.description
				order by latecount DESC,vs.operatorid 

		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Ticket @_statusCounts - By WTB Date
IF @_statusCounts = 1 AND @date = 1
	BEGIN
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
						operator.operatorid = vc.operatorid and
						operator.customerid = vc.customerid
					join job on 
						vc.jobid = job.jobid and
						vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.wtbdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				count(*) as grandtotal

				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				
	END

-- Detailed Report - By Xmite Dt
IF @_detail = 1 AND @date = 2
	BEGIN
		IF @output = 'detailed'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					vc.completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				join job on 
					vc.jobid = job.jobid and
					vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.latestxmitdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)

			)  
			SELECT TOP 1000
				job.customerid,
				mobile.regionid,
				mobile.districtid,
				job.sendto,
				job.mobileid,
				vs.operatorid,
				vs.description,
				job.jobid,
				job.worktype,
				CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
					vs.completiondt,
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) / 60) as lateHours,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt) % 60) AS lateMinutes,
				CASE 
					WHEN vs.completiondt IS NULL then 'Open / Late'
					WHEN vs.completiondt IS NOT NULL then 'Complete / Late'
				END AS completionStatus
			FROM
				job
			inner join visitsequence as vs on
			vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1

			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				-- Filter critera by mobile, district, region, emergencies and projects
			WHERE
			 ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))

			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)



		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Operator Graph - By Xmite Dt
IF @_lateOperatorCounts = 1 AND @date = 2
BEGIN	
	WITH visitsequence as (
	SELECT 
			vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
				join operator on 
					operator.operatorid = vc.operatorid and
					operator.customerid = vc.customerid
				join job on 
					vc.jobid = job.jobid and
					vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.latestxmitdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				TOP 5 
				vs.operatorid, count(*) as totalLate
				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid
				order by count(*) desc 


	END

-- Late Ticket Summary - By Xmit Dt
IF @_summary = 1 AND @date = 2
	BEGIN
	IF @output = 'summary'
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
						operator.operatorid = vc.operatorid and
						operator.customerid = vc.customerid
					join job on 
						vc.jobid = job.jobid and
						vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.latestxmitdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  

			SELECT 
				vs.operatorid, vs.description, count(*) as latecount,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) / 60) as avgHrs,
				((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vs.completiondt))) / COUNT(job.jobid)) % 60) as avgMins


				from job

				inner join visitsequence as vs on
				vs.jobid = job.jobid and vs.customerid = job.customerid and vs.completiondt = completiondt and seq = 1
				LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
				where  ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)

				group by vs.operatorid, vs.description
				order by latecount DESC,vs.operatorid 

		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END

-- Late Ticket @_statusCounts - By Xmit Date
IF @_statusCounts = 1 AND @date = 2
	BEGIN
		WITH visitsequence as (
			SELECT 
				vc.customerid, 
					vc.operatorid,
					description,
					completiondt,
					vc.jobid,
					ROW_NUMBER() over(partition by vc.customerid, vc.jobid order by completiondt) as seq
				FROM 
					visitcommon as vc
					join operator on 
						operator.operatorid = vc.operatorid and
						operator.customerid = vc.customerid
					join job on 
						vc.jobid = job.jobid and
						vc.customerid = job.customerid 	
				WHERE 
					vc.customerid = @customer AND
					job.latestxmitdtdate BETWEEN CAST(@from as DATE) and CAST(@through as DATE)
					-- and (@operator = '0' OR vc.operatorid = @operator or vc.operatorid is null)
			)  
			SELECT 
				count(*) as grandtotal

				from visitsequence as vs
				join job on
				vs.customerid = job.customerid and
				vs.jobid = job.jobid
				join mobile on 
				job.customerid = mobile.customerid and
				job.mobileid = mobile.mobileid
				where vs.seq = 1
				AND ((vs.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (vs.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	    		AND (@operator = '0' OR vs.operatorid = @operator or vs.operatorid is null)


				
	END