﻿CREATE PROCEDURE [dbo].[sp_change_mobileid]
	@oldmobileid NVARCHAR(16) = NULL,
	@newmobileid NVARCHAR(16) = NULL,
	@customerid NVARCHAR(32) = NULL
AS
BEGIN
	SET NOCOUNT ON
	---------------------
	-- 
	-- Validate the parameters
	--
	RAISERROR('Validate the parameters', 0, 1) WITH NOWAIT;
	IF @oldmobileid IS NULL OR @newmobileid IS NULL OR @customerid IS NULL
	BEGIN
		RAISERROR(N'USAGE: sp_change_mobileid <@oldmobileid>, <@newmobileid>, <@customerid>', 0, 1) WITH NOWAIT;
		RETURN;
	END;
	IF @oldmobileid = @newmobileid
	BEGIN
		RAISERROR(N'FAILED: cannot change %s to %s', 0, 1, @oldmobileid, @newmobileid) WITH NOWAIT;
		RETURN;
	END;
	RAISERROR('DONE: Validating the parameters', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Make sure the old mobileid exists
	--
	RAISERROR('Make sure the old mobileid exists', 0, 1) WITH NOWAIT;
	DECLARE @oldmobilecount INT;
	SELECT @oldmobilecount = COUNT(*)
		FROM [MOBILE]
		WHERE customerid = @customerid
		AND mobileid = @oldmobileid
		;
	IF @oldmobilecount <= 0
	BEGIN
		RAISERROR('FAIL: Old mobile %s/%s does not exist', 0, 1, @customerid, @oldmobileid) WITH NOWAIT;
		RETURN;
	END;
	RAISERROR('DONE: Making sure the old customerid exists', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Copy the mobile record, if it doesn't already exist
	--
	RAISERROR('Copy the mobile record, if it doesn''t already exist', 0, 1) WITH NOWAIT;
	DECLARE @newmobileAlreadyExists BIT;
	SELECT @newmobileAlreadyExists = CONVERT(BIT, COUNT(*))
	FROM mobile
	WHERE customerid = @customerid
	AND mobileid = @newmobileid
	;
	IF @newmobileAlreadyExists <> 0
	BEGIN
		RAISERROR('Mobile %s already exists', 0, 1, @newmobileid) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Creating new mobile %s', 0, 1, @newmobileid) WITH NOWAIT;
		INSERT INTO [mobile]
		(
			[customerid],
			[mobileid],
			[description],
			[regionid],
			[districtid],
			[mobiletype],
			[isidle],
			[newemergencytot],
			[newemergencyvisitreqtot],
			[newmeettot],
			[newmeetvisitreqtot],
			[newroutinetot],
			[newroutinevisitreqtot],
			[emergencytot],
			[emergencyvisitreqtot],
			[meettot],
			[meetvisitreqtot],
			[routinetot],
			[routinevisitreqtot],
			[projecttot],
			[projectvisitreqtot],
			[isautosend],
			[isautosendnew],
			[schemaversion],
			[isdisableprs]
		)
		SELECT
			[customerid],
			@newmobileid,
			[description],
			[regionid],
			[districtid],
			[mobiletype],
			[isidle],
			[newemergencytot],
			[newemergencyvisitreqtot],
			[newmeettot],
			[newmeetvisitreqtot],
			[newroutinetot],
			[newroutinevisitreqtot],
			[emergencytot],
			[emergencyvisitreqtot],
			[meettot],
			[meetvisitreqtot],
			[routinetot],
			[routinevisitreqtot],
			[projecttot],
			[projectvisitreqtot],
			[isautosend],
			[isautosendnew],
			[schemaversion],
			[isdisableprs]
		FROM [mobile]
		WHERE [customerid] = @customerid
		AND [mobileid] = @oldmobileid
		;
	END;
	RAISERROR('DONE: Copying the mobile record', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Update the records
	--
	RAISERROR('Update the records', 0, 1) WITH NOWAIT;
	-- We don't update mobile
	-- We may or may not update mobdest
	-- And we don't update zone here
	DECLARE cursor1xmd CURSOR LOCAL FOR
		SELECT s.name, t.name
		FROM sys.tables t
		JOIN sys.schemas s ON s.schema_id = t.schema_id
		JOIN sys.columns c1 ON c1.object_id = t.object_id
		JOIN sys.columns c2 ON c2.object_id = t.object_id
		WHERE c1.name = 'customerid'
		AND c2.name = 'mobileid'
		AND t.type = 'U'
		AND t.name <> 'mobile'
		AND t.name <> 'mobdest'
		AND t.name <> 'zone'
		ORDER BY s.name, t.name
		;
	DECLARE cursor1wmd CURSOR LOCAL FOR
		SELECT s.name, t.name
		FROM sys.tables t
		JOIN sys.schemas s ON s.schema_id = t.schema_id
		JOIN sys.columns c1 ON c1.object_id = t.object_id
		JOIN sys.columns c2 ON c2.object_id = t.object_id
		WHERE c1.name = 'customerid'
		AND c2.name = 'mobileid'
		AND t.type = 'U'
		AND t.name <> 'mobile'
		AND t.name = 'mobdest'
		AND t.name <> 'zone'
		ORDER BY s.name, t.name
		;
	DECLARE @sSQL1 NVARCHAR(MAX);
	DECLARE @params1 NVARCHAR(MAX);
	DECLARE @tableschema1 NVARCHAR(MAX);
	DECLARE @tablename1 NVARCHAR(MAX);
	OPEN cursor1xmd;
	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM cursor1xmd INTO @tableschema1, @tablename1;
		IF @@FETCH_STATUS <> 0
			BREAK;
		SELECT @sSQL1 = 
			N' UPDATE ' + quotename(@tableschema1) + '.' + quotename(@tablename1) +
			N' SET mobileid = @newmid ' +
			N' WHERE customerid = @cid ' +
			N' AND mobileid = @oldmid ';
		SELECT @params1 = 
			N'@oldmid VARCHAR(16), @newmid VARCHAR(16), @cid VARCHAR(32)';
		RAISERROR('Updating %s', 0, 1, @tablename1) WITH NOWAIT;
		EXEC sp_executesql @sSQL1, @params1, @oldmobileid, @newmobileid, @customerid
	END
	IF @newmobileAlreadyExists <> 0
	BEGIN
		RAISERROR('Mobile %s already exists, not updating mobdests', 0, 1, @newmobileid) WITH NOWAIT;
	END
	ELSE
	BEGIN
		OPEN cursor1wmd;
		WHILE 1 = 1
		BEGIN
			FETCH NEXT FROM cursor1wmd INTO @tableschema1, @tablename1;
			IF @@FETCH_STATUS <> 0
				BREAK;
			SELECT @sSQL1 = 
				N' UPDATE ' + quotename(@tableschema1) + '.' + quotename(@tablename1) +
				N' SET mobileid = @newmid ' +
				N' WHERE customerid = @cid ' +
				N' AND mobileid = @oldmid ';
			SELECT @params1 = 
				N'@oldmid VARCHAR(16), @newmid VARCHAR(16), @cid VARCHAR(32)';
			RAISERROR('Updating %s', 0, 1, @tablename1) WITH NOWAIT;
			EXEC sp_executesql @sSQL1, @params1, @oldmobileid, @newmobileid, @customerid
		END
	END;
	RAISERROR('DONE: Updating the records', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Updating zone, jobsend, jobsendcompatible, jobmobhistory and actionaudit
	--
	RAISERROR('Update zone', 0, 1) WITH NOWAIT;
	UPDATE [zone]
	SET [mobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [mobileid] = @oldmobileid
	;
	UPDATE [zone]
	SET [screeningmobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [screeningmobileid] = @oldmobileid
	;
	RAISERROR('Update jobsend', 0, 1) WITH NOWAIT;
	UPDATE [jobsend]
	SET [frommobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [frommobileid] = @oldmobileid
	;
	UPDATE [jobsend]
	SET [tomobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [tomobileid] = @oldmobileid
	;
	RAISERROR('Update jobsendcompatible', 0, 1) WITH NOWAIT;
	UPDATE [jobsendcompatible]
	SET [tomobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [tomobileid] = @oldmobileid
	;
	UPDATE [jobsendcompatible]
	SET [frommobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [frommobileid] = @oldmobileid
	;
	RAISERROR('Update jobmobilehistory', 0, 1) WITH NOWAIT;
	UPDATE [jobmobilehistory]
	SET [tomobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [tomobileid] = @oldmobileid
	;
	UPDATE [jobmobilehistory]
	SET [frommobileid] = @newmobileid
	WHERE [customerid] = @customerid
	AND [frommobileid] = @oldmobileid
	;
	RAISERROR('Update actionaudit', 0, 1) WITH NOWAIT;
	UPDATE [actionaudit]
	SET [actionto] = @newmobileid
	WHERE [customerid] = @customerid
	AND [actionto] = @oldmobileid
	;
	UPDATE [actionaudit]
	SET [actionfrom] = @newmobileid
	WHERE [customerid] = @customerid
	AND [actionfrom] = @oldmobileid
	;
	RAISERROR('DONE: Updating jobmobhistory and actionaudit', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Deleting old mobile record
	--
	RAISERROR('Delete old mobile record', 0, 1) WITH NOWAIT;
	
	DELETE FROM [mobile]
	WHERE customerid = @customerid
	AND mobileid = @oldmobileid;
	RAISERROR('DONE: Deleting old mobile record', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Done!
	--
	RAISERROR('Done!', 0, 1) WITH NOWAIT;
	SET NOCOUNT OFF
END