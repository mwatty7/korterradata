﻿CREATE PROCEDURE [dbo].[sp_getIMID]
	@customerid NVARCHAR(32),
	@currentdate DATETIME,
	@incidentnumber NVARCHAR(12) OUTPUT
AS
BEGIN
DECLARE 
	@yr AS CHAR(2),
	@cnt AS TINYINT,
	@doy AS VARCHAR(3),
	@sequence AS VARCHAR(3),
	@cdate AS DATETIME,
	@ldate AS DATETIME;
	IF @customerid IS NULL 
	  SET @customerid = 'SYSTEM';
	IF @currentdate IS NULL 
	  SET @currentdate = GETUTCDATE();
	
	SET @yr = (YEAR( GETDATE() ) % 100 );
	SET @cdate = @currentdate;
	SET @doy = DATEDIFF(day,STR(YEAR(@cdate),4)+'0101',@cdate)+1;
	SET @doy = REPLICATE('0', 3 - LEN(@doy)) + @doy;
	/* Check to see if customer ID exists */
	SELECT @cnt = COUNT(*) FROM IMsequence
	WHERE customerid = @customerid;
	
	/*NEW CUSTOMER ID*/
	IF (@cnt = 0)
	BEGIN
	 INSERT INTO IMsequence(customerid,sequence,lastupdatedtutc)
	   VALUES(@customerid,0,@cdate);
	END
	/*NEW CUSTOMER ID*/
	/*DETERMINE IF WE NEED TO RESET THE SEQUENCE NUMBER*/
	SELECT @ldate = lastupdatedtutc, @sequence = sequence FROM IMsequence
	WHERE customerid = @customerid;
	IF DATEADD(dd, 0, DATEDIFF(dd, 0, @cdate)) <> DATEADD(dd, 0, DATEDIFF(dd, 0, @ldate))
	BEGIN
		UPDATE IMsequence 
		SET sequence = 1,
		lastupdatedtutc = @cdate
		WHERE customerid = @customerid;
	END
	ELSE 
	BEGIN	
	
	/* UPDATE THE SEQUENCE */
		UPDATE IMsequence 
		SET sequence = sequence + 1,
		lastupdatedtutc = @cdate
		WHERE customerid = @customerid;
	END
	SELECT @sequence = (sequence) FROM IMsequence
	WHERE customerid = @customerid;
	SET @sequence = REPLICATE('0', 3 - LEN(@sequence)) + @sequence;
	SET @incidentnumber = @yr + @doy + @sequence;
	RETURN
END