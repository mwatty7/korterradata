﻿
CREATE PROCEDURE [dbo].[sp_customer_insert1]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL

AS
BEGIN
    SET NOCOUNT ON; 
DECLARE @SQLCommand NVARCHAR(MAX);
DECLARE @return Int;

    --CHECK the source server and source DB
    DECLARE @sqlSourceServerCheck  NVARCHAR(MAX);
    SET @sqlSourceServerCheck = 'SELECT [name] FROM sys.servers WHERE name = ''' + @sourceServer + ''' ';

    DECLARE @serverCheckResults  TABLE([name] VARCHAR(50));

    INSERT @serverCheckResults
    EXECUTE(@sqlSourceServerCheck)


    IF NOT EXISTS (SELECT *
    FROM @serverCheckResults
    WHERE [name] = @sourceServer)

BEGIN
        PRINT 'ERROR! Must have source server linked'
        RETURN;
    END;


    DECLARE @sqlSourceDBCheck  NVARCHAR(MAX);
    SET @sqlSourceDBCheck = 'SELECT [name] FROM ['+ @sourceServer + '].master.sys.databases WHERE name = ''' + @sourceDatabase + ''' ';

    DECLARE @sourceDBCheckResults  TABLE([name] VARCHAR(50));

    INSERT @sourceDBCheckResults
    EXECUTE(@sqlSourceDBCheck)

    IF NOT EXISTS (SELECT *
    FROM @sourceDBCheckResults
    WHERE [name] = @sourceDatabase)

BEGIN
        PRINT 'ERROR! Please check your source server and ensure source DB exists...'
        RETURN;
    END;


    ---CHECK Target Source and DB: 

    DECLARE @sqlTargetServerCheck  NVARCHAR(MAX);
    SET @sqlTargetServerCheck = 'SELECT [name] FROM sys.servers WHERE name = ''' + @targetServer + ''' ';

    DECLARE @targetServerCheckResults  TABLE([name] VARCHAR(50));

    INSERT @targetServerCheckResults
    EXECUTE(@sqlTargetServerCheck)


    IF NOT EXISTS (SELECT *
    FROM @targetServerCheckResults
    WHERE [name] = @targetServer)

BEGIN
        PRINT 'ERROR! Must execute within target server!'
        RETURN;
    END;


    DECLARE @sqlTargetDBCheck  NVARCHAR(MAX);
    SET @sqlTargetDBCheck = 'SELECT [name] FROM sys.databases WHERE name = ''' + @targetDatabase + ''' ';

    DECLARE @targetDBCheckResults  TABLE([name] VARCHAR(50));

    INSERT @targetDBCheckResults
    EXECUTE(@sqlTargetDBCheck)

    IF NOT EXISTS (SELECT *
    FROM @targetDBCheckResults
    WHERE [name] = @targetDatabase)

BEGIN
        PRINT 'ERROR! Target DB does not exist. Please check target environment and ensure target DB is setup...'
        RETURN;
    END;

    ---CHECK customer doesn't already exist in Target Server:

    DECLARE @sqlcheckCustomer  NVARCHAR(MAX);
    SET @sqlcheckCustomer = 'SELECT customerid FROM ['+ @targetDatabase +'].dbo.Customer WHERE customerid = ''' + @customerId + ''' ';

    DECLARE @checkCustomerResults  TABLE([customerid] VARCHAR(50));

    INSERT @checkCustomerResults
    EXECUTE(@sqlcheckCustomer)

    IF EXISTS (SELECT @customerId
    FROM @checkCustomerResults
    WHERE customerid = @customerId)

BEGIN
        PRINT 'Customer already exists within the Target Database! Please ensure this is the correct customer'
        RETURN;
    END;

    ---CHECK if customer doesn't exist in source server:

    DECLARE @sqlcheckSourceCustomer  NVARCHAR(MAX);
    DECLARE @tableName NVARCHAR(80)
    SET @tableName = 'customer';

    SET @sqlcheckSourceCustomer = 'SELECT customerid, ''customer'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.customer WHERE customerid = ''' + @customerId + ''' ';

    DECLARE @checkSourceResults  TABLE([customerid] VARCHAR(50),
        [table] VARCHAR (80));


    INSERT @checkSourceResults
    EXECUTE(@sqlcheckSourceCustomer)

    IF NOT EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'customer')

    BEGIN
        PRINT 'Customer does not exist within the Source Database! Please verify customer migration.'
        RETURN;
    END;

    --QUEUE Checks: If we're migrating a customer, we want to ensure no queue data currently
    DECLARE @queueChecks NVARCHAR(MAX)

    /*msgqueue check*/
    SET @queueChecks =  'SELECT customerid, ''msgqueue'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.msgqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'msgqueue')
    BEGIN
        PRINT 'Stop! Customer related data exists in the MsgQueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RETURN;
    END;


    /*screeningqueue check*/
    SET @queueChecks =  'SELECT customerid, ''ScreeningQueue'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.ScreeningQueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'ScreeningQueue')

BEGIN
        PRINT 'Stop! Customer related data exists in the ScreeningQueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RETURN;
    END;

    /*visitqueue check*/
    SET @queueChecks =  'SELECT customerid, ''visitqueue'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.visitqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitqueue')

BEGIN
        PRINT 'Stop! Customer related data exists in the Visitqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RETURN;
    END;

    /*visitprsqueue check*/
    SET @queueChecks =  'SELECT customerid, ''visitprsqueue'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.visitprsqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitprsqueue')

BEGIN
        PRINT 'Stop! Customer related data exists in the Visitprsqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        DELETE FROM @checkSourceResults
        RETURN;
    END;


    /*visitbackward check*/
SET @queueChecks =  'SELECT customerid, ''visitbackward'' FROM ['+ @sourceServer + '].['+ @sourceDatabase + '].dbo.visitbackward WHERE customerid = ''' + @customerId + ''' ';
INSERT @checkSourceResults
EXECUTE(@queueChecks)

IF EXISTS (SELECT *
FROM @checkSourceResults
WHERE customerid = @customerId and [table] = 'visitbackward')

BEGIN
    PRINT 'Stop! Customer related data exists in the Visitbackward table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
    DELETE FROM @checkSourceResults
    RETURN;
END;

    --POPULATE customer/msg group 

    DECLARE @sqlPopulateCustMsgGroup NVARCHAR(MAX);
    SET @sqlPopulateCustMsgGroup = '
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customer SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.customer WHERE customerid = ''' + @customerid + '''
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msggroup SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.msggroup WHERE customerid = ''' + @customerid + '''

';
    BEGIN
        EXECUTE (@sqlPopulateCustMsgGroup);
        PRINT @sqlPopulateCustMsgGroup;

    END;

    --POPULATE CustomerScheduleTemplates/ScheduleTemplateTimes/HeraldNotifyGroup, HeraldNotifyScheduleTemplate, HeraldNotifyEmails

    DECLARE @sqlCSTSTTHNG NVARCHAR(MAX);
    SET @sqlCSTSTTHNG = '
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.CustomerScheduleTemplates SELECT customerid, [name] FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.CustomerScheduleTemplates WHERE customerid = ''' + @customerid + '''
CREATE TABLE #CSTLink (cst_id_old INT, cst_id_new INT, customerid VARCHAR(32), [name]     VARCHAR(64), lrn        INT IDENTITY NOT NULL);
INSERT INTO #CSTLink (cst_id_old, customerid, [name]) SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.CustomerScheduleTemplates WHERE customerid = ''' + @customerid + ''';

UPDATE #CSTLink 
SET cst_id_new = t.cst_id
FROM #CSTLink as ccl
JOIN dbo.CustomerScheduleTemplates t on t.[customerid] = ccl.[customerid] and t.[name] = ccl.[name];

INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScheduleTemplateTimes(cst_id, [day], starttime, endtime)
SELECT c.cst_id_new, [day], starttime, endtime FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScheduleTemplateTimes p JOIN #CSTLink c on c.cst_id_old = p.cst_id WHERE c.customerid = ''' + @customerid + ''';

INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyGroup SELECT customerid, msggroupid, msgtype FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + '''
CREATE TABLE  #HNGLink (hng_id_old INT, hng_id_new INT, customerid VARCHAR(32), msggroupid VARCHAR(16), msgtype VARCHAR(32), lrn INT IDENTITY NOT NULL);
INSERT INTO #HNGLink (hng_id_old, customerid, msggroupid, msgtype) SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + ''';

UPDATE #HNGLink 
SET hng_id_new = hng.hng_id
FROM #HNGLink as hngl
JOIN dbo.HeraldNotifyGroup hng on hng.[customerid] = hngl.[customerid] and hng.[msggroupid] = hngl.[msggroupid] and hng.[msgtype] = hngl.[msgtype];

INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate(hng_id, cst_id, tplfilename)
SELECT hngl.hng_id_new, cstl.cst_id_new, tplfilename FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate hnst JOIN #HNGLink hngl on hngl.hng_id_old = hnst.hng_id 
JOIN #CSTLink  cstl ON cstl.cst_id_old = hnst.cst_id

CREATE TABLE #HNGSTe (hnst_id_old INT, hnst_id_new INT, hng_id_old INT, hng_id_new INT, emailaddress VARCHAR(320),lrn INT IDENTITY NOT NULL);
INSERT INTO #HNGSTe (hnst_id_old, hng_id_old, emailaddress) SELECT hne.hnst_id, hnst.hng_id, emailaddress FROM  ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyEmails hne JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate hnst on hne.hnst_id = hnst.hnst_id
JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup hng ON hng.hng_id = hnst.hnst_id 
WHERE hng.customerid =  ''' + @customerid + ''';

UPDATE #HNGSTe
SET hng_id_old = #HNGLink.hng_id_old, hng_id_new = #HNGLink.hng_id_new
FROM #HNGSTe
JOIN #HNGLink ON #HNGLink.hng_id_old = #HNGSTe.hng_id_old

UPDATE #HNGSTe
SET hnst_id_new = hnst.hnst_id
FROM #HNGSTe
JOIN dbo.HeraldNotifyScheduleTemplate hnst ON hnst.hng_id = #HNGSTe.hng_id_new

INSERT INTO heraldnotifyemails SELECT hnst_id_new, hne.emailaddress FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyEmails hne JOIN #HNGSTe on #HNGSTe.hnst_id_old = hne.hnst_id AND #HNGSTe.emailaddress = hne.emailaddress

DROP TABLE #CSTLink, #HNGLink, #HNGSTe

';
    BEGIN
        EXECUTE(@sqlCSTSTTHNG)
        PRINT @sqlCSTSTTHNG;

    END

    --POPULATE mapannotation and mapannotationpoint

    DECLARE @sqlMapNot NVARCHAR(MAX);
    SET @sqlMapNot= '

IF EXISTS (SELECT *
FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''')

INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mapannotation(customerid, label, shapetype, color, comment, operatorid, isactive, lastmodifieddt)
SELECT customerid, label, shapetype, color, comment, operatorid, isactive, lastmodifieddt FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid =''' + @customerid + ''';

CREATE TABLE #mpann (shapeid_old INT, shapeid_new INT, customerid VARCHAR(32), label VARCHAR(64), color VARCHAR(32), operatorid VARCHAR(16), isactive smallint, lastmodifieddt datetime, lrn INT IDENTITY NOT NULL);
INSERT INTO #mpann (shapeid_old, customerid, label, color, operatorid, isactive, lastmodifieddt) SELECT shapeid, customerid, label, color, operatorid, isactive, lastmodifieddt FROM ['+ @sourceServer + ']
' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''';

UPDATE #mpann
SET shapeid_new = m.shapeid
FROM #mpann
    JOIN dbo.mapannotation m on #mpann.customerid = m.customerid 
    and #mpann.operatorid = m.operatorid 
    and #mpann.color = m.color and #mpann.label = m.label 
    and #mpann.lastmodifieddt = m.lastmodifieddt

INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mapannotationpoint
SELECT #mpann.shapeid_new, m.sequence, m.latitude, m.longitude
FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotationpoint m JOIN #mpann on #mpann.shapeid_old = m.shapeid;

DROP TABLE #mpann

'
    BEGIN
        EXECUTE(@sqlMapNot)
        PRINT @sqlMapNot;

    END

    BEGIN
    SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachment WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachmentexif SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachmentexif WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachmentsize SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachmentsize WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAwidgetAvailable SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAwidgetAvailable WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAwidgetInUse SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAwidgetInUse WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.actionaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.actionaudit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.activity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.activity WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.addmembers SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.addmembers WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.archsettings SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.archsettings WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.area SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.area WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.areapoint SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.areapoint WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.areatypes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.areatypes WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.attachment WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --attachmentbackward
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachmentext SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.attachmentext WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.audititm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.audititm WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.auditsend SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.auditsend WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.autodept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.autodept WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.autosendrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.autosendrule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.batch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.batch WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bbillstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bbillstatus WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusbatch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusbatch WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusledger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusledger WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusperiod SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusperiod WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.calendar SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.calendar WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.childdept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.childdept WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.citycounty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.citycounty WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.company SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.company WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.comppref SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.comppref WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.contact SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.contact WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.contrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.contrem WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.currentmobloc SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.currentmobloc WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customactcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customactcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --customer
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customerattribute SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customerattribute WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --CustomerScheduleTemplates
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customfaccode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customfaccode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customreport SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customreport WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customreportsecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customreportsecurity WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custsearchdtl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custsearchdtl WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custsearchhdr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custsearchhdr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custwindowfld SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custwindowfld WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.defaultvisit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.defaultvisit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.deptbill SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.deptbill WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.district SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.district WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dptgp SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dptgp WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dptgpmbr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dptgpmbr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.duedaterules SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.duedaterules WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dupaddr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dupaddr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e6visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e7visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e8visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e8visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e9visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e9visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.editaddr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.editaddr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emailnotify SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emailnotify WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emailtrigger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emailtrigger WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emergencycontact SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emergencycontact WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emergencygroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emergencygroup WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_addmembers SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_addmembers WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_completion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_completion WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_comprem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_comprem WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_extjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_extjob WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_locpoints SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_locpoints WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_order SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_order WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_remarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_remarks WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_ticket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_ticket WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.expcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.expcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.expdetl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.expdetl WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.extjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.extjob WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.extjobcol SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.extjobcol WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.f1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.f1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.filteraudititm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.filteraudititm WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.formvalidation SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.formvalidation WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g1visit  SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g6visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.groupoperator SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.groupoperator WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.herald SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.herald WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.heraldmessenger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.heraldmessenger WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --HeraldNotifyGroup
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.holiday SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.holiday WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMattachment WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincident SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincident WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMaudit (customerid, incidentnumber, createdbyoperatorid, createddt, description) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, description FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMaudit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMcode (customerid, codetype, codeid, description, dirtcodetype, dirtid, parentid, dirtid2) SELECT customerid, codetype, codeid, description, dirtcodetype, dirtid, parentid, dirtid2 FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMconfig SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMconfig WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMcontractor (customerid, createddt, company, email, phone, altphone, besttimetocall, streetnumber, address1, address2, city, county, state, zip, country, contact, altcontact) SELECT customerid, createddt, company, email, phone, altphone, besttimetocall, streetnumber, address1, address2, city, county, state, zip, country, contact, altcontact FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMcontractor WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMfilter (customerid, operatorid, createdbyoperatorid, createddt, name, query) SELECT customerid, operatorid, createdbyoperatorid, createddt, name, query FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMfilter WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincidentContent 
		(customerid, incidentnumber, revisedby, reviseddt, incidentstatus, priority, incidentassignedto, isvalidticket, ticket, emergencyticket, ticketdue, memberid, membercode, ishighprofile, callername, callerphone, reportedstreetnumber, reportedaddress1, reportedaddress2, 
		reportedcity, reportedcounty, reportedstate, reportedzip, reportedcountry, reportednearinter, reportedlatitude, reportedlongitude, reporteddt, reportednotes, investigationstatus, investigationassignedto, actualstreetnumber, actualaddress1, actualaddress2, actualcity, 
		actualcounty, actualstate, actualzip, actualcountry, actualnearinter, actuallatitude, actuallongitude, descriptionofmarks, ismarkedcorrectly, ismarksvisible, isfacilityabandoned, isproject, isrow, isonecallmember, isjointtrench, isexcavatordowntime, excavatordowntimeamount, 
		contractorid, subcontractorid, damagecause, rootcause, extentofdamage, isdamaged, isserviceinterruption, serviceinterruptionduration, customersaffected, injuries, fatalities, regionid, districtid, investigatorphone, duration, dpsexperience, investigationdt, ticketstreetnumber, 
		ticketaddress1, ticketaddress2, ticketcity, ticketcounty, ticketstate, ticketzip, ticketcountry, ticketnearinter, ticketlatitude, ticketlongitude, facilitytypeid, facilitysizeid, materialid, damagecodeid, damagereasonid, deviceid, devicefrequencyid, excavatortypeid, 
		excavationequipmentid, excavationtypeid, conditionofmarksid, polygonpoints, intrefnum, serviceinterruptioncost, onecallcenterid, iscallcenternotified, rightofwaytypeid, rowtype)
		SELECT 
			customerid, incidentnumber, revisedby, reviseddt, incidentstatus, priority, incidentassignedto, isvalidticket, ticket, emergencyticket, ticketdue, memberid, membercode, ishighprofile, callername, callerphone, reportedstreetnumber, reportedaddress1, reportedaddress2, 
			reportedcity, reportedcounty, reportedstate, reportedzip, reportedcountry, reportednearinter, reportedlatitude, reportedlongitude, reporteddt, reportednotes, investigationstatus, investigationassignedto, actualstreetnumber, actualaddress1, actualaddress2, actualcity, 
			actualcounty, actualstate, actualzip, actualcountry, actualnearinter, actuallatitude, actuallongitude, descriptionofmarks, ismarkedcorrectly, ismarksvisible, isfacilityabandoned, isproject, isrow, isonecallmember, isjointtrench, isexcavatordowntime, excavatordowntimeamount, 
			contractorid, subcontractorid, damagecause, rootcause, extentofdamage, isdamaged, isserviceinterruption, serviceinterruptionduration, customersaffected, injuries, fatalities, regionid, districtid, investigatorphone, duration, dpsexperience, investigationdt, ticketstreetnumber, 
			ticketaddress1, ticketaddress2, ticketcity, ticketcounty, ticketstate, ticketzip, ticketcountry, ticketnearinter, ticketlatitude, ticketlongitude, facilitytypeid, facilitysizeid, materialid, damagecodeid, damagereasonid, deviceid, devicefrequencyid, excavatortypeid, 
			excavationequipmentid, excavationtypeid, conditionofmarksid, polygonpoints, intrefnum, serviceinterruptioncost, onecallcenterid, iscallcenternotified, rightofwaytypeid, rowtype
		FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincidentContent WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincidentContributingFactor SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincidentContributingFactor WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMinvestigationremark (customerid, incidentnumber, createdbyoperatorid, createddt, description) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, description FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMinvestigationremark WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMpayableclaim (customerid, incidentnumber, createdbyoperatorid, createddt, amount, payableclaimstatus) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, amount, payableclaimstatus FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMpayableclaim WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMreceivableclaim (customerid, incidentnumber, createdbyoperatorid, createddt, amount, receivableclaimstatus) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, amount, receivableclaimstatus FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMreceivableclaim WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMsequence SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMsequence WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMtracelog (applicationid, customerid, operatorid, logdt, type, message, component, stacktrace, loginid, controllerid, url) SELECT applicationid, customerid, operatorid, logdt, type, message, component, stacktrace, loginid, controllerid, url FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMtracelog WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMwitness (customerid, incidentnumber, firstname, lastname, type, createddt, phone, altphone, statement) SELECT customerid, incidentnumber, firstname, lastname, type, createddt, phone, altphone, statement FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMwitness WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.invoice SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.invoice WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.invrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.invrule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.j_sortedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.j_sortedjob WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.job SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.job WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobidsuffix SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobidsuffix WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.joblock SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.joblock WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsend SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsend WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsendcompatible SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsendcompatible WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsort SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsort WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ledger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ledger WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ledgrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ledgrem WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.locpoints SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.locpoints WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.manrequest SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.manrequest WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --mapannotation moved in SSIS package
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.masterdept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.masterdept WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mdsisends SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mdsisends WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.member SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.member WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.membrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.membrem WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.membzone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.membzone WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.memcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.memcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.memcodemap SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.memcodemap WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.menu SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.menu WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobdest SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobdest WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobile SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobile WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobilenotification SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobilenotification WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobstat SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobstat WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msg SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msg WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgdata SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msgdata WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --msggroup   

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlrcmpny SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlrcmpny WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlrdone4 SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlrdone4 WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.notes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.notes WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occ SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occ WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occattribute SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occattribute WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occpriority WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.operator SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.operator WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.opermember SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.opermember WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.opertype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.opertype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p6visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p7visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p8visit  SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p8visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --PAcontact
    --PAmaint
    --PAtemplate
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.paymastr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.paymastr WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.payrate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.payrate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.pigeonhl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.pigeonhl WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.pmpacket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.pmpacket WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.preferences SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.preferences WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.price SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.price WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prioritymap SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prioritymap WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsaudit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsauditattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsauditattachment WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsmemcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsmemcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsonecalls SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsonecalls WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsreasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsreasonlist WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsregion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsregion WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prssends SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prssends WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prssetup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prssetup WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.r1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.r1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rawaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rawaudit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rawticket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rawticket WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rdsecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rdsecurity WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reason SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reason WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasonlist WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.region SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.region WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.remarks WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    /*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquerysecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END*/

    --SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquery SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquery WHERE queryname IN (SELECT queryname FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''');
    --SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquerytext SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerytext WHERE queryname IN (SELECT queryname FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''');
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.request SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.request WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.requesttype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.requesttype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.requesttypetable SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.requesttypetable WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptdetail SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptdetail WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptheader SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptheader WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptsched SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptsched WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptsched_print SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptsched_print WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rpttemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rpttemplate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqmemcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqmemcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvaldpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvaldpt WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvalidphone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvalidphone WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvalqty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvalqty WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.s1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.s1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.s2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.s2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.savedrpttemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.savedrpttemplate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.savedrpttemplateline SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.savedrpttemplateline WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    --ScreeningQueue
    --ScreeningRuleassignment
    --ScreeningRuleset
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.security SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.security WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.securitygroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.securitygroup WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.seqid SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.seqid WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.servicearea SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.servicearea WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.statcols SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.statcols WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.suffixrequesttypes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.suffixrequesttypes WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.t1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.t1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.t2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.t2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketentrytype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketentrytype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketflag SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketflag WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketflagremarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketflagremarks WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketstatus WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketupdate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketupdate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tplformat SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.tplformat WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.u1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.u1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.updatetype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.updatetype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.userinfo SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.userinfo WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.userquery SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.userquery WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validdpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validdpt WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validphone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validphone WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validqty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validqty WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validrule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validterritory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validterritory WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.valruledpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.valruledpt WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.valruleitm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.valruleitm WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime, triggerdtdate, triggerdttime, action, direction) SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime, triggerdtdate, triggerdttime, action, direction FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitbackward WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitcommon SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitcommon WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitdata SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitdata WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitrem WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitreq SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitreq WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END


    --NEW Tables
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitprsaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitprsaudit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitqueue SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitqueue WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitaction SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitaction WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitrule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitruleaction SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitruleaction WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vmrequesttype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    /*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vmrequesttypetemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttypetemplate WHERE templateid IN (SELECT completiontemplateid FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttype WHERE customerid = ''' + @customerid + ''')';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END*/
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqcustomactcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqcustomactcode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqcustomfaccode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqcustomfaccode WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqreason SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqreason WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqreasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqreasonlist WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalidrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalidrule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalidterritory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalidterritory WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalruledpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalruledpt WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalruleitm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalruleitm WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.watch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.watch WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.wojobid SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.wojobid WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.workdetl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.workdetl WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.worktype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.worktype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.woworktype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.woworktype WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x1visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x2visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x3visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x4visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x5visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x6visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x7visit WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.xmlcompletion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.xmlcompletion WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXconfig SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXconfig WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXgenericnotes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXgenericnotes WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXpreference SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXpreference WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtermsaccept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtermsaccept WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    /*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtracelog SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtracelog WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      */
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.zone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.zone WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.zoneinterface SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.zoneinterface WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobmobilehistory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobmobilehistory WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    ---New Tables

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.blacklist(customerid, phone, username, domain, listtype, creationdateutc, createdby, comments) 
		SELECT customerid, phone, username, domain, listtype, creationdateutc, createdby, comments  FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.blacklist WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.FirstAlertPricingSchedule(customerid, schedulename, description, locateprice, locateotprice, onecallprice, onecallotprice, 
		gdprice, gdotprice, reportresearchprice, onecallplacementprice, kilometersabprice, kilometersotherprice, suppliesprice, subsistenceprice, drugtestingprice, lodgingpercent, taxpercent, isactive)
		  SELECT customerid, schedulename, description, locateprice, locateotprice, onecallprice, onecallotprice, 
			gdprice, gdotprice, reportresearchprice, onecallplacementprice, kilometersabprice, kilometersotherprice, suppliesprice, subsistenceprice, drugtestingprice, lodgingpercent, taxpercent, isactive
			 FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.FirstAlertPricingSchedule WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = '
		CREATE TABLE #jobdigConvert
	(
		id INT,
		customerid VARCHAR(32),
		jobid VARCHAR(16),
		jobgeometry GEOMETRY
	)

	INSERT INTO #jobdigConvert SELECT * FROM OPENQUERY(['+ @targetServer + '],' + '''SELECT * FROM ' + @targetDatabase + '.dbo.jobdigpoint WHERE customerid = ''''' + @customerid + ''''' '' '+');
	INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobdigpoint(customerid, jobid, jobgeometry) SELECT customerid, jobid, jobgeometry FROM  #jobdigConvert

	DROP table #jobdigConvert
	';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobstatus WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END


    --SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msggroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msggroup WHERE customerid = ''' + @customerid + '''';
    --    EXEC @return = sp_executesql @SQLCommand OUTPUT
    --    IF @return <> 0 BEGIN
    --         PRINT @SQLCommand + ' FAILED!'
    --         RETURN -1
    --    END

    --SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgqueue(status, queueddt, processingdt, failedreason, msgsource, customerid, msgtype, msggroupid, msgtag, msgdescription, msghandler, msgdatatext)
    --	 SELECT status, queueddt, processingdt, failedreason, msgsource, customerid, msgtype, msggroupid, msgtag, msgdescription, msghandler, msgdatatext FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msgqueue WHERE customerid = ''' + @customerid + '''';
    --    EXEC @return = sp_executesql @SQLCommand OUTPUT
    --    IF @return <> 0 BEGIN
    --         PRINT @SQLCommand + ' FAILED!'
    --         RETURN -1
    --    END


    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasonoccpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasonoccpriority WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasontemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasontemplate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtemplate( name, description, customerid, template_type, content, creationdt, modifiedby, modifiedon)
		 SELECT name, description, customerid, template_type, content, creationdt, modifiedby, modifiedon FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtemplate WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketpriority WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketpriorityoccpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketpriorityoccpriority WHERE customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
        RETURN -1
    END

    BEGIN
        PRINT 'Baseline Data Migration Complete!'
    END


END	

END;