﻿CREATE PROCEDURE [dbo].[sp_kt_reports_ticketsreceivedv2]
	@customerId varchar(32),
	@occid varchar(8),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(16),
	@memberid varchar(16),
	@membercode varchar(16)
	--,@sendto varchar(16)
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @occ varchar(8) = @occid
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @region varchar(16) = @regionid
DECLARE @district varchar(16) = @districtid
DECLARE @mobile varchar(16) = @mobileid
DECLARE @member varchar(16) = @memberid
DECLARE @memcode varchar(16) = @membercode
--DECLARE @sendtoMember varchar(16) = @sendto
CREATE TABLE #TR
(customerid     VARCHAR(32), 
 jobid          VARCHAR(16), 
 originpriority VARCHAR(16), 
 regionid       VARCHAR(16), 
 districtid		VARCHAR(16),
 mobileid       VARCHAR(16), 
 address        VARCHAR(8), 
 city           VARCHAR(64), 
 street         VARCHAR(32), 
 worktype       VARCHAR(64)
);
INSERT INTO #TR
       SELECT 
			  t.customerid, 
              t.jobid, 
              t.origpriority, 
              m.regionid,
			  m.districtid,
			  m.mobileid, 
              j.address, 
              j.city, 
              j.street, 
              j.worktype
       FROM ticketstatus t
            JOIN mobile m ON m.customerid = t.customerid
                             AND m.mobileid = t.mobileid
            JOIN memcode c ON c.customerid = t.customerid
                              AND c.membercode = t.sendto
            JOIN
       (
           SELECT DISTINCT 
                  customerid, 
                  jobidnosuffix, 
                  COALESCE(address, '''') AS address, 
                  COALESCE(street, '''') AS street, 
                  COALESCE(city, '''') AS city, 
                  COALESCE(worktype, '''') AS worktype
           FROM job
       ) j ON j.customerid = t.customerid
              AND j.jobidnosuffix = t.jobid
       WHERE t.customerid = @customerid
               AND (t.xmitdtdate BETWEEN CONVERT(VARCHAR(30), @fromDate, 121) AND CONVERT(VARCHAR(30), @throughDate, 121)
                 AND (@regionid = '0' OR m.regionid =@region)
                 AND (@districtid = '0' OR m.districtid = @district)
                 AND (@mobileid = '0' OR  m.mobileid = @mobile)
                 AND (@occid = '0' OR t.occid= @occ)
                 AND (@memberid = '0' OR c.memberid= @memberid)
                 AND (@membercode = '0' OR t.sendto = @memcode )
				 )
				 
				  GROUP BY t.customerid, 
              t.jobid, 
              t.origpriority, 
              m.regionid,
			  m.districtid,
			  m.mobileid, 
              j.address, 
              j.city, 
              j.street, 
              j.worktype
				  ORDER BY t.jobid;
SELECT customerid, jobid, originpriority, regionid, districtid, mobileid, address, city, worktype, COUNT(*) FROM #TR 
GROUP BY customerid, jobid, originpriority, regionid, districtid, mobileid, address, city, worktype
ORDER BY COUNT(*) DESC 
--SELECT COUNT(*) FROM #TR
DROP TABLE #TR