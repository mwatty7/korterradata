﻿
CREATE PROCEDURE [dbo].[sp_move_precheck]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
    @includeTableCountAudit bit = 0
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @return Int;
    DECLARE @TableCount INT
    SET @TableCount = 386
    DECLARE @errMsg NVARCHAR(MAX) = NULL;

    IF @includeTableCountAudit = 1
		IF(SELECT COUNT(*)
    FROM sys.tables
    WHERE type_desc = 'USER_TABLE') != @TableCount
			BEGIN
        SET @errMsg = 'There are a different number of tables than when this script was written. Either you have extraneous tables or the dev team needs to update this script.';
        RAISERROR (@errMsg, 15, 10);
    END

    --CHECK the source server and source DB
    DECLARE @sqlSourceServerCheck  NVARCHAR(MAX);
    SET @sqlSourceServerCheck = 'SELECT [name] FROM sys.servers WHERE name = ''' + @sourceServer + ''' ';

    DECLARE @serverCheckResults  TABLE([name] VARCHAR(50));

    INSERT @serverCheckResults
    EXECUTE(@sqlSourceServerCheck)


    IF NOT EXISTS (SELECT *
    FROM @serverCheckResults
    WHERE [name] = @sourceServer)

	BEGIN
        SET @errMsg = 'ERROR! Must have source server linked';
        RAISERROR (@errMsg, 15, 10);
    END;


    DECLARE @sqlSourceDBCheck  NVARCHAR(MAX);

    DECLARE @stripSourceDBBrackets NVARCHAR(MAX) = @sourceDatabase
    SET @stripSourceDBBrackets = RTRIM(REPLACE(REPLACE(@stripSourceDBBrackets,'[', ''), ']', ''))


    SET @sqlSourceDBCheck = 'SELECT [name] FROM ['+ @sourceServer + '].master.sys.databases WHERE name = ''' + @stripSourceDBBrackets + ''' ';


    DECLARE @sourceDBCheckResults  TABLE([name] VARCHAR(50));

    INSERT @sourceDBCheckResults
    EXECUTE(@sqlSourceDBCheck)

    IF NOT EXISTS (SELECT *
    FROM @sourceDBCheckResults
    WHERE [name] = @stripSourceDBBrackets)

	BEGIN
        SET @errMsg = 'ERROR! Please check your source server and ensure source DB exists...';
        RAISERROR (@errMsg, 15, 10);
    END;


    ---CHECK Target Source and DB: 
    DECLARE @sqlTargetServerCheck  NVARCHAR(MAX);
    SET @sqlTargetServerCheck = 'SELECT [name] FROM sys.servers WHERE name = ''' + @targetServer + ''' ';
    DECLARE @targetServerCheckResults  TABLE([name] VARCHAR(50));

    INSERT @targetServerCheckResults
    EXECUTE(@sqlTargetServerCheck)


    IF NOT EXISTS (SELECT *
    FROM @targetServerCheckResults
    WHERE [name] = @targetServer)

	BEGIN
        SET @errMsg = 'ERROR! Must execute within target server!';
        RAISERROR (@errMsg, 15, 10);
    END;


    DECLARE @sqlTargetDBCheck  NVARCHAR(MAX);
    DECLARE @stripTargetDBBrackets NVARCHAR(MAX) = @targetDatabase;
    SET @stripTargetDBBrackets = RTRIM(REPLACE(REPLACE(@stripTargetDBBrackets,'[', ''), ']', ''))

    SET @sqlTargetDBCheck = 'SELECT [name] FROM ['+ @targetServer + '].master.sys.databases WHERE name = ''' + @stripTargetDBBrackets + '''';

    --PRINT @sqlTargetDBCheck;

    DECLARE @targetDBCheckResults  TABLE([name] VARCHAR(50));

    INSERT @targetDBCheckResults
    EXECUTE(@sqlTargetDBCheck)

    IF NOT EXISTS (SELECT *
    FROM @targetDBCheckResults
    WHERE [name] = @stripTargetDBBrackets)

	BEGIN
        SET @errMsg = 'ERROR! Target DB does not exist. Please check target environment and ensure target DB is setup...';
        RAISERROR (@errMsg, 15, 10);
    END;

    ---CHECK customer doesn't already exist in Target Server:

    DECLARE @sqlcheckCustomer  NVARCHAR(MAX);
    SET @sqlcheckCustomer = 'SELECT customerid FROM '+ @targetDatabase +'.dbo.Customer WHERE customerid = ''' + @customerId + ''' ';

    DECLARE @checkCustomerResults  TABLE([customerid] VARCHAR(50));

    INSERT @checkCustomerResults
    EXECUTE(@sqlcheckCustomer)

    IF EXISTS (SELECT @customerId
    FROM @checkCustomerResults
    WHERE customerid = @customerId)

	BEGIN
        SET @errMsg = 'Customer already exists within the Target Database! Please ensure this is the correct customer';
        RAISERROR (@errMsg, 15, 10);
    END;

    ---CHECK if customer doesn't exist in source server:

    DECLARE @sqlcheckSourceCustomer  NVARCHAR(MAX);
    DECLARE @tableName NVARCHAR(80)
    SET @tableName = 'customer';

    SET @sqlcheckSourceCustomer = 'SELECT customerid, ''customer'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.customer WHERE customerid = ''' + @customerId + ''' ';

    DECLARE @checkSourceResults  TABLE([customerid] VARCHAR(50),
        [table] VARCHAR (80));


    INSERT @checkSourceResults
    EXECUTE(@sqlcheckSourceCustomer)

    IF NOT EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'customer')

    BEGIN
        SET @errMsg = 'Customer does not exist within the Source Database! Please verify customer migration.';
        RAISERROR (@errMsg, 15, 10);
    END;

    --QUEUE Checks: If we're migrating a customer, we want to ensure no queue data currently
    DECLARE @queueChecks NVARCHAR(MAX)

    /*msgqueue check*/
    SET @queueChecks =  'SELECT customerid, ''msgqueue'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.msgqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'msgqueue')
    BEGIN
        SET @errMsg = 'Stop! Customer related data exists in the MsgQueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.';
        RAISERROR (@errMsg, 15, 10);
    END;


    /*screeningqueue check*/
    SET @queueChecks =  'SELECT customerid, ''ScreeningQueue'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.ScreeningQueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'ScreeningQueue')

	BEGIN
        SET @errMsg = 'Stop! Customer related data exists in the ScreeningQueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RAISERROR (@errMsg, 15, 10);
    END;

    /*visitqueue check*/
    SET @queueChecks =  'SELECT customerid, ''visitqueue'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.visitqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitqueue')

	BEGIN
        SET @errMsg = 'Stop! Customer related data exists in the Visitqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RAISERROR (@errMsg, 15, 10);
    END;

    /*visitprsqueue check*/
    SET @queueChecks =  'SELECT customerid, ''visitprsqueue'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.visitprsqueue WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitprsqueue')

	BEGIN
        DELETE FROM @checkSourceResults
        SET @errMsg = 'Stop! Customer related data exists in the Visitprsqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RAISERROR (@errMsg, 15, 10);
    END;


    /*visitbackward check*/
    SET @queueChecks =  'SELECT customerid, ''visitbackward'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.visitbackward WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitbackward')

	BEGIN
        DELETE FROM @checkSourceResults
        SET @errMsg = 'Stop! Customer related data exists in the Visitbackward table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RAISERROR (@errMsg, 15, 10);
    END;

    /*attachment backward check*/
    SET @queueChecks =  'SELECT customerid, ''attachmentbackward'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.attachmentbackward WHERE customerid = ''' + @customerId + ''' ';
    INSERT @checkSourceResults
    EXECUTE(@queueChecks)

    IF EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'visitbackward')

	BEGIN
        DELETE FROM @checkSourceResults
        SET @errMsg = 'Stop! Customer related data exists in the Attachmentbackward table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
        RAISERROR (@errMsg, 15, 10);
    END;

    
/*jobsendcompatibleforward check*/
SET @queueChecks =  'SELECT customerid, ''jobsendcompatibleforward'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.jobsendcompatibleforward WHERE customerid = ''' + @customerId + ''' ';
INSERT @checkSourceResults
EXECUTE(@queueChecks)

IF EXISTS (SELECT *
FROM @checkSourceResults
WHERE customerid = @customerId and [table] = 'jobsendcompatibleforward')

	BEGIN
    DELETE FROM @checkSourceResults
    SET @errMsg = 'Stop! Customer related data exists in the jobsendcompatibleforward table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
    RAISERROR (@errMsg, 15, 10);
END;

/*queue check*/
SET @queueChecks =  'SELECT customerid, ''xxqueue'' FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.xxqueue WHERE customerid = ''' + @customerId + ''' ';
INSERT @checkSourceResults
EXECUTE(@queueChecks)

IF EXISTS (SELECT *
FROM @checkSourceResults
WHERE customerid = @customerId and [table] = 'xxqueue')

	BEGIN
    DELETE FROM @checkSourceResults
    SET @errMsg = 'Stop! Customer related data exists in the xxqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.'
    RAISERROR (@errMsg, 15, 10);
END;

/*emailqueue check*/
DECLARE @EmailQueueCheck NVARCHAR(MAX)
SET @EmailQueueCheck = 'SELECT id FROM ['+ @sourceServer + '].'+ @sourceDatabase + '.dbo.emailqueue';

DECLARE @CheckEmailQueueForRecords TABLE(id INT);
INSERT @CheckEmailQueueForRecords
EXECUTE (@EmailQueueCheck)

IF EXISTS (SELECT *
FROM @CheckEmailQueueForRecords)

BEGIN
    SET @errMsg = 'Stop! Data exists in the emailqueue table. Please run the appropriate apps to clear out the data or delete the data in the customer database if not needed.';
    RAISERROR (@errMsg, 15, 10);
END;


    DECLARE @DataAccessCheck NVARCHAR(MAX)
    SET @DataAccessCheck = 'SELECT is_data_access_enabled FROM sys.servers WHERE name = ''' + @targetServer + ''' ';

    DECLARE @CheckTargetDataAccess  TABLE(is_data_access_enabled BIT);
    INSERT @CheckTargetDataAccess
    EXECUTE(@DataAccessCheck)

    IF EXISTS (SELECT is_data_access_enabled
    FROM @CheckTargetDataAccess
    WHERE is_data_access_enabled = 0)

	BEGIN
        SET @errMsg = 'Please enable data access for your target server. This feature can be enabled by executing the sp_serveroption against your target server with DATA ACCESS set to TRUE.
    We recommend re-setting the feature to False upon successful completion of the data migration.';
        RAISERROR (@errMsg, 15, 10);
    END;

    SET @DataAccessCheck = 'SELECT is_data_access_enabled FROM sys.servers WHERE name = ''' + @sourceServer + ''' ';
    DECLARE @CheckSourceDataAccess TABLE(is_data_access_enabled BIT);
    INSERT @CheckSourceDataAccess
    EXECUTE(@DataAccessCheck)

    IF EXISTS (SELECT is_data_access_enabled
    FROM @CheckSourceDataAccess
    WHERE is_data_access_enabled = 0)

    BEGIN
        SET @errMsg = 'Please enable data access for your source server. This feature can be enabled by executing the sp_serveroption against your source server with DATA ACCESS set to TRUE.
        We recommend re-setting the feature to False upon successful completion of the data migration.';
        RAISERROR (@errMsg, 15, 10);
    END;

END