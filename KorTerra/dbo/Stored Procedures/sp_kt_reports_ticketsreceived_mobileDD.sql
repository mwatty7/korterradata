﻿ 
CREATE PROCEDURE [dbo].[sp_kt_reports_ticketsreceived_mobileDD]
	@customerId varchar(32),
	@occid varchar(8),
	@regionid varchar(16),
	@districtid varchar(16)
AS
BEGIN
	Declare @Where NVARCHAR(MAX) 
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = 'SELECT mobileid,  concat(mobileid, '' - '', description) as fulldescription FROM mobile'
	If( @customerId <> '' )
	   Set @Command = @Command + ' WHERE customerid = ''' + @customerId + ''' '
	If( @regionid <> '0' )
	   Set @Command = @Command + ' AND regionid = ''' + @regionid + ''' '
	If( @districtid <> '0' )
	   Set @Command = @Command + ' AND districtid = ''' + @districtid + ''' '
	Set @Command = @Command + ' UNION SELECT ''0'', ''ALL'' ORDER BY mobileid'
	Execute SP_ExecuteSQL  @Command
END