﻿CREATE PROCEDURE [dbo].[sp_audit_moved_ktable]
    @tableName VARCHAR(50) = NULL,
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @ParmDefinition nvarchar(500);
	DECLARE @CountSQLQuery varchar(30);
    SET @SQLCommand = N'SELECT @result = ABS(((SELECT COUNT(*) cnt FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.' + @tableName+ '
        WHERE k_customerid = ''' + @customerId + ''') - (SELECT COUNT(*) cnt FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.' + @tableName+ '
        WHERE k_customerid = ''' + @customerid + ''')))';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	BEGIN TRY
		EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
		SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
		IF @CountSQLQuery <> 0
		BEGIN
			PRINT @SQLCommand + ' FAILED!'
			DECLARE @errMsg VARCHAR(200) = 'KTABLE: ' + @tableName + ' FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
			RAISERROR (@errMsg, 15, 10)
		END
		ELSE IF @CountSQLQuery = 0
		BEGIN 
			PRINT @tableName + ' Records Match'
		END
		   
	END TRY
    
	BEGIN CATCH 
		PRINT('AUDIT MOVED KTABLE FAILED: ' + @tableName);
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END;