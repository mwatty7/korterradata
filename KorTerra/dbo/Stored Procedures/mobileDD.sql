﻿CREATE PROCEDURE [dbo].[mobileDD] 
@customerId varchar(32),
@occid varchar(8),
@regionid varchar(16),
@districtid varchar(16)

AS

Declare @Where NVARCHAR(MAX) 
DECLARE @Command NVARCHAR(MAX) 

-- set @customerId = 'KORCUST'

Set @Command = 'SELECT mobileid,  concat(mobileid, '' - '', description) as fulldescription FROM mobile
				WHERE customerid = ''' + @customerId + ''''

If( @regionid <> 'ALL' )
   Set @Command = @Command + ' AND regionid = ''' + @regionid + ''' '

If( @districtid <> 'ALL' )
   Set @Command = @Command + ' AND districtid = ''' + @districtid + ''' '

Set @Command = @Command + ' UNION SELECT ''ALL'', ''ALL'' ORDER BY mobileid'

Execute SP_ExecuteSQL  @Command