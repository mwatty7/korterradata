﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbyoperatoridtop5v3]
	@customerid varchar(32),
	@operatorid varchar(32),
	@regionid varchar(32),
	@districtid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@offset int = 0
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @operator varchar(32) = @operatorid
DECLARE @region varchar(32) = @regionid
DECLARE @district varchar(32) = @districtid
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @nowUtc DATETIME = SYSUTCDATETIME()
create table #summary(
	customerid varchar(32),
	LateCount bigint, 
	operatorid varchar(32),
	operatordescription varchar (128)
	)
BEGIN
 if @date = '1' 
	SET @through = dateAdd(dy, 1, @throughDate)
 BEGIN
	SET @from = dbo.DateOnly(dateAdd(MINUTE, @offset,  @from))
	SET @through = dbo.DateOnly(dateAdd(MINUTE, @offset, @through))
 END
insert into #summary (customerid,  LateCount, operatorid, operatordescription)
			SELECT
			job.customerid,
			CAST(COUNT_BIG(job.jobid) as bigint) LateCount,
			vc.operatorid,
			operator.description as operatordescription
		
		FROM job WITH(NOLOCK)
		-- Rule I: Tickets with multi member codes, Late ticket will count once in each member code.
			JOIN request WITH(NOLOCK) on request.customerid = @customerid AND request.jobid = job.jobid
			LEFT JOIN mobile WITH(NOLOCK) on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			OUTER APPLY (
				SELECT TOP 1 operatorid FROM visitcommon WITH(NOLOCK)
				WHERE visitcommon.customerid = job.customerid AND visitcommon.jobid = job.jobid AND visitcommon.membercode = request.membercode AND visitcommon.completiondateutc = request.firstcompletiondateutc
			) as vc
			LEFT JOIN operator on operator.customerid = @customerid AND operator.operatorid = vc.operatorid
		WHERE
			-- Filter by customerid always
			job.customerid = @customerid AND
			-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
   		   ((@dateType = '1' AND duedateutc BETWEEN @from AND @through) OR
			@dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through)) AND
			-- Rule G: Filter out all cancelled / nlr tickets
			(job.status NOT IN ('R', 'X') AND
			-- Rule C: Filter out updates without completiondt
			(job.status = 'U' AND request.firstcompletiondtdate IS NOT NULL OR job.status != 'U')) AND 
			-- Rule B: Late is always determined by UTC duedate
			-- Rule H: If One facility for a membercode is on time, count it as On Time. ONLY LOOK AT FIRST COMPLETION PER MEMBERCODE
			((request.firstcompletiondateutc >  job.duedateutc OR
			-- Rule D: Membercodes still open are late if "right now" is late.
			(request.firstcompletiondateutc IS NULL AND @nowUtc > job.duedateutc))
			-- These rules are inherited because we are not looking at completion levels
			-- Rule E: Reopened are treated like partials - if one complete was on time, we count it as on time.
			-- Rule F: Partial completes count as complete.
			-- Filter Emergencies and Projects by user criteria
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
			--Filter By membercode and memberid by user criteria
			AND ((@operator = '0' AND operator.operatorid IS NULL) OR (@operator = '0' OR vc.operatorid = @operator))
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region))
		GROUP BY job.customerid, vc.operatorid, operator.description
		--ORDER BY vc.operatorid 
		
		--Set null operatorids to 'N/A'
		UPDATE #summary set operatorid ='N/A' where operatorid is null;
		SELECT top 5 * from  #summary order by LateCount desc
END
SET ANSI_NULLS ON