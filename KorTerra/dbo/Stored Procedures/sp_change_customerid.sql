﻿CREATE PROCEDURE [dbo].[sp_change_customerid]
	@oldcustomerid NVARCHAR(32) = NULL,
	@newcustomerid NVARCHAR(32) = NULL,
	@activewithinminutes INT = NULL
AS
BEGIN
	SET NOCOUNT ON
	---------------------
	-- 
	-- Validate the parameters
	--
	RAISERROR('Validate the parameters', 0, 1) WITH NOWAIT;
	IF @oldcustomerid IS NULL OR @newcustomerid IS NULL
	BEGIN
		RAISERROR(N'USAGE: sp_change_customerid <oldcustomerid>, <newcustomerid>', 0, 1) WITH NOWAIT;
		RETURN;
	END;
	IF @oldcustomerid = @newcustomerid
	BEGIN
		RAISERROR(N'FAILED: cannot change %s to %s', 0, 1, @oldcustomerid, @newcustomerid) WITH NOWAIT;
		RETURN;
	END;
	IF @activewithinminutes IS NULL
	BEGIN
		SELECT @activewithinminutes = 60
	END
	RAISERROR('Change %s to %s', 0, 1, 	@oldcustomerid, @newcustomerid) WITH NOWAIT;
	RAISERROR('Validating the parameters', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Check to make sure no one is logged in
	--
	DECLARE @activewithinminutesStr VARCHAR(32) = @activewithinminutes;
	RAISERROR('Check to make sure no one has been active within the last %s minutes', 0, 1, @activewithinminutesStr) WITH NOWAIT;
	DECLARE @lastActive DATETIME;
	DECLARE @lastActiveContents VARCHAR(MAX);
	SELECT TOP 1 
		@lastActive = x.lastAccessed, @lastActiveContents = x.contents
	FROM XXtokencache x
	ORDER BY x.lastAccessed DESC
	DECLARE @lastActiveAgo FLOAT = DATEDIFF(SECOND, @lastActive, CURRENT_TIMESTAMP)
	SELECT @lastActiveAgo = IIF(@lastActiveAgo > 0, @lastActiveAgo, 1.7976931348623158E+308);
	IF @lastActiveAgo < @activewithinminutes * 60
	BEGIN
		DECLARE @lastActiveAgoStr VARCHAR(32)
		SELECT @lastActiveAgoStr = FORMAT(DATEADD(SECOND, @lastActiveAgo, 0), 'HH:mm:ss')
		DECLARE @activewithinminutesStrFmt VARCHAR(32)
		SELECT @activewithinminutesStrFmt = FORMAT(DATEADD(MINUTE, @activewithinminutes, 0), 'HH:mm:ss')
		RAISERROR('FAILED: Someone logged in only %s h:m:s ago (less than %s): %s', 1, 1, 
			@lastActiveAgoStr, @activewithinminutesStrFmt, @lastActiveContents) WITH NOWAIT;
		RETURN;
	END;
	RAISERROR('DONE: Check to make sure no one has been active within the last %s minutes', 0, 1, @activewithinminutesStr) WITH NOWAIT;
	---------------------
	-- 
	-- Make sure the old customer exists
	--
	RAISERROR('Make sure the old customer exists', 0, 1) WITH NOWAIT;
	DECLARE @oldcustomercount INT;
	SELECT @oldcustomercount = COUNT(*)
		FROM [customer]
		WHERE customerid = @oldcustomerid
		;
	IF @oldcustomercount <= 0
	BEGIN
		RAISERROR('FAIL: Old customer %s does not exist', 0, 1, @oldcustomerid) WITH NOWAIT;
		RETURN;
	END;
	RAISERROR('DONE: Making sure the old customer exists', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Make sure the new customerid isn't used, anywhere
	--
	RAISERROR('Make sure the new customerid isn''t used, anywhere', 0, 1) WITH NOWAIT;
	DECLARE cursor1 CURSOR LOCAL FOR
		SELECT DISTINCT TABLE_SCHEMA, TABLE_NAME
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE COLUMN_NAME = 'customerid'
		AND DATA_TYPE IN ('CHAR', 'NCHAR', 'VARCHAR', 'NVARCHAR')
		;
	DECLARE @tablename1 VARCHAR(32);
	DECLARE @tableschema1 VARCHAR(32);
	OPEN cursor1;
	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM cursor1 INTO @tableschema1, @tablename1;
		IF @@FETCH_STATUS <> 0
			BREAK;
		DECLARE @count1 INT;
		DECLARE @sql1 NVARCHAR(MAX);
		DECLARE @params1 NVARCHAR(MAX);
		SELECT @sql1 = 
			N' SELECT @cnt = COUNT(*) FROM ' + quotename(@tableschema1) + '.' + quotename(@tablename1) +
			N' WHERE customerid = @customerid';
		SELECT @params1 =
			N'@customerid VARCHAR(32), @cnt INT OUTPUT';
		EXEC sp_executesql @sql1, @params1, @newcustomerid, @cnt = @count1 OUTPUT;
		IF @count1 > 0
		BEGIN
			RAISERROR('FAILED: Table %s already contains records with customerid %s', 1, 1, @tablename1, @newcustomerid) WITH NOWAIT;
			RETURN
		END
	END
	RAISERROR('No table contains records with customerid %s', 0, 1, @newcustomerid) WITH NOWAIT;
	RAISERROR('DONE: Making sure the new customerid isn''t used, anywhere', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Disable any foreign keys that reference customerid
	--
	RAISERROR('Disable any foreign keys that reference customerid', 0, 1) WITH NOWAIT;
	DECLARE @disableFks NVARCHAR(MAX) = N'';
	DECLARE @enableFks NVARCHAR(MAX) = N'';
	SELECT @disableFks += 
		N'RAISERROR(''disabling %s'', 0, 1, ''' + kf.constraint_name + N''') WITH NOWAIT;' + 
		N'ALTER TABLE ' + QUOTENAME(kf.table_schema) + N'.' + QUOTENAME(kf.table_name) +
		N' NOCHECK CONSTRAINT ' + QUOTENAME(kf.constraint_name) + N';'
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kf ON rc.constraint_name = kf.constraint_name
	WHERE kf.column_name = 'customerid';
	SELECT @enableFks += 
		N'RAISERROR(''enabling %s'', 0, 1, ''' + kf.constraint_name + N''') WITH NOWAIT;' + 
		N'ALTER TABLE ' + QUOTENAME(kf.table_schema) + N'.' + QUOTENAME(kf.table_name) +
		N' CHECK CONSTRAINT ' + QUOTENAME(kf.constraint_name) + N';'
	FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
	JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kf ON rc.constraint_name = kf.constraint_name
	WHERE kf.column_name = 'customerid';
	EXEC sp_executesql @disableFks;
	RAISERROR('DONE: Disabling foreign keys', 0, 1) WITH NOWAIT;
		---------------------
	-- 
	-- Disable Trigger on Holiday Table
	--
	---------------------
	DISABLE TRIGGER [kt_tr_populate_calendar] ON Holiday 
		RAISERROR('DONE: Disabling trigger on holiday', 0, 1) WITH NOWAIT;
	-- 
	-- Update any records that use @oldcustomerid to use @newcustomerid
	--
	RAISERROR('Update any records that use %s to use %s', 0, 1, @oldcustomerid, @newcustomerid) WITH NOWAIT;
	-- Run visitbackward last
	DECLARE cursor2xvb CURSOR LOCAL FOR
		SELECT DISTINCT s.name, t.name
		FROM sys.columns c
		JOIN sys.tables t ON c.object_id = t.object_id
		JOIN sys.schemas s ON s.schema_id = t.schema_id
		WHERE c.name = N'customerid'
		AND t.name <> 'visitbackward'
		ORDER BY s.name, t.name;
	DECLARE cursor2wvb CURSOR LOCAL FOR
		SELECT DISTINCT s.name, t.name
		FROM sys.columns c
		JOIN sys.tables t ON c.object_id = t.object_id
		JOIN sys.schemas s ON s.schema_id = t.schema_id
		WHERE c.name = N'customerid'
		AND t.name = 'visitbackward'
		ORDER BY s.name, t.name;
	DECLARE @sSQL2 NVARCHAR(MAX);
	DECLARE @params2 NVARCHAR(MAX);
	DECLARE @tableschema2 NVARCHAR(MAX);
	DECLARE @tablename2 NVARCHAR(MAX);
	OPEN cursor2xvb;
	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM cursor2xvb INTO @tableschema2, @tablename2;
		IF @@FETCH_STATUS <> 0
			BREAK;
		SELECT @sSQL2 = 
			N' UPDATE ' + quotename(@tableschema2) + '.' + quotename(@tablename2) +
			N' SET customerid = @newcid ' +
			N' WHERE customerid = @oldcid ';
		SELECT @params2 = 
			N'@oldcid VARCHAR(32), @newcid VARCHAR(32)';
		RAISERROR('Updating %s', 0, 1, @tablename2) WITH NOWAIT;
		EXEC sp_executesql @sSQL2, @params2, @oldcustomerid, @newcustomerid
	END
	OPEN cursor2wvb;
	WHILE 1 = 1
	BEGIN
		FETCH NEXT FROM cursor2wvb INTO @tableschema2, @tablename2;
		IF @@FETCH_STATUS <> 0
			BREAK;
		SELECT @sSQL2 = 
			N' UPDATE ' + quotename(@tableschema2) + '.' + quotename(@tablename2) +
			N' SET customerid = @newcid ' +
			N' WHERE customerid = @oldcid ';
		SELECT @params2 = 
			N'@oldcid VARCHAR(32), @newcid VARCHAR(32)';
		RAISERROR('Updating %s last', 0, 1, @tablename2) WITH NOWAIT;
		EXEC sp_executesql @sSQL2, @params2, @oldcustomerid, @newcustomerid
	END
	RAISERROR('DONE: Updating records', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Re-enable any foreign keys that reference customerid
	--
	RAISERROR('Re-enable any foreign keys that reference customerid', 0, 1) WITH NOWAIT;
	EXEC sp_executesql @enableFks;
	RAISERROR('DONE: Re-enabling foreign keys', 0, 1) WITH NOWAIT;
			---------------------
	-- 
	-- Re-enable Trigger on Holiday Table
	--
	---------------------
	ENABLE TRIGGER [kt_tr_populate_calendar] ON Holiday 
		RAISERROR('DONE: Re-enabling trigger on holiday', 0, 1) WITH NOWAIT;
	---------------------
	-- 
	-- Done!
	--
	RAISERROR('Done!', 0, 1) WITH NOWAIT;
	SET NOCOUNT OFF
END