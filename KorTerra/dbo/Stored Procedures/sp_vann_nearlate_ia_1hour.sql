﻿

CREATE PROCEDURE [dbo].[sp_vann_nearlate_ia_1hour]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;


	DECLARE @customerid VARCHAR(32);
	DECLARE @heraldmessengerid VARCHAR(32);
	DECLARE @intervalSeconds INT;

	SET @customerid = 'SYSTEM';
	SET @heraldmessengerid = 'VANN-IA-1H';
	SET @intervalSeconds = 3600;

	SELECT DISTINCT j.jobid
		FROM job j
	LEFT OUTER JOIN visitreq v
		ON v.customerid = j.customerid
		AND v.jobid = j.jobid
	LEFT OUTER JOIN visitview vv
		ON vv.customerid = v.customerid
		AND vv.jobid = v.jobid
		AND vv.membercode = v.membercode
		AND vv.requesttype = v.requesttype
	LEFT OUTER JOIN messagedjob m
		ON m.customerid = j.customerid
		AND m.jobid = j.jobid
		AND m.heraldmessengerid = @heraldmessengerid
	WHERE j.customerid = @customerid
		AND status in ('A', 'N')
		AND priority <> 'EMERGENCY'
		AND (latestxmitsource LIKE 'IA%' OR latestxmitsource LIKE 'IL%')
		AND DATEDIFF(SS, GETDATE(), j.wtbdtdate + j.wtbdttime) >= 0
		AND DATEDIFF(SS, GETDATE(), j.wtbdtdate + j.wtbdttime) <= @intervalSeconds
		AND vv.membercode IS NULL
		AND m.jobid IS NULL
	;

END