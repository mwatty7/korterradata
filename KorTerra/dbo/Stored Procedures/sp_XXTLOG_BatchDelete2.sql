﻿ 
CREATE PROCEDURE dbo.sp_XXTLOG_BatchDelete2
    @increment BIGINT


AS
    BEGIN TRY
     
    -- Declare local variables
    DECLARE @sqlstr NVARCHAR(2000);
	DECLARE @daysToRetain BIGINT;
    DECLARE @rowcount BIGINT;
    DECLARE @loopcount BIGINT;
	SET @increment = @increment;

    -- Initialize the loop counter
    SET @loopcount = 1;
     

	 --SET days to retain from XXconfig
	SELECT @daysToRetain = ISNULL(
		(SELECT CONVERT(BIGINT, value) FROM XXconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'DAYSTORETAIN'), 30)
	;
	PRINT 'daysToRetain = ' + CONVERT(VARCHAR, @daysToRetain)
 
	--SET rowcount for loop 
	SELECT @rowcount =  COUNT(*) FROM XXtracelog WHERE logdt <= DATEADD(DAY, - @daysToRetain, CURRENT_TIMESTAMP)
	--SELECT COUNT(*) FROM XXtracelog WHERE logdt <= DATEADD(DAY, - 90, CURRENT_TIMESTAMP)



	PRINT 'rowCount = ' + CONVERT(VARCHAR, @rowcount)
      
    -- Perform the loop while there are rows to delete
    WHILE @loopcount < @rowcount
    BEGIN

SET @sqlstr = 'DELETE TOP ('+CAST(@increment AS VARCHAR(10))+') FROM XXtracelog WHERE logdt <= DATEADD(DAY, -'+CONVERT(VARCHAR,@daysToRetain)+', CURRENT_TIMESTAMP)';          
         -- Execute the dynamic SQL string to delete a batch of rows
         EXEC(@sqlstr);
          
         -- Add the @increment value to @loopcount
         SET @loopcount = @loopcount + @increment;    
    END
     
    END TRY

    BEGIN CATCH
         
        SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
 
    END CATCH

		BEGIN
     	          PRINT CAST(@rowcount AS VARCHAR(10)) + ' total rows deleted.'
	END