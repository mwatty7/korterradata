﻿
CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymobileidv2]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32) = null,
	
	@summary bit = 0,
	@detail bit = 0,
	@statusCounts bit = 0,
	@lateMobileCounts bit = 0,
	@grandTotal bit = 0
AS
DECLARE @customer varchar(32) = @customerId
DECLARE @region varchar(16) = @regionid
DECLARE @district varchar(16) = @districtid
DECLARE @mobile varchar(32) = @mobileid
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_statusCounts bit = @statusCounts
DECLARE @_lateMobileCounts bit = @lateMobileCounts
DECLARE @_grandTotal bit = @grandTotal
DECLARE @_summary bit = @summary
-- Late Ticket Detail - By WTB
IF @_detail = 1 
	BEGIN
		IF @output = 'detailed'
		WITH detailed (customerid,regionid,districtid,sendto,mobileid,description,jobid,worktype,Transmit,Wtb,duedate,completiondt,origpriority,address,street,city,county,state,isproject,status,lateHours,lateMinutes,completionStatus)
		AS
		(SELECT
			job.customerid,
			mobile.regionid,
			mobile.districtid,
			job.sendto,
			job.mobileid,
			mobile.description,
			job.jobid,
			job.worktype,
			CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(job.duedateutc, occ.timezone, 1) as duedate,
			visitcommon.completiondt,
			job.origpriority,
			job.address, 
			job.street,
			job.city, 
			job.county, 
			job.state,
			job.isproject,
			job.status,
			(DATEDIFF(minute,dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(visitcommon.completiondt, @clientDt)) / 60) as lateHours,
			(DATEDIFF(minute,dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(visitcommon.completiondt, @clientDt)) % 60) AS lateMinutes,
			CASE 
				WHEN visitcommon.completiondt IS NULL then 'Open / Late'
				WHEN visitcommon.completiondt IS NOT NULL then 'Completed / Late'
			END AS completionStatus
		FROM 
			job
			join occ on occ.customerid = job.customerid and
			occ.occid = job.latestxmitsource
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X')
				AND ((job.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (job.status != 'U'))
				AND	((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through) OR @dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through)))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND (visitcommon.completiondt >  (dbo.GetUTC(job.duedateutc, occ.timezone, 1))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt >  dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			)
			SELECT * from detailed ORDER BY jobid;
			IF @output = 'summary'
				SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Mobile Counts - By WTB
IF @_lateMobileCounts = 1
	BEGIN
		SELECT TOP 5 mobile.mobileid, COUNT(jobid) as totalLate
		FROM 
			job
			join occ on occ.customerid = job.customerid and
			occ.occid = job.latestxmitsource
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X')
				AND ((job.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (job.status != 'U'))
				AND	(@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through) OR @dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND (visitcommon.completiondt >  (dbo.GetUTC(job.duedateutc, occ.timezone, 1))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt >  dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY mobile.mobileid
			ORDER BY totalLate DESC
	END
-- Late Ticket @_statusCounts - By WTB
IF @_statusCounts = 1 
	BEGIN
		SELECT
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS null THEN 1
				END
			) as openLateCount,
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS NOT null THEN 1
				END
			) as lateCount,
			COUNT(job.jobid) as grandTotal
			FROM 
				job
				join occ on occ.customerid = job.customerid and
				occ.occid = job.latestxmitsource
				OUTER APPLY
				(
					SELECT
						mobile.mobileid,
						mobile.description,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = job.customerid 
						AND mobile.mobileid = job.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = job.customerid 
						AND visitcommon.jobid = job.jobid
				) visitcommon
				WHERE 
					job.customerid = @customer
					AND job.status NOT IN ('R', 'X')
					AND ((job.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (job.status != 'U'))
					AND	((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through) OR @dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through)))	
				-- Grab only the records that are greater than the wtbdtdate and time
					AND (visitcommon.completiondt >  (dbo.GetUTC(job.duedateutc, occ.timezone, 1))
					-- If the completion date is null then check compared to the clients current date and time
					OR  (visitcommon.completiondt IS NULL AND @clientDt >  dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1))))
					AND (@mobile = '0' OR mobile.mobileid = @mobile)
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR job.isproject = '0')
	END
-- Late Ticket Summary By - WTB Date
IF @_summary = 1
	BEGIN
		IF @output = 'summary'
		WITH summary(LateCount, mobileid ,description, avgHrs, avgMins, TotalMobileCount )
		AS
		(SELECT
			COUNT(job.jobid) as LateCount,
			job.mobileid,
			MAX(mobile.description) as description,
			((SUM((DATEDIFF(minute, dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) / 60) as avgHrs,
			((SUM((DATEDIFF(minute,dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) % 60) as avgMins,
			(
				SELECT COUNT(SubJob.jobid)
				FROM job as SubJob
				join occ on occ.occid = Subjob.latestxmitsource and
				occ.customerid = Subjob.customerid
				OUTER APPLY
				(
					SELECT
						mobile.description,
						mobile.mobileid,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = SubJob.customerid 
						AND mobile.mobileid = SubJob.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = SubJob.customerid 
						AND visitcommon.jobid = SubJob.jobid
				) visitcommon
				WHERE 
					SubJob.customerid = @customer
					AND SubJob.status NOT IN ('R', 'X') 
					AND SubJob.mobileid = job.mobileid
					AND ((SubJob.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (SubJob.status != 'U'))
					AND	(@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(SubJob.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through) OR @dateType = '2' AND (SubJob.latestxmitdtdate BETWEEN @from AND @through))
					-- Grab only the records that are greater than the wtbdtdate and time
					--AND (visitcommon.completiondt >  (dbo.GetUTC(SubJob.duedateutc, occ.timezone, 1))
					-- If the completion date is null then check compared to the clients current date and time
					--OR  (visitcommon.completiondt IS NULL AND @clientDt >  dbo.DateOnly(dbo.GetUTC(SubJob.duedateutc, occ.timezone, 1))))
					AND (@mobile = '0' OR mobile.mobileid = @mobile)
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR SubJob.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR SubJob.isproject = '0')
		
			) as TotalMobileCount
		FROM 
			job
			join occ on occ.customerid = job.customerid and
			occ.occid = job.latestxmitsource
			OUTER APPLY
			(
				SELECT
					mobile.description,
					mobile.mobileid,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X')
				AND ((job.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (job.status != 'U'))
				AND	((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through) OR @dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through)))
					-- Grab only the records that are greater than the wtbdtdate and time
				AND (visitcommon.completiondt >  (dbo.GetUTC(job.duedateutc, occ.timezone, 1))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt >  dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY job.mobileid)
			select * from  summary	ORDER BY mobileid
			IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Detail - By WTB