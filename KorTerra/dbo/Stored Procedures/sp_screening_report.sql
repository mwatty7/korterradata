﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_screening_report] 
	-- Add the parameters for the stored procedure here
	@datefrom date,
	@datethrough date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	create table #summary (
	memberid varchar(30),
	completiondt datetime,
	total_notifications bigint,
	auto_conflicts bigint,
	manual_conflicts bigint,
	auto_clear bigint,
	manual_clear bigint,
	total_voids bigint

	)
	insert into #summary (memberid,  completiondt, total_notifications)
	select memcode.memberid, cast(completiondt as date), count(*) from visitcommon
	join memcode on memcode.membercode = visitcommon.membercode AND memcode.memberid is not null and memcode.memberid <> ''
	where cast(completiondt as date) between @datefrom and @datethrough	
	group by memberid, cast(completiondt as date)

	update #summary set auto_conflicts = (select count(*) from visitcommon join memcode on memcode.membercode = visitcommon.membercode where operatorid = 'ContractScreen' and memcode.memberid = #summary.memberid and  cast(completiondt as date) = cast(#summary.completiondt as date) and compstatus = 'NOT COMPLETE')

	update #summary set manual_conflicts = (select count(*) from visitcommon join memcode on memcode.membercode = visitcommon.membercode where operatorid <> 'ContractScreen' and memcode.memberid = #summary.memberid and  cast(completiondt as date) = cast(#summary.completiondt as date) and compstatus = 'NOT COMPLETE')

	update #summary set auto_clear = (select count(*) from visitcommon join memcode on memcode.membercode = visitcommon.membercode  where operatorid = 'ContractScreen' and memcode.memberid = #summary.memberid and  cast(completiondt as date) = cast(#summary.completiondt as date) and compstatus = 'CLEARED')

	update #summary set manual_clear = (select count(*) from visitcommon join memcode on memcode.membercode = visitcommon.membercode  where operatorid <> 'ContractScreen' and memcode.memberid = #summary.memberid and  cast(completiondt as date) = cast(#summary.completiondt as date) and compstatus = 'CLEARED')

	update #summary set total_voids = (select count(*) from visitcommon
	join job on 
	job.jobid = visitcommon.jobid
	 where membercode = #summary.memberid and  cast(completiondt as date) = cast(#summary.completiondt as date) and job.status = 'X')

	select * from  #summary order by memberid, cast ( completiondt as date)

	drop table #summary

END