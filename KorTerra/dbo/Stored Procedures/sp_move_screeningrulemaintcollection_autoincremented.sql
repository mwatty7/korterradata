﻿-- =============================================
-- Description:	Move the Screening Rule Maint tables with autoincremented key from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_screeningrulemaintcollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
		DECLARE @sqlPopulateScreeningRules NVARCHAR(MAX);
		IF @includeAudit = 1
		BEGIN
			DECLARE @tableName VARCHAR(50) = 'ScreeningRuleSet';
			DECLARE @expectedCount INT = 5;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @tableName = 'ScreeningRule';
			SET @expectedCount = 6;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @tableName = 'ScreeningRuletype';
			SET @expectedCount = 6;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @tableName = 'ScreeningRuleassignment';
			SET @expectedCount = 6;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @tableName = 'ScreeningRulevalue';
			SET @expectedCount = 2;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
		END
        SET @sqlPopulateScreeningRules = '
			INSERT INTO 
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleSet(
				customerid, 
				[name], 
				version, 
				status, 
				description
			)
			SELECT
				customerid, 
				[name], 
				version, 
				status, 
				description 
			FROM 
				['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleSet 
			WHERE 
				customerid =''' + @customerid + ''';
			CREATE TABLE #screeningRuleLink
			(
				ruleid_old INT,
				ruleid_new INT,
				rulesetid_old INT,
				rulesetid_new INT,
				ruletypeid_old INT,
				ruletypeid_new INT,
				ruletype VARCHAR(32),
				[ruletype_version] INT,
				[ruletype_category] VARCHAR(32),
				[ruletype_desc] VARCHAR(256),
				[ruletypeconfig] VARCHAR(2048),
				[name] VARCHAR(64),
				[version] INT,
				[status] VARCHAR(16),
				seqno INT,
				action VARCHAR(32),
				condition VARCHAR(32),
				mobileid VARCHAR(16),
				customerid VARCHAR(32)
			);
			INSERT INTO #screeningRuleLink (
				ruleid_old,
				rulesetid_old,
				ruletypeid_old,
				ruletype,
				[ruletype_version],
				[ruletype_category],
				[ruletype_desc],
				[ruletypeconfig],
				[name],
				version,
				status,
				seqno,
				ACTION,
				condition,
				mobileid,
				customerid
			)
			SELECT DISTINCT
				srS.ruleid,
				srS.rulesetid,
				srS.ruletypeid,
				srtS.ruletype,
				srtS.version,
				srtS.category,
				srtS.description,
				srtS.ruletypeconfig,
				srsS.name,
				srsS.version,
				srsS.status,
				srS.seqno,
				srS.action,
				srS.condition,
				srS.mobileid,
				srsS.customerid
			FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRule srS
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleset srsS ON srsS.rulesetid = srS.rulesetid
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuletype srtS ON srtS.ruletypeid = srS.ruletypeid
			WHERE 
				srsS.customerid = '''+@customerId +''';
			UPDATE 
				#screeningRuleLink
			SET 
				rulesetid_new = a.rulesetid
			FROM 
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleSet a 
			JOIN 
				#screeningRuleLink b on a.customerid = b.customerid
				AND a.[name] = b.[name]
				AND a.version = b.version
				AND a.status = b.status
			UPDATE 
				#screeningRuleLink
			SET 
				ruletypeid_new = a.ruletypeid
			FROM 
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuletype a 
			JOIN 
				#screeningRuleLink b on a.ruletype = b.ruletype
				AND a.[category] = b.[ruletype_category]
				AND a.version = b.[ruletype_version]
				AND a.description = b.[ruletype_desc]
				AND a.ruletypeconfig = b.[ruletypeconfig]
			--ScreeningRuleAssignment
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleassignment
				SELECT DISTINCT 
					b.rulesetid_new, 
					a.customerid, 
					a.memberid, 
					a.assignmentstartdt, 
					a.assignmentenddt, 
					a.status
				FROM 
					['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleassignment a 
				JOIN 
					#screeningRuleLink b on a.rulesetid = b.rulesetid_old
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule (
				rulesetid, 
				ruletypeid, 
				seqno, 
				action, 
				condition, 
				mobileid
			)
			SELECT 
				a.rulesetid_new, 
				a.ruletypeid_new, 
				a.seqno, 
				a.action, 
				a.condition, 
				a.mobileid 
			FROM 
				#screeningRuleLink a 
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleset b on a.rulesetid_new = b.rulesetid
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuletype c on a.ruletypeid_new = c.ruletypeid 
			UPDATE 
				#screeningRuleLink
			SET 
				ruleid_new = a.ruleid
			FROM 
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule a 
			JOIN 
				#screeningRuleLink b on a.rulesetid = b.rulesetid_new
				AND a.ruletypeid = b.ruletypeid_new
				AND a.seqno = b.seqno
			CREATE TABLE #srValueLink
			(
				rulevalueid_old INT,
				ruleid_old INT,
				ruleid_new INT,
				[value] VARCHAR(2048),
				customerid VARCHAR(32)
			)
			INSERT INTO #srValueLink (
				rulevalueid_old, 
				ruleid_old, 
				ruleid_new, 
				[value], 
				customerid
			)
			SELECT 
				a.rulevalueid, 
				a.ruleid, 
				c.ruleid, 
				a.value, 
				b.customerid
			FROM 
				['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRulevalue a
				JOIN #screeningRuleLink b on a.ruleid = b.ruleid_old
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule c on b.ruleid_new = c.ruleid
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRulevalue (
				ruleid, 
				value
			)
			SELECT 
				b.ruleid, 
				a.value 
			FROM 
				#srValueLink a 
			JOIN 
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule b on a.ruleid_new = b.ruleid
			DROP TABLE #srValueLink, #screeningRuleLink
		'
        EXECUTE @return = sp_executesql @sqlPopulateScreeningRules OUTPUT
        IF @return <> 0 BEGIN
            SET @errMsg = @sqlPopulateScreeningRules + 'FAILED!'
            RAISERROR (@errMsg, 15, 10);
        END
		IF @includeAudit = 1
			EXEC sp_audit_move_screeningrulemaintcollection_autoincremented @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''Screening Rule Maint'' FAILED Stored Procedure: ''sp_move_screeningrulemaintcollection_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END