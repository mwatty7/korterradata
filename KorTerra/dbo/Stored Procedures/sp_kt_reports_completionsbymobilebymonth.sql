﻿
CREATE PROCEDURE [dbo].[sp_kt_reports_completionsbymobilebymonth]
	@customerId varchar(32),
	@startdate date,
	@enddate date,
	@bymobile bit
AS
BEGIN
declare 
@edatetime as datetime = CAST(@enddate as datetime) + cast('23:59:59' as datetime);
if @bymobile = 0
BEGIN
	select m.regionid as reg, m.districtid  as dist , vc.membercode, 
		 operatorid = 
	CASE WHEN o.description is null 
		THEN o.operatorid 
		ELSE  o.operatorid  + ' - ' + o.description
	 END,
	vc.compstatus, DATEPART(yy, vc.completiondt) as year, DATEPART(mm, vc.completiondt) as monthnum,  DATENAME(month, vc.completiondt) as monthname , count(*) as completions
	 from visitcommon vc 
	join mobile m on
	m.customerid = vc.customerid and 
	m.mobileid = vc.mobileid
	join district d on 
	d.customerid = m.customerid and 
	d.districtid = m.districtid and
	d.regionid = m.regionid
	join region r on
	r.customerid = m.customerid and
	r.regionid = m.regionid
	join operator o on
	o.operatorid = vc.operatorid
	where vc.customerid = @customerId and vc.completiondt between @startdate and @edatetime 
	group by m.regionid, m.districtid, vc.membercode, o.operatorid, o.description,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
	order by m.regionid, m.districtid,  vc.membercode,  o.operatorid, o.description,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
END
ELSE 
BEGIN
	select m.regionid as reg, m.districtid  as dist , vc.membercode,
	 mobileid = 
		CASE WHEN m.description is null 
		THEN m.mobileid 
		ELSE  m.mobileid  + ' - ' + m.description
	 END, vc.compstatus, DATEPART(yy, vc.completiondt) as year, DATEPART(mm, vc.completiondt) as monthnum,  DATENAME(month, vc.completiondt) as monthname , count(*) as completions
	 from visitcommon vc 
	join mobile m on
	m.customerid = vc.customerid and 
	m.mobileid = vc.mobileid
	join district d on 
	d.customerid = m.customerid and 
	d.districtid = m.districtid and
	d.regionid = m.regionid
	join region r on
	r.customerid = m.customerid and
	r.regionid = m.regionid
	where vc.customerid = @customerId and vc.completiondt between @startdate and @edatetime 
	group by m.regionid, m.districtid, vc.membercode, CASE WHEN m.description is null 
		THEN m.mobileid 
		ELSE  m.mobileid  + ' - ' + m.description
	 END,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
	order by m.regionid, m.districtid,  vc.membercode, CASE WHEN m.description is null 
		THEN m.mobileid 
		ELSE  m.mobileid  + ' - ' + m.description
	 END,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
END
END