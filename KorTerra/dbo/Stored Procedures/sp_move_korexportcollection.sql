﻿-- =============================================
-- Description:	Move the KorExport Collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_korexportcollection]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_addmembers', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_completion', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_comprem', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_extjob', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_locpoints', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_order', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_remarks', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_ktable] @tableName = 'exp_ticket', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'mdsisends', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''KorExCollection'' FAILED Stored Procedure: ''sp_move_korexportcollection''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END