﻿-- =============================================
-- Description:	Move the Maintenance Tables for ADLS
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_adlsmaintcollection]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		EXEC [dbo].[sp_move_table] @tableName = 'archsettings', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'area', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'areapoint', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'areatypes', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'autosendrule', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'batch', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'company', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'comppref', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'customactcode', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'customfaccode', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'custsearchdtl', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'custsearchhdr', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'custwindowfld', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'defaultvisit', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'district', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'duedaterules', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'emailtrigger', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'emergencycontact', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'emergencygroup', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'extjobcol', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'formvalidation', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'groupoperator', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'heraldmessenger', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'holiday', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'jobidsuffix', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'member', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'membzone', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'memcode', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'memcodemap', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'menu', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'mobdest', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'mobile', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'mobilenotification', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'nlrcmpny', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'nlrdone4', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'occ', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'occattribute', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'occpriority', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'operator', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'opermember', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'opertype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit; 
		EXEC [dbo].[sp_move_table] @tableName = 'preferences', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'price', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prioritymap', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prsmemcode', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prsonecalls', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prsreasonlist', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prsregion', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'prssetup', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rdsecurity', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'reasonlist', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'reason', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'reasonoccpriority', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'region', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'requesttypetable', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rptdetail', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rptheader', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rptsched', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rptsched_print', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'rpttemplate', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'security', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'securitygroup', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'servicearea', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'statcols', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'suffixrequesttypes', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ticketflag', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ticketpriority', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ticketupdate', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'updatetype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'userinfo', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'vmrequesttype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'woworktype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'zone', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'zoneinterface', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'AAwidgetAvailable', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'AAwidgetInUse', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'customreport', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'customreportsecurity', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_firstalertmaint_autoincremented] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Message Customer / Message Detail Here
		EXEC [dbo].[sp_move_publicawarenesscollection_autoincremented] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'reasontemplate', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_screeningrulemaintcollection_autoincremented] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'tplformat', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'userquery', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'requesttype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'savedrpttemplate', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'savedrpttemplateline', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ticketentrytype', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''ADLS Maintenance Collection'' FAILED Stored Procedure: ''sp_move_adlsmaintcollection''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END