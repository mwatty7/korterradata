﻿
CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymembercode_totals2]
	@customerId varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@statusCounts bit = 0,
	@lateMobile bit = 0,
	@runTotals bit = 0
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @member varchar(16) = @memberid
DECLARE @memcode varchar(16) = @membercode
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @_lateMobileCounts bit = @lateMobile
DECLARE @_statusCounts bit = @statusCounts
DECLARE @reportTotal bit = @runTotals

BEGIN
/*Locates and Completion Date */
IF @date = 0  and @_statusCounts = 1
BEGIN
	SELECT
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
		JOB
		OUTER APPLY 
		(
			SELECT TOP 1
				request.membercode,
				request.jobid,
				request.firstcompletiondtdate,
				CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid
				) request
				OUTER APPLY
				(
					SELECT TOP 1
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')  
					and request.firstcompletiondtdate between CAST(@from as date) AND CAST(@through as date)
					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by MEMBER, emergencies and projects
				AND (@member = '0' OR memcode.memberid = @member)
				AND (@memcode = '0' OR memcode.membercode = @memcode)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
	END

/* Locates and WTB Date */
ELSE IF (@date = 1) and @_statusCounts = 1
BEGIN
 SELECT
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
		JOB
		OUTER APPLY 
		(
			SELECT  TOP 1
				request.membercode,
				request.jobid,
				request.firstcompletiondtdate,
				CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid
				) request
			OUTER APPLY
				(
					SELECT  TOP 1
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U') 

					--and job.wtbdtdate between cast(@from as date) AND cast(@through as date)

					AND  job.wtbdtdate between cast(@from as date) AND cast(@through as date)



					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by mobile, district, region, emergencies and projects
				AND (@member = '0' OR memcode.memberid = @member)
				AND (@memcode = '0' OR memcode.membercode = @memcode)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
END
/* Locates and XMit Date */
ELSE IF @date = 2  and @_statusCounts = 1
BEGIN
	SELECT 
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
	JOB
	OUTER APPLY 
	(
		SELECT TOP 1
			request.membercode,
			request.jobid,
			request.firstcompletiondtdate,
			CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
				FROM 
					request
				WHERE 
					request.customerid = job.customerid 
					AND request.jobid = job.jobid
				) request
				OUTER APPLY
				(
					SELECT  TOP 1
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')  
					and job.latestxmitdtdate between @from AND @through
					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			-- Filter critera by mobile, district, region, emergencies and projects
			AND (@member = '0' OR memcode.memberid = @member)
			AND (@memcode = '0' OR memcode.membercode = @memcode)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
END
/* Mobiles and Completion Date */
ELSE IF @date = 0  and @_lateMobileCounts = 1
	BEGIN
	SELECT 
		TOP 5 job.mobileid, COUNT(job.jobid) as totalLate
		FROM
			job
			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				visitcommon.customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			LEFT JOIN memcode on
			visitcommon.membercode = memcode.membercode and
			visitcommon.customerid = memcode.customerid
			WHERE 
				visitcommon.customerid = @customer AND 
				(@member = '0' OR memcode.memberid = @member) AND
				(@memcode = '0' OR memcode.membercode = @memcode)
			GROUP BY visitcommon.customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			-- Make sure we are only returning tickets that are actually late
			AND vc.completiondt between @from and @through
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			--AND (@member = '0' OR memcode.memberid = @member)
			--AND (@memcode = '0' OR memcode.membercode = @memcode)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
		GROUP BY job.mobileid
		ORDER BY totalLate DESC
END
/* Mobile and WTB Date */
ELSE IF @date = 1  and @_lateMobileCounts = 1
	BEGIN
	SELECT 
		TOP 5 job.mobileid, COUNT(job.jobid) as totalLate
		FROM
			job
			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				visitcommon.customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			LEFT JOIN memcode on
			visitcommon.membercode = memcode.membercode and
			visitcommon.customerid = memcode.customerid
			WHERE 
				visitcommon.customerid = @customer AND 
				(@member = '0' OR memcode.memberid = @member) AND
				(@memcode = '0' OR memcode.membercode = @memcode) 
				
			GROUP BY visitcommon.customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			AND job.wtbdtdate between @from and @through
			-- Make sure we are only returning tickets that are actually late
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			--AND (@member = '0' OR memcode.memberid = @member)
			--AND (@memcode = '0' OR memcode.membercode = @memcode)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
		GROUP BY job.mobileid
		ORDER BY totalLate DESC
END
/* Mobile and XMit Date */
ELSE IF @date = 2 and @_lateMobileCounts = 1
	BEGIN
	SELECT 
		TOP 5 job.mobileid, COUNT(job.jobid) as totalLate
		FROM
			job
			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				visitcommon.customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			LEFT JOIN memcode on
			visitcommon.membercode = memcode.membercode and
			visitcommon.customerid = memcode.customerid
			WHERE 
				visitcommon.customerid = @customer AND 
				(@member = '0' OR memcode.memberid = @member) AND
				(@memcode = '0' OR memcode.membercode = @memcode) 
				
			GROUP BY visitcommon.customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			AND job.latestxmitdtdate between @from and @through
			-- Make sure we are only returning tickets that are actually late
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			--AND (@member = '0' OR memcode.memberid = @member)
			--AND (@memcode = '0' OR memcode.membercode = @memcode)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
		GROUP BY job.mobileid
		ORDER BY totalLate DESC
END
ELSE IF @date = 0 and @reportTotal = 1 
BEGIN
	SELECT
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
		JOB
		OUTER APPLY 
		(
			SELECT TOP 1
				request.membercode,
				request.jobid,
				request.firstcompletiondtdate,
				CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid 
						--AND request.firstcompletiondtdate between  CAST(@from as date) and CAST(@through as date)
				) request
				OUTER APPLY
				(
					SELECT TOP 1 
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')  
					and request.firstcompletiondtdate between @from AND @through
					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by MEMBER, emergencies and projects
				AND (@member = '0' OR memcode.memberid = @member)
				AND (@memcode = '0' OR memcode.membercode = @memcode)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')

END
ELSE IF @date = 1 and @reportTotal = 1 
BEGIN
	SELECT
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
		JOB
		OUTER APPLY 
		(
			SELECT TOP 1
				request.membercode,
				request.jobid,
				request.firstcompletiondtdate,
				CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid 
						--AND request.firstcompletiondtdate between  CAST(@from as date) and CAST(@through as date)
				) request
				OUTER APPLY
				(
					SELECT TOP 1 
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')  
					and job.wtbdtdate between @from AND @through
					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by MEMBER, emergencies and projects
				AND (@member = '0' OR memcode.memberid = @member)
				AND (@memcode = '0' OR memcode.membercode = @memcode)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')

END
ELSE IF @date = 2 and @reportTotal = 1 
BEGIN
	SELECT
	SUM(IIF(job.status = 'C', 1, 0)) as CompletedLate,
	SUM(IIF(job.status <> 'C', 1, 0)) as OpenLate,
	COUNT(*) as grandTotal
	FROM 
		JOB
		OUTER APPLY 
		(
			SELECT TOP 1
				request.membercode,
				request.jobid,
				request.firstcompletiondtdate,
				CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid 
						--AND request.firstcompletiondtdate between  CAST(@from as date) and CAST(@through as date)
				) request
				OUTER APPLY
				(
					SELECT TOP 1 
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')  
					and job.latestxmitdtdate between @from AND @through
					AND (request.firstcompletiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by MEMBER, emergencies and projects
				AND (@member = '0' OR memcode.memberid = @member)
				AND (@memcode = '0' OR memcode.membercode = @memcode)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')

END

END