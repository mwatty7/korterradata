﻿-- =============================================
-- Description:	Make sure screening maint moved correctly
-- =============================================
CREATE PROCEDURE [dbo].[sp_audit_move_misc_messages_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @errMsg NVARCHAR(MAX);
	DECLARE @tableName VARCHAR(30);
	DECLARE @ParmDefinition nvarchar(500);
	DECLARE @CountSQLQuery varchar(30);
	DECLARE @SQLCommand NVARCHAR(MAX);
	
	SET @tableName = 'Messageactivity';
	EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS messagedetailCount
				FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedetail AS MD
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messageactivity AS MA ON MD.messageid = MA.messageid
			WHERE MA.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS messagedetailCount
				FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.messagedetail AS MD
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.messageactivity AS MA ON MD.messageid = MA.messageid
			WHERE MA.customerid = ''' + @customerId + ''')
					
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: MessageDetail FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'MessageDetail Records Match'
	END
END