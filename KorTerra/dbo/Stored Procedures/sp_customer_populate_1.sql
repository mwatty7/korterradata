﻿
CREATE PROCEDURE [dbo].[sp_customer_populate_1]
    @customerid VARCHAR(32),
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL

AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @STRSQL NVARCHAR(MAX);

    SET @STRSQL = N'

IF NOT EXISTS(SELECT *
FROM sys.servers
WHERE name = ''' + @sourceServer + ''') 
PRINT ''Error! Source Server Must be linked''';

EXECUTE(@STRSQL);

END