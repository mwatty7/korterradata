﻿
CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymembercodev2]
	@customerid varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32),
	@summary bit = 0,
	@detail bit = 1
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @member varchar(16) = @memberid
DECLARE @memcode varchar(16) = @membercode
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_summary bit = @summary
BEGIN
 if @_detail = 1
   BEGIN 
	 if @output = 'detailed'
		WITH detailed (customerid,jobid,mobileid,worktype,Transmit,Wtb,duedate,origpriority,address,street,city,county,state,isproject,status,membercode, memberid, description,lateHours,lateMinutes,firstcompletiondt,CompletedLate,OpenLate,AllLate,operatorid) AS
		(SELECT -- TOP 1000 
			job.customerid,
			job.jobid,
			job.mobileid,
			job.worktype,
			CONVERT(VARCHAR, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
			CONVERT(VARCHAR, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
		    dbo.GetUTC(job.duedateutc, occ.timezone, 1) as duedate,
			job.origpriority,
			job.address, 
			job.street,
			job.city, 
			job.county, 
			job.state,
			job.isproject,
			job.status,
			request.membercode, 
			memcode.memberid,
			memcode.description,
			(DATEDIFF(minute, dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime), @clientDt)) / 60) as lateHours,
			(DATEDIFF(minute, dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime), @clientDt)) % 60) AS lateMinutes,
			CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime) as firstcompletiondt,
			CASE when request.firstcompletiondtdate is not null
				THEN 1
				ELSE 0
			END as CompletedLate,
			CASE when request.firstcompletiondtdate is null
				THEN 1
				ELSE 0
			END as OpenLate,
			1 as AllLate,
			vc.operatorid
		FROM job
			JOIN occ on
			occ.occid = job.latestxmitsource AND
			occ.customerid = job.customerid
			-- Rule I: Tickets with multi member codes, Late ticket will count once in each member code.
			JOIN request on request.customerid = job.customerid AND request.jobid = job.jobid
			JOIN memcode on memcode.customerid = job.customerid AND memcode.membercode = request.membercode
			OUTER APPLY (
				SELECT TOP 1 operatorid FROM visitcommon
				WHERE visitcommon.customerid = job.customerid AND visitcommon.jobid = job.jobid AND visitcommon.membercode = request.membercode AND visitcommon.completiondt = CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime)
			) as vc
		WHERE
			-- Filter by customerid always
			job.customerid = @customerid AND
			-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
			((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through)) OR
			(@dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through))) AND
			-- Rule G: Filter out all cancelled / nlr tickets
			(job.status NOT IN ('R', 'X') AND
			-- Rule C: Filter out updates without completiondt
			(job.status = 'U' AND request.firstcompletiondtdate IS NOT NULL OR job.status != 'U')) AND 
			-- Rule B: Late is always determined by WTB date 
			-- Rule H: If One facility for a membercode is on time, count it as On Time. ONLY LOOK AT FIRST COMPLETION PER MEMBERCODE
			(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime) > dbo.GetUTC(job.duedateutc, occ.timezone, 1) OR
			-- Rule D: Membercodes still open are late if "right now" is late.
			(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime) IS NULL AND @clientDt > dbo.GetUTC(job.duedateutc, occ.timezone, 1)))
			-- These rules are inherited because we are not looking at completion levels
			-- Rule E: Reopened are treated like partials - if one complete was on time, we count it as on time.
			-- Rule F: Partial completes count as complete.
			-- Filter Emergencies and Projects by user criteria
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
			--Filter By membercode and memberid by user criteria
			AND (@member = '0' OR memcode.memberid = @member)
			AND (@memcode = '0' OR memcode.membercode = @memcode))
			select * from detailed;
		
		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
		
END
	 
IF @_summary = 1
	BEGIN
	IF @output = 'summary'
		 WITH summary (LateCount, CompletedLate, OpenLate, avgHrs,avgMins, membercode, fulldescription, TotalTicketCount)
		 AS
			(SELECT COUNT(job.jobid) as LateCount,
			SUM(CASE when request.firstcompletiondtdate is not null
				THEN 1
				ELSE 0
			END) as CompletedLate,
			SUM (CASE when request.firstcompletiondtdate is null
				THEN 1
				ELSE 0
			END) as OpenLate,
			((SUM((DATEDIFF(minute, dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime), @clientDt)))) / COUNT(job.jobid)) / 60) as avgHrs,
			((SUM((DATEDIFF(minute, dbo.GetUTC(job.duedateutc, occ.timezone, 1), ISNULL(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime), @clientDt)))) / COUNT(job.jobid)) % 60) as avgMins,
			request.membercode as membercode,
			memcode.description as fulldescription,
			(select count(*) from job jb
				left join request rq on
				rq.jobid = jb.jobid and
				rq.customerid = jb.customerid
				left join memcode mc on
				rq.membercode = mc.membercode and
				rq.customerid = mc.customerid
				join occ oc on
				oc.customerid = jb.customerid and
				oc.occid = jb.latestxmitsource
				where 
					jb.customerid = @customerid AND
					jb.status NOT IN ('R', 'X') AND
					((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(jb.duedateutc, oc.timezone, 1)) BETWEEN @from AND @through)) OR (@dateType = '2' AND (jb.latestxmitdtdate BETWEEN @from AND @through)))
					AND (@member = '0' OR mc.memberid = @member)
					AND (mc.membercode = request.membercode)
					AND (@emergencies = 'Yes' OR jb.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR jb.isproject = '0')
			) as TotalTicketCount
		FROM job
			JOIN occ on
			occ.occid = job.latestxmitsource AND
			occ.customerid = job.customerid
			-- Rule I: Tickets with multi member codes, Late ticket will count once in each member code.
			JOIN request on request.customerid = job.customerid AND request.jobid = job.jobid
			JOIN memcode on memcode.customerid = job.customerid AND memcode.membercode = request.membercode
		WHERE
			-- Filter by customerid always
			job.customerid = @customerid AND
			-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
			((@dateType = '1' AND (dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) BETWEEN @from AND @through)) OR
			(@dateType = '2' AND (job.latestxmitdtdate BETWEEN @from AND @through))) AND
			-- Rule G: Filter out all cancelled / nlr tickets
			(job.status NOT IN ('R', 'X') AND
			-- Rule C: Filter out updates without completiondt
			(job.status = 'U' AND request.firstcompletiondtdate IS NOT NULL OR job.status != 'U')) AND 
			-- Rule B: Late is always determined by WTB date 
			-- Rule H: If One facility for a membercode is on time, count it as On Time. ONLY LOOK AT FIRST COMPLETION PER MEMBERCODE
			(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime) > dbo.GetUTC(job.duedateutc, occ.timezone, 1) OR
			-- Rule D: Membercodes still open are late if "right now" is late.
			(CONVERT(DATETIME, request.firstcompletiondtdate + request.firstcompletiondttime) IS NULL AND @clientDt > dbo.GetUTC(job.duedateutc, occ.timezone, 1)))
			-- These rules are inherited because we are not looking at completion levels
			-- Rule E: Reopened are treated like partials - if one complete was on time, we count it as on time.
			-- Rule F: Partial completes count as complete.
			-- Filter Emergencies and Projects by user criteria
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
			--Filter By membercode and memberid by user criteria
			AND (@member = '0' OR memcode.memberid = @member)
			AND (@memcode = '0' OR memcode.membercode = @memcode)
		GROUP BY request.membercode, memcode.description)
		select * from summary ORDER BY membercode 
		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
END