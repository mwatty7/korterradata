﻿CREATE PROCEDURE [dbo].[sp_kt_widget_myteamslatetickettrend] 
				@customerId varchar(32), @regionId varchar(32), @districtId varchar(32), @mobileId varchar(max), @timeframe int
AS
BEGIN
	BEGIN
		DECLARE @edate date= GETDATE();
		DECLARE @enddate datetime= CAST(DATEADD(DAY, -1, @edate) AS datetime)+CAST('23:59:59' AS datetime), @startdate date;
		IF @customerId IS NULL
		BEGIN
			SELECT NULL AS [value], NULL AS [date], NULL AS [percent];
			RETURN 0;
		END;
		CREATE TABLE #pastduereport
		( 
					 region varchar(50), district varchar(50), timezone varchar(50), code varchar(50), mobile varchar(80), year int, month_name varchar(15), month_number tinyint, completiondtutc datetime, duedateutc datetime, xmitdate datetime, ontime tinyint, late tinyint,
		);
		/*Get the base data needed for the report*/
		IF @timeframe = 1
		BEGIN
			SET @startdate = CAST(DATEADD(DAY, -6, GETDATE()) AS date);
		END;
		ELSE
		BEGIN
			IF @timeframe = 2
			BEGIN
				SET @startdate = CAST(DATEADD(DAY, -43, GETDATE()) AS date);
			END;
			ELSE
			BEGIN
				SET @startdate = CAST(DATEADD(MONTH, -5, GETDATE()) AS date);
				SET @startdate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @startdate), 0);
			END;
		END;
		BEGIN
			INSERT INTO #pastduereport
				   SELECT r.regionid, d.districtid, o.timezone, req.membercode, j.mobileid, DATEPART(yy, req.xmitdtdate), DATENAME(MONTH, req.xmitdtdate), DATEPART(mm, req.xmitdtdate), req.firstcompletiondtdate + req.firstcompletiondttime AS firstcompletiondate, j.duedateutc, j.latestxmitdtdate, 0, 0
				   FROM request AS req
						LEFT JOIN
						job AS j
						ON req.customerid = j.customerid AND 
						   req.jobid = j.jobid
						LEFT JOIN
						mobile AS mob
						ON j.mobileid = mob.mobileid AND 
						   j.customerid = mob.customerid
						LEFT JOIN
						district AS d
						ON mob.customerid = d.customerid AND 
						   mob.districtid = d.districtid
						LEFT JOIN
						region AS r
						ON d.customerid = r.customerid AND 
						   d.regionid = r.regionid
						LEFT JOIN
						memcode AS mem
						ON req.membercode = mem.membercode AND 
						   req.customerid = mem.customerid
						LEFT JOIN
						occ AS o
						ON mem.occid = o.occid AND 
						   mem.customerid = o.customerid
				   WHERE req.customerid = @customerId AND 
						 req.xmitdtdate BETWEEN @startdate AND @enddate AND 
						 j.status NOT IN( 'X', 'N' ) AND 
						 ( j.status = 'U' AND 
						   req.firstcompletiondtdate IS NOT NULL OR 
						   j.status != 'U'
						 ) AND 
						 ( mob.regionid = @regionId OR 
						   @regionId IS NULL OR 
						   @regionId = ''
						 ) AND 
						 ( mob.districtid = @districtId OR 
						   @districtId IS NULL OR 
						   @districtId = ''
						 ) AND 
						 ( mob.mobileid IN
				   (
					   SELECT Value
					   FROM dbo.fn_Split( @mobileId, ',' )
				   ) OR 
						   @mobileId IS NULL OR 
						   @mobileId = ''
						 );
			UPDATE #pastduereport
			  SET completiondtutc = CASE
									WHEN timezone = 'CENTRAL' THEN DATEADD(hh, 6, completiondtutc)
									WHEN timezone = 'ALASKAN' THEN DATEADD(hh, 9, completiondtutc)
									WHEN timezone = 'EASTERN' THEN DATEADD(hh, 5, completiondtutc)
									WHEN timezone = 'MOUNTAIN' THEN DATEADD(hh, 7, completiondtutc)
									WHEN timezone = 'PACIFIC' THEN DATEADD(hh, 8, completiondtutc)
									ELSE DATEADD(hh, -6, completiondtutc)
									END;
			UPDATE #pastduereport
			  SET ontime = 1
			WHERE completiondtutc IS NOT NULL AND 
				  completiondtutc <= duedateutc OR 
				  completiondtutc IS NULL AND 
				  GETUTCDATE() < duedateutc;
			UPDATE #pastduereport
			  SET late = 1
			WHERE ontime = 0;
			IF @timeframe = 1 OR 
			   @timeframe = 2
			BEGIN
				IF NOT @districtId IS NULL
				BEGIN
					SELECT mobile AS [value],
					--CAST(xmitdate AS VARCHAR(20)) AS [date],
					FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
					FROM #pastduereport
					GROUP BY mobile, FORMAT(xmitdate, 'yyyy-MM')
					ORDER BY FORMAT(xmitdate, 'yyyy-MM');
				END;
				ELSE
				BEGIN
					IF NOT @regionId IS NULL
					BEGIN
						SELECT district AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY district, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM');
					END;
					ELSE
					BEGIN
						SELECT region AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY region, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM')
					END;
				END;
			END;
/* ELSE IF @timeframe = 2
    BEGIN
        IF NOT @districtId IS NULL
        BEGIN
            SELECT mobile AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY mobile,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY mobile,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
        ELSE IF NOT @regionId IS NULL
        BEGIN
            SELECT district AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY district,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY district,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
        ELSE
        BEGIN
            SELECT region AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY region,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY region,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
    END;*/
			ELSE
			BEGIN
				IF NOT @districtId IS NULL OR 
				   @districtId <> ''
				BEGIN
					SELECT mobile AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
					FROM #pastduereport
					GROUP BY mobile, FORMAT(xmitdate, 'yyyy-MM')
					ORDER BY FORMAT(xmitdate, 'yyyy-MM');
				END;
				ELSE
				IF NOT @regionId IS NULL
				BEGIN
					SELECT district AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
					FROM #pastduereport
					GROUP BY district, FORMAT(xmitdate, 'yyyy-MM')
					ORDER BY FORMAT(xmitdate, 'yyyy-MM');
				END;
				ELSE
				BEGIN
					SELECT region AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
					FROM #pastduereport
					GROUP BY region, FORMAT(xmitdate, 'yyyy-MM')
					ORDER BY FORMAT(xmitdate, 'yyyy-MM');
				END;
			END;
		END;
			BEGIN
				INSERT INTO #pastduereport
					   SELECT r.regionid, d.districtid, o.timezone, req.membercode, j.mobileid, DATEPART(yy, req.xmitdtdate), DATENAME(MONTH, req.xmitdtdate), DATEPART(mm, req.xmitdtdate), req.firstcompletiondtdate + req.firstcompletiondttime AS firstcompletiondate, j.duedateutc, j.latestxmitdtdate, 0, 0
					   FROM request AS req
							LEFT JOIN
							job AS j
							ON req.customerid = j.customerid AND 
							   req.jobid = j.jobid
							LEFT JOIN
							mobile AS mob
							ON j.mobileid = mob.mobileid AND 
							   j.customerid = mob.customerid
							LEFT JOIN
							district AS d
							ON mob.customerid = d.customerid AND 
							   mob.districtid = d.districtid
							LEFT JOIN
							region AS r
							ON d.customerid = r.customerid AND 
							   d.regionid = r.regionid
							LEFT JOIN
							memcode AS mem
							ON req.membercode = mem.membercode AND 
							   req.customerid = mem.customerid
							LEFT JOIN
							occ AS o
							ON mem.occid = o.occid AND 
							   mem.customerid = o.customerid
					   WHERE req.customerid = @customerId AND 
							 req.xmitdtdate BETWEEN @startdate AND @enddate AND 
							 j.status NOT IN( 'X', 'N' ) AND 
							 ( j.status = 'U' AND 
							   req.firstcompletiondtdate IS NOT NULL OR 
							   j.status != 'U'
							 ) AND 
							 ( mob.regionid = @regionId OR 
							   @regionId IS NULL OR 
							   @regionId = ''
							 ) AND 
							 ( mob.districtid = @districtId OR 
							   @districtId IS NULL OR 
							   @districtId = ''
							 ) AND 
							 ( mob.mobileid IN
					   (
						   SELECT Value
						   FROM dbo.fn_Split( @mobileId, ',' )
					   ) OR 
							   @mobileId IS NULL OR 
							   @mobileId = ''
							 );
				UPDATE #pastduereport
				  SET completiondtutc = CASE
										WHEN timezone = 'CENTRAL' THEN DATEADD(hh, 6, completiondtutc)
										WHEN timezone = 'ALASKAN' THEN DATEADD(hh, 9, completiondtutc)
										WHEN timezone = 'EASTERN' THEN DATEADD(hh, 5, completiondtutc)
										WHEN timezone = 'MOUNTAIN' THEN DATEADD(hh, 7, completiondtutc)
										WHEN timezone = 'PACIFIC' THEN DATEADD(hh, 8, completiondtutc)
										ELSE DATEADD(hh, -6, completiondtutc)
										END;
				UPDATE #pastduereport
				  SET ontime = 1
				WHERE completiondtutc IS NOT NULL AND 
					  completiondtutc <= duedateutc OR 
					  completiondtutc IS NULL AND 
					  GETUTCDATE() < duedateutc;
				UPDATE #pastduereport
				  SET late = 1
				WHERE ontime = 0;
				IF @timeframe = 1 OR 
				   @timeframe = 2
				BEGIN
					IF NOT @districtId IS NULL
					BEGIN
						SELECT mobile AS [value],
						--CAST(xmitdate AS VARCHAR(20)) AS [date],
						FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY mobile, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM');
					END;
					ELSE
					BEGIN
						IF NOT @regionId IS NULL
						BEGIN
							SELECT district AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
							FROM #pastduereport
							GROUP BY district, FORMAT(xmitdate, 'yyyy-MM')
							ORDER BY FORMAT(xmitdate, 'yyyy-MM');
						END;
						ELSE
						BEGIN
							SELECT region AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
							FROM #pastduereport
							GROUP BY region, FORMAT(xmitdate, 'yyyy-MM')
							ORDER BY FORMAT(xmitdate, 'yyyy-MM')
						END;
					END;
				END;
/* ELSE IF @timeframe = 2
    BEGIN
        IF NOT @districtId IS NULL
        BEGIN
            SELECT mobile AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY mobile,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY mobile,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
        ELSE IF NOT @regionId IS NULL
        BEGIN
            SELECT district AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY district,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY district,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
        ELSE
        BEGIN
            SELECT region AS [value],
                   DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0) AS [date],
                   100.00 * SUM(late) / SUM(ontime + late) AS [percent]
            FROM #pastduereport
            GROUP BY region,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0)
            ORDER BY region,
                     DATEADD(ww, DATEDIFF(ww, 0, CAST(xmitdate AS DATE)), 0);
        END;
    END;*/
				ELSE
				BEGIN
					IF NOT @districtId IS NULL OR 
					   @districtId <> ''
					BEGIN
						SELECT mobile AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY mobile, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM');
					END;
					ELSE
					IF NOT @regionId IS NULL
					BEGIN
						SELECT district AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY district, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM');
					END;
					ELSE
					BEGIN
						SELECT region AS [value], FORMAT(xmitdate, 'yyyy-MM') AS [date], CAST(ROUND(100.00 * SUM(late) / SUM(ontime + late), 2) AS numeric(36, 2)) AS [percent]
						FROM #pastduereport
						GROUP BY region, FORMAT(xmitdate, 'yyyy-MM')
						ORDER BY FORMAT(xmitdate, 'yyyy-MM');
					END;
				END;
			END;
		END;
		SELECT *
		FROM #pastduereport
		WHERE late = 1
		ORDER BY ontime, mobile, xmitdate;
		SELECT *
		FROM #pastduereport
		WHERE late = 0
		ORDER BY ontime, mobile, xmitdate;
		DROP TABLE #pastduereport;
	END;
	SET ANSI_NULLS ON;