﻿-- =============================================
-- Description:	Move the XX DB Maint Collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_xx_dbmaintcollection]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMSG NVARCHAR(MAX); 
		DECLARE @ParmDefinition nvarchar(500);
		DECLARE @CountSQLQuery varchar(30);
		EXEC [dbo].[sp_move_table] @tableName = 'xxconfig', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'xxpreference', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'xxsessionduration', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'xxqueuehistory', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'XXemaildata', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		---XXemailhistory
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'XXemailhistory', @sourceServer, @sourceDatabase, 6
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.XXemailhistory 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.XXemailhistory eh WHERE eh.queueid IN 
				(SELECT queueid FROM XXemaildata ed WHERE ed.customerid = '''+@customerId+''') 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''XXDBMaintCollection'' FAILED Stored Procedure: ''sp_move_xx_dbmaintcollection''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
/**XXemailhistory audit record count**/
		SET @SQLCommand = N'
			SELECT @result = ABS(
				(SELECT 
					COUNT(*) 
				FROM 
					['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXemailhistory AS XXh
					WHERE customerid = ''' + @customerId + ''') -
				(SELECT 
					COUNT(*) 
				FROM 
					['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.XXemailhistory AS XXh
				WHERE customerid = ''' + @customerId + ''')
								
			)';
		SET @ParmDefinition = N'@result varchar(30) OUTPUT';
			EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
			SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
		IF @CountSQLQuery <> 0
		BEGIN
			PRINT @SQLCommand + ' FAILED!'
			SET @errMsg = 'TABLE: xxemailhistory FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
			RAISERROR (@errMsg, 15, 10)
		END
		ELSE IF @CountSQLQuery = 0
		BEGIN 
			PRINT 'xxemailhistory Records Match'
		END	
	
END