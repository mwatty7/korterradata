﻿CREATE PROCEDURE [dbo].[sp_AustinWater_completions] 
	@customerid varchar(32),
	@membercode varchar(16)
AS
BEGIN
	CREATE TABLE #AW1visit
	(
		customerid varchar(32) NOT NULL,
		membercode varchar(16) NOT NULL,
		jobid varchar(16) NOT NULL,
		requesttype varchar(9) NOT NULL,
		creationdt datetime2 NOT NULL,
		operatorid varchar(16) NULL,
		mobileid varchar(16) NULL,
		completiondt datetime2 NULL,
		reasonid varchar(32) NULL,
		travelstarttime varchar(32) NULL,
		ticketstarttime varchar(32) NULL,
		totaltraveltime varchar(32) NULL,
		totaltickettime varchar(32) NULL,
		mileage varchar(16) NULL,
		vehicleid varchar(64) NULL,
		wastewater varchar(64) NULL,
		wastewateraction varchar(64) NULL,
		wastewatermanhole varchar(32) NULL,
		wastewatereolcleanouts varchar(32) NULL,
		wastewaterhouseconnection varchar(32) NULL,
		wastewatergravitymains varchar(32) NULL,
		gravitymainssize1 varchar(32) NULL,
		gravitymainssize2 varchar(32) NULL,
		gravitymainssize3 varchar(32) NULL,
		gravitymainssize4 varchar(32) NULL,
		gravitymainssize5 varchar(32) NULL,
		gravitymainssize6 varchar(32) NULL,
		wastewaterforcemains varchar(32) NULL,
		forcemainssize1 varchar(32) NULL,
		forcemainssize2 varchar(32) NULL,
		forcemainssize3 varchar(32) NULL,
		forcemainssize4 varchar(32) NULL,
		forcemainssize5 varchar(32) NULL,
		forcemainssize6 varchar(32) NULL,
		water varchar(64) NULL,
		wateraction varchar(64) NULL,
		waterhydrants varchar(32) NULL,
		waterarv varchar(32) NULL,
		watervalves varchar(32) NULL,
		waterprv varchar(32) NULL,
		watermains varchar(32) NULL,
		watermainssize1 varchar(32) NULL,
		watermainssize2 varchar(32) NULL,
		watermainssize3 varchar(32) NULL,
		watermainssize4 varchar(32) NULL,
		watermainssize5 varchar(32) NULL,
		watermainssize6 varchar(32) NULL,
		waterservicelines varchar(32) NULL,
		reclaimed varchar(64) NULL,
		reclaimedaction varchar(64) NULL,
		reclaimedservicelines varchar(32) NULL,
		reclaimedarv varchar(32) NULL,
		reclaimedmains varchar(32) NULL,
		reclaimedmainssize1 varchar(32) NULL,
		reclaimedmainssize2 varchar(32) NULL,
		reclaimedmainssize3 varchar(32) NULL,
		reclaimedmainssize4 varchar(32) NULL,
		reclaimedmainssize5 varchar(32) NULL,
		reclaimedmainssize6 varchar(32) NULL,
		reclaimedvalves varchar(32) NULL, 
		remarks nvarchar(max) NULL
	);
	DECLARE @twoweeksago DATETIME2;
	SELECT @twoweeksago = DATEADD(day, -14, SYSDATETIME())
	----------------- Prepare the where clause -----------------------
	DECLARE @Where NVARCHAR(MAX) 
	SET @Where = ' WHERE customerid = ''' + @customerid + ''''
	SET @Where = @Where + ' AND requesttype = ''AUSTINW1'''
	
	If( @membercode <> 'ALL' AND @membercode <> '' AND @membercode <> ' ')
		SET @Where = @Where + ' AND membercode = ''' + @membercode + ''''
	SET @Where = @Where + ' AND creationdt >= ''' + CONVERT(VARCHAR(MAX), @twoweeksago, 121) + ''''
	----------------- Insert the visitcommon portion -----------------
	DECLARE @InsertStmt NVARCHAR(MAX) 
	SET @InsertStmt = 'INSERT INTO #AW1visit'
	SET @InsertStmt = @InsertStmt + 
		' SELECT DISTINCT customerid, membercode, jobid, requesttype, creationdt, operatorid, mobileid, completiondt, reasonid,
		NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL,
		NULL, NULL, NULL, NULL, NULL, NULL,
		remarks
		FROM visitcommon'
		
	SET @InsertStmt = @InsertStmt + @Where
	Execute SP_ExecuteSQL @InsertStmt
	DECLARE @UpdateStmt NVARCHAR(MAX)
	
	----------------- Update the visitdata travelstarttime (travel start time) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET travelstarttime = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''travelstarttime'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata ticketstarttime (ticket start time) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET ticketstarttime = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''ticketstarttime'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata totaltraveltime (total travel time) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET totaltraveltime = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''totaltraveltime'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata totaltickettime (total ticket time) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET totaltickettime = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''totaltickettime'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata mileage (mileage) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET mileage = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''mileage'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata vehicleid (vehicleid) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET vehicleid = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''vehicleid'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata workperformed1 (wastewater) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewater = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''workperformed1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata actcode1 (action of wastewater) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewateraction = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''actcode1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata wastewatermanhole (Manhole)) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewatermanhole = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''wastewatermanhole'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata wastewatereolcleanouts (EOL Cleanouts) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewatereolcleanouts = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''wastewatereolcleanouts'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata wastewaterhouseconnection (House Connection) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewaterhouseconnection = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''wastewaterhouseconnection'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata wastewatergravitymains (Gravity Mains) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewatergravitymains = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''wastewatergravitymains'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize1 (Gravity Main Size 1) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize1 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize2 (Gravity Main Size 2) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize2 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize3 (Gravity Main Size 3) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize3 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize4 (Gravity Main Size 4) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize4 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize4'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize5 (Gravity Main Size 5) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize5 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize5'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata gravitymainssize6 (Gravity Main Size 6) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET gravitymainssize6 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''gravitymainssize6'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata wastewaterforcemains (Force Mains) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wastewaterforcemains = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''wastewaterforcemains'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize1 (Force Main Size 1) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize1 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize2 (Force Main Size 2) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize2 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize3 (Force Main Size 3) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize3 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize4 (Force Main Size 4) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize4 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize4'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize5 (Force Main Size 5) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize5 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize5'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata forcemainssize6 (Force Main Size 6) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET forcemainssize6 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''forcemainssize6'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata workperformed2 (water) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET water = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''workperformed2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata actcode2 (action of water) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET wateraction = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''actcode2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata waterhydrants (Hydrants) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET waterhydrants = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''waterhydrants'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata waterarv (ARV) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET waterarv = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''waterarv'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watervalves (Valves) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watervalves = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watervalves'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata waterprv (PRV) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET waterprv = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''waterprv'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermains (Mains) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermains = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermains'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize1 (Water Main Size 1) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize1 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize2 (Water Main Size 2) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize2 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize3 (Water Main Size 3) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize3 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize4 (Water Main Size 4) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize4 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize4'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize5 (Water Main Size 5) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize5 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize5'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata watermainssize6 (Water Main Size 6) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET watermainssize6 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''watermainssize6'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata waterservicelines (Service Lines) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET waterservicelines = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''waterservicelines'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata workperformed3 (reclaimed) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimed = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''workperformed3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata actcode3 (action of reclaimed) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedaction = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''actcode3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedservicelines (Service Lines) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedservicelines = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedservicelines'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata reclaimedarv (ARV) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedarv = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedarv'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Update the visitdata reclaimedmains (Mains) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmains = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmains'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize1 (Reclaimed Main Size 1) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize1 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize1'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize2 (Reclaimed Main Size 2) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize2 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize2'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize3 (Reclaimed Main Size 3) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize3 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize3'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize4 (Reclaimed Main Size 4) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize4 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize4'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize5 (Reclaimed Main Size 5) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize5 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize5'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedmainssize6 (Reclaimed Main Size 6) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedmainssize6 = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedmainssize6'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	
	----------------- Update the visitdata reclaimedvalves (Valves) portion -----------------
	SET @UpdateStmt = 'UPDATE #AW1visit SET reclaimedvalves = visitdata.value FROM visitdata'
	SET @UpdateStmt = @UpdateStmt + ' WHERE #AW1visit.customerid = visitdata.customerid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.membercode = visitdata.membercode'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.jobid = visitdata.jobid'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.requesttype = visitdata.requesttype'
	SET @UpdateStmt = @UpdateStmt + ' AND #AW1visit.creationdt = visitdata.creationdt'
	SET @UpdateStmt = @UpdateStmt + ' AND visitdata.fieldid = ''reclaimedvalves'''
	
	Execute SP_ExecuteSQL @UpdateStmt
	----------------- Get the final result ------------------
	SELECT customerid, membercode, jobid, requesttype, creationdt,
		operatorid, mobileid, completiondt, reasonid AS 'response',
		travelstarttime AS 'Travel Start Time',
		ticketstarttime AS 'Ticket Start Time',
		totaltraveltime AS 'Total Travel Time',
		totaltickettime AS 'Total Ticket Time',
		mileage, vehicleid,
		wastewater AS 'Wastewater',
		wastewateraction AS 'Action of Wastewater',
		wastewatermanhole AS 'Manhole (WMOSMHLOC)',
		wastewatereolcleanouts AS 'EOL Cleanouts (WMOSCOLOC)',
		wastewaterhouseconnection AS 'House Connection (WMOSSLOC)',
		wastewatergravitymains AS 'Gravity Mains (WMOSMLOC)',
		gravitymainssize1 AS 'Gravity Main Size 1',
		gravitymainssize2 AS 'Gravity Main Size 2',
		gravitymainssize3 AS 'Gravity Main Size 3',
		gravitymainssize4 AS 'Gravity Main Size 4',
		gravitymainssize5 AS 'Gravity Main Size 5',
		gravitymainssize6 AS 'Gravity Main Size 6',
		wastewaterforcemains AS 'Force Mains (WMOFMLOC)',
		forcemainssize1 AS 'Force Main Size 1',
		forcemainssize2 AS 'Force Main Size 2',
		forcemainssize3 AS 'Force Main Size 3',
		forcemainssize4 AS 'Force Main Size 4',
		forcemainssize5 AS 'Force Main Size 5',
		forcemainssize6 AS 'Force Main Size 6',
		water AS 'Water',
		wateraction AS 'Action of Water',
		waterhydrants AS 'Hydrants (WMOFHYLOC)',
		waterarv AS 'ARV (WMOARVLOC-W)',
		watervalves AS 'Valves (WMOVLVLOC-W)',
		waterprv AS 'PRV (WMOPRVLOC)',
		watermains AS 'Mains (WMOWMLOC)',
		watermainssize1 AS 'Water Main Size 1',
		watermainssize2 AS 'Water Main Size 2',
		watermainssize3 AS 'Water Main Size 3',
		watermainssize4 AS 'Water Main Size 4',
		watermainssize5 AS 'Water Main Size 5',
		watermainssize6 AS 'Water Main Size 6',
		waterservicelines AS 'Service Lines (WMOWSLOC-W)',
		reclaimed AS 'Reclaimed',
		reclaimedaction AS 'Action of Reclaimed',
		reclaimedservicelines AS 'Service Lines (WMOWSLOC-R)',
		reclaimedarv AS 'ARV (WMOARVLOC-R)',
		reclaimedmains AS 'Mains (WMORCLWM)',
		reclaimedmainssize1 AS 'Reclaimed Main Size 1',
		reclaimedmainssize2 AS 'Reclaimed Main Size 2',
		reclaimedmainssize3 AS 'Reclaimed Main Size 3',
		reclaimedmainssize4 AS 'Reclaimed Main Size 4',
		reclaimedmainssize5 AS 'Reclaimed Main Size 5',
		reclaimedmainssize6 AS 'Reclaimed Main Size 6',
		reclaimedvalves AS 'Valves (WMOVLVLOC-R)',
		remarks
		FROM #AW1visit
		WHERE (wastewateraction LIKE '%PAINT%' OR wateraction LIKE '%PAINT%' OR reclaimedaction LIKE '%PAINT%')
		ORDER BY customerid, membercode, jobid, creationdt
END