﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbyoperatorid3]
	@customer varchar(32),
	@region varchar(16) = 'DEFAULT',
	@district varchar(16) = 'DEFAULT',
	@mobile varchar(32),
	@date varchar(16) = '1',
	@from varchar(30),
	@through varchar(30),
	@emergencies varchar(16) = 'no',
	@projects varchar(16) = 'no',
	@clientDt datetime,
	@output varchar(32) = 'summary',
	@_detail bit = 0,
	@_statusCounts bit = 0,
	@_lateMobileCounts bit = 0,
	@_grandTotal bit = 0,
	@_summary bit = 0
AS
-- WARNING: STORED PROCEDURE QUERY SPOOFING!!!


-- DECLARE @customer varchar(32) = @customerId
-- DECLARE @region varchar(16) = @regionid
-- DECLARE @district varchar(16) = @districtid
-- DECLARE @mobile varchar(32) = @mobileid
-- DECLARE @date varchar(16) = @dateType
-- DECLARE @from varchar(30) = @fromDate
-- DECLARE @through varchar(30) = @throughDate
-- DECLARE @emergencies varchar(16) = @includeEmergencies
-- DECLARE @projects varchar(16) = @includeProjects
-- DECLARE @client datetime = @clientDt
-- DECLARE @output varchar(32) = @outputOptions
-- DECLARE @_detail bit = @detail
-- DECLARE @_statusCounts bit = @statusCounts
-- DECLARE @_lateMobileCounts bit = @lateMobileCounts
-- DECLARE @_grandTotal bit = @grandTotal
-- DECLARE @_summary bit = @summary
-- -- Detail Report - By CompletionDt
IF @_detail = 1 AND @date = 0
	BEGIN
		IF @output = 'detailed'
			SELECT TOP 1000
				job.customerid,
				mobile.regionid,
				mobile.districtid,
				job.sendto,
				job.mobileid,
				mobile.description,
				job.jobid,
				job.worktype,
				CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
				vc.completiondt,
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vc.completiondt) / 60) as lateHours,
				(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vc.completiondt) % 60) AS lateMinutes,
				CASE 
					WHEN vc.completiondt IS NULL then 'Open / Late'
					WHEN vc.completiondt IS NOT NULL then 'Complete / Late'
				END AS completionStatus
			FROM
				job
			LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
			JOIN (
				-- Get the first completion date/time for each jobid in the given date range and join that with job
				SELECT 
					customerid,
					MIN(completiondt) as completiondt,
					jobid
				FROM 
					visitcommon
				WHERE 
					customerid = @customer AND
					visitcommon.completiondt BETWEEN @from AND @through
				GROUP BY customerid, jobid
			) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
			WHERE
				job.customerid = @customer
				-- Filter out all cancelled / nlr tickets
				AND job.status NOT IN ('R', 'X', 'U')
				-- Make sure we are only returning tickets that are actually late
				AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- Filter critera by mobile, district, region, emergencies and projects
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
		
		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Locates Completion Status Counts - By CompletionDt
IF @_statusCounts = 1 AND @date = 0
	BEGIN
		SELECT
			COUNT(
				CASE
					WHEN vc.completiondt IS null THEN 1
				END
			) as openLateCount,
			COUNT(
				CASE
					WHEN vc.completiondt IS NOT null THEN 1
				END
			) as lateCount,
			COUNT(job.jobid) as grandTotal
		FROM
			job
		LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
		JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			WHERE 
				customerid = @customer AND
				visitcommon.completiondt BETWEEN @from AND @through
			GROUP BY customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			-- Make sure we are only returning tickets that are actually late
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			-- Filter critera by mobile, district, region, emergencies and projects
			AND (@mobile = '0' OR mobile.mobileid = @mobile)
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
	END
-- Late Ticket - Top 5 Late Mobiles Count - By Completion Dt
IF @_lateMobileCounts = 1 AND @date = 0
	BEGIN
		SELECT 
			TOP 5 job.mobileid, COUNT(job.jobid) as totalLate
		FROM
			job
		LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
		JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			WHERE 
				customerid = @customer AND
				visitcommon.completiondt BETWEEN @from AND @through
			GROUP BY customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			-- Make sure we are only returning tickets that are actually late
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			-- Filter critera by mobile, district, region, emergencies and projects
			AND (@mobile = '0' OR mobile.mobileid = @mobile)
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
		GROUP BY job.mobileid
		ORDER BY totalLate DESC
	END
-- Late Ticket Summary - By Completion Dt
IF @_summary = 1 AND @date = 0
	BEGIN
	IF @output = 'summary'
		SELECT
			COUNT(job.jobid) as LateCount,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vc.completiondt))) / COUNT(job.jobid)) / 60) as avgHrs,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), vc.completiondt))) / COUNT(job.jobid)) % 60) as avgMins,
			job.mobileid,
			MAX(mobile.[description]) as [description]
		FROM
			job
		LEFT JOIN mobile on mobile.customerid = job.customerid AND mobile.mobileid = job.mobileid
		JOIN (
			-- Get the first completion date/time for each jobid in the given date range and join that with job
			SELECT 
				customerid,
				MIN(completiondt) as completiondt,
				jobid
			FROM 
				visitcommon
			WHERE 
				customerid = @customer AND
				visitcommon.completiondt BETWEEN @from AND @through
			GROUP BY customerid, jobid
		) as vc on job.customerid = vc.customerid AND job.jobid = vc.jobid
		WHERE
			job.customerid = @customer
			-- Filter out all cancelled / nlr tickets
			AND job.status NOT IN ('R', 'X', 'U')
			-- Make sure we are only returning tickets that are actually late
			AND (vc.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
			-- Filter critera by mobile, district, region, emergencies and projects
			AND (@mobile = '0' OR mobile.mobileid = @mobile)
			AND (@district = '0' OR mobile.districtid = @district)
			AND (@region = '0' OR mobile.regionid = @region)
			AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
			AND (@projects = 'Yes' OR job.isproject = '0')
		GROUP BY job.mobileid
		ORDER BY job.mobileid
		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Detail - By WTB
IF @_detail = 1 AND @date = 1
	BEGIN
		IF @output = 'detailed'
		SELECT TOP 1000
			job.customerid,
			mobile.regionid,
			mobile.districtid,
			job.sendto,
			job.mobileid,
			mobile.description,
			job.jobid,
			job.worktype,
			CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
			visitcommon.completiondt,
			job.origpriority,
			job.address, 
			job.street,
			job.city, 
			job.county, 
			job.state,
			job.isproject,
			job.status,
			(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)) / 60) as lateHours,
			(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)) % 60) AS lateMinutes,
			CASE 
				WHEN visitcommon.completiondt IS NULL then 'Open / Late'
				WHEN visitcommon.completiondt IS NOT NULL then 'Completed / Late'
			END AS completionStatus
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				-- Run Search Against WTB
				AND (CAST(job.wtbdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.wtbdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			ORDER BY job.jobid
			IF @output = 'summary'
				SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Mobile Counts - By WTB
IF @_lateMobileCounts = 1 AND @date = 1
	BEGIN
		SELECT TOP 5 mobile.mobileid, COUNT(jobid) as totalLate
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				-- Run Search Against WTB
				AND (CAST(job.wtbdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.wtbdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
	
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY mobile.mobileid
			ORDER BY totalLate DESC
	END
-- Late Ticket @_statusCounts - By WTB
IF @_statusCounts = 1 AND @date = 1
	BEGIN
		SELECT
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS null THEN 1
				END
			) as openLateCount,
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS NOT null THEN 1
				END
			) as lateCount,
			COUNT(job.jobid) as grandTotal
			FROM 
				job
				OUTER APPLY
				(
					SELECT
						mobile.mobileid,
						mobile.description,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = job.customerid 
						AND mobile.mobileid = job.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = job.customerid 
						AND visitcommon.jobid = job.jobid
				) visitcommon
				WHERE 
					job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')
					-- Run Search Against WTB
					AND (CAST(job.wtbdtdate as date) >= convert(varchar(30), @from, 101))
					AND (CAST(job.wtbdtdate as date) <= convert(varchar(30), @through, 101))
					-- Grab only the records that are greater than the wtbdtdate and time
					AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
					-- If the completion date is null then check compared to the clients current date and time
					OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
					AND (@mobile = '0' OR mobile.mobileid = @mobile)
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR job.isproject = '0')
	END
-- Late Ticket Summary By - WTB Date
IF @_summary = 1 AND @date = 1
	BEGIN
		IF @output = 'summary'
		SELECT
			COUNT(job.jobid) as LateCount,
			job.mobileid,
			MAX(mobile.description) as description,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) / 60) as avgHrs,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) % 60) as avgMins,
			(
				SELECT COUNT(SubJob.jobid)
				FROM job as SubJob
				OUTER APPLY
				(
					SELECT
						mobile.description,
						mobile.mobileid,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = SubJob.customerid 
						AND mobile.mobileid = SubJob.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = SubJob.customerid 
						AND visitcommon.jobid = SubJob.jobid
				) visitcommon
				WHERE 
					SubJob.customerid = @customer
					AND SubJob.status NOT IN ('R', 'X', 'U')
					AND SubJob.mobileid = job.mobileid
					AND (CAST(SubJob.wtbdtdate as date) >= convert(varchar(30), @from, 101))
					AND (CAST(SubJob.wtbdtdate as date) <= convert(varchar(30), @through, 101))
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR SubJob.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR SubJob.isproject = '0')
			) as TotalMobileCount
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.description,
					mobile.mobileid,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				AND (CAST(job.wtbdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.wtbdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY job.mobileid
			ORDER BY job.mobileid
			IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Detail - By XMIT
IF @_detail = 1 AND @date = 2
	BEGIN
		IF @output = 'detailed'
		SELECT TOP 1000
			job.customerid,
			mobile.regionid,
			mobile.districtid,
			job.sendto,
			job.mobileid,
			mobile.description,
			job.jobid,
			job.worktype,
			CONVERT(datetime, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
			visitcommon.completiondt,
			job.origpriority,
			job.address, 
			job.street,
			job.city, 
			job.county, 
			job.state,
			job.isproject,
			job.status,
			(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)) / 60) as lateHours,
			(DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)) % 60) AS lateMinutes,
			CASE 
				WHEN visitcommon.completiondt IS NULL then 'Open / Late'
				WHEN visitcommon.completiondt IS NOT NULL then 'Completed / Late'
			END AS completionStatus
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				-- Run Search Against WTB
				AND (CAST(job.latestxmitdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.latestxmitdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			ORDER BY job.jobid
		IF @output = 'summary'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END
-- Late Ticket Mobile Counts - By XMIT
IF @_lateMobileCounts = 1 AND @date = 2
	BEGIN
		SELECT TOP 5 mobile.mobileid, COUNT(jobid) as totalLate
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.mobileid,
					mobile.description,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				-- Run Search Against WTB
				AND (CAST(job.latestxmitdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.latestxmitdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
	
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY mobile.mobileid
			ORDER BY totalLate DESC
	END
-- Late Ticket @_statusCounts - By XMIT
IF @_statusCounts = 1 AND @date = 2
	BEGIN
		SELECT
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS null THEN 1
				END
			) as openLateCount,
			COUNT(
				CASE
					WHEN visitcommon.completiondt IS NOT null THEN 1
				END
			) as lateCount,
			COUNT(job.jobid) as grandTotal
			FROM 
				job
				OUTER APPLY
				(
					SELECT
						mobile.mobileid,
						mobile.description,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = job.customerid 
						AND mobile.mobileid = job.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = job.customerid 
						AND visitcommon.jobid = job.jobid
				) visitcommon
				WHERE 
					job.customerid = @customer
					AND job.status NOT IN ('R', 'X', 'U')
					-- Run Search Against WTB
					AND (CAST(job.latestxmitdtdate as date) >= convert(varchar(30), @from, 101))
					AND (CAST(job.latestxmitdtdate as date) <= convert(varchar(30), @through, 101))
					-- Grab only the records that are greater than the wtbdtdate and time
					AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
					-- If the completion date is null then check compared to the clients current date and time
					OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
					AND (@mobile = '0' OR mobile.mobileid = @mobile)
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR job.isproject = '0')
	END
-- Late Ticket Summary By - XMIT
IF @_summary = 1 AND @date = 2
	BEGIN
		IF @output = 'summary'
		SELECT
			COUNT(job.jobid) as LateCount,
			job.mobileid,
			MAX(mobile.description) as description,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) / 60) as avgHrs,
			((SUM((DATEDIFF(minute, (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)), ISNULL(visitcommon.completiondt, @clientDt)))) / COUNT(job.jobid)) % 60) as avgMins,
			(
				SELECT COUNT(SubJob.jobid)
				FROM job as SubJob
				OUTER APPLY
				(
					SELECT
						mobile.description,
						mobile.mobileid,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = SubJob.customerid 
						AND mobile.mobileid = SubJob.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = SubJob.customerid 
						AND visitcommon.jobid = SubJob.jobid
				) visitcommon
				WHERE 
					SubJob.customerid = @customer
					AND SubJob.status NOT IN ('R', 'X', 'U')
					AND SubJob.mobileid = job.mobileid
					AND (CAST(SubJob.latestxmitdtdate as date) >= convert(varchar(30), @from, 101))
					AND (CAST(SubJob.latestxmitdtdate as date) <= convert(varchar(30), @through, 101))
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR SubJob.priority <> 'EMERGENCY')
					AND (@projects = 'Yes' OR SubJob.isproject = '0')
			) as TotalMobileCount
		FROM 
			job
			OUTER APPLY
			(
				SELECT
					mobile.description,
					mobile.mobileid,
					mobile.regionid,
					mobile.districtid
				FROM 
					mobile
				WHERE 
					mobile.customerid = job.customerid 
					AND mobile.mobileid = job.mobileid
			) mobile
			OUTER APPLY
			(
				SELECT
					MIN(completiondt) as completiondt
				FROM 
					visitcommon
				WHERE 
					visitcommon.customerid = job.customerid 
					AND visitcommon.jobid = job.jobid
			) visitcommon
			WHERE 
				job.customerid = @customer
				AND job.status NOT IN ('R', 'X', 'U')
				AND (CAST(job.latestxmitdtdate as date) >= convert(varchar(30), @from, 101))
				AND (CAST(job.latestxmitdtdate as date) <= convert(varchar(30), @through, 101))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND ((visitcommon.completiondt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime)))
				-- If the completion date is null then check compared to the clients current date and time
				OR  (visitcommon.completiondt IS NULL AND @clientDt > (CAST(job.wtbdtdate AS datetime) + CAST(job.wtbdttime AS datetime))))
				AND (@mobile = '0' OR mobile.mobileid = @mobile)
				AND (@district = '0' OR mobile.districtid = @district)
				AND (@region = '0' OR mobile.regionid = @region)
				AND (@emergencies = 'Yes' OR job.priority <> 'EMERGENCY')
				AND (@projects = 'Yes' OR job.isproject = '0')
			GROUP BY job.mobileid
			ORDER BY job.mobileid
		IF @output = 'detailed'
			SELECT TOP 0 jobid FROM job WHERE customerid = @customer
	END