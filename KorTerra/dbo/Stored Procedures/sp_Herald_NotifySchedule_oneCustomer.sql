﻿
CREATE PROCEDURE [dbo].[sp_Herald_NotifySchedule_oneCustomer]
	@customerid VARCHAR(32)
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID('#table1') IS NOT NULL DROP TABLE #table1;
	IF OBJECT_ID('#table2') IS NOT NULL DROP TABLE #table2;

	CREATE TABLE #table1 (days VARCHAR(32), starttime VARCHAR(12), endtime VARCHAR(12), rownum INT IDENTITY(1, 1));
	INSERT INTO #table1 (days, starttime, endtime)
		SELECT DISTINCT days, SUBSTRING(starttime, 0, 6), SUBSTRING(endtime, 0, 6) FROM emailnotify 
		WHERE customerid = @customerid ORDER BY days, SUBSTRING(starttime, 0, 6), SUBSTRING(endtime, 0, 6);

	DECLARE @curr INT;

	PRINT N'*** Inserting CustomerScheduleTemplates for customer ' + @customerid + ' ***';
	SET @curr = (SELECT COUNT(*) FROM CustomerScheduleTemplates WHERE customerid = @customerid);
	IF @curr > 0
	BEGIN
		PRINT N'*** CustomerScheduleTemplates data is already set up for customer [' + @customerid + '], skip.';
		RETURN 1
	END
	ELSE IF @curr = 0
	BEGIN
		DECLARE @days VARCHAR(32)
		DECLARE @starttime VARCHAR(12)
		DECLARE @endtime VARCHAR(12)
		DECLARE @rownum INT

		DECLARE cursor1 CURSOR FOR
			SELECT days, starttime, endtime, rownum FROM #table1 ORDER BY days, starttime, endtime
		OPEN cursor1;

		FETCH NEXT FROM cursor1 INTO @days, @starttime, @endtime, @rownum;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @days = '1;2;3;4;5;6;7'
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						'EVERYDAY' + ' ' +  SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
			END
			ELSE IF @days = '2;3;4;5;6'
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						'MTWRF' + ' ' +  SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
			END
			ELSE IF @days = '2;3;4;5'
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						'MTWR' + ' ' +  SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
			END
			ELSE IF @days = '2;3;4'
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						'MTW' + ' ' +  SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
			END
			ELSE IF @days = '1;7'
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						'WEEKEND' + ' ' +  SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
			END
			ELSE
			BEGIN
				INSERT INTO CustomerScheduleTemplates (customerid, name)
				VALUES(@customerid, 
						@customerid + '_' + CAST(@rownum AS VARCHAR(16)) + ' ' + SUBSTRING(@starttime, 0, 6) + ' - ' + SUBSTRING(@endtime, 0, 6));
						
			END
			FETCH NEXT FROM cursor1 INTO @days, @starttime, @endtime, @rownum;
		END

		CLOSE cursor1;
		DEALLOCATE cursor1;
	END

	CREATE TABLE #table2 (cst_id INT, rownum INT IDENTITY(1, 1));
	INSERT INTO #table2 (cst_id) SELECT cst_id FROM CustomerScheduleTemplates WHERE customerid = @customerid ORDER BY cst_id;

	PRINT N'*** Inserting ScheduleTemplateTimes for customer ' + @customerid + ' ***';
	SET @curr = (SELECT COUNT(*) FROM ScheduleTemplateTimes WHERE cst_id IN 
					(SELECT cst_id FROM CustomerScheduleTemplates WHERE customerid = @customerid AND name LIKE @customerid + '%'));
	IF @curr = 0
	BEGIN
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '1', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('1', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '2', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('2', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '3', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('3', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '4', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('4', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '5', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('5', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '6', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('6', t1.days) > 0
		;
		INSERT INTO ScheduleTemplateTimes (cst_id, day, starttime, endtime)
			SELECT DISTINCT t2.cst_id, '7', t1.starttime, t1.endtime 
				FROM #table2 t2 INNER JOIN #table1 t1 ON t1.rownum = t2.rownum
			WHERE CHARINDEX('7', t1.days) > 0
		;
	END

	PRINT N'*** Inserting HeraldNotifyGroup for customer ' + @customerid + ' ***';
	SET @curr = (SELECT COUNT(*) FROM HeraldNotifyGroup WHERE customerid = @customerid);
	IF @curr = 0
	BEGIN
		INSERT INTO HeraldNotifyGroup (customerid, msggroupid, msgtype)
			SELECT DISTINCT e.customerid, e.msggroupid, e.msgtype FROM emailnotify e
		WHERE e.customerid = @customerid
		;
	END

	PRINT N'*** Inserting HeraldNotifyScheduleTemplate for customer ' + @customerid + ' ***';
	SET @curr = (SELECT COUNT(*) FROM HeraldNotifyScheduleTemplate WHERE cst_id IN (SELECT cst_id FROM CustomerScheduleTemplates WHERE customerid = @customerid));
	IF @curr = 0
	BEGIN
		INSERT INTO HeraldNotifyScheduleTemplate (hng_id, cst_id, tplfilename)
			SELECT DISTINCT g.hng_id, c.cst_id, LOWER(e.tplfilename) 
			FROM HeraldNotifyGroup g
				INNER JOIN CustomerScheduleTemplates c ON c.customerid = g.customerid AND c.customerid = @customerid
				INNER JOIN emailnotify e ON e.customerid = g.customerid AND e.customerid = c.customerid AND e.customerid = @customerid AND 
												e.msggroupid = g.msggroupid AND e.msgtype = g.msgtype
				INNER JOIN ScheduleTemplateTimes s ON s.cst_id = c.cst_id AND 
												SUBSTRING(s.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(s.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												CHARINDEX(s.day, e.days) > 0
				INNER JOIN #table2 t2 ON t2.cst_id = s.cst_id AND t2.cst_id = c.cst_id
				INNER JOIN #table1 t1 ON t1.rownum = t2.rownum AND 
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												t1.days = e.days AND
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(s.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(s.endtime, 0, 6) AND 
												CHARINDEX(s.day, t1.days) > 0
		;
	END

	PRINT N'*** Updating TplFilesForNotification configurator for customer ' + @customerid + ' ***';
	BEGIN
		DECLARE @onefile VARCHAR(255)
		DECLARE @tplfiles VARCHAR(max)

		SET @tplfiles = 'standard_emergency.tpl';

		DECLARE cursor2 CURSOR FOR
			SELECT DISTINCT LOWER(tplfilename) FROM emailnotify WHERE customerid = @customerid
		OPEN cursor2;

		FETCH NEXT FROM cursor2 INTO @onefile;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF LEN(@onefile) > 0 AND CHARINDEX(@onefile, @tplfiles) <= 0
			BEGIN
				SET @tplfiles += ',';
				SET @tplfiles += @onefile;
			END
			FETCH NEXT FROM cursor2 INTO @onefile;
		END

		CLOSE cursor2;
		DEALLOCATE cursor2;

		IF EXISTS (SELECT * FROM xxconfig WHERE name = 'TplFileNamesForHerald' AND sectionid = 'KTSHARED' AND customerid = @customerid)
		BEGIN
			IF NOT EXISTS (SELECT * FROM xxconfig WHERE name = 'TplFilesForNotification' AND sectionid = 'KTSHARED' AND customerid = @customerid)
			BEGIN
				UPDATE xxconfig SET name = 'TplFilesForNotification' WHERE name = 'TplFileNamesForHerald' AND customerid = @customerid;
			END
			ELSE
			BEGIN
				DELETE FROM xxconfig WHERE name = 'TplFileNamesForHerald'
			END
		END

		IF NOT EXISTS (SELECT * FROM xxconfig WHERE name = 'TplFilesForNotification' AND sectionid = 'KTSHARED' AND customerid = @customerid)
		BEGIN
			INSERT INTO xxconfig (customerid, sectionid, name, type, value) 
			VALUES(@customerid, 'KTSHARED', 'TplFilesForNotification', 'ENUMERATION', @tplfiles);
		END
		ELSE
		BEGIN
			UPDATE xxconfig SET value = @tplfiles 
			WHERE customerid = @customerid AND name = 'TplFilesForNotification' AND sectionid = 'KTSHARED' AND type = 'ENUMERATION'
			AND (value IS NULL OR len(value) = 0);
		END
	END

	PRINT N'*** Inserting HeraldNotifyEmails for customer ' + @customerid + ' ***';
	SET @curr = (SELECT COUNT(*) FROM HeraldNotifyEmails WHERE hnst_id IN (SELECT hnst_id FROM HeraldNotifyScheduleTemplate t WHERE t.cst_id IN
									(SELECT cst_id FROM CustomerScheduleTemplates WHERE customerid = @customerid)));
	IF @curr = 0
	BEGIN
		DECLARE @hnst_id INT;
		DECLARE @email VARCHAR(320);
		DECLARE @file VARCHAR(255);

		DECLARE cursor3 CURSOR FOR
			SELECT DISTINCT t.hnst_id, LOWER(e.emailaddress1), LOWER(e.tplfilename)
			FROM HeraldNotifyGroup g
				INNER JOIN CustomerScheduleTemplates c ON c.customerid = g.customerid AND c.customerid = @customerid
				INNER JOIN HeraldNotifyScheduleTemplate t ON t.hng_id = g.hng_id AND t.cst_id = c.cst_id
				INNER JOIN emailnotify e ON e.customerid = g.customerid AND e.customerid = c.customerid AND e.customerid = @customerid AND 
												e.msggroupid = g.msggroupid AND e.msgtype = g.msgtype AND 
												(LOWER(e.tplfilename) = LOWER(t.tplfilename) OR 
												(e.tplfilename IS NULL AND t.tplfilename IS NULL)) AND
												e.emailaddress1 IS NOT NULL
				INNER JOIN ScheduleTemplateTimes s ON s.cst_id = c.cst_id AND 
												SUBSTRING(s.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(s.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												CHARINDEX(s.day, e.days) > 0
				INNER JOIN #table2 t2 ON t2.cst_id = s.cst_id AND t2.cst_id = c.cst_id AND t2.cst_id = t.cst_id
				INNER JOIN #table1 t1 ON t1.rownum = t2.rownum AND 
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												t1.days = e.days AND
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(s.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(s.endtime, 0, 6) AND 
												CHARINDEX(s.day, t1.days) > 0
				;
		OPEN cursor3;

		FETCH NEXT FROM cursor3 INTO @hnst_id, @email, @file;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF NOT EXISTS (SELECT * FROM HeraldNotifyEmails WHERE hnst_id = @hnst_id AND emailaddress = @email)
			BEGIN
				INSERT INTO HeraldNotifyEmails (hnst_id, emailaddress)
				VALUES(@hnst_id, @email);
			END
			FETCH NEXT FROM cursor3 INTO @hnst_id, @email, @file;
		END
		CLOSE cursor3;

		DECLARE cursor4 CURSOR FOR
			SELECT DISTINCT t.hnst_id, LOWER(e.emailaddress2), LOWER(e.tplfilename)
			FROM HeraldNotifyGroup g
				INNER JOIN CustomerScheduleTemplates c ON c.customerid = g.customerid AND c.customerid = @customerid
				INNER JOIN HeraldNotifyScheduleTemplate t ON t.hng_id = g.hng_id AND t.cst_id = c.cst_id
				INNER JOIN emailnotify e ON e.customerid = g.customerid AND e.customerid = c.customerid AND e.customerid = @customerid AND 
												e.msggroupid = g.msggroupid AND e.msgtype = g.msgtype AND 
												(LOWER(e.tplfilename) = LOWER(t.tplfilename) OR 
												(e.tplfilename IS NULL AND t.tplfilename IS NULL)) AND
												e.emailaddress2 IS NOT NULL
				INNER JOIN ScheduleTemplateTimes s ON s.cst_id = c.cst_id AND 
												SUBSTRING(s.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(s.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												CHARINDEX(s.day, e.days) > 0
				INNER JOIN #table2 t2 ON t2.cst_id = s.cst_id AND t2.cst_id = c.cst_id AND t2.cst_id = t.cst_id
				INNER JOIN #table1 t1 ON t1.rownum = t2.rownum AND 
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												t1.days = e.days AND
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(s.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(s.endtime, 0, 6) AND 
												CHARINDEX(s.day, t1.days) > 0
				;
		OPEN cursor4;

		FETCH NEXT FROM cursor4 INTO @hnst_id, @email, @file;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF NOT EXISTS (SELECT * FROM HeraldNotifyEmails WHERE hnst_id = @hnst_id AND emailaddress = @email)
			BEGIN
				INSERT INTO HeraldNotifyEmails (hnst_id, emailaddress)
				VALUES(@hnst_id, @email);
			END
			FETCH NEXT FROM cursor4 INTO @hnst_id, @email, @file;
		END
		CLOSE cursor4;
		
		DECLARE cursor5 CURSOR FOR
			SELECT DISTINCT t.hnst_id, LOWER(e.emailaddress3), LOWER(e.tplfilename)
			FROM HeraldNotifyGroup g
				INNER JOIN CustomerScheduleTemplates c ON c.customerid = g.customerid AND c.customerid = @customerid
				INNER JOIN HeraldNotifyScheduleTemplate t ON t.hng_id = g.hng_id AND t.cst_id = c.cst_id
				INNER JOIN emailnotify e ON e.customerid = g.customerid AND e.customerid = c.customerid AND e.customerid = @customerid AND 
												e.msggroupid = g.msggroupid AND e.msgtype = g.msgtype AND 
												(LOWER(e.tplfilename) = LOWER(t.tplfilename) OR 
												(e.tplfilename IS NULL AND t.tplfilename IS NULL)) AND
												e.emailaddress3 IS NOT NULL
				INNER JOIN ScheduleTemplateTimes s ON s.cst_id = c.cst_id AND 
												SUBSTRING(s.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(s.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												CHARINDEX(s.day, e.days) > 0
				INNER JOIN #table2 t2 ON t2.cst_id = s.cst_id AND t2.cst_id = c.cst_id AND t2.cst_id = t.cst_id
				INNER JOIN #table1 t1 ON t1.rownum = t2.rownum AND 
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(e.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(e.endtime, 0, 6) AND 
												t1.days = e.days AND
												SUBSTRING(t1.starttime, 0, 6) = SUBSTRING(s.starttime, 0, 6) AND 
												SUBSTRING(t1.endtime, 0, 6) = SUBSTRING(s.endtime, 0, 6) AND 
												CHARINDEX(s.day, t1.days) > 0
				;
		OPEN cursor5;

		FETCH NEXT FROM cursor5 INTO @hnst_id, @email, @file;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF NOT EXISTS (SELECT * FROM HeraldNotifyEmails WHERE hnst_id = @hnst_id AND emailaddress = @email)
			BEGIN
				INSERT INTO HeraldNotifyEmails (hnst_id, emailaddress)
				VALUES(@hnst_id, @email);
			END
			FETCH NEXT FROM cursor5 INTO @hnst_id, @email, @file;
		END
		CLOSE cursor5;

		DEALLOCATE cursor3;
		DEALLOCATE cursor4;
		DEALLOCATE cursor5;
	END

	PRINT N'*** Updating time format to use HH:MM instead of HH:MM:SS (used by the old system) and handle midnight';
	UPDATE ScheduleTemplateTimes SET starttime = '24:00' WHERE starttime LIKE '23:59%';
	UPDATE ScheduleTemplateTimes SET endtime = '24:00' WHERE endtime LIKE '23:59%';
	UPDATE ScheduleTemplateTimes SET starttime = SUBSTRING(starttime, 0, 6) WHERE len(starttime) > 5;
	UPDATE ScheduleTemplateTimes SET endtime = SUBSTRING(endtime, 0, 6) WHERE len(endtime) > 5;
	UPDATE CustomerScheduleTemplates SET name = REPLACE(name, '23:59', '24:00') WHERE CHARINDEX('23:59', name) > 0;

	PRINT N'*** Storing tpl file names in lower cases';
	UPDATE HeraldNotifyScheduleTemplate SET tplfilename = LOWER(tplfilename) WHERE LEN(tplfilename) > 0;
	UPDATE xxconfig SET value = LOWER(value) WHERE name = 'TplFilesForNotification';

END