﻿CREATE PROCEDURE [dbo].[sp_move_ktable] 
	@tableName VARCHAR(50) = NULL,
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @return Int;
    SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.' + @tableName+ ' SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.' + @tableName+ ' WHERE k_customerid = ''' + @customerid + '''';
    EXEC @return = sp_executesql @SQLCommand OUTPUT
    IF @return <> 0 BEGIN
        PRINT @SQLCommand + ' FAILED!'
		DECLARE @errMsg VARCHAR(50) = 'KTABLE: ' + @tableName + ' FAILED TO MOVE!'
        RAISERROR (@errMsg, 15, 10)
    END
	END TRY
	BEGIN CATCH
		PRINT('MOVE TABLE FAILED: ' + @tableName);
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
	IF (@includeAudit = 1)
	BEGIN
		BEGIN TRY
			EXEC sp_audit_moved_ktable @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
		END TRY
		BEGIN CATCH
			PRINT('AUDIT MOVED KTABLE FAILED: ' + @tableName);
			declare @auditError int, @auditMessage varchar(4000), @auditXstate int;
			select @auditError = ERROR_NUMBER(),
			@auditMessage = ERROR_MESSAGE(), 
			@auditXstate = XACT_STATE();
			RAISERROR (@auditMessage, 15, 10);
		END CATCH
	END
END