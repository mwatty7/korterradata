﻿-- Late Tickets By MobileID Continued
CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymobileid]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(32),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeNLR varchar(16),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
			SELECT 
				job.customerid,
				mobile.regionid,
				mobile.districtid,
				job.sendto,
				ISNULL(visitcommon.mobileid, job.mobileid) as mobileid,
				mobile.description,
				job.jobid,
				job.worktype,
				CONVERT(VARCHAR, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(VARCHAR, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
				visitcommon.completiondt,
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status
			FROM 
				job
				OUTER APPLY
				(
					SELECT
						mobile.mobileid,
						mobile.description,
						mobile.regionid,
						mobile.districtid
					FROM 
						mobile
					WHERE 
						mobile.customerid = job.customerid 
						AND mobile.mobileid = job.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT
						mobileid,
						MIN(completiondt) as completiondt
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = job.customerid 
						AND visitcommon.jobid = job.jobid
					GROUP BY mobileid
				) visitcommon
				WHERE 
					job.customerid = ''' + @customerId + '''
	'
	-- Report Options
	If( @includeNLR = 'No')
		Set @Command = @Command + ' AND job.status NOT IN (''R'', ''X'', ''U'')'
	If( @includeEmergencies = 'No')
		Set @Command = @Command + '  AND job.priority <> ''EMERGENCY'''
	if( @includeProjects = 'No' )
		Set @Command = @Command + '  AND job.isproject = 0'
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(visitcommon.completiondt as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(visitcommon.completiondt as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND (CAST(job.wtbdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.wtbdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(job.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	-- Regionid / Districtid / Mobileid
	If( @regionid <> '0' )
	   Set @Command = @Command + ' AND (mobile.regionid = ''' + @regionid + ''') '
	If( @districtid <> '0' )
	   Set @Command = @Command + ' AND (mobile.districtid = ''' + @districtid + ''') '
	If( @mobileid <> '0' )
	   Set @Command = @Command + ' AND (ISNULL(visitcommon.mobileid, job.mobileid) = ''' + @mobileid + ''') '
	-- End Regionid / Districtid / Mobileid
	Set @Command = @Command + ' ORDER BY mobile.regionid, mobile.districtid, job.mobileid, job.jobid, Wtb'
	Execute SP_ExecuteSQL  @Command
END
-- End of Late Tickets By MobileID