﻿
CREATE PROCEDURE [dbo].[sp_kt_reports_latestcompletionbymembercodev2]
	@customerId varchar(32),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
				WITH data (	jobid,
					operatorid,
					operatordescription,
					creationdt,
					customerid,
					membercode,
					membercodedescription,
					company,
					donefor,
					address,
					street,
					city,
					state,
					county,
					Transmit, 
					Wtb, 
					completiondt,
					remarks
				) AS
				(SELECT TOP 10000
					vs1.jobid,
					vs1.operatorid,
					o.description as operatordescription,
					vs2.creationdt,
					vs2.customerid,
					vs2.membercode,
					m.description as membercodedescription,
					j.company,
					j.donefor,
					j.address,
					j.street,
					j.city,
					j.state,
					j.county,
					CONVERT(VARCHAR, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
					CONVERT(VARCHAR, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
					vs1.completiondt,
					vs1.remarks
				FROM visitcommon as vs1
				LEFT OUTER JOIN job j on j.customerid = vs1.customerid AND j.jobid = vs1.jobid
				JOIN occ on 
				occ.occid = j.latestxmitsource AND
				occ.customerid = j.customerid
				LEFT OUTER JOIN operator o on o.customerid = vs1.customerid and o.operatorid = vs1.operatorid
				OUTER APPLY (
					SELECT
						MAX(creationdt) as creationdt,
						customerid,
						membercode,
						jobid
					FROM
						visitcommon
					GROUP BY customerid, membercode, jobid
				) as vs2
				LEFT OUTER JOIN memcode m on m.customerid = vs1.customerid and m.membercode = vs2.membercode
				WHERE vs1.customerid = vs2.customerid
				AND vs1.jobid = vs2.jobid
				AND vs1.creationdt = vs2.creationdt
				AND vs2.customerid = ''' + @customerId + '''
	'
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(vs1.completiondt as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(vs1.completiondt as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND dbo.DateOnly(dbo.GetUTC(duedateutc, occ.timezone, 1)) between ''' + @fromDate  + ''' AND ''' + @throughDate + ''''
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(j.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(j.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	If( @membercode <> '0' )
	   Set @Command = @Command + ' AND (vs2.membercode = ''' + @membercode + ''') '
	-- End Member / Member Code
	Set @Command = @Command + ' ) SELECT * from data ORDER BY customerid,membercode,jobid'
	--select @Command;
	Execute SP_ExecuteSQL  @Command
END