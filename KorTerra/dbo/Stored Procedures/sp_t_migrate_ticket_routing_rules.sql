﻿CREATE PROCEDURE [dbo].[sp_t_migrate_ticket_routing_rules]
    @customerid VARCHAR(32)
  --  @occid VARCHAR(32),
   -- @timeframe INT,
   -- @islatlong INT
AS
--SELECT * FROM t_routingrulepoints
--DELETE FROM t_routingrulepoints
--SELECT * FROM t_routingrulevalue
--DELETE FROM t_routingrulevalue
--SELECT * FROM t_routingruletype
--DELETE FROM t_routingruletype
--SELECT * FROM t_routingruleversion
--DELETE FROM t_routingruleversion
-----------------------------------------------------------------------------------------
/* create a temp routing rule version table based off DISTINCT area records (customerid, occid, suffixid) */
-----------------------------------------------------------------------------------------
--Let's Check if these routing rules for the combo customerid, occid, suffix already exist!
DECLARE @errMsg NVARCHAR(MAX) = NULL;
IF EXISTS (SELECT 'x'  FROM t_routingruleversion
WHERE customerid + '- ' + occid + '- ' + suffixid  IN (SELECT customerid + '- ' + occid + '- ' + suffixid FROM area WHERE customerid = @customerid) )
BEGIN
        SET @errMsg = 'There already exists rule version records for this customer. Please review AND check customer data to migrate';
        RAISERROR (@errMsg, 15, 10) WITH NOWAIT;
		RETURN
    END
BEGIN
-- -- 01. Check for bad data!
-- SELECT * FROM area WHERE customerid+occid+method NOT IN (
-- SELECT customerid+occid+method FROM areatypes 
-- )
-- A. Insert t_routingruleversion with guid and a bunch of dates and createdby, etc. for each: DISTINCT customerid, occid, suffixid in area
-- SELECT DISTINCT customerid, occid, suffixid FROM area
-----------------------------------------------------------------------------------------
SELECT * INTO #aTemp_t_routingruleversion FROM (
	SELECT DISTINCT customerid, occid, suffixid 
	FROM area
	WHERE customerid = @customerid
) AS a
INSERT INTO t_routingruleversion
	(rrversionid, customerid, occid, suffixid, isactive, startingrule, publisheddtutc, createddtutc, createdby, lastmodifiedby, lastmodifieddtutc, routingversionnumber)
	SELECT newid(),
	       customerid, 
	       occid, 
	       suffixid, 
	       1, 
	       NULL, 
	       GETUTCDATE(), 
	       GETUTCDATE(), 
	       'DB SCRIPT', 
	       'DB SCRIPT', 
	       GETUTCDATE(), 
	       1 
	FROM #aTemp_t_routingruleversion
	WHERE customerid = @customerid
-----------------------------------------------------------------------------------------
-- B. Insert t_routingruletype with t_routingruleversion.guid and new guid for each: DISTINCT customerid, occid, suffixid, method in area
-- SELECT DISTINCT customerid, occid, suffixid, method FROM area
-- Count of areatypes should match the unfolding of areatypes for the first 2 unions
-- SELECT * FROM areatypes WHERE isscreening = 0
                                                                  
-- Need to unfold areatypes with field1, field2, field3 into separate rules:                                                                  
SELECT * INTO #bTemp1_t_routingruletype FROM (
	SELECT customerid, occid, method, priority, field1 AS field, field1, field2, field3 
	FROM areatypes
	WHERE method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		AND isscreening = 0
		AND customerid = @customerid
union
	SELECT customerid, occid, method, priority, field1 AS field, field1, NULL AS field2, NULL AS field3 
	FROM areatypes
	WHERE method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		AND isscreening = 0
		AND customerid = @customerid
union
	SELECT customerid, occid, method, priority, field2 AS field, NULL AS field1, field2, NULL AS field3 
	FROM areatypes
	WHERE method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		AND isscreening = 0 
		AND field2 IS NOT NULL 
		AND customerid = @customerid
union
	SELECT customerid, occid, method, priority, field3 AS field, NULL AS field1, NULL AS field2, field3 
	FROM areatypes
	WHERE method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		AND isscreening = 0 
		AND field3 IS NOT NULL
		AND customerid = @customerid
) AS b1
SELECT * INTO #bTemp2_t_routingruletype FROM (
	SELECT DISTINCT customerid, occid, suffixid, method, value
	FROM area
	WHERE 
	customerid = @customerid
		AND 
		method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		and CHARINDEX(';', value) > 0
	UNION
	SELECT DISTINCT customerid, occid, suffixid, method, ''
	FROM area
	WHERE 
	customerid = @customerid
		AND 
		method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
		and CHARINDEX(';', value) = 0
	UNION
	SELECT DISTINCT customerid, occid, suffixid, method, ''
	FROM area
	WHERE 
	customerid = @customerid
		AND 
		method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') 
) AS b2
SELECT * INTO #bTemp3_t_routingruletype FROM (
	SELECT DISTINCT bt1.customerid, bt1.occid, bt2.suffixid, 
	bt2.value, 
	bt1.method, bt1.priority, bt1.field, bt1.field1, bt1.field2, bt1.field3  
	FROM #bTemp1_t_routingruletype bt1,
	     #bTemp2_t_routingruletype bt2
	WHERE bt1.customerid = bt2.customerid
		AND bt1.occid = bt2.occid
		and bt1.method = bt2.method
		and value != ''
UNION
	SELECT DISTINCT bt1.customerid, bt1.occid, bt2.suffixid, 
	bt2.value, 
	bt1.method, bt1.priority, bt1.field, bt1.field1, bt1.field2, bt1.field3  
	FROM #bTemp1_t_routingruletype bt1,
	     #bTemp2_t_routingruletype bt2
	WHERE bt1.customerid = bt2.customerid
		AND bt1.occid = bt2.occid
--		and bt1.method = bt2.method
		and value = ''
		and bt1.method not IN (
			SELECT DISTINCT bt1.method
			FROM #bTemp1_t_routingruletype bt1,
				#bTemp2_t_routingruletype bt2
			WHERE bt1.customerid = bt2.customerid
				AND bt1.occid = bt2.occid
				and bt1.method = bt2.method
				and value != ''
		) 
		and bt2.method not IN (
			SELECT DISTINCT bt1.method
			FROM #bTemp1_t_routingruletype bt1,
				#bTemp2_t_routingruletype bt2
			WHERE bt1.customerid = bt2.customerid
				AND bt1.occid = bt2.occid
				and bt1.method = bt2.method
				and value != ''
		) 
		AND bt1.customerid = @customerid
) AS b3
SELECT * INTO #bTemp4_t_routingruletype FROM (
	SELECT  bt3.customerid, bt3.occid, bt3.suffixid, newid() AS rrtypeid, rrv.rrversionid, CAST(NULL AS uniqueidentifier) AS match, CAST(NULL AS uniqueidentifier) AS nomatch, bt3.method, 
	bt3.value,
	bt3.priority, bt3.field, bt3.field1, bt3.field2, bt3.field3 
	FROM t_routingruleversion rrv,
	     #bTemp3_t_routingruletype bt3
	WHERE rrv.customerid = bt3.customerid
		AND rrv.occid = bt3.occid
		AND rrv.suffixid = bt3.suffixid
		AND rrv.customerid = @customerid
) AS b4
-- set the match field for multifield methods with match pointing to the field2 record
UPDATE bt4a
set match = bt4b.rrtypeid
	FROM #bTemp4_t_routingruletype bt4a,
	     #bTemp4_t_routingruletype bt4b
	WHERE bt4a.rrversionid = bt4b.rrversionid
		AND bt4a.method = bt4b.method
		AND bt4a.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND bt4a.field1 IS NOT NULL AND bt4b.field2 IS NOT NULL
		AND bt4a.value = bt4b.value
		AND bt4a.customerid = @customerid
		AND bt4b.customerid = @customerid
-- set the match field for multifield methods with match pointing to the field3 record
UPDATE bt4a
	set match = bt4b.rrtypeid
	FROM #bTemp4_t_routingruletype bt4a,
	     #bTemp4_t_routingruletype bt4b
	WHERE bt4a.rrversionid = bt4b.rrversionid
		AND bt4a.method = bt4b.method
		AND bt4a.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND bt4a.field2 IS NOT NULL 
		AND bt4b.field3 IS NOT NULL
		AND bt4a.value = bt4b.value
		AND bt4a.customerid = @customerid
		AND bt4b.customerid = @customerid
-- F. Set the match and nomatch values in t_routingruletype        
-- Add row numbers to arbitrarily set the next nomatch 
SELECT * INTO #bTemp6_t_routingruletype FROM (
--select row_number() OVER (order by customerid, occid, suffixid, method, value) as roe, *  from #bTemp4_t_routingruletype bt4a
--where value != '' and field1 != ''
 SELECT row_number() OVER (order by customerid, occid, suffixid, priority) AS roe, *
	FROM #bTemp4_t_routingruletype bt4a
 WHERE
		bt4a.nomatch IS NULL
		AND (bt4a.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') or (
		     bt4a.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')  AND bt4a.field1 IS NOT NULL
		))
		AND bt4a.customerid = @customerid
		--AND bt4a.occid = 'ALOC'
		--and bt4a.suffixid = 'NONE'
		--order by customerid, occid, suffixid, priority
) AS b6
UPDATE bt6a
	set nomatch = bt6b.rrtypeid
	FROM #bTemp6_t_routingruletype bt6a,
	     #bTemp6_t_routingruletype bt6b
	WHERE bt6a.rrversionid = bt6b.rrversionid
		AND bt6a.roe = bt6b.roe - 1
		AND bt6a.customerid = @customerid
		AND bt6b.customerid = @customerid
		AND bt6a.occid = bt6b.occid
		AND bt6a.suffixid = bt6b.suffixid
-- Insert the nomatch values back into #bTemp4_t_routingruletype for multi-field types of the same method
UPDATE bt4
	set nomatch = bt6.nomatch
	FROM #bTemp4_t_routingruletype bt4,
	     #bTemp6_t_routingruletype bt6
	WHERE bt4.rrtypeid = bt6.rrtypeid
		AND bt4.customerid = @customerid
		AND bt6.customerid = @customerid
--SELECT * INTO #bTemp7_t_routingruletype FROM (
-- SELECT *
--	FROM #bTemp4_t_routingruletype bt4a
-- WHERE
--		bt4a.nomatch is null
--		AND (bt4a.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') or (
--		     bt4a.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')  AND bt4a.field1 IS NOT NULL
--		))
--		AND bt4a.customerid = @customerid
--) AS b7
--UPDATE bt7a
--	SET nomatch = bt7b.rrtypeid
--	FROM #bTemp7_t_routingruletype bt7a,
--	     #bTemp7_t_routingruletype bt7b
--	WHERE bt7a.rrversionid = bt7b.rrversionid
--		AND bt7a.priority = bt7b.priority - 1
--		AND bt7a.nomatch is null
--		AND (bt7a.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') or (
--		     bt7a.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')  AND bt7a.field1 IS NOT NULL
--		))
--		AND bt7b.nomatch is null
--		AND (bt7b.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE') or (
--		     bt7b.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')  AND bt7b.field1 IS NOT NULL
--		))
--		AND bt7a.customerid = @customerid
--		AND bt7b.customerid = @customerid
---- Insert the nomatch values back into #bTemp4_t_routingruletype for multi-field types of the different methods
--UPDATE bt4
--	set nomatch = bt7.nomatch
--	FROM #bTemp4_t_routingruletype bt4,
--	     #bTemp7_t_routingruletype bt7
--	WHERE bt4.rrtypeid = bt7.rrtypeid
--		AND bt4.customerid = @customerid
--		AND bt7.customerid = @customerid
-- Need to unfold the values that have multiple fields!
SELECT * INTO #gTemp FROM (
	SELECT customerid, suffixid, zoneid, areanumber, occid, method, value, minx, maxx, miny, maxy,
	CASE WHEN PATINDEX('%;%', value) > 0 THEN SUBSTRING(value, 1, PATINDEX('%;%', value) - 1) ELSE value END AS value1, 
	CASE WHEN PATINDEX('%;%', value) > 0 THEN SUBSTRING(value, PATINDEX('%;%', value) + 1, len(value) - PATINDEX('%;%', value) ) ELSE NULL END AS value2, 
	CASE WHEN PATINDEX('%;%', value) > 0 THEN SUBSTRING(value, PATINDEX('%;%', value) + 1, len(value) - PATINDEX('%;%', value) ) ELSE NULL END AS value3 
	FROM area
	WHERE customerid = @customerid
) AS g
-- Fix up value2 and value3
UPDATE #gTemp 
	set value2 = 
	CASE WHEN PATINDEX('%;%', value2) > 0 THEN SUBSTRING(value2, 1, PATINDEX('%;%', value2) - 1) ELSE value2 END
	, value3 = 
	CASE WHEN PATINDEX('%;%', value2) > 0 THEN SUBSTRING(value3, PATINDEX('%;%', value3) + 1, len(value3) - PATINDEX('%;%', value3) ) ELSE NULL END
	WHERE customerid = @customerid
-- Insert records for 'LATLONG POLYGON' so we can populate t_routingrulepoints
SELECT * INTO #bTemp5_t_routingruletypeLatLong FROM (
	SELECT newid() AS rrvalueid, bt4.rrtypeid, bt4.rrversionid, bt4.customerid, bt4.occid, bt4.suffixid, gt.zoneid, gt.areanumber, gt.method, gt.value, bt4.match, bt4.nomatch, bt4.field 
	FROM #gTemp gt,
	     #bTemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
		AND gt.method = 'LATLONG POLYGON'
		AND gt.customerid = @customerid
) AS tempLatLong
                                                           
-- Insert the records into t_routingruletype
INSERT INTO t_routingruletype
	(rrtypeid, rrversionid, match, nomatch, method, field)
	SELECT rrtypeid, rrversionid, match, nomatch, method, field 
	FROM #bTemp4_t_routingruletype
-------------------
SELECT * INTO #temp_area_areapoint FROM (
	SELECT ap.customerid, a.occid, ap.suffixid, ap.zoneid, ap.areanumber, ap.seqno, ap.x, ap.y, a.method, a.value 
	FROM areapoint ap,
	     area a
	WHERE ap.customerid = a.customerid
		AND ap.suffixid = a.suffixid
		AND ap.zoneid = a.zoneid
		AND ap.areanumber = a.areanumber
		AND a.method = 'LATLONG POLYGON'
		AND ap.customerid = @customerid
) AS aap1
-- C. Insert t_routingrulevalue with t_routingruletype.guid and new guid for each: record in araa
-- SELECT value, zoneid, minx, maxx, miny, maxy FROM area
-- Insert records for 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE'
-- but NOT 'LATLONG POLYGON'
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT newid() AS rrvalueid, bt4.rrtypeid, gt.value, gt.zoneid, gt.minx, gt.maxx, gt.miny, gt.maxy 
	FROM #gTemp gt,
	     #bTemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
		-- AND gt.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.method IN ('MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.customerid = @customerid
-- Insert records for 'LATLONG POLYGON'
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT bt5.rrvalueid, bt5.rrtypeid, gt.value, gt.zoneid, gt.minx, gt.maxx, gt.miny, gt.maxy 
	FROM #gTemp gt,
	     #bTemp5_t_routingruletypeLatLong bt5
	WHERE gt.customerid = bt5.customerid
		AND gt.occid = bt5.occid
		AND gt.suffixid = bt5.suffixid
		AND gt.method = bt5.method
		AND gt.areanumber = bt5.areanumber
		AND gt.zoneid = bt5.zoneid
		-- AND gt.method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.method = 'LATLONG POLYGON'
		AND gt.customerid = @customerid
-- Insert records with single field: value1 / field1
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT newid() AS rrvalueid, bt4.rrtypeid, gt.value1, 
	-- Zones on Multi-Field methods: Zone should not be there until they have all matched
	CASE WHEN bt4.match IS NULL THEN gt.zoneid ELSE NULL END AS zoneid, 
	gt.minx, gt.maxx, gt.miny, gt.maxy
	FROM #gTemp gt,
	     #bTemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
--		AND gt.value = bt4.value 
		AND bt4.value = ''
		AND gt.value1 IS NOT NULL
		AND bt4.field1 IS NOT NULL
		AND gt.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.customerid = @customerid
-- Insert records with multiple fields: value1 / field1
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT newid() AS rrvalueid, bt4.rrtypeid, gt.value1, 
	-- Zones on Multi-Field methods: Zone should not be there until they have all matched
	CASE WHEN bt4.match IS NULL THEN gt.zoneid ELSE NULL END AS zoneid, 
	gt.minx, gt.maxx, gt.miny, gt.maxy
	FROM #gTemp gt,
	     #bTemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
		AND gt.value = bt4.value 
		AND gt.value1 IS NOT NULL
		AND bt4.field1 IS NOT NULL
		AND gt.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.customerid = @customerid
-- Insert records with  value2 / field2
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT newid() AS rrvalueid, bt4.rrtypeid, gt.value2, 
	-- Zones on Multi-Field methods: Zone should not be there until they have all matched
	CASE WHEN bt4.match IS NULL THEN gt.zoneid ELSE NULL END AS zoneid, 
	gt.minx, gt.maxx, gt.miny, gt.maxy
	FROM #gTemp gt,
	     #bTemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
		AND gt.value = bt4.value 
		AND gt.value2 IS NOT NULL
		AND bt4.field2 IS NOT NULL
		AND gt.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.customerid = @customerid
-- Insert Records With  Value3 / Field3
INSERT INTO t_routingrulevalue
	(rrvalueid, rrtypeid, value, zoneid, minx, maxx, miny, maxy)
	SELECT newid() AS rrvalueid, bt4.rrtypeid, gt.value3, 
	-- Zones on Multi-Field methods: Zone should not be there until they have all matched
	CASE WHEN bt4.match IS NULL THEN gt.zoneid ELSE NULL END AS zoneid, 
	gt.minx, gt.maxx, gt.miny, gt.maxy
	FROM #gtemp gt,
	     #Btemp4_t_routingruletype bt4
	WHERE gt.customerid = bt4.customerid
		AND gt.occid = bt4.occid
		AND gt.suffixid = bt4.suffixid
		AND gt.method = bt4.method
		AND gt.value = bt4.value 
		AND gt.value3 IS NOT NULL
		AND bt4.field3 IS NOT NULL
		AND gt.method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND gt.customerid = @customerid
-- D. Insert t_routingrulepoints with t_routingrulevalue.guid and new guid for each: record in areapoint
INSERT INTO t_routingrulepoints
	(rrpointid, rrvalueid, seqno, x, y)
	SELECT newid() AS rrpointid, rrtll.rrvalueid, aap.seqno, aap.x, aap.y
	FROM #temp_area_areapoint aap,
	     #bTemp5_t_routingruletypeLatLong rrtll
	WHERE aap.customerid = rrtll.customerid
		AND aap.occid = rrtll.occid
		AND aap.suffixid = rrtll.suffixid
		AND aap.method = rrtll.method
		AND aap.zoneid = rrtll.zoneid
		AND aap.areanumber = rrtll.areanumber
		AND aap.customerid = @customerid
-- E. Set the startingrule for each t_routingruleversion based on areatypes.priority
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4  
	WHERE rrv.rrversionid = bt4.rrversionid
		AND priority = 1
		AND method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- Incase there was no record with priority 1, try priority 2
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4  
	WHERE rrv.startingrule IS NULL
		AND rrv.rrversionid = bt4.rrversionid
		AND priority = 2
		AND method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- Incase there was no record with priority 2, try priority 3
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4  
	WHERE rrv.startingrule IS NULL
		AND rrv.rrversionid = bt4.rrversionid
		AND priority = 3
		AND method IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- Set the startingrule for field / value matching types
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4 
	WHERE rrv.rrversionid = bt4.rrversionid
		AND priority = 1
		AND method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND bt4.field1 IS NOT NULL
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- Incase there was no record with priority 1, try priority 2
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4 
	WHERE rrv.startingrule IS NULL
		AND rrv.rrversionid = bt4.rrversionid
		AND priority = 2
		AND method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND bt4.field1 IS NOT NULL
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- Incase there was no record with priority 2, try priority 3
UPDATE t_routingruleversion
	set startingrule = bt4.rrtypeid
	FROM t_routingruleversion rrv,
	     #bTemp4_t_routingruletype bt4 
	WHERE rrv.startingrule IS NULL
		AND rrv.rrversionid = bt4.rrversionid
		AND priority = 3
		AND method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
		AND bt4.field1 IS NOT NULL
		AND rrv.customerid = @customerid
		AND bt4.customerid = @customerid
-- G. Set all user defined methods as VALUE
UPDATE t_routingruletype set method = 'VALUE' 
WHERE method NOT IN ('LATLONG POLYGON', 'MAP GRID', 'MAP GRID 2', 'TOWNSHIP RANGE')
--------------------------------------------
-- -- Look for unset startingrules:
-- SELECT * FROM t_routingruleversion WHERE startingrule IS NULL
-- 
-- -- Possible records / priority > 1 that should be the starting rule
-- SELECT customerid, occid, suffixid, min(priority) AS p1 FROM #bTemp4_t_routingruletype bt4  
-- group by customerid, occid, suffixid
-- Having min(priority) > 1
-- order by customerid, occid, suffixid
                                              
END