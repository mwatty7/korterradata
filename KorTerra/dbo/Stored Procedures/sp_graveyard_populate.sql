﻿
/*IF EXISTS (SELECT *
FROM sys.objects
WHERE type = 'P' AND name = 'sp_customer_populate_1')
DROP PROCEDURE sp_customer_populate_1
GO*/

CREATE PROCEDURE [dbo].[sp_graveyard_populate]
   @customerid VARCHAR(32) = NULL,
   @sourceServer VARCHAR(50) = NULL,
   @sourceDatabase VARCHAR(50) = NULL,
   @targetServer VARCHAR(50) = NULL,
   @targetDatabase VARCHAR(50) = NULL

AS
BEGIN
   SET NOCOUNT ON;
   DECLARE @SQLCommand NVARCHAR(MAX);
   DECLARE @return Int;
 --  SET @SQLCommand = '

	--IF NOT EXISTS(SELECT *
	--FROM sys.servers
	--WHERE name = ''' + @sourceServer + ''')
	--PRINT ''Error! Source Server Must be linked''';

 --  EXECUTE(@SQLCommand);
    
END

--BEGIN
--  SET @SQLCommand = '

--    IF NOT EXISTS (SELECT * FROM [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.customer
--   WHERE customerid = '''+@customerid +''')
--    PRINT ''Customer does not exist. Import begins''

--   INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customer
--   SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.customer WHERE customerid = ''' + @customerid + ''''
--   	EXEC @return = sp_executesql @SQLCommand OUTPUT
--	IF @return <> 0 BEGIN 
--		PRINT @SQLCommand + ' FAILED!'
--		RETURN -1
--	END


-- END
BEGIN
	SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachment WHERE customerid = ''' + @customerid + '''';
	EXEC @return = sp_executesql @SQLCommand OUTPUT
	IF @return <> 0 BEGIN 
		PRINT @SQLCommand + ' FAILED!'
		RETURN -1
	END

	SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachmentexif SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachmentexif WHERE customerid = ''' + @customerid + '''';
	EXEC @return = sp_executesql @SQLCommand OUTPUT
	IF @return <> 0 BEGIN 
		PRINT @SQLCommand + ' FAILED!'
		RETURN -1
	END
	
	SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAattachmentsize SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAattachmentsize WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAwidgetAvailable SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAwidgetAvailable WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.AAwidgetInUse SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.AAwidgetInUse WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.actionaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.actionaudit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.activity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.activity WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.addmembers SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.addmembers WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.archsettings SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.archsettings WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.area SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.area WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.areapoint SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.areapoint WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.areatypes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.areatypes WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.attachment WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
          
	--attachmentbackward
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachmentext SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.attachmentext WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.audititm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.audititm WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.auditsend SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.auditsend WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.autodept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.autodept WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.autosendrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.autosendrule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.batch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.batch WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bbillstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bbillstatus WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusbatch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusbatch WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusledger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusledger WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.bonusperiod SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.bonusperiod WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.c5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.c5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.calendar SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.calendar WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.childdept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.childdept WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.citycounty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.citycounty WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.company SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.company WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.comppref SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.comppref WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.contact SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.contact WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.contrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.contrem WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.currentmobloc SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.currentmobloc WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customactcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customactcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	--customer
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customerattribute SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customerattribute WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	--CustomerScheduleTemplates
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customfaccode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customfaccode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customreport SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customreport WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.customreportsecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.customreportsecurity WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custsearchdtl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custsearchdtl WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custsearchhdr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custsearchhdr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.custwindowfld SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.custwindowfld WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.defaultvisit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.defaultvisit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.deptbill SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.deptbill WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.district SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.district WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dptgp SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dptgp WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dptgpmbr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dptgpmbr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.duedaterules SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.duedaterules WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.dupaddr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.dupaddr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e6visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e7visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e8visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e8visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.e9visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.e9visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.editaddr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.editaddr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emailnotify SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emailnotify WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emailtrigger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emailtrigger WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emergencycontact SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emergencycontact WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.emergencygroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.emergencygroup WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_addmembers SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_addmembers WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_completion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_completion WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_comprem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_comprem WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_extjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_extjob WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_locpoints SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_locpoints WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_order SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_order WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_remarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_remarks WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.exp_ticket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.exp_ticket WHERE k_customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.expcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.expcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.expdetl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.expdetl WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.extjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.extjob WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.extjobcol SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.extjobcol WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.f1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.f1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.filteraudititm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.filteraudititm WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
           
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.formvalidation SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.formvalidation WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
           
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g1visit  SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.g6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.g6visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.groupoperator SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.groupoperator WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.herald SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.herald WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.heraldmessenger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.heraldmessenger WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	--HeraldNotifyGroup
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.holiday SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.holiday WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMattachment WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincident SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincident WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMaudit (customerid, incidentnumber, createdbyoperatorid, createddt, description) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, description FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMaudit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMcode (customerid, codetype, codeid, description, dirtcodetype, dirtid, parentid, dirtid2) SELECT customerid, codetype, codeid, description, dirtcodetype, dirtid, parentid, dirtid2 FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMconfig SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMconfig WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMcontractor (customerid, createddt, company, email, phone, altphone, besttimetocall, streetnumber, address1, address2, city, county, state, zip, country, contact, altcontact) SELECT customerid, createddt, company, email, phone, altphone, besttimetocall, streetnumber, address1, address2, city, county, state, zip, country, contact, altcontact FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMcontractor WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMfilter (customerid, operatorid, createdbyoperatorid, createddt, name, query) SELECT customerid, operatorid, createdbyoperatorid, createddt, name, query FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMfilter WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincidentContent 
		(customerid, incidentnumber, revisedby, reviseddt, incidentstatus, priority, incidentassignedto, isvalidticket, ticket, emergencyticket, ticketdue, memberid, membercode, ishighprofile, callername, callerphone, reportedstreetnumber, reportedaddress1, reportedaddress2, 
		reportedcity, reportedcounty, reportedstate, reportedzip, reportedcountry, reportednearinter, reportedlatitude, reportedlongitude, reporteddt, reportednotes, investigationstatus, investigationassignedto, actualstreetnumber, actualaddress1, actualaddress2, actualcity, 
		actualcounty, actualstate, actualzip, actualcountry, actualnearinter, actuallatitude, actuallongitude, descriptionofmarks, ismarkedcorrectly, ismarksvisible, isfacilityabandoned, isproject, isrow, isonecallmember, isjointtrench, isexcavatordowntime, excavatordowntimeamount, 
		contractorid, subcontractorid, damagecause, rootcause, extentofdamage, isdamaged, isserviceinterruption, serviceinterruptionduration, customersaffected, injuries, fatalities, regionid, districtid, investigatorphone, duration, dpsexperience, investigationdt, ticketstreetnumber, 
		ticketaddress1, ticketaddress2, ticketcity, ticketcounty, ticketstate, ticketzip, ticketcountry, ticketnearinter, ticketlatitude, ticketlongitude, facilitytypeid, facilitysizeid, materialid, damagecodeid, damagereasonid, deviceid, devicefrequencyid, excavatortypeid, 
		excavationequipmentid, excavationtypeid, conditionofmarksid, polygonpoints, intrefnum, serviceinterruptioncost, onecallcenterid, iscallcenternotified, rightofwaytypeid, rowtype)
		SELECT 
			customerid, incidentnumber, revisedby, reviseddt, incidentstatus, priority, incidentassignedto, isvalidticket, ticket, emergencyticket, ticketdue, memberid, membercode, ishighprofile, callername, callerphone, reportedstreetnumber, reportedaddress1, reportedaddress2, 
			reportedcity, reportedcounty, reportedstate, reportedzip, reportedcountry, reportednearinter, reportedlatitude, reportedlongitude, reporteddt, reportednotes, investigationstatus, investigationassignedto, actualstreetnumber, actualaddress1, actualaddress2, actualcity, 
			actualcounty, actualstate, actualzip, actualcountry, actualnearinter, actuallatitude, actuallongitude, descriptionofmarks, ismarkedcorrectly, ismarksvisible, isfacilityabandoned, isproject, isrow, isonecallmember, isjointtrench, isexcavatordowntime, excavatordowntimeamount, 
			contractorid, subcontractorid, damagecause, rootcause, extentofdamage, isdamaged, isserviceinterruption, serviceinterruptionduration, customersaffected, injuries, fatalities, regionid, districtid, investigatorphone, duration, dpsexperience, investigationdt, ticketstreetnumber, 
			ticketaddress1, ticketaddress2, ticketcity, ticketcounty, ticketstate, ticketzip, ticketcountry, ticketnearinter, ticketlatitude, ticketlongitude, facilitytypeid, facilitysizeid, materialid, damagecodeid, damagereasonid, deviceid, devicefrequencyid, excavatortypeid, 
			excavationequipmentid, excavationtypeid, conditionofmarksid, polygonpoints, intrefnum, serviceinterruptioncost, onecallcenterid, iscallcenternotified, rightofwaytypeid, rowtype
		FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincidentContent WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMincidentContributingFactor SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMincidentContributingFactor WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMinvestigationremark (customerid, incidentnumber, createdbyoperatorid, createddt, description) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, description FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMinvestigationremark WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMpayableclaim (customerid, incidentnumber, createdbyoperatorid, createddt, amount, payableclaimstatus) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, amount, payableclaimstatus FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMpayableclaim WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMreceivableclaim (customerid, incidentnumber, createdbyoperatorid, createddt, amount, receivableclaimstatus) SELECT customerid, incidentnumber, createdbyoperatorid, createddt, amount, receivableclaimstatus FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMreceivableclaim WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMsequence SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMsequence WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMtracelog (applicationid, customerid, operatorid, logdt, type, message, component, stacktrace, loginid, controllerid, url) SELECT applicationid, customerid, operatorid, logdt, type, message, component, stacktrace, loginid, controllerid, url FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMtracelog WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.IMwitness (customerid, incidentnumber, firstname, lastname, type, createddt, phone, altphone, statement) SELECT customerid, incidentnumber, firstname, lastname, type, createddt, phone, altphone, statement FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.IMwitness WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.invoice SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.invoice WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.invrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.invrule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.j_sortedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.j_sortedjob WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.job SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.job WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobidsuffix SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobidsuffix WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.joblock SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.joblock WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsend SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsend WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsendcompatible SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsendcompatible WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobsort SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobsort WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ledger SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ledger WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ledgrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ledgrem WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.locpoints SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.locpoints WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.manrequest SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.manrequest WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	--mapannotation moved in SSIS package
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.masterdept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.masterdept WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mdsisends SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mdsisends WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.member SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.member WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.membrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.membrem WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.membzone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.membzone WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.memcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.memcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.memcodemap SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.memcodemap WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.menu SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.menu WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
       
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
       
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobdest SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobdest WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobile SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobile WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobilenotification SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobilenotification WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mobstat SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.mobstat WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msg SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msg WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgdata SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msgdata WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	--msggroup   
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlrcmpny SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlrcmpny WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.nlrdone4 SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.nlrdone4 WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.notes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.notes WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occ SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occ WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occattribute SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occattribute WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.occpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.occpriority WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.operator SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.operator WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.opermember SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.opermember WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.opertype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.opertype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p6visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p7visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.p8visit  SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.p8visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	--PAcontact
	--PAmaint
	--PAtemplate
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.paymastr SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.paymastr WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.payrate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.payrate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.pigeonhl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.pigeonhl WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.pmpacket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.pmpacket WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.preferences SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.preferences WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.price SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.price WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prioritymap SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prioritymap WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsaudit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsauditattachment SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsauditattachment WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsmemcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsmemcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsonecalls SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsonecalls WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsreasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsreasonlist WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prsregion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prsregion WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prssends SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prssends WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.prssetup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.prssetup WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.r1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.r1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rawaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rawaudit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rawticket SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rawticket WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rdsecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rdsecurity WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reason SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reason WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasonlist WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.region SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.region WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.remarks WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	 /*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquerysecurity SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END*/
     
	--SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquery SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquery WHERE queryname IN (SELECT queryname FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''');
	--SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reportquerytext SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerytext WHERE queryname IN (SELECT queryname FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reportquerysecurity WHERE customerid = ''' + @customerid + '''');
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.request SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.request WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
     
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.requesttype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.requesttype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
         
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.requesttypetable SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.requesttypetable WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
         
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptdetail SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptdetail WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptheader SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptheader WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptsched SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptsched WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rptsched_print SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rptsched_print WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
           
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rpttemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rpttemplate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
           
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqmemcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqmemcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvaldpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvaldpt WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvalidphone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvalidphone WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.rqvalqty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.rqvalqty WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.s1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.s1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.s2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.s2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.savedrpttemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.savedrpttemplate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.savedrpttemplateline SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.savedrpttemplateline WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	--ScreeningQueue
	--ScreeningRuleassignment
	--ScreeningRuleset
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.security SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.security WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.securitygroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.securitygroup WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.seqid SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.seqid WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.servicearea SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.servicearea WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
              
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.statcols SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.statcols WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.suffixrequesttypes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.suffixrequesttypes WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
       
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.t1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.t1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.t2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.t2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketentrytype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketentrytype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketflag SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketflag WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketflagremarks SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketflagremarks WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketstatus WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketupdate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketupdate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tplformat SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.tplformat WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.u1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.u1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.updatetype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.updatetype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.userinfo SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.userinfo WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.userquery SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.userquery WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validdpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validdpt WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validphone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validphone WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validqty SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validqty WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validrule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.validterritory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.validterritory WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
           
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.valruledpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.valruledpt WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.valruleitm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.valruleitm WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
               
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime, triggerdtdate, triggerdttime, action, direction) SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime, triggerdtdate, triggerdttime, action, direction FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitbackward WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitcommon SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitcommon WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitdata SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitdata WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitrem SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitrem WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitreq SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitreq WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
          

	--NEW Tables
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitprsaudit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitprsaudit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitqueue SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitqueue WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitaction SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitaction WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
         
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitrule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
          
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vmrequesttype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	/*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vmrequesttypetemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttypetemplate WHERE templateid IN (SELECT completiontemplateid FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vmrequesttype WHERE customerid = ''' + @customerid + ''')';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END*/
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqcustomactcode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqcustomactcode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
         
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqcustomfaccode SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqcustomfaccode WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
         
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqreason SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqreason WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqreasonlist SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqreasonlist WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalidrule SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalidrule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
             
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalidterritory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalidterritory WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
        
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalruledpt SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalruledpt WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.vrqvalruleitm SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.vrqvalruleitm WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.w5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.w5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.watch SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.watch WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.wojobid SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.wojobid WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.workdetl SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.workdetl WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.worktype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.worktype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.woworktype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.woworktype WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
                 
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x1visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x1visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x2visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x2visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x3visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x3visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x4visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x4visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x5visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x5visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x6visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x6visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.x7visit SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.x7visit WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.xmlcompletion SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.xmlcompletion WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXconfig SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXconfig WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXgenericnotes SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXgenericnotes WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXpreference SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXpreference WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtermsaccept SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtermsaccept WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
            
	 /*SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtracelog SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtracelog WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
      */      
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.zone SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.zone WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
       
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.zoneinterface SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.zoneinterface WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
    
	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobmobilehistory SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobmobilehistory WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	 ---New Tables

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.blacklist(customerid, phone, username, domain, listtype, creationdateutc, createdby, comments) 
		SELECT customerid, phone, username, domain, listtype, creationdateutc, createdby, comments  FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.blacklist WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.FirstAlertPricingSchedule(customerid, schedulename, description, locateprice, locateotprice, onecallprice, onecallotprice, 
		gdprice, gdotprice, reportresearchprice, onecallplacementprice, kilometersabprice, kilometersotherprice, suppliesprice, subsistenceprice, drugtestingprice, lodgingpercent, taxpercent, isactive)
		  SELECT customerid, schedulename, description, locateprice, locateotprice, onecallprice, onecallotprice, 
			gdprice, gdotprice, reportresearchprice, onecallplacementprice, kilometersabprice, kilometersotherprice, suppliesprice, subsistenceprice, drugtestingprice, lodgingpercent, taxpercent, isactive
			 FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.FirstAlertPricingSchedule WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
	 
	SET @SQLCommand = '
		CREATE TABLE #jobdigConvert
	(
		id INT,
		customerid VARCHAR(32),
		jobid VARCHAR(16),
		jobgeometry GEOMETRY
	)

	INSERT INTO #jobdigConvert SELECT * FROM OPENQUERY(['+ @targetServer + '],' + '''SELECT * FROM ' + @targetDatabase + '.dbo.jobdigpoint WHERE customerid = ''''' + @customerid + ''''' '' '+');
	INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobdigpoint(customerid, jobid, jobgeometry) SELECT customerid, jobid, jobgeometry FROM  #jobdigConvert

	DROP table #jobdigConvert
	';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			PRINT @SQLCommand + ' FAILED!'
			RETURN -1
		END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.jobstatus SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.jobstatus WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END


	--SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msggroup SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msggroup WHERE customerid = ''' + @customerid + '''';
 --    EXEC @return = sp_executesql @SQLCommand OUTPUT
 --    IF @return <> 0 BEGIN
 --         PRINT @SQLCommand + ' FAILED!'
 --         RETURN -1
 --    END

	--SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgqueue(status, queueddt, processingdt, failedreason, msgsource, customerid, msgtype, msggroupid, msgtag, msgdescription, msghandler, msgdatatext)
	--	 SELECT status, queueddt, processingdt, failedreason, msgsource, customerid, msgtype, msggroupid, msgtag, msgdescription, msghandler, msgdatatext FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msgqueue WHERE customerid = ''' + @customerid + '''';
 --    EXEC @return = sp_executesql @SQLCommand OUTPUT
 --    IF @return <> 0 BEGIN
 --         PRINT @SQLCommand + ' FAILED!'
 --         RETURN -1
 --    END


	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasonoccpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasonoccpriority WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reasontemplate SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.reasontemplate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	 SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtemplate( name, description, customerid, template_type, content, creationdt, modifiedby, modifiedon)
		 SELECT name, description, customerid, template_type, content, creationdt, modifiedby, modifiedon FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtemplate WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedjob SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.messagedjob WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketpriority WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END

	SET @SQLCommand = ' INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ticketpriorityoccpriority SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.ticketpriorityoccpriority WHERE customerid = ''' + @customerid + '''';
     EXEC @return = sp_executesql @SQLCommand OUTPUT
     IF @return <> 0 BEGIN
          PRINT @SQLCommand + ' FAILED!'
          RETURN -1
     END
	 
	 BEGIN
	 PRINT 'Baseline Data Migration Complete!'
	 END


END