﻿
-- =============================================
-- Description:	Move the Misc Table collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_misctablecollection]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
		DECLARE @ParmDefinition nvarchar(500);
		DECLARE @CountSQLQuery varchar(30);

		--Column Count Match
				IF (@includeAudit = 1)
			BEGIN
				EXEC sp_audit_column_count 'mobstat', @sourceServer, @sourceDatabase, 36
				EXEC sp_audit_column_count 'dupaddr', @sourceServer, @sourceDatabase, 6
				EXEC sp_audit_column_count 'AAattachmentsize', @sourceServer, @sourceDatabase, 5
				EXEC sp_audit_column_count 'currentmobloc', @sourceServer, @sourceDatabase, 5
				EXEC sp_audit_column_count 'ticketpriorityoccpriority', @sourceServer, @sourceDatabase, 4
				EXEC sp_audit_column_count 'ticketsort', @sourceServer, @sourceDatabase, 6
				EXEC sp_audit_column_count 'duedatestatus', @sourceServer, @sourceDatabase, 6

				EXEC sp_audit_column_count 'remindercommon', @sourceServer, @sourceDatabase, 12
				EXEC sp_audit_column_count 'reminderdata', @sourceServer, @sourceDatabase, 4
				EXEC sp_audit_column_count 'remindernotification', @sourceServer, @sourceDatabase, 5

				EXEC sp_audit_column_count 'tag', @sourceServer, @sourceDatabase, 3
				EXEC sp_audit_column_count 'tagversion', @sourceServer, @sourceDatabase, 9
				EXEC sp_audit_column_count 'tagemail', @sourceServer, @sourceDatabase, 5
				EXEC sp_audit_column_count 'taggeometry', @sourceServer, @sourceDatabase, 5
				EXEC sp_audit_column_count 'tagjob', @sourceServer, @sourceDatabase, 3
				EXEC sp_audit_column_count 'tagfield', @sourceServer, @sourceDatabase, 4
				EXEC sp_audit_column_count 'tagvalue', @sourceServer, @sourceDatabase, 3

				EXEC sp_audit_column_count 'emailrecipient', @sourceServer, @sourceDatabase, 3
				EXEC sp_audit_column_count 'schedule', @sourceServer, @sourceDatabase, 3
				EXEC sp_audit_column_count 'scheduleversion', @sourceServer, @sourceDatabase, 7
				EXEC sp_audit_column_count 'scheduledetail', @sourceServer, @sourceDatabase, 13

			END		

		EXEC [dbo].[sp_move_table] @tableName = 'mobstat', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;  
		EXEC [dbo].[sp_move_table] @tableName = 'dupaddr', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'AAattachmentsize', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'currentmobloc', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		--EXEC [dbo].[sp_move_table] @tableName = 'calendar', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ticketpriorityoccpriority', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
        EXEC [dbo].[sp_move_table] @tableName = 'ticketsort', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'duedatestatus', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'remindercommon', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'tag', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'tagversion', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'tagjob', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'emailrecipient', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'schedule', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;

--tagemail				
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.tagemail (tagversionid, emailrecipientid, scheduleid, customerid) 
				SELECT tagversionid, emailrecipientid, scheduleid, customerid FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.tagemail tg WHERE tg.tagversionid IN 
				(SELECT tagversionid FROM tagversion tv WHERE tv.customerid = '''+@customerId+''') 

				UPDATE ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.tagemail
				SET template_id = xxt.template_id
				FROM ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.xxtemplate xxt
				WHERE xxt.customerid = ''DEFAULT''
				AND xxt.[name] = ''Tag Email''

';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END

/**tagemail record count**/
  IF (@includeAudit = 1) 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagemail AS TE
							WHERE te.customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagemail AS TE
						WHERE te.customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: tagemail FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'tagemail Records Match'
				END

--scheduleversion				
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.scheduleversion 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.scheduleversion sv WHERE sv.scheduleid IN 
				(SELECT s.scheduleid FROM ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.schedule s WHERE s.customerid = '''+@customerId+''')
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END

/**scheduleversion record count**/
  IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.scheduleversion AS SV
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.schedule AS S ON sv.scheduleid = s.scheduleid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.scheduleversion AS SV
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.schedule AS S ON sv.scheduleid = s.scheduleid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: scheduleversion FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'scheduleversion Records Match'
				END
			END;


--scheduledetail
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.scheduledetail 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.scheduledetail sd WHERE sd.scheduleversionid IN 
				(SELECT scheduleversionid FROM ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.scheduleversion sv WHERE sv.scheduleid IN (SELECT s.scheduleid FROM ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.schedule s WHERE s.customerid = '''+@customerId+''')) 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END

/**scheduledetail record count**/
  IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.scheduledetail AS SD
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.scheduleversion AS SV ON sd.scheduleversionid = sv.scheduleversionid
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.schedule AS S ON sv.scheduleid = s.scheduleid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.scheduledetail AS SD
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.scheduleversion AS SV ON sd.scheduleversionid = sv.scheduleversionid
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.schedule AS S ON sv.scheduleid = s.scheduleid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: scheduledetail FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'scheduledetail Records Match'
				END
			END;

---taggeometry
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.taggeometry 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.taggeometry tg WHERE tg.tagversionid IN 
				(SELECT tagversionid FROM tagversion tv WHERE tv.customerid = '''+@customerId+''') 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
 /**taggeometry audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.taggeometry AS TG
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagversion AS TV ON tg.tagversionid = tv.tagversionid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.taggeometry AS TG
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagversion AS TV ON tg.tagversionid = tv.tagversionid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: taggeometry FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'taggeometry Records Match'
				END
			END;
		
--tagfield
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.tagfield 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.tagfield  WHERE tagversionid IN 
				(SELECT tagversionid FROM tagversion tv WHERE tv.customerId = '''+@customerId+''') 

';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END	
	
 /**tagfield audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagfield AS TF
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagversion AS TV ON tf.tagversionid = tv.tagversionid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagfield AS TF
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagversion AS TV ON tf.tagversionid = tv.tagversionid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: tagfield FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'tagfield Records Match'
				END
			END;	
		
--tagvalue
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.tagvalue 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.tagvalue tv WHERE tv.tagfieldid IN 
				(SELECT tf.tagfieldid FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.tagfield tf WHERE tf.tagversionid IN 
				 (SELECT tv.tagversionid FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.tagversion tv WHERE tv.customerid = '''+@customerId+''')) 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END

 /**tagvalue audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagvalue AS TGV
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagfield AS TF ON tgv.tagfieldid = tf.tagfieldid
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.tagversion AS TV ON tf.tagversionid = tv.tagversionid

							WHERE tv.customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagvalue AS TGV
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagfield AS TF ON tgv.tagfieldid = tf.tagfieldid
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.tagversion AS TV ON tf.tagversionid = tv.tagversionid

						WHERE tv.customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: tagvalue FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'tagvalue Records Match'
				END
			END

--reminderdata
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.reminderdata 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.reminderdata rd WHERE rd.reminderid IN 
				(SELECT reminderid FROM remindercommon rc WHERE rc.customerid = '''+@customerId+''') 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END

 /**reminderdata audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reminderdata AS RD ON rc.reminderid = rd.reminderid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.reminderdata AS RD ON rc.reminderid = rd.reminderid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: reminderdata FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'reminderdata Records Match'
				END
			END;
		
--remindernotifications
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.remindernotification 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.remindernotification rn WHERE rn.reminderid IN 
				(SELECT reminderid FROM remindercommon rc WHERE rc.customerid = '''+@customerId+''') 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END	

 /**remindernotifications audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remindernotification AS RN ON rc.reminderid = rn.reminderid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.remindernotification AS RN ON rc.reminderid = rn.reminderid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: remindernotifications FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'remindernotifications Records Match'
				END
			END;

--reminderrecipients
SET @SQLCommand = N' 
			INSERT INTO ['+@targetServer+']'+'.'+@targetDatabase+'.dbo.reminderrecipient 
				SELECT * FROM ['+@SourceServer+']'+'.'+@SourceDatabase+'.dbo.reminderrecipient rc WHERE rc.reminderid IN 
				(SELECT reminderid FROM remindercommon rc WHERE rc.customerid = '''+@customerId+''') 
';
				EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END	

	END TRY
	BEGIN CATCH
		PRINT('MOVE ''MiscTableCollection'' FAILED Stored Procedure: ''sp_move_misctablecollection''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END

 /**reminderrecipients audit - record count**/
  		IF (@includeAudit = 1) 
			BEGIN 
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.reminderrecipient AS RRC ON rc.reminderid = rrc.reminderid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.remindercommon AS RC
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.reminderrecipient AS RRC ON rc.reminderid = rrc.reminderid
						WHERE customerid = ''' + @customerId + ''')
								
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
					EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
					SELECT CAST(@CountSQLQuery as int) [Record Count Difference];

				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: reminderrecipient FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'reminderrecipient Records Match'
				END
			END;