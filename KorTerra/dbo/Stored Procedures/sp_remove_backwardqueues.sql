﻿-- =============================================
-- Description:	Cleanup backward queues in target server
-- =============================================
CREATE PROCEDURE [dbo].[sp_remove_backwardqueues]
    @customerId VARCHAR(32) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
        SET @SQLCommand = '
			IF EXISTS (SELECT * FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitbackward WHERE customerid = ''' + @customerid + ''' )
			DELETE FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitbackward WHERE customerid = ''' + @customerid + ''';'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
        END
		SET @SQLCommand = '
			IF EXISTS (SELECT * FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachmentbackward WHERE customerid = ''' + @customerid + ''' )
			DELETE FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.attachmentbackward WHERE customerid = ''' + @customerid + ''';'
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            SET @errMsg = @SQLCommand + ' FAILED!'
            RAISERROR (@errMsg, 15, 10);
        END
	END TRY
	BEGIN CATCH
		PRINT('Cleanup ''backward queues'' FAILED Stored Procedure: ''sp_remove_backwardqueues''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END