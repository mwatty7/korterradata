﻿-- =============================================
-- Description:	Move the Message Customer/Detail/Activity tables with autoincremented key from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_misc_messages_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
    @includeAudit BIT = 0
AS
BEGIN
    BEGIN TRY
			SET NOCOUNT ON;
			DECLARE @SQLCommand NVARCHAR(MAX);
			DECLARE @return Int;
			DECLARE @errMsg NVARCHAR(MAX);
			DECLARE @populateMsgDetail NVARCHAR(MAX);
	IF @includeAudit = 1
		BEGIN
			DECLARE @tableName VARCHAR(50) = 'MessageDetail';
			DECLARE @expectedCount INT = 11;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @tableName = 'MessageCustomer';
			SET @expectedCount = 2;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			
			SET @tableName = 'MessageActivity';
			SET @expectedCount = 5;
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
		END
	SET @populateMsgDetail = '
	CREATE TABLE #messagedetail
	(
		messageid_old INT,
		messageid_new INT,
		internalname VARCHAR(255),
		displayname VARCHAR(255),
		content VARCHAR(MAX),
		link VARCHAR(MAX),
		activefromdt DATETIME2(7),
		activethroughdt DATETIME2(7),
		reminderdelay INT,
		messagetype VARCHAR(32),
		customerlisttype VARCHAR(1),
		creationdt DATETIME2(7),
		isactive BIT
	);
	CREATE TABLE #messagecustomer
	(
		customerid VARCHAR(32),
		messageid_old INT,
		messageid_new INT
	)
	CREATE TABLE #messageactivity
	(
		customerid VARCHAR(32),
		operatorid VARCHAR(32),
		messageid_old INT,
		messageid_new INT,
		creationdt DATETIME2(7),
		action VARCHAR(32)
	)
	INSERT INTO #messagedetail(messageid_old, internalname, displayname, content, link, activefromdt, activethroughdt, messagetype, customerlisttype, creationdt, isactive)
	SELECT messageid, internalname, displayname, content, link, activefromdt, activethroughdt, messagetype, customerlisttype, creationdt, isactive
	 FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.messagedetail WHERE messageid IN (SELECT messageid FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.messageactivity  WHERE customerid = '''+@customerId +''')
	INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedetail
	(
	internalname, 
	displayname, 
	content, 
	link, 
	activefromdt, 
	activethroughdt, 
	messagetype, 
	customerlisttype, 
	creationdt, 
	isactive
	)
	SELECT 
	internalname, 
	displayname, 
	content, 
	link, 
	activefromdt, 
	activethroughdt, 
	messagetype, 
	customerlisttype, 
	creationdt, 
	isactive
	 FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.messagedetail 
	 WHERE messageid IN (SELECT messageid FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.messageactivity  WHERE customerid = '''+ @customerId +''')
	 
 UPDATE #messagedetail
 SET messageid_new = a.messageid
 FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagedetail a
	JOIN #messagedetail b on a.internalname = b.internalname
		AND a.displayname = b.displayname
		AND a.content = b.content
		AND a.activefromdt = b.activefromdt
		AND a.activethroughdt = b.activethroughdt
		AND a.messagetype = b.messagetype
		AND a.customerlisttype = b.customerlisttype
		AND a.creationdt = b.creationdt
		AND a.isactive = b.isactive
INSERT INTO #messageactivity
	(customerid,
	 operatorid, 
	 messageid_old, 
	 creationdt, 
	 action)
SELECT *
FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.MessageActivity WHERE customerid = '''+@customerId +'''
UPDATE #messageactivity
SET messageid_new = a.messageid_new
FROM #messagedetail a
	JOIN #messageactivity b on a.messageid_old = b.messageid_old
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messageactivity
SELECT DISTINCT  
a.customerid, 
a.operatorid, 
b.messageid_new, 
a.creationdt, 
a.action
FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.messageactivity a
	JOIN #messageactivity b on a.messageid = b.messageid_old AND a.operatorid = b.operatorid
WHERE a.customerid = '''+@customerId +'''
INSERT INTO #messagecustomer(customerid, messageid_old)
SELECT *
FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.MessageCustomer
WHERE customerid = '''+@customerId +'''
UPDATE #messagecustomer
SET messageid_new = a.messageid_new
FROM #messageactivity a
	JOIN #messagecustomer b on a.messageid_old = b.messageid_old
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messagecustomer
SELECT DISTINCT a.customerid, b.messageid
FROM #messagecustomer a
	JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.messageactivity b on a.messageid_new = b.messageid
WHERE b.customerid = '''+@customerId +'''
DROP TABLE #messagedetail, #messageactivity, #messagecustomer
		';
	EXECUTE @return = sp_executesql @populateMsgDetail OUTPUT
	IF @return <> 0 BEGIN
		SET @errMsg = @populateMsgDetail + 'FAILED!'
		RAISERROR (@errMsg, 15, 10);
	END
	IF @includeAudit = 1
			EXEC sp_audit_move_misc_messages_autoincremented @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
			
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''Move Misc Tables'' FAILED Stored Procedure: ''sp_move_misc_messages_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
		select @error = ERROR_NUMBER(),
			@message = ERROR_MESSAGE(),
			@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END