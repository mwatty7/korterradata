﻿ 
CREATE PROCEDURE dbo.sp_XXTLOG_BatchDelete3
   @increment BIGINT

AS
     
    BEGIN TRY
     
    -- Declare local variables
    DECLARE @sqlstr NVARCHAR(2000);
    DECLARE @rowcount BIGINT;
    DECLARE @loopcount BIGINT;
	DECLARE @rowsKeep BIGINT;
	DECLARE @usedDiskSpace FLOAT;
	DECLARE @maxDiskSpace BIGINT;
	DECLARE @usedRows BIGINT;

	SET @increment = @increment;

    -- Set the parameters for the sp_executesql statement
    -- Initialize the loop counter
    SET @loopcount = 1;

	--Set MAX disk space
	SELECT @maxDiskSpace = COALESCE(
	(SELECT CONVERT(BIGINT, value) FROM XXconfig
	WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
	AND name = 'MAXDISKSPACE'), 128)
     
		-- sp_spaceused doesn't return it's data in a way that is accessible from
	-- within a stored procedure, so we pull it's data into temp table
	CREATE TABLE #tblResults
	(
		name NVARCHAR(20),
		rows BIGINT,
		reserved VARCHAR(18),
		reserved_int BIGINT DEFAULT(0),
		data VARCHAR(18),
		data_int BIGINT DEFAULT(0),
		index_size VARCHAR(18),
		index_size_int BIGINT DEFAULT(0),
		unused VARCHAR(18),
		unused_int BIGINT DEFAULT(0)
	)
	;
	
	INSERT INTO #tblResults
	(name, rows, reserved, data, index_size, unused)
	EXEC sp_spaceused 'XXtracelog'

	--DROP TABLE #tblResults
	UPDATE #tblResults 
	SET reserved_int = CAST(SUBSTRING(reserved, 1, CHARINDEX(' ', reserved)) AS BIGINT),
		data_int = CAST(SUBSTRING(data, 1, CHARINDEX(' ', data)) AS BIGINT),
		index_size_int = CAST(SUBSTRING(index_size, 1, CHARINDEX(' ', index_size)) AS BIGINT),
		unused_int = CAST(SUBSTRING(unused, 1, CHARINDEX(' ', unused)) AS BIGINT)


     
    --SET @usedRows
	SELECT @usedRows = rows,  @usedDiskSpace = data_int / 1024.0
	FROM #tblResults

	--SET @rowskeep
	SELECT @rowsKeep = (@maxDiskSpace / ISNULL(NULLIF(@usedDiskSpace,0),1) ) * @usedRows

	--SET @rowcount
	SELECT @rowcount =  COUNT(*) FROM XXtracelog WHERE logid < (
		SELECT MAX(logid) - @rowsKeep
		FROM XXtracelog)

	PRINT 'usedRows = ' + CONVERT(VARCHAR, @usedRows)
	PRINT 'usedDiskSpace = ' + CONVERT(VARCHAR, @usedDiskSpace)
	PRINT 'maxDiskSpace = ' + CONVERT(VARCHAR, @maxDiskSpace)
	PRINT 'rowKeep = ' + CONVERT(VARCHAR, @rowsKeep)
	PRINT 'rowCount = ' + CONVERT(VARCHAR, @rowcount)


    -- Perform the loop while there are rows to delete
    WHILE @loopcount < @rowcount
    BEGIN
          
    -- Build a dynamic SQL string to delete rows
	SET @sqlstr = 'DELETE TOP ('+CAST(@increment AS VARCHAR(10))+') FROM XXtracelog WHERE logid < (SELECT MAX(logid) - '+@rowsKeep+' FROM XXtracelog)';          
         -- Execute the dynamic SQL string to delete a batch of rows
         EXEC(@sqlstr);
          
         -- Add the @increment value to @loopcount
         SET @loopcount = @loopcount + @increment;
          
          
    END
     
    END TRY
     
    BEGIN CATCH
         
        SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
 
    END CATCH
	BEGIN 
	         PRINT CAST(@rowcount AS VARCHAR(10)) + ' rows deleted.'
END;