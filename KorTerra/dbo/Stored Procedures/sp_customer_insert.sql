﻿CREATE PROCEDURE [dbo].[sp_customer_insert]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0,
	@includeTableCountAudit bit = 0
AS
BEGIN
    SET NOCOUNT ON;
	BEGIN TRY
		-- Execute Precheck validation
		EXEC [dbo].[sp_move_precheck] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeTableCountAudit = @includeTableCountAudit;
		--POPULATE customer/msg group 
		EXEC [dbo].[sp_move_zmaintcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move Herald Maint
		EXEC [dbo].[sp_move_herald_maint_autoincremented] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move ZHerald Collection
		EXEC [dbo].[sp_move_z_heraldcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		--POPULATE XXDB Tables
		EXEC [dbo].[sp_move_xxdbcollection_autoincremented] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move XXDBMaint Collection
		EXEC [dbo].[sp_move_xx_dbmaintcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- ADLS Maintenance Tables
		EXEC [dbo].[sp_move_adlsmaintcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- JOB Collection
		EXEC [dbo].[sp_move_jobcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call sp_move_filteraudititm
		EXEC [dbo].[sp_move_filteraudititm] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call sp_move_ticketstatus
		EXEC [dbo].[sp_move_ticketstatus] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call sp_move_rawaudit
		EXEC [dbo].[sp_move_rawaudit] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call sp_move_invoice
		EXEC [dbo].[sp_move_invoice] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move Billing Maintenance
		EXEC [dbo].[sp_move_billingmaintcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move Payroll Maint Collection
		EXEC [dbo].[sp_move_payrollmaintcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move Payroll Collection
		EXEC [dbo].[sp_move_payrollcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move KorExport Collection
		EXEC [dbo].[sp_move_korexportcollection] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Routing Rule Maint
		EXEC [dbo].[sp_move_routingrule_maint] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;		
		-- Visit Rule Maint
		EXEC [dbo].[sp_move_visitrulemaint] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call Move MiscTables Collection
		EXEC [dbo].[sp_move_misc] @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		-- Call To Remove Backward Queues
		EXEC [dbo].[sp_remove_backwardqueues] @customerId = @customerId, @targetServer = @targetServer, @targetDatabase = @targetDatabase;
        PRINT 'Congratulations! Customer Data Migration has been completed! Please verify data counts for consistency...'
    END TRY
	BEGIN CATCH
		PRINT 'Move customer failed';
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		PRINT(@error)
		PRINT(@message)
		PRINT(@xstate)
		RETURN -1;
	END CATCH
END;