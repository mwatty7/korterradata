﻿
CREATE PROCEDURE dbo.sp_ticketpriority_add
    @occpriority VARCHAR(80),
	@ticketpriority VARCHAR(80),
	@occid VARCHAR(8)

AS

BEGIN TRY

DECLARE @return Int;


SET @occpriority = @occpriority;
SET @ticketpriority = @ticketpriority; 
SET @occid = @occid

CREATE TABLE #ticketpriority_occpriority
(
ticketpriorityid UNIQUEIDENTIFIER,
customerid       VARCHAR(32), 
occid            VARCHAR(8), 
origpriority     VARCHAR(80), 
ticketpriority   VARCHAR(80)

); 


INSERT INTO #ticketpriority_occpriority (ticketpriority, occid, origpriority) 
SELECT @ticketpriority, @occid, @occpriority

--SET customerid to Default
UPDATE #ticketpriority_occpriority
SET customerid = 'DEFAULT';

--Set ticketpriorityid guid to our defined one based on if Emergency, Routine, or Meet
UPDATE #ticketpriority_occpriority
SET ticketpriorityid = CASE WHEN ticketpriority = 'EMERGENCY' THEN 'DF215E10-8BD4-4401-B2DC-99BB03135F2A' 
							WHEN ticketpriority = 'ROUTINE' THEN 'DF215E10-8BD4-4401-B2DC-99BB03135F2B' 
							WHEN ticketpriority = 'MEET' THEN 'DF215E10-8BD4-4401-B2DC-99BB03135F2C'
							ELSE NULL 
							END;

IF EXISTS(SELECT 0 FROM #ticketpriority_occpriority WHERE ticketpriorityid IS NULL)
BEGIN 
	PRINT 'Failed! Gaaawd Dammmit! Must provide a ticket priority of Emergency, Routine, or Meet'
	RETURN -1
END

							
 --Add New Priorities
--INSERT INTO occpriority
INSERT INTO occpriority SELECT customerid, occid, origpriority FROM #ticketpriority_occpriority
WHERE customerid + occid + origpriority NOT IN (SELECT customerid + occid + origpriority FROM occpriority)

--INSERT INTO ticketpriorityoccpriority
INSERT INTO ticketpriorityoccpriority  SELECT ticketpriorityid, customerid, occid, origpriority FROM #ticketpriority_occpriority
WHERE CAST(ticketpriorityid AS varchar(64)) + customerid + occid + origpriority NOT IN (SELECT CAST(ticketpriorityid AS varchar(64)) + customerid + occid + origpriority FROM ticketpriorityoccpriority)

DROP TABLE #ticketpriority_occpriority

END TRY 

BEGIN CATCH
        SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
END CATCH

PRINT 'Priority Added'
;