﻿CREATE PROCEDURE [dbo].[ticketsReceivedReport1] 
@customerId varchar(32),
@occid varchar(8),
@fromDate varchar(30),
@throughDate varchar(30),
@regionid varchar(16),
@districtid varchar(16),
@mobileid varchar(16)

AS 

Declare @Where NVARCHAR(MAX) 
DECLARE @Command NVARCHAR(MAX) 

Set @Command = 'SELECT  regionid, t.jobid, districtid, m.mobileid, j.city, j.priority
FROM mobile m 
JOIN ticketstatus t ON t.customerid = m.customerid AND t.mobileid = m.mobileid 
JOIN job j ON j.customerid = m.customerid AND t.jobid = j.jobid
WHERE (t.customerid = ''' + @customerId + ''') AND (t.xmitdtdate BETWEEN ''' + convert(varchar(30), @fromDate, 121) + ''' AND ''' + convert(varchar(30), @throughDate, 121) + ''')' 

If( @regionid <> '' )
   Set @Command = @Command + ' AND (m.regionid = ''' + @regionid + ''') '

If( @districtid <> '' )
   Set @Command = @Command + ' AND (m.districtid = ''' + @districtid + ''') '

If( @mobileid <> '' )
   Set @Command = @Command + ' AND (m.mobileid = ''' + @mobileid + ''') '

If( @occid <> '' )
   Set @Command = @Command + ' AND (t.occid = ''' + @occid + ''') '

Execute SP_ExecuteSQL  @Command