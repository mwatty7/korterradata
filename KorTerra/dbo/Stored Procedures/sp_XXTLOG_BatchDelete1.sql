﻿ 
CREATE PROCEDURE dbo.sp_XXTLOG_BatchDelete1
    @increment BIGINT

AS
     
    BEGIN TRY
     
    -- Declare local variables
    DECLARE @sqlstr NVARCHAR(2000);
    DECLARE @rowcount BIGINT;
    DECLARE @loopcount BIGINT;
	DECLARE @recordsToRetain BIGINT;
	DECLARE @tablename SYSNAME; 

	SET @increment = @increment;
	SET @tablename = 'XXtracelog'

    -- Initialize the loop counter
    SET @loopcount = 1;
     

	 -- Set recordsToRetain
	SELECT @recordsToRetain = ISNULL(
	(SELECT CONVERT(BIGINT, value) FROM XXconfig
	WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
	AND name = 'RECORDSTORETAIN'), 100000)
	;
	PRINT 'recordsToRetain = ' + CONVERT(VARCHAR, @recordsToRetain)
 
	-- Set rowCount
	SELECT @rowcount =  COUNT(*) FROM XXtracelog WHERE logid < (
		SELECT MAX(logid) - @recordsToRetain
		FROM XXtracelog)


	PRINT 'rowCount = ' + CONVERT(VARCHAR, @rowcount)
      
    -- Perform the loop while there are rows to delete
    WHILE @loopcount < @rowcount
    BEGIN
          
       -- Build a dynamic SQL string to delete rows
		SET @sqlstr = 'DELETE TOP ('+CAST(@increment AS VARCHAR(10))+') FROM XXtracelog WHERE logid < (SELECT max(logid) -'+CONVERT(VARCHAR,@recordsToRetain)+' FROM XXtracelog)';          
         -- Execute the dynamic SQL string to delete a batch of rows
         EXEC(@sqlstr);
          
         -- Add the @increment value to @loopcount
         SET @loopcount = @loopcount + @increment;
          
         PRINT CAST(@rowcount AS VARCHAR(10)) + ' rows deleted.'
          
    END
     
    END TRY
     
    BEGIN CATCH
         
        SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
 
    END CATCH