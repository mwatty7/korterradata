﻿  CREATE PROCEDURE [dbo].[sp_kt_reports_ticketsreceivedv5]
	@customerId varchar(32),
	@occid varchar(8),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(16),
	@memberid varchar(16),
	@membercode varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
	SELECT
		t.customerid,
		t.jobid,
		t.origpriority,
		Cast(t.xmitdtdate + t.xmitdttime as datetime) as xmitdatetime,
		sendto,
		m.regionid,
		m.districtid,
		m.mobileid,
		j.address,
		j.city,
		j.street,
		j.worktype
	FROM ticketstatus t
	JOIN mobile m on m.customerid = t.customerid AND m.mobileid = t.mobileid
	JOIN memcode c on c.customerid = t.customerid AND c.membercode = t.sendto
	JOIN (
		SELECT DISTINCT customerid, jobidnosuffix, 
		COALESCE(address, '''') AS address, COALESCE(street, '''') AS street,
		COALESCE(city, '''') AS city, COALESCE(worktype, '''') AS worktype
		FROM job
	) j on j.customerid = t.customerid AND j.jobidnosuffix = t.jobid'
	Set @Command = @Command + ' WHERE (t.customerid = ''' + @customerId + ''') AND (t.xmitdtdate BETWEEN ''' + convert(varchar(30), @fromDate, 121) + ''' AND ''' + convert(varchar(30), @throughDate, 121) + ''')' 
	If( @regionid <> '0' )
	   Set @Command = @Command + ' AND (m.regionid = ''' + @regionid + ''') '
	If( @districtid <> '0' )
	   Set @Command = @Command + ' AND (m.districtid = ''' + @districtid + ''') '
	If( @mobileid <> '0' )
	   Set @Command = @Command + ' AND (m.mobileid = ''' + @mobileid + ''') '
	If( @occid <> '0' )
	   Set @Command = @Command + ' AND (t.occid = ''' + @occid + ''') '
	If( @memberid <> '0' )
	   Set @Command = @Command + ' AND (c.memberid = ''' + @memberid + ''') '
	If( @membercode <> '0' )
	   Set @Command = @Command + ' AND (t.sendto = ''' + @membercode + ''') '
	Execute SP_ExecuteSQL  @Command
END
--SELECT
--		t.customerid,
--		t.jobid,
--		t.origpriority,
--		Cast(t.xmitdtdate + t.xmitdttime as datetime),
--		m.regionid,
--		m.districtid,
--		m.mobileid,
--		j.address,
--		j.city,
--		j.street,
--		j.worktype
--	FROM ticketstatus t
--	JOIN mobile m on m.customerid = t.customerid AND m.mobileid = t.mobileid
--	JOIN memcode c on c.customerid = t.customerid AND c.membercode = t.sendto
--	JOIN (
--		SELECT DISTINCT customerid, jobidnosuffix, 
--		COALESCE(address, '''') AS address, COALESCE(street, '''') AS street,
--		COALESCE(city, '''') AS city, COALESCE(worktype, '''') AS worktype
--		FROM job
--	) j on j.customerid = t.customerid AND j.jobidnosuffix = t.jobid