﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymembercode_totals]
	@customerId varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeNLR varchar(16),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
			SELECT
				job.jobid,
				job.worktype,
				CONVERT(VARCHAR, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
				request.firstcompletiondt, 
				request.firstcompletiondtdate,
				job.origpriority,
				job.status
			FROM 
				JOB
				OUTER APPLY 
				(
					SELECT TOP 1
						request.membercode,
						request.jobid,
						request.firstcompletiondtdate,
						CONVERT(VARCHAR, request.firstcompletiondtdate + request.firstcompletiondttime, 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid
				) request
				OUTER APPLY
				(
					SELECT TOP 1 
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				WHERE
					job.customerid = ''' + @customerId + '''
	'
	-- Report Options
	If( @includeNLR = 'No')
		Set @Command = @Command + ' AND job.status NOT IN (''R'', ''X'', ''U'')'
	If( @includeEmergencies = 'No')
		Set @Command = @Command + '  AND job.priority <> ''EMERGENCY'''
	if( @includeProjects = 'No' )
		Set @Command = @Command + '  AND job.isproject = 0'
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(request.firstcompletiondtdate as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(request.firstcompletiondtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND (CAST(job.wtbdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.wtbdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(job.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	-- Member / Member Code
	If( @memberid <> '0' )
	   Set @Command = @Command + ' AND (memcode.memberid = ''' + @memberid + ''') '
	If( @membercode <> '0' )
	   Set @Command = @Command + ' AND (memcode.membercode = ''' + @membercode + ''') '
	-- End Member / Member Code
	Execute SP_ExecuteSQL  @Command
END