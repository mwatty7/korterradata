﻿-- =============================================
-- Description:	Make sure herald maint tables moved correctly
-- =============================================
CREATE PROCEDURE [dbo].[sp_audit_move_herald_maint_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @errMsg NVARCHAR(MAX);
    DECLARE @tableName VARCHAR(30) = 'HeraldNotifyGroup';
    DECLARE @ParmDefinition nvarchar(500);
    DECLARE @CountSQLQuery varchar(30);
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @tableNameCST VARCHAR(30) = 'CustomerScheduleTemplates';
    EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
    EXEC sp_audit_moved_table @tableNameCST, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
BEGIN
    EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
    SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT
							COUNT(*) AS HeraldNotifyEmailsCount
							FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyEmails AS HNE
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate AS HNST ON HNE.hnst_id = HNST.hnst_id
                            JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyGroup AS HNG ON HNST.hng_id = HNG.hng_id
						WHERE HNG.customerid = ''' + @customerId + ''') -
						(SELECT
							COUNT(*) AS HeraldNotifyEmailsCount
							FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyEmails AS HNE
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate AS HNST ON HNE.hnst_id = HNST.hnst_id
                            JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup AS HNG ON HNST.hng_id = HNG.hng_id
						WHERE HNG.customerid = ''' + @customerId + ''')
					
				)';
    SET @ParmDefinition = N'@result varchar(30) OUTPUT';
    EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
    SELECT CAST(@CountSQLQuery as int) [Record HeraldNotifyEmail Count Difference];
    IF @CountSQLQuery <> 0
				BEGIN
        PRINT @SQLCommand + ' FAILED!'
        SET @errMsg = 'TABLE: HeraldNotifyEmails FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
        RAISERROR (@errMsg, 15, 10)
    END
				ELSE IF @CountSQLQuery = 0
				BEGIN
        PRINT 'HeraldNotifyEmails Records Match'
    END
    SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT
							COUNT(*) AS HeraldNotifyScheduleTemplateCount
						FROM
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate AS HNST
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyGroup AS HNG ON HNST.hng_id = HNG.hng_id
						WHERE HNG.customerid = ''' + @customerId + ''') -
						(SELECT
							COUNT(*) AS HeraldNotifyScheduleTemplateCount
						FROM
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate AS HNST
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup AS HNG ON HNST.hng_id = HNG.hng_id
						WHERE HNG.customerid = ''' + @customerId + '''))';
    SET @ParmDefinition = N'@result varchar(30) OUTPUT';
    EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
    SELECT CAST(@CountSQLQuery as int) [Record HeraldScheduleTemplate Count Difference];
    IF @CountSQLQuery <> 0
				BEGIN
        PRINT @SQLCommand + ' FAILED!'
        SET @errMsg = 'TABLE: HeraldNotifyScheduleTemplate FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
        RAISERROR (@errMsg, 15, 10)
    END
				ELSE IF @CountSQLQuery = 0
				BEGIN
        PRINT 'HeraldNotifyScheduleTemplate Records Match'
    END
    SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT
							COUNT(*) AS ScheduleTemplateTimesCount
						FROM
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScheduleTemplateTimes AS STT
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.CustomerScheduleTemplates AS CST ON STT.cst_id = CST.cst_id
						WHERE CST.customerid = ''' + @customerId + ''') -
						(SELECT
							COUNT(*) AS ScheduleTemplateTimesCount
						FROM
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScheduleTemplateTimes AS STT
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.CustomerScheduleTemplates AS CST ON STT.cst_id = CST.cst_id
						WHERE CST.customerid = ''' + @customerId + '''))';
    SET @ParmDefinition = N'@result varchar(30) OUTPUT';
    EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
    SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
    IF @CountSQLQuery <> 0
				BEGIN
        PRINT @SQLCommand + ' FAILED!'
        SET @errMsg = 'TABLE: ScheduleTemplateTimes FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
        RAISERROR (@errMsg, 15, 10)
    END
				ELSE IF @CountSQLQuery = 0
				BEGIN
        PRINT 'ScheduleTemplateTimes Records Match'
    END
END
END