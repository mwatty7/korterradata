﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymonth]
	@customerId varchar(32),
	@startdate date,
	@enddate date

AS
BEGIN
create table #pastduereport (
	region varchar(50),
	district varchar(50),
	timezone varchar(50),
	code varchar(50),
	mobile varchar(50),
	year int,
	month_name varchar(15),
	month_number tinyint,
	completiondtutc datetime,
	duedateutc datetime,
	xmitdate datetime,
	ontime tinyint,
	late tinyint,
	timeframe tinyint,
	mo3ontime tinyint,
	mo3late tinyint,
	mo6ontime tinyint,
	mo6late tinyint,
	mo12ontime tinyint,
	mo12late tinyint

);


/*Get the base data needed for the report*/

insert into #pastduereport 

select 
r.regionid, 
d.districtid,
o.timezone,
req.membercode,
j.mobileid,
DATEPART(yy, req.xmitdtdate),
DATENAME(month, req.xmitdtdate),
DATEPART(mm, req.xmitdtdate), 
req.firstcompletiondtdate + req.firstcompletiondttime as firstcompletiondate,
j.duedateutc,
j.latestxmitdtdate,
0, 0, 
timeframe = case 
    WHEN getdate() < DATEADD(MONTH,  3, j.latestxmitdtdate) THEN 3
      WHEN  getdate() < DATEADD(MONTH,  6, j.latestxmitdtdate) THEN 6
      WHEN getdate() < DATEADD(MONTH, 12, j.latestxmitdtdate) THEN 12
      ELSE 0
end,
0, 0, 0, 0, 0, 0
from request req
left join job j on
	req.customerid = j.customerid and
	req.jobid = j.jobid
left join mobile mob on 
	j.mobileid = mob.mobileid and
	j.customerid = mob.customerid
left join district d on
	mob.customerid = d.customerid and
	mob.districtid = d.districtid
left join region r on
	d.customerid = r.customerid and
	d.regionid = r.regionid
left join memcode mem on 
	req.membercode = mem.membercode and
	req.customerid = mem.customerid
left join occ o on
	mem.occid = o.occid and
	mem.customerid = o.customerid
where req.customerid = @customerid and req.xmitdtdate between @startdate and @enddate and j.mobileid is not null-- and duedateutc is not null;




update #pastduereport
set completiondtutc = 
	CASE WHEN timezone = 'CENTRAL' THEN DATEADD(hh, -6, completiondtutc)
	 WHEN timezone = 'ALASKAN' THEN DATEADD(hh, -6, completiondtutc)
	 WHEN timezone = 'EASTERN' THEN DATEADD(hh, -6, completiondtutc)
	 WHEN timezone = 'MOUNTAIN' THEN DATEADD(hh, -6, completiondtutc)
	 WHEN timezone = 'PACIFIC' THEN DATEADD(hh, -6, completiondtutc)
	 ELSE DATEADD(hh, -6, completiondtutc)
	 END
	

update #pastduereport set 
ontime = 1
where (completiondtutc is not null and completiondtutc < duedateutc) or (completiondtutc is null and getutcdate() < duedateutc);
update #pastduereport set late = 1 where ontime = 0; 
update #pastduereport set mo3late = 1 where timeframe = 3 and late = 1;
update #pastduereport set mo3ontime = 1 where timeframe = 3 and late = 0;
update #pastduereport set mo6late = 1 where timeframe = 6 and late = 1;
update #pastduereport set mo6ontime = 1 where timeframe = 6 and late = 0;
update #pastduereport set mo12late = 1 where timeframe = 12 and late = 1;
update #pastduereport set mo12ontime = 1 where timeframe = 12 and late = 0;



select region, district, code, year, month_number, month_name, sum(ontime + late) as [Total Tickes], sum(late) as [Late Tickets], ((sum(late) * 1.00/sum(ontime + late) * 1.00) * 100) as [Percent Late],
 sum(mo3late + mo3ontime) month3total, sum(mo3late) month3late, 
 sum(mo6late + mo6ontime) month6total, sum(mo6late) month6late ,
 sum(mo12late + mo12ontime) month12total, sum(mo12late) month12late 
from #pastduereport
group by region, district, code, year, month_number, month_name
order by region, district, code, year, month_number;

--select * from #pastduereport order by xmitdate;
drop table #pastduereport;


END