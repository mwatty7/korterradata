﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymobilefirstcompletionv1]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(32),
	@dateType varchar(1),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32) = null,
	
	@summary bit = 0,
	@detail bit = 0,
	@statusCounts bit = 0,
	@lateMobileCounts bit = 0,
	@grandTotal bit = 0,
	@offset int = 0
AS
DECLARE @customer varchar(32) = @customerId
DECLARE @region varchar(16) = @regionid
DECLARE @district varchar(16) = @districtid
DECLARE @mobile varchar(32) = @mobileid
DECLARE @date varchar(1) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_statusCounts bit = @statusCounts
DECLARE @_lateMobileCounts bit = @lateMobileCounts
DECLARE @_grandTotal bit = @grandTotal
DECLARE @_summary bit = @summary
DECLARE @count BIGINT
DECLARE @nowUtc DATETIME = SYSUTCDATETIME()
if @date = '1' 
	SET @through = dateAdd(dy, 1, @throughDate)
ELSE
BEGIN
	SET @from = dbo.DateOnly(dateAdd(HH, @offset,  @from))
	SET @through = dbo.DateOnly(dateAdd(HH, @offset, @through))
END;
if @_detail = 1
	BEGIN
	if @output = 'summary'
		BEGIN
			SELECT 0;
			return;
		END;
	END;
if @_summary = 1
	BEGIN
	if @output = 'detailed'
		BEGIN
			SELECT 0;
			return;
		END
	END;
-- Late Ticket Detail - By WTB
WITH CompletedLate (customerid,regionid,districtid,membercode,mobileid,description,jobid,worktype,Transmit,Wtb,duedate,completiondt,origpriority,address,street,city,county,state,isproject,status,lateHours,lateMinutes,completionStatus, CompletedLate, OpenLate, AllLate, NotLate)
		AS
		(SELECT TOP 10000
			j.customerid,
			m.regionid,
			m.districtid,
			r.membercode,
			j.mobileid,
			m.description,
			j.jobid,
			j.worktype,
			CONVERT(datetime, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(j.duedateutc,occ.timezone, 1) as duedate,
			dbo.GetUTC(r.firstcompletiondateutc,occ.timezone, 1),
			j.origpriority,
			j.address, 
			j.street,
			j.city, 
			j.county, 
			j.state,
			j.isproject,
			j.status,
			(DATEDIFF(hour,j.duedateutc, ISNULL(r.firstcompletiondateutc, @nowUtc)) / 60) as lateHours,
			(DATEDIFF(minute,j.duedateutc, ISNULL(r.firstcompletiondateutc, @nowUtc)) % 60) AS lateMinutes,
			'Completed / Late' AS completionStatus,
			1 as CompletedLate,
			0 as OpenLate,
			1 as AllLate,
			0 as NotLate
		FROM request r
				join job j on
				r.customerid = j.customerid and
				r.jobid = j.jobid
				join mobile m on
				j.mobileid = m.mobileid and
				j.customerid = m.customerid
				join jobstatus js on
				j.customerid = js.customerid and
				j.jobid = js.jobid
				join ticketpriority tp on
				js.ticketpriorityid = tp.ticketpriorityid 
				join occ on occ.customerid = j.customerid and
				occ.occid = j.latestxmitsource
	
			WHERE 
				r.customerid = @customer
				AND j.status NOT IN ('R', 'X', 'U')
				--AND ((j.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (j.status != 'U'))
				AND	((@dateType = '1' AND j.duedateutc BETWEEN @from AND @through) OR @dateType = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))
				-- Grab only the records that are greater than the wtbdtdate and time
				AND (r.firstcompletiondateutc is not null and r.firstcompletiondateutc > j.duedateutc)
				-- If the completion date is null then check compared to the clients current date and time
				--OR  (r.firstcompletiondateutc IS NULL AND @nowUtc >  j.duedateutc))
				AND (@mobile = '0' OR m.mobileid = @mobile)
				AND (@district = '0' OR m.districtid = @district)
				AND (@region = '0' OR m.regionid = @region)
				AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
				AND (@projects = 'Yes' OR j.isproject = '0')
			),
		OpenLate (customerid,regionid,districtid,membercode,mobileid,description,jobid,worktype,Transmit,Wtb,duedate,completiondt,origpriority,address,street,city,county,state,isproject,status,lateHours,lateMinutes,completionStatus, CompletedLate, OpenLate, AllLate, NotLate)
		AS
		(SELECT TOP 10000
			j.customerid,
			m.regionid,
			m.districtid,
			r.membercode,
			j.mobileid,
			m.description,
			j.jobid,
			j.worktype,
			CONVERT(datetime, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(j.duedateutc,occ.timezone, 1) as duedate,
			dbo.GetUTC(r.firstcompletiondateutc,occ.timezone, 1),
			j.origpriority,
			j.address, 
			j.street,
			j.city, 
			j.county, 
			j.state,
			j.isproject,
			j.status,
			(DATEDIFF(hour,j.duedateutc, ISNULL(r.firstcompletiondateutc, @nowUtc)) / 60) as lateHours,
			(DATEDIFF(minute,j.duedateutc, ISNULL(r.firstcompletiondateutc, @nowUtc)) % 60) AS lateMinutes,
			'Open / Late' AS completionStatus,
			0 as CompletedLate,
			1 as OpenLate,
			1 as AllLate,
			0 as NotLate
		FROM request r
				join job j on
				r.customerid = j.customerid and
				r.jobid = j.jobid
				join mobile m on
				j.mobileid = m.mobileid and
				j.customerid = m.customerid
				join jobstatus js on
				j.customerid = js.customerid and
				j.jobid = js.jobid
				join ticketpriority tp on
				js.ticketpriorityid = tp.ticketpriorityid 
				join occ on occ.customerid = j.customerid and
				occ.occid = j.latestxmitsource
	
			WHERE 
				r.customerid = @customer
				AND j.status NOT IN ('R', 'X', 'U')
				--AND ((j.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (j.status != 'U'))
				AND	((@dateType = '1' AND j.duedateutc BETWEEN @from AND @through) OR @dateType = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))
				-- Grab only the records that are greater than the wtbdtdate and time
				-- If the completion date is null then check compared to the clients current date and time
				AND  (r.firstcompletiondateutc IS NULL AND @nowUtc >  j.duedateutc)
				AND (@mobile = '0' OR m.mobileid = @mobile)
				AND (@district = '0' OR m.districtid = @district)
				AND (@region = '0' OR m.regionid = @region)
				AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
				AND (@projects = 'Yes' OR j.isproject = '0')
			),
	AllTickets AS (
		SELECT * FROM CompletedLate
		UNION ALL
		SELECT * FROM OpenLate
	)
	SELECT * INTO #TEMPDetails FROM AllTickets;
if @_detail = 1
BEGIN
	SELECT * FROM #TEMPDetails ;
	--select mobileid, (jobid + membercode), count(*) from #TEMPDetails
	--group by mobileid, (jobid + membercode)
	--having count(*) > 1
	--select sum(OpenLate) as openLateCount, sum(CompletedLate) as completedLateCount from #TEMPDetails
END
-- Late Ticket Summary By - WTB Date
if @_summary = 1
BEGIN
	    select j.mobileid mobileid, m.description as description, (j.jobid + r.membercode) as k, sum(0) as AllLate, sum(0) avgHrs, sum(0) avgMins, count(*) TotalMobileCount into #temp 
		FROM request r
				join job j on
				r.customerid = j.customerid and
				r.jobid = j.jobid
				join mobile m on
				j.mobileid = m.mobileid and
				j.customerid = m.customerid
				join jobstatus js on
				j.customerid = js.customerid and
				j.jobid = js.jobid
				join ticketpriority tp on
				js.ticketpriorityid = tp.ticketpriorityid 
				join occ on occ.customerid = j.customerid and
				occ.occid = j.latestxmitsource
			WHERE 
				r.customerid = @customer
				AND j.status NOT IN ('R', 'X', 'U')
				--AND ((j.status = 'U' AND visitcommon.completiondt IS NOT NULL) OR (j.status != 'U'))
				AND	((@dateType = '1' AND j.duedateutc BETWEEN @from AND @through) OR @dateType = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))
				-- Grab only the records that are greater than the wtbdtdate and time
				-- If the completion date is null then check compared to the clients current date and time
				--AND  (r.firstcompletiondateutc IS NULL AND @nowUtc >  j.duedateutc)
				AND (@mobile = '0' OR m.mobileid = @mobile)
				AND (@district = '0' OR m.districtid = @district)
				AND (@region = '0' OR m.regionid = @region)
				AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
				AND (@projects = 'Yes' OR j.isproject = '0')
			group by j.mobileid, m.description,  (j.jobid + r.membercode);
		--select k, count(*) from #temp group by k having count(*) > 1;
		with summary as (
		SELECT	
		d.mobileid,
		m.description,
		AllLate = SUM(AllLate),
		sum(lateHours)/SUM(AllLate) as avgHrs, sum(lateMinutes)/SUM(AllLate) as avgMins, 0 as TotalMobileCount
		FROM
		 #TEMPDetails d
			join mobile m on
			m.customerid = @customer and m.mobileid = d.mobileid
		group by d.mobileid, m.description
		UNION ALL
		select mobileid,description, sum(AllLate) as LateCount, Sum(avgMins) as avgMins, sum(avgHrs) as avgHrs, sum(TotalMobileCount) as TotalMobileCount
		from #temp group by mobileid,description) --order by mobileid,description
		select mobileid,description, sum(AllLate) as LateCount, Sum(avgMins) as avgMins, sum(avgHrs) as avgHrs, sum(TotalMobileCount) as TotalMobileCount from summary 
		group by mobileid,description
		order by mobileid, description
 END
if @_statusCounts = 1
BEGIN
	select sum(OpenLate) as OpenLate, sum(CompletedLate) as CompletedLate from #TEMPDetails
END
IF @_lateMobileCounts = 1
BEGIN
	SELECT TOP 5 mobileid, SUM(AllLate) as totalLate
		FROM #TEMPDetails
			GROUP BY mobileid
			ORDER BY SUM(AllLate)  DESC
END