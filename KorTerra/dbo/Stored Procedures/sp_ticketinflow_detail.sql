﻿ 
CREATE PROCEDURE [dbo].[sp_ticketinflow_detail]
(
	@fromDate DATETIME2,
	@todate DATETIME2
)
AS
BEGIN
	SET NOCOUNT ON;
	WITH
	usher_end (customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, usherenddt)
	AS
	(
		SELECT customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, MAX(eventdt) AS usherenddt
		FROM appprofile
		WHERE workflow = 'Processing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		AND customerid IS NOT NULL
		AND eventdt BETWEEN @fromDate AND @todate
		GROUP BY customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier
	),
	usher_start (guid, identifier, usherstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS usherstartdt
		FROM appprofile
		WHERE workflow = 'Processing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	email_start (guid, identifier, emailstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS emailstartdt
		FROM appprofile
		WHERE workflow = 'Email Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	email_end (guid, identifier, emailenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS emailenddt
		FROM appprofile
		WHERE workflow = 'Email Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	ftp_start (guid, identifier, ftpstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS ftpstartdt
		FROM appprofile
		WHERE workflow = 'ftp Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	ftp_end (guid, identifier, ftpenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS ftpenddt
		FROM appprofile
		WHERE workflow = 'Ftp Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	occr_start (guid, identifier, occrstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS occrstartdt
		FROM appprofile
		WHERE workflow = 'Retrieving Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	occr_end (guid, identifier, occrenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS occrenddt
		FROM appprofile
		WHERE workflow = 'Retrieving Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	occp_start (guid, identifier, occpstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS occpstartdt
		FROM appprofile
		WHERE workflow = 'Parsing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	occp_end (guid, identifier, occpenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS occpenddt
		FROM appprofile
		WHERE workflow = 'Parsing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	start_ends (customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, 
			emailstartdt, emailenddt, ftpstartdt, ftpenddt, 
			occrstartdt, occrenddt, occpstartdt, occpenddt, usherstartdt, usherenddt)
	AS
	(
		SELECT ue.customerid, ue.jobid, ue.korterrapriority, ue.customerpriority, ue.occid, ue.guid, ue.identifier, 
			es.emailstartdt, ee.emailenddt, fs.ftpstartdt, fe.ftpenddt, 
			rs.occrstartdt, re.occrenddt, ps.occpstartdt, pe.occpenddt, us.usherstartdt, ue.usherenddt
		FROM usher_end ue
		LEFT OUTER JOIN usher_start us ON us.identifier = ue.identifier
		LEFT OUTER JOIN email_start es ON es.identifier = ue.identifier
		LEFT OUTER JOIN email_end ee ON ee.identifier = ue.identifier
		LEFT OUTER JOIN ftp_start fs ON fs.identifier = ue.identifier
		LEFT OUTER JOIN ftp_end fe ON fe.identifier = ue.identifier
		LEFT OUTER JOIN occr_start rs ON rs.identifier = ue.identifier
		LEFT OUTER JOIN occr_end re ON re.identifier = ue.identifier
		LEFT OUTER JOIN occp_start ps ON ps.identifier = ue.identifier
		LEFT OUTER JOIN occp_end pe ON pe.identifier = ue.identifier
	)
	SELECT customerid, korterrapriority, customerpriority, occid, jobid,
		CAST(usherenddt AS DATE) AS usherDate,
		dbo.fn_datediff_milliseconds(emailstartdt, emailenddt) AS [Milliseconds on Email Server],
		dbo.fn_datediff_milliseconds(ftpstartdt, ftpenddt) AS [Milliseconds on Ftp Server],
		dbo.fn_datediff_milliseconds(COALESCE(emailenddt, ftpenddt), occrstartdt) AS [Milliseconds in OCCR Queue],
		dbo.fn_datediff_milliseconds(occrstartdt, occrenddt) AS [Milliseconds in OCCR Processing],
		dbo.fn_datediff_milliseconds(occrenddt, occpstartdt) AS [Milliseconds in OCCP Queue],
		dbo.fn_datediff_milliseconds(occpstartdt, occpenddt) AS [Milliseconds in OCCP Processing],
		dbo.fn_datediff_milliseconds(occpenddt, usherstartdt) AS [Milliseconds in Usher Queue],
		dbo.fn_datediff_milliseconds(usherstartdt, usherenddt) AS [Milliseconds Usher Processing],
		dbo.fn_datediff_milliseconds(COALESCE(emailstartdt, ftpstartdt), usherenddt) AS [Milliseconds Total]
	FROM start_ends
END