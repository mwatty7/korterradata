﻿CREATE PROCEDURE [dbo].[sp_kt_reports_completionsummarybyregion]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30)
AS
BEGIN
	declare @endDate as datetime = CAST(@throughDate as datetime) + cast('23:59:59' as datetime);
	with tickets (region, district, mobile, ticketnumber, status, project, completed, cleared, marked, notcomplete, compstatus, completiondateutc)
	as
		(select r.regionid + IIF(r.description is null,'', ' - ' + r.description), 
		d.districtid+ IIF(d.description is null,'', ' - ' + d.description), 
		m.mobileid + IIF(m.description is null,'', ' - ' + m.description), 
		(rq.jobid + rq.membercode) as ticketnumber, j.status, j.isproject,
		CASE when status = 'C'
						THEN 1
						ELSE 0
					END as completed,
		CASE when v.compstatus = 'CLEARED'
						THEN 1
						ELSE 0
					END as cleared,
		CASE when v.compstatus = 'MARKED'
						THEN 1
						ELSE 0
					END as marked,
		CASE when v.compstatus = 'NOT COMPLETE'
						THEN 1
						ELSE 0
					END as notcomplete,
		v.compstatus, v.completiondateutc 
		from request rq
		OUTER APPLY (
		  SELECT TOP 1 vc.compstatus, vc.mobileid, vc.completiondateutc, vc.requesttype from visitcommon vc 
		  where vc.customerid = rq.customerid and
		  vc.jobid = rq.jobid  AND
		  vc.membercode = rq.membercode
		  order by vc.customerid, vc.jobid, completiondateutc desc
		) v
		join job j on
		rq.customerid = j.customerid and
		rq.jobid = j.jobid 
		join mobile m ON
		m.customerid = j.customerid AND
		m.mobileid = coalesce(v.mobileid, j.mobileid)
		join region r on 
		r.customerid = m.customerid and
		r.regionid = m.regionid
		join district d on 
		d.districtid = m.districtid and
		d.customerid = m.customerid and
		d.regionid = m.regionid
		where j.customerid = @customerId 
		and status not in('X', 'R', 'U')
		AND (@districtid = '0' OR m.districtid = @districtid)
		AND (@regionid = '0' OR m.regionid = @regionid)
		AND	j.latestxmitdtdate BETWEEN  @fromDate AND @endDate
	)
	select region, district, mobile, count(*) Recieved, sum(completed) Completed, sum(cleared) Cleared, sum(marked) Marked, sum(notcomplete) [Not Complete], sum(project) Projects
	from tickets
	group by region, district, mobile
	order by region, district, mobile
END