﻿CREATE PROCEDURE [dbo].[sp_move_herald_maint_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
    BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg VARCHAR(255) = NULL;
		EXEC [dbo].[sp_move_table] @tableName = 'emailnotify', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'herald', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'msggroup', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
	
		SET @SQLCommand = 'INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgtype SELECT * FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.msgtype WHERE msgtype NOT IN (SELECT msgtype FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.msgtype)';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
	
		EXEC [dbo].[sp_move_table] @tableName = 'pigeonhl', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'watch', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'CustomerScheduleTemplates', @sourceServer, @sourceDatabase, 2
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'ScheduleTemplateTimes', @sourceServer, @sourceDatabase, 4
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'HeraldNotifyGroup', @sourceServer, @sourceDatabase, 3
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'HeraldNotifyScheduleTemplate', @sourceServer, @sourceDatabase, 3
		if (@includeAudit = 1)
				EXEC sp_audit_column_count 'HeraldNotifyEmails', @sourceServer, @sourceDatabase, 2
		--POPULATE TABLES:
		--CustomerScheduleTemplates, ScheduleTemplateTimes, HeraldNotifyGroup, HeraldNotifyScheduleTemplate, HeraldNotifyEmails
		DECLARE @sqlCSTSTTHNG NVARCHAR(MAX);
		SET @sqlCSTSTTHNG = '
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.CustomerScheduleTemplates SELECT customerid, [name] FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.CustomerScheduleTemplates WHERE customerid = ''' + @customerid + '''
			CREATE TABLE #CSTLink (cst_id_old INT, cst_id_new INT, customerid VARCHAR(32), [name]     VARCHAR(64), lrn        INT IDENTITY NOT NULL);
			INSERT INTO #CSTLink (cst_id_old, customerid, [name]) SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.CustomerScheduleTemplates WHERE customerid = ''' + @customerid + ''';
			UPDATE #CSTLink 
			SET cst_id_new = t.cst_id
			FROM #CSTLink as ccl
			JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.CustomerScheduleTemplates t on t.[customerid] = ccl.[customerid] and t.[name] = ccl.[name];
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScheduleTemplateTimes(cst_id, [day], starttime, endtime)
			SELECT c.cst_id_new, [day], starttime, endtime FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScheduleTemplateTimes p JOIN #CSTLink c on c.cst_id_old = p.cst_id WHERE c.customerid = ''' + @customerid + ''';
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyGroup SELECT customerid, msggroupid, msgtype FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + '''
			CREATE TABLE  #HNGLink (hng_id_old INT, hng_id_new INT, customerid VARCHAR(32), msggroupid VARCHAR(16), msgtype VARCHAR(32), lrn INT IDENTITY NOT NULL);
			INSERT INTO #HNGLink (hng_id_old, customerid, msggroupid, msgtype) SELECT * FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup WHERE customerid = ''' + @customerid + ''';
			UPDATE #HNGLink 
			SET hng_id_new = hng.hng_id
			FROM #HNGLink as hngl
			JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyGroup hng on hng.[customerid] = hngl.[customerid] and hng.[msggroupid] = hngl.[msggroupid] and hng.[msgtype] = hngl.[msgtype];
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate(hng_id, cst_id, tplfilename)
			SELECT hngl.hng_id_new, cstl.cst_id_new, tplfilename FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate hnst JOIN #HNGLink hngl on hngl.hng_id_old = hnst.hng_id 
			JOIN #CSTLink  cstl ON cstl.cst_id_old = hnst.cst_id
			CREATE TABLE #HNGSTe 
			(
				hnst_id_old INT, 
				hnst_id_new INT, 
				hng_id_old INT, 
				hng_id_new INT, 
				cst_id_old INT, 
				cst_id_new INT, 
				emailaddress VARCHAR(320),
				tplfilename VARCHAR(255),
				lrn INT IDENTITY NOT NULL
			);
			INSERT INTO #HNGSTe
			(
				hnst_id_old, 
				hng_id_old, 
				cst_id_old,
				emailaddress,
				tplfilename
			) 
			SELECT 
				c.hnst_id, 
				b.hng_id, 
				b.cst_id,
				c.emailaddress,
				b.tplfilename
			FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyGroup a
			JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.HeraldNotifyScheduleTemplate b on a.hng_id = b.hng_id
			JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.heraldnotifyemails c on b.hnst_id = c.hnst_id
			WHERE a.customerid = ''' + @customerid + '''
			UPDATE #HNGSTe
			SET hng_id_new = a.hng_id_new
			FROM #HNGLink a
				JOIN #HNGSTe b on a.hng_id_old = b.hng_id_old
			UPDATE #HNGSTe
			SET cst_id_new = a.cst_id_new
			FROM #CSTLink a
				JOIN #HNGSTe b on a.cst_id_old = b.cst_id_old
			UPDATE #HNGSTe
			SET hnst_id_new = a.hnst_id
			FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate a
				JOIN #HNGSTe b on a.hng_id = b.hng_id_new  AND a.cst_id = b.cst_id_new AND  a.tplfilename = b.tplfilename
			UPDATE #HNGSTe
			SET hnst_id_new = a.hnst_id
			FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate a
				JOIN #HNGSTe b on a.hng_id = b.hng_id_new  AND a.cst_id = b.cst_id_new 
				WHERE b.tplfilename IS NULL AND b.hnst_id_new IS NULL
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.heraldnotifyemails 
			SELECT a.hnst_id, b.emailaddress FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.HeraldNotifyScheduleTemplate a JOIN #HNGSTe b on b.hnst_id_new = a.hnst_id
			DROP TABLE #CSTLink, #HNGLink, #HNGSTe
		';
		EXECUTE @return = sp_executesql @sqlCSTSTTHNG OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @sqlCSTSTTHNG + 'FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END;
		IF @includeAudit = 1
			EXEC sp_audit_move_herald_maint_autoincremented @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
    END TRY
    BEGIN CATCH 
		PRINT('MOVE ''HeraldMaintAutoIncremented'' FAILED Stored Procedure: ''sp_move_herald_maint_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
		select @error = ERROR_NUMBER(),
			@message = ERROR_MESSAGE(),
			@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
    END CATCH 
END
SET ANSI_NULLS ON