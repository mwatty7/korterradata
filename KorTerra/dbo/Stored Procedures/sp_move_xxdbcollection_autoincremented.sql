﻿-- =============================================
-- Description:	Move the XXDB Tables
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_xxdbcollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
		IF @includeAudit = 1
			EXEC sp_audit_column_count 'xxtemplate', @sourceServer, @sourceDatabase, 10
		EXEC [dbo].[sp_move_table] @tableName = 'XXgenericnotes', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'XXtermsaccept', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		
        SET @SQLCommand = N' 
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXtemplate (
				name, 
				description, 
				customerid, 
				template_type, 
				content, 
				creationdt, 
				modifiedby, 
				modifiedon,
				ishtml,
				json
			)
			SELECT 
				name, 
				description, 
				customerid, 
				template_type, 
				content, 
				creationdt, 
				modifiedby, 
				modifiedon,
				ishtml,
				json
			FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXtemplate WHERE customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            SET @errMsg = @SQLCommand + ' FAILED!'
            RAISERROR (@errMsg, 15, 10);
        END
		IF @includeAudit = 1
			EXEC sp_audit_moved_table 'xxtemplate', @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''XXDB Collection'' FAILED Stored Procedure: ''sp_move_xxdbcollection_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END