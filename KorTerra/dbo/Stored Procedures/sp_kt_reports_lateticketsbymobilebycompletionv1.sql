﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymobilebycompletionv1]
	@customerId varchar(32),
	@regionid varchar(16),
	@districtid varchar(16),
	@mobileid varchar(32),
	@dateType varchar(1),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32) = null,
	
	@summary bit = 0,
	@detail bit = 0,
	@statusCounts bit = 0,
	@lateMobileCounts bit = 0,
	@grandTotal bit = 0,
	@offset int = 0
AS
DECLARE @customer varchar(32) = @customerId
DECLARE @region varchar(16) = @regionid
DECLARE @district varchar(16) = @districtid
DECLARE @mobile varchar(32) = @mobileid
DECLARE @date varchar(1) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_statusCounts bit = @statusCounts
DECLARE @_lateMobileCounts bit = @lateMobileCounts
DECLARE @_grandTotal bit = @grandTotal
DECLARE @_summary bit = @summary
DECLARE @count BIGINT
DECLARE @nowUtc DATETIME = SYSUTCDATETIME()
if @date = '1' 
	SET @through = dateAdd(dy, 1, @throughDate)
ELSE
BEGIN
	SET @from = dbo.DateOnly(dateAdd(HH, @offset,  @from))
	SET @through = dbo.DateOnly(dateAdd(HH, @offset, @through))
END;
-- Late Ticket Detail - By WTB
		WITH CompletedLate (customerid,regionid,districtid,membercode,mobileid,description,jobid,worktype,Transmit,Wtb,duedate,completiondt,origpriority,
		address,street,city,county,state,isproject,status,lateHours,lateMinutes,CompletedLate, OpenLate, AllLate, completionStatus)
		AS
		(SELECT TOP 10000
			j.customerid,
			m.regionid,
			m.districtid,
			j.sendto,
			j.mobileid,
			m.description,
			vc.jobid,
			j.worktype,
			CONVERT(datetime, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(vc.duedateutc,occ.timezone, 1) as duedate,
			dbo.GetUTC(vc.completiondateutc,occ.timezone, 1) as completiondt,
			j.origpriority,
			j.address, 
			j.street,
			j.city, 
			j.county, 
			j.state,
			j.isproject,
			j.status,
			(DATEDIFF(minute,vc.duedateutc, ISNULL(vc.completiondateutc, @nowUtc)) / 60) as lateHours,
			(DATEDIFF(minute,vc.duedateutc, ISNULL(vc.completiondateutc, @nowUtc)) % 60) AS lateMinutes,
			1 as CompletedLate,
			0 as OpenLate,
			1 as AllLate,
			'Completed/Late' as completionStatus
	FROM visitcommon vc
			join job j on
			j.jobid = vc.jobid and
			j.customerid = vc.customerid
			join jobstatus js on
			j.customerid = js.customerid and
			j.jobid = js.jobid
			join ticketpriority tp on
			js.ticketpriorityid = tp.ticketpriorityid 
			join occ on occ.customerid = j.customerid and
			occ.occid = j.latestxmitsource
			join mobile m on 
			m.customerid = j.customerid 
			AND m.mobileid = j.mobileid
			WHERE 
					vc.customerid = @customer AND
					vc.islate = 1 AND
					-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
					((@date = '1' AND vc.duedateutc BETWEEN @from AND @through) OR
					(@date = '2' AND j.latestxmitdtdate BETWEEN @from AND @through)) AND
					-- Rule G: Filter out all cancelled / nlr tickets
					j.status NOT IN ('R', 'X','U') AND  
					vc.completiondateutc > vc.duedateutc
				-- Grab only the records that are greater than the wtbdtdate and time
				-- If the completion date is null then check compared to the clients current date and time
					AND (@mobile = '0' OR m.mobileid = @mobile)
					AND (@district = '0' OR m.districtid = @district)
					AND (@region = '0' OR m.regionid = @region)
					AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
					AND (@projects = 'Yes' OR j.isproject = '0')
			),
		OpenLate (customerid,regionid,districtid,membercode,mobileid,description,jobid,worktype,Transmit,Wtb,duedate,completiondt,origpriority,
		address,street,city,county,state,isproject,status,lateHours,lateMinutes,CompletedLate, OpenLate, AllLate, completionStatus)
		AS
		(SELECT TOP 10000
			j.customerid,
			m.regionid,
			m.districtid,
			j.sendto,
			j.mobileid,
			m.description,
			vc.jobid,
			j.worktype,
			CONVERT(datetime, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
			CONVERT(datetime, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
			dbo.GetUTC(j.duedateutc,occ.timezone, 1) as duedate,
			null as completiondt,
			j.origpriority,
			j.address, 
			j.street,
			j.city, 
			j.county, 
			j.state,
			j.isproject,
			j.status,
			(DATEDIFF(minute,j.duedateutc, @nowUtc) / 60) as lateHours,
			(DATEDIFF(minute,j.duedateutc, @nowUtc) % 60) AS lateMinutes,
			0 as CompletedLate,
			1 as OpenLate,
			1 as AllLate,
			'Open/Late' as completionStatus
			
		FROM visitreq vc
			join job j on
			j.jobid = vc.jobid and
			j.customerid = vc.customerid
			join jobstatus js on
			j.customerid = js.customerid and
			j.jobid = js.jobid
			join ticketpriority tp on
			js.ticketpriorityid = tp.ticketpriorityid 
			join occ on occ.customerid = j.customerid and
			occ.occid = j.latestxmitsource
			join mobile m on 
			m.customerid = j.customerid 
			AND m.mobileid = j.mobileid
			WHERE 
					vc.customerid = @customer AND
					vc.lastcompletiondtdate IS NULL AND
					-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
					((@date = '1' AND j.duedateutc BETWEEN @from AND @through) OR
					(@date = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))) AND
					-- Rule G: Filter out all cancelled / nlr tickets
					j.status NOT IN ('R', 'X', 'U') AND  
					@nowUtc > j.duedateutc
				-- Grab only the records that are greater than the wtbdtdate and time
				-- If the completion date is null then check compared to the clients current date and time
					AND (@mobile = '0' OR m.mobileid = @mobile)
					AND (@district = '0' OR m.districtid = @district)
					AND (@region = '0' OR m.regionid = @region)
					AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
					AND (@projects = 'Yes' OR j.isproject = '0')
			),
	AllLate AS (
		SELECT * FROM CompletedLate
		UNION ALL
		SELECT * FROM OpenLate
	)
	SELECT * INTO #TEMPDetails FROM AllLate;
if @_detail = 1
BEGIN
	SELECT * FROM #TEMPDetails;
END
IF @_lateMobileCounts = 1
	BEGIN
		SELECT TOP 5 mobileid, SUM(AllLate) as totalLate
		FROM #TEMPDetails
			GROUP BY mobileid
			ORDER BY SUM(AllLate)  DESC
	END
-- Late Ticket @_statusCounts - By WTB
IF @_statusCounts = 1 
	BEGIN
		SELECT
			SUM(OpenLate) as openLateCount,
			SUM(CompletedLate) as lateCount,
			SUM(CompletedLate + OpenLate) as grandTotal
			FROM #TEMPDetails
	END
if @_summary = 1
BEGIN
	SELECT 
	COUNT_BIG(d.jobid) as LateCount,
	SUM(d.CompletedLate) as CompletedLate,
	SUM(d.OpenLate) as OpenLate,
	(SUM(d.lateHours) / COUNT(d.jobid)) as avgHrs,
	(SUM(d.lateMinutes) / COUNT(d.jobid)) as avgMins,
	d.mobileid as mobileid,
	d.description as description,
	(select COUNT_BIG(*) from visitreq vr
			join job jb on vr.customerid = jb.customerid AND vr.jobid = jb.jobid
			join jobstatus js on
			jb.customerid = js.customerid and
			jb.jobid = js.jobid
			join ticketpriority tp on
			js.ticketpriorityid = tp.ticketpriorityid 
			join request rq on
			rq.jobid = jb.jobid and
			rq.customerid = jb.customerid
	     	left join mobile  on 
			mobile.customerid = jb.customerid 
			AND mobile.mobileid = jb.mobileid
			where 
				jb.customerid = @customer AND
				jb.status NOT IN ('R', 'X') AND
				((@date = '1' AND jb.duedateutc BETWEEN @from AND @through) OR (@date = '2' AND jb.latestxmitdtdate BETWEEN @from AND @through))
					AND (d.mobileid = jb.mobileid)
					AND (@mobile = '0' OR jb.mobileid = @mobile)
					AND (@district = '0' OR mobile.districtid = @district)
					AND (@region = '0' OR mobile.regionid = @region)
					AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
					AND (@projects = 'Yes' OR jb.isproject = '0')	
	) as TotalTicketCount
	FROM
		#TEMPDetails as d
	GROUP BY d.mobileid, d.description
END
DROP TABLE #TEMPDetails;
SET ANSI_NULLS ON
/****** Object:  StoredProcedure [dbo].[sp_kt_reports_lateticketsbymobilefirstcompletionv1]    Script Date: 10/16/2019 3:03:49 PM ******/
SET ANSI_NULLS ON