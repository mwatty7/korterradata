﻿-- =============================================
-- Description:	Make sure screening maint moved correctly
-- =============================================
CREATE PROCEDURE [dbo].[sp_audit_move_screeningrulemaintcollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @errMsg NVARCHAR(MAX);
	DECLARE @tableName VARCHAR(30) = 'ScreeningRuleset';
	DECLARE @ParmDefinition nvarchar(500);
	DECLARE @CountSQLQuery varchar(30);
	DECLARE @SQLCommand NVARCHAR(MAX);
	EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS ScreeningRuleassignmentCount
				FROM ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleset AS SR
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleassignment AS SRA ON SR.rulesetid = SRA.rulesetid
			WHERE SR.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS ScreeningRuleassignmentCount
				FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleset AS SR
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleassignment AS SRA ON SR.rulesetid = SRA.rulesetid
			WHERE SR.customerid = ''' + @customerId + ''')
					
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: ScreeningRuleassignment FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'ScreeningRuleassignment Records Match'
	END
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS ScreeningRuleCount
			FROM
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
			WHERE SRS.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS ScreeningRuleCount
			FROM
				['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
			WHERE SRS.customerid = ''' + @customerId + ''')
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: Screeningrule FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'Screeningrule Records Match'
	END
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS ScreeningRuleTypeCount
			FROM
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuletype AS SRT ON SR.ruletypeid = SRT.ruletypeid
			WHERE SRS.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS ScreeningRuleTypeCount
			FROM
				['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuletype AS SRT ON SR.ruletypeid = SRT.ruletypeid
			WHERE SRS.customerid = ''' + @customerId + ''')
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: ScreeningRuletype FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'ScreeningRuletype Records Match'
	END
	SET @SQLCommand = N'
		SELECT @result = ABS(
			(SELECT
				COUNT(*) AS ScreeningRuleValueCount
			FROM
				['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
				JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.ScreeningRulevalue AS SRV ON SR.ruleid = SRV.ruleid
			WHERE SRS.customerid = ''' + @customerId + ''') -
			(SELECT
				COUNT(*) AS ScreeningRuleValueCount
			FROM
				['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRuleset AS SRS
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRule AS SR ON SRS.rulesetid = SR.rulesetid
				JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.ScreeningRulevalue AS SRV ON SR.ruleid = SRV.ruleid
			WHERE SRS.customerid = ''' + @customerId + ''')
	)';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
	SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
	IF @CountSQLQuery <> 0
	BEGIN
		PRINT @SQLCommand + ' FAILED!'
		SET @errMsg = 'TABLE: ScreeningRulevalue FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
		RAISERROR (@errMsg, 15, 10)
	END
	ELSE IF @CountSQLQuery = 0
	BEGIN 
		PRINT 'ScreeningRulevalue Records Match'
	END
END