﻿
CREATE PROCEDURE [dbo].[sp_remove_customer_test]
    @customerid VARCHAR(32) = NULL,
    @server VARCHAR(100) = NULL,
    @database VARCHAR(50) = NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @return Int;
    select @return = count(*)
    from sys.servers
    where name = @server;
    IF @return = 0
   BEGIN
        PRINT 'Error! Source Server Must be linked'
        RETURN -1
    END
 --   BEGIN
	--	--SELECT CAST (@return AS INT);
	--	SET NOCOUNT ON;
	--	DECLARE @return1 VARCHAR(100);
	--	SET @SQLCommand = N'SELECT * FROM [' + @server + '].'  + @database + '.dbo.customer WHERE customerid = ''' + @customerid + '''';
 --       ---EXEC @return1 = sp_executesql @SQLCommand OUTPUT
	--	SELECT @return1 = 'SELECT * FROM [' + @server + '].'  + @database + '.dbo.customer WHERE customerid = ''' + @customerid + '''';
	--	PRINT @return1
	--	IF @return1 <> NULL BEGIN
 --           PRINT @SQLCommand + ' SUCCEED! Customer does Exist. Good!'
 --           RETURN 1
 --       END
	--	ELSE BEGIN
 --           PRINT @SQLCommand + ' FAILED! Customer does NOT exist.  Ouch!!!'
 --           RETURN -1
	--	END
	--END

	DECLARE @errMsg NVARCHAR(MAX) = NULL;
	DECLARE @sqlcheckSourceCustomer  NVARCHAR(MAX);
    DECLARE @tableName NVARCHAR(80)
    SET @tableName = 'customer';
    SET @sqlcheckSourceCustomer = 'SELECT customerid, ''customer'' FROM ['+ @server + '].'+ @database + '.dbo.customer WHERE customerid = ''' + @customerid + ''' ';
    DECLARE @checkSourceResults  TABLE([customerid] VARCHAR(50),
        [table] VARCHAR (80));
    INSERT @checkSourceResults
    EXECUTE(@sqlcheckSourceCustomer)
    IF NOT EXISTS (SELECT *
    FROM @checkSourceResults
    WHERE customerid = @customerId and [table] = 'customer')
    BEGIN
        SET @errMsg = 'Customer does NOT exist within the Source Database! Please verify you have not already deleted customer.';
        RAISERROR (@errMsg, 15, 10);
    END;

END