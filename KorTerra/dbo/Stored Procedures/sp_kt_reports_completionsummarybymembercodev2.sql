﻿CREATE PROCEDURE [dbo].[sp_kt_reports_completionsummarybymembercodev2]
	@customerId varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeMarked varchar(16),
	@includeNotCompleted varchar(16),
	@includeClear varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = 'WITH data (customerid,jobid,Transmit, duedate, origpriority,address,street,city, county, state,status,operatorid,operatordescription,remarks,completiondt,mobileid,mobiledescription,membercode,memberid,membercodedescription,
					faccode1,faccode2,faccode3,faccode4,faccode5,faccode6,actcode1,actcode2,actcode3,actcode4,actcode5,actcode6,workperformed1,workperformed2,workperformed3,workperformed4,workperformed5,workperformed6,reasonid,compstatus,worktype,
					facdescription1, facdescription2, facdescription3, facdescription4, facdescription5,facdescription6)
				AS (SELECT 
					visitcommon.customerid,
					visitcommon.jobid,
					CONVERT(VARCHAR, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
					dbo.GetUTC(job.duedateutc, occ.timezone, 1) as duedate, 
					job.origpriority,
					job.address, 
					job.street,
					job.city, 
					job.county, 
					job.state,
					job.status,
					visitcommon.operatorid,
					operator.description AS operatordescription,
					visitcommon.remarks,
					visitcommon.completiondt,
					visitcommon.mobileid,
					mobile.description as mobiledescription,
					memcode.membercode, 
					memcode.memberid,
					memcode.description as membercodedescription,
					visitdata.faccode1,
					visitdata.faccode2,
					visitdata.faccode3,
					visitdata.faccode4,
					visitdata.faccode5,
					visitdata.faccode6,
					visitdata.actcode1,
					visitdata.actcode2,
					visitdata.actcode3,
					visitdata.actcode4,
					visitdata.actcode5,
					visitdata.actcode6,
					visitdata.workperformed1,
					visitdata.workperformed2,
					visitdata.workperformed3,
					visitdata.workperformed4,
					visitdata.workperformed5,
					visitdata.workperformed6,
					visitcommon.reasonid,
					visitcommon.compstatus,
					job.worktype,
					visitdata.facdescription1, 
					visitdata.facdescription2, 
					visitdata.facdescription3, 
                    visitdata.facdescription4, 
					visitdata.facdescription5, 
					visitdata.facdescription6
				FROM visitcommon
				LEFT OUTER JOIN job on job.customerid = visitcommon.customerid AND job.jobid = visitcommon.jobid
				LEFT OUTER JOIN memcode on memcode.customerid = visitcommon.customerid AND memcode.membercode = visitcommon.membercode
				LEFT OUTER JOIN mobile on mobile.customerid = visitcommon.customerid AND mobile.mobileid = visitcommon.mobileid
				LEFT OUTER JOIN operator on operator.customerid = visitcommon.customerid AND operator.operatorid = visitcommon.operatorid
				JOIN occ on
				occ.occid = job.latestxmitsource and
				occ.customerid = job.customerid
				OUTER APPLY (
					SELECT
						*
					FROM (
						SELECT
							*
						FROM
							visitdata
						WHERE
							visitdata.customerid = visitcommon.customerid AND
							visitdata.jobid = visitcommon.jobid AND
							visitdata.creationdt = visitcommon.creationdt AND
							visitdata.membercode = visitcommon.membercode AND
							visitdata.requesttype = visitcommon.requesttype
					) as s
					PIVOT (
						MAX(value)
						FOR [fieldid] IN (faccode1, faccode2, faccode3, 
						faccode4, faccode5, faccode6, facdescription1, facdescription2, facdescription3, facdescription4, facdescription5, facdescription6, actcode1,actcode2,
						actcode3, actcode4, actcode5, actcode6, workperformed1,
						workperformed2, workperformed3, workperformed4,
						workperformed5, workperformed6)
					) AS pvt
				) visitdata
				WHERE
					visitcommon.customerid = ''' + @customerId + '''
	'
	-- Report Options
	If( @includeMarked = 'No')
		Set @Command = @Command + ' AND visitcommon.compstatus <> ''MARKED'''
	If( @includeClear = 'No')
		Set @Command = @Command + ' AND visitcommon.compstatus <> ''CLEARED'''
	if( @includeNotCompleted = 'No' )
		Set @Command = @Command + ' AND visitcommon.compstatus <> ''NOT COMPLETE'''
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(visitcommon.completiondt as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(visitcommon.completiondt as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND dbo.DateOnly(dbo.GetUTC(job.duedateutc, occ.timezone, 1)) between ''' + @fromDate  + ''' AND ''' + @throughDate + ''''
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(job.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	-- Member / Member Code
	If( @memberid <> '0' )
	   Set @Command = @Command + ' AND (memcode.memberid = ''' + @memberid + ''') '
	If( @membercode <> '0' )
	   Set @Command = @Command + ' AND (memcode.membercode = ''' + @membercode + ''') '
	-- End Member / Member Code
	Set @Command = @Command + ') select * from data ORDER BY memberid, membercode, jobid, completiondt'
	--select @Command;
	Execute SP_ExecuteSQL  @Command
END