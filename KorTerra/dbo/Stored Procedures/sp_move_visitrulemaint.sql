﻿-- =============================================
-- Description:	Move the visitrule maint tables from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_visitrulemaint]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
		
		EXEC [dbo].[sp_move_table] @tableName = 'visitrule', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		
				
        SET @SQLCommand = N' 
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitaction 
				SELECT 
					s1.visitactionid, 
					s1.customerid, 
					s1.actiontype, 
					s1.actiontemplate, 
					(
						SELECT
							XXTarget.template_id
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.XXTemplate AS XXTarget
							JOIN ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.XXTemplate AS XXSource ON  XXSource.customerid = XXTarget.customerid AND XXSource.name = XXTarget.name
						WHERE 
							XXSource.template_id = s1.templateid
					),
					s1.description 
				FROM 
					['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitaction s1
				WHERE
					s1.customerid = ''' + @customerid + '''
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
		SET @SQLCommand = N' 
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitruleaction 
			SELECT 
				* 
			FROM 
				['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.visitruleaction 
			WHERE 
				visitactionid IN (
					SELECT 
						visitactionid 
					FROM 
						['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitaction 
					WHERE customerid = ''' + @customerid + '''
				)
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
		IF (@includeAudit = 1)
		BEGIN
			DECLARE @ParmDefinition nvarchar(500);
			DECLARE @CountSQLQuery varchar(30);
			EXEC sp_audit_moved_table 'visitrule', @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
			EXEC sp_audit_moved_table 'visitaction', @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
			SET @SQLCommand = N'
				SELECT @result = ABS(
					(SELECT 
						COUNT(*) 
					FROM 
						['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitrule AS VR
						JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.visitruleaction AS VRA ON VR.visitruleid = VRA.visitruleid
					WHERE 
						customerid = ''' + @customerId + ''') - 
					(SELECT 
						COUNT(*) 
					FROM 
						['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.visitrule AS VR
						JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.visitruleaction AS VRA ON VR.visitruleid = VRA.visitruleid
					WHERE 
						customerid = ''' + @customerId + ''')
					
			)';
			SET @ParmDefinition = N'@result varchar(30) OUTPUT';
			EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
			SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
			IF @CountSQLQuery <> 0
			BEGIN
				PRINT @SQLCommand + ' FAILED!'
				SET @errMsg = 'TABLE: visitruleaction FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
				RAISERROR (@errMsg, 15, 10)
			END
			ELSE IF @CountSQLQuery = 0
			BEGIN 
				PRINT 'visitruleaction Records Match'
			END
		END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''Visit Rule Maint'' FAILED Stored Procedure: ''sp_move_visitrulemaint''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END