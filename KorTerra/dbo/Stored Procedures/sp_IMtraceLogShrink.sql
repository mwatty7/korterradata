﻿
CREATE PROCEDURE [dbo].[sp_IMtraceLogShrink]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @daysToRetain VARCHAR(MAX);
	DECLARE @recordsToRetain VARCHAR(MAX);
	DECLARE @maxDiskSpace VARCHAR(MAX);
	
	DECLARE @usedDiskSpace FLOAT;
	DECLARE @usedRows BIGINT;
	DECLARE @rowsKeep BIGINT;
	-- Load the cutoffs from the config table
	-- using coalesce around a subselect allows us to set a default, if the
	-- record doesn't exist		
	SELECT @daysToRetain = COALESCE(
		(SELECT value FROM IMconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'DAYSTORETAIN'), 30)
	;
		
	SELECT @maxDiskSpace = COALESCE(
		(SELECT value FROM IMconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'MAXDISKSPACE'), 128)
	;
		
	SELECT @recordsToRetain = COALESCE(
		(SELECT value FROM IMconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'RECORDSTORETAIN'), 100000)
	;
	--------------------
	-- delete if there are too many records
	--------------------
	
	DELETE FROM IMtracelog
	WHERE logid < (
		SELECT MAX(logid) - @recordsToRetain
		FROM IMtracelog
	)
	;
	--------------------
	-- delete if there are too old records
	--------------------
	
	--DELETE FROM IMtracelog
	--WHERE logdt <= DATEADD(DAY, - CAST(@daysToRetain AS INT), CURRENT_TIMESTAMP)
	--;
	--------------------
	-- delete if we're using too much disk space
	--------------------
	
	-- sp_spaceused doesn't return it's data in a way that is accessible from
	-- within a stored procedure, so we pull it's data into a store procedure
	CREATE TABLE #tblResults
	(
		name NVARCHAR(20),
		rows INT,
		reserved VARCHAR(18),
		reserved_int INT DEFAULT(0),
		data VARCHAR(18),
		data_int INT DEFAULT(0),
		index_size VARCHAR(18),
		index_size_int INT DEFAULT(0),
		unused VARCHAR(18),
		unused_int INT DEFAULT(0)
	)
	;
	INSERT INTO #tblResults
	(name, rows, reserved, data, index_size, unused)
	EXEC sp_spaceused 'IMTraceLog'
	;
	-- We populate the _int fields by stripping off the trailing ' KB'
	UPDATE #tblResults 
	SET reserved_int = CAST(SUBSTRING(reserved, 1, CHARINDEX(' ', reserved)) AS INT),
		data_int = CAST(SUBSTRING(data, 1, CHARINDEX(' ', data)) AS INT),
		index_size_int = CAST(SUBSTRING(index_size, 1, CHARINDEX(' ', index_size)) AS INT),
		unused_int = CAST(SUBSTRING(unused, 1, CHARINDEX(' ', unused)) AS INT)
	;
	SELECT @usedRows = rows,  @usedDiskSpace = data_int / 1024.0
	FROM #tblResults
	;
	SELECT @rowsKeep = (@maxDiskSpace / @usedDiskSpace ) * @usedRows
	;
	DELETE FROM IMtracelog
	WHERE logid < (
		SELECT MAX(logid) - @rowsKeep
		FROM IMtracelog
	)
	;
END