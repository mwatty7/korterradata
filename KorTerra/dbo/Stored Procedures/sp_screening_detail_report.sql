﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_screening_detail_report] 
--DROP PROCEDURE [sp_screening_detail_report_dates]
	-- Add the parameters for the stored procedure here
	@datefrom date,
	@datethrough date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


CREATE TABLE #detail

(
 customerid             VARCHAR(150), 
 memberid               VARCHAR(30), 
 operatorid             VARCHAR(50), 
 membercode             VARCHAR(50),
 creationdt				DATETIME2, 
 completiondt           DATETIME2, 
 completionstatus       VARCHAR(150), 
 auto_conflict_ticket   VARCHAR(150), 
 manual_conflict_ticket VARCHAR(150), 
 auto_cleared_ticket    VARCHAR(150), 
 manual_cleared_ticket  VARCHAR(150), 
 void_ticket            VARCHAR(150)
);

--select * from #detail
INSERT INTO #detail
(customerid, 
 memberid, 
 membercode, 
 creationdt,
 completiondt, 
 completionstatus, 
 operatorid
)
       SELECT vc.customerid, 
              memcode.memberid, 
              vc.membercode,
			  vc.creationdt, 
              vc.completiondt,
			  vc. 
              compstatus, 
              operatorid
       FROM visitcommon vc
            JOIN memcode ON memcode.membercode = vc.membercode
       WHERE memcode.memberid IS NOT NULL
             AND memcode.memberid <> ''
             AND vc.compstatus IN('CLEARED', 'NOT COMPLETE') 
			 AND vc.completiondt = (SELECT  MAX(vc1.completiondt) FROM visitcommon vc1 WHERE vc.jobid = vc1.jobid)
			 AND cast(completiondt as date) between @datefrom and @datethrough	
			 ORDER BY vc.jobid;




--UPDATE: Auto-Conflict Tickets
UPDATE #detail
  SET 
      auto_conflict_ticket =
(
    SELECT jobid
    FROM visitcommon vc
         JOIN memcode mc ON mc.membercode = vc.membercode
    WHERE #detail.operatorid = 'ContractScreen'
          AND mc.memberid = #detail.memberid
          AND vc.customerid = #detail.customerid
		  AND vc.creationdt = #detail.creationdt
          AND vc.completiondt = #detail.completiondt
          AND vc.compstatus = 'NOT COMPLETE'
);

--UPDATE: Manual Conflict Tickets 
UPDATE #detail
  SET 
      manual_conflict_ticket =
(
    SELECT 
           jobid
    FROM visitcommon vc
         JOIN memcode mc ON mc.membercode = vc.membercode
    WHERE #detail.operatorid = vc.operatorid
          AND #detail.completionstatus = 'NOT COMPLETE'
          AND #detail.operatorid <> 'ContractScreen'
          AND mc.memberid = #detail.memberid
          AND vc.customerid = #detail.customerid
		  AND vc.creationdt = #detail.creationdt
          AND vc.completiondt = #detail.completiondt
);

--UPDATE: Auto-Clear Tickets
UPDATE #detail
  SET 
      auto_cleared_ticket =
(
    SELECT jobid
    FROM visitcommon vc
         JOIN memcode mc ON mc.membercode = vc.membercode
    WHERE #detail.operatorid = 'ContractScreen'
          AND mc.memberid = #detail.memberid
          AND vc.customerid = #detail.customerid
		  AND vc.creationdt = #detail.creationdt
          AND vc.completiondt = #detail.completiondt
          AND vc.compstatus = 'CLEARED'
);

--UPDATE: Manual Clear Tickets 
UPDATE #detail
  SET 
      manual_cleared_ticket =
(
    SELECT DISTINCT 
           jobid
    FROM visitcommon vc
         JOIN memcode mc ON mc.membercode = vc.membercode
    WHERE #detail.operatorid = vc.operatorid
          AND #detail.completionstatus = 'CLEARED'
          AND #detail.operatorid <> 'ContractScreen'
          AND mc.memberid = #detail.memberid
          AND vc.customerid = #detail.customerid
          AND vc.completiondt = #detail.completiondt
);

--UPDATE: Void Tickets (Cancels) 
UPDATE #detail
  SET 
      void_ticket =
(
    SELECT DISTINCT 
           vc.jobid
    FROM visitcommon vc
         JOIN memcode mc ON mc.membercode = vc.membercode
         JOIN job j ON j.jobid = vc.jobid
    WHERE #detail.operatorid = vc.operatorid
          AND #detail.completionstatus = vc.compstatus
          AND mc.memberid = #detail.memberid
          AND vc.customerid = #detail.customerid
		  AND vc.creationdt = #detail.creationdt
          AND vc.completiondt = #detail.completiondt
          AND j.status = 'X'
);

SELECT * FROM #Detail

SELECT COUNT(*) AS auto_conflict_ticket_total FROM #DETAIL WHERE auto_conflict_ticket IS NOT NULL; 
SELECT COUNT(*) AS manual_conflict_ticket_total FROM #DETAIL WHERE manual_conflict_ticket IS NOT NULL;
SELECT COUNT(*) AS manual_cleared_ticket_total FROM #DETAIL WHERE manual_cleared_ticket IS NOT NULL;
SELECT COUNT(*) AS auto_cleared_ticket_total FROM #DETAIL WHERE auto_cleared_ticket IS NOT NULL;
SELECT COUNT(*) AS cancel_tickets_total FROM #DETAIL WHERE void_ticket IS NOT NULL;

SELECT 
(SELECT COUNT(*) FROM #DETAIL WHERE auto_conflict_ticket IS NOT NULL) +
(SELECT COUNT(*) FROM #DETAIL WHERE manual_conflict_ticket IS NOT NULL) + 
(SELECT COUNT(*) FROM #DETAIL WHERE auto_cleared_ticket IS NOT NULL) +
(SELECT COUNT(*) FROM #DETAIL WHERE manual_cleared_ticket IS NOT NULL) + 
(SELECT COUNT(*) FROM #DETAIL WHERE void_ticket IS NOT NULL) as TotalTicketCount


--SELECT COUNT(*) FROM #Detail
DROP TABLE  #detail


END