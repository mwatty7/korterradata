﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymonthemergencyonly]
	@customerId varchar(32),
	@startdate date,
	@enddate date
AS
BEGIN
declare  
	@edatetime as datetime = CAST(@enddate as datetime) + cast('23:59:59' as datetime);
create table #pastduereport (
	region varchar(50),
	district varchar(50),
	timezone varchar(50),
	code varchar(50),
	mobile varchar(50),
	year int,
	month_name varchar(15),
	month_number tinyint,
	completiondtutc datetime,
	duedateutc datetime,
	xmitdate datetime,
	ontime tinyint,
	late tinyint,
	timeframe tinyint,
	mo3ontime tinyint,
	mo3late tinyint,
	mo6ontime tinyint,
	mo6late tinyint,
	mo12ontime tinyint,
	mo12late tinyint
);
/*Get the base data needed for the report*/
insert into #pastduereport 
select 
r.regionid, 
d.districtid,
o.timezone,
req.membercode,
mobileid = case when mob.description is null then j.mobileid else j.mobileid + ' - ' + mob.description end,
DATEPART(yy, j.latestxmitdtdate),
DATENAME(month, j.latestxmitdtdate),
DATEPART(mm, j.latestxmitdtdate), 
dbo.GetUTC(req.firstcompletiondtdate + req.firstcompletiondttime, o.timezone, -1),
j.duedateutc,
j.latestxmitdtdate,
0, 0, 
timeframe = case 
    WHEN @enddate <= DATEADD(MONTH,  3, j.latestxmitdtdate) THEN 3
      WHEN  @enddate <= DATEADD(MONTH,  6, j.latestxmitdtdate) THEN 6
      WHEN @enddate <= DATEADD(MONTH, 12, j.latestxmitdtdate) THEN 12
      ELSE 0
end,
0, 0, 0, 0, 0, 0
from request req
 join job j on
	req.customerid = j.customerid and
	req.jobid = j.jobid
 join mobile mob on 
	j.mobileid = mob.mobileid and
	j.customerid = mob.customerid
 join district d on
	mob.customerid = d.customerid and
	mob.districtid = d.districtid
 join region r on
	d.customerid = r.customerid and
	d.regionid = r.regionid
 join memcode mem on 
	req.membercode = mem.membercode and
	req.customerid = mem.customerid
 join occ o on
	mem.occid = o.occid and
	mem.customerid = o.customerid
where req.customerid = @customerid and j.latestxmitdtdate between @startdate and @edatetime  and j.priority = 'EMERGENCY' and j.status not in ('X','R') and (j.status = 'U' AND req.firstcompletiondtdate IS NOT NULL OR j.status != 'U'); 
update #pastduereport set 
ontime = 1
where (completiondtutc is not null and completiondtutc <= duedateutc) or (completiondtutc is null and getutcdate() < duedateutc);
update #pastduereport set late = 1 where ontime = 0; 
update #pastduereport set mo3late = 1 where timeframe = 3 and late = 1;
update #pastduereport set mo3ontime = 1 where timeframe = 3 and late = 0;
update #pastduereport set mo6late = 1 where timeframe = 6 and late = 1;
update #pastduereport set mo6ontime = 1 where timeframe = 6 and late = 0;
update #pastduereport set mo12late = 1 where timeframe = 12 and late = 1;
update #pastduereport set mo12ontime = 1 where timeframe = 12 and late = 0;
update #pastduereport set mo6late = mo6late + mo3late;
update #pastduereport set mo6ontime = mo6ontime + mo3ontime;
update #pastduereport set mo12late = mo12late + mo6late;
update #pastduereport set mo12ontime = mo12ontime + mo6ontime;
select region, district, code, year, month_number, month_name, sum(ontime + late) as [Total Tickes], sum(late) as [Late Tickets],
 sum(mo3late + mo3ontime) as month3total, sum(mo3late) as month3late, 
 sum(mo6late + mo6ontime) as month6total, sum(mo6late) as month6late ,
 sum(mo12late + mo12ontime) as month12total, sum(mo12late) as month12late
from #pastduereport
group by region, district, code, year, month_number, month_name
order by region, district, code, year, month_number, month_name;
drop table #pastduereport;
END
SET ANSI_NULLS ON