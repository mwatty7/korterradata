﻿

CREATE PROCEDURE [dbo].[sp_HeraldMessenger_nearlate2]
	@customerid VARCHAR(32),
	@heraldmessengerid VARCHAR(16),
	@sp_param1 VARCHAR(64),
	@sp_param2 VARCHAR(64),
	@sp_param3 VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @intervalSeconds INT;
	SET @intervalSeconds = 60 * CAST(@sp_param1 AS INT);

	CREATE TABLE #UnmessagedJobs
	(
		jobid varchar(16) NOT NULL,
		timezone VARCHAR(128) NULL,
		wtbdt DATETIME2 NULL
	);
	
	CREATE TABLE #AlertJobs
	(
		jobid varchar(16) NOT NULL
	);
	
	DECLARE @customerClause VARCHAR(80);
	SET @customerClause = '';
	IF (@customerid <> 'ALL' AND @customerid <> '' AND @customerid <> ' ')
		SET @customerClause = ' AND j.customerid = ''' + @customerid + '''';
		
	DECLARE @selectStmt NVARCHAR(MAX);
	SET @selectStmt = '
	SELECT DISTINCT j.jobid, o.timezone, (j.wtbdtdate + j.wtbdttime) 
		FROM job j
	LEFT OUTER JOIN occ o
		ON o.customerid = j.customerid
		AND o.occid = j.latestxmitsource
	LEFT OUTER JOIN visitreq v
		ON v.customerid = j.customerid
		AND v.jobid = j.jobid
	LEFT OUTER JOIN visitcommon vc
		ON vc.customerid = v.customerid
		AND vc.jobid = v.jobid
		AND vc.membercode = v.membercode
		AND vc.requesttype = v.requesttype
	LEFT OUTER JOIN messagedjob m
		ON m.customerid = j.customerid
		AND m.jobid = j.jobid
		AND m.heraldmessengerid = ''' + @heraldmessengerid + '''';
		
	SET @selectStmt = @selectStmt + ' WHERE j.status in (''A'', ''N'')';
	SET @selectStmt = @selectStmt + @customerClause;
	SET @selectStmt = @selectStmt + '
		AND vv.membercode IS NULL
		AND m.jobid IS NULL';

	DECLARE @InsertStmt NVARCHAR(MAX)	
	SET @InsertStmt = 'INSERT INTO #UnmessagedJobs ';
	SET @InsertStmt = @InsertStmt + @selectStmt;
	EXECUTE SP_ExecuteSQL @InsertStmt;
	
	DECLARE @UpdateStmt NVARCHAR(MAX)
	SET @UpdateStmt = 'UPDATE #UnmessagedJobs SET wtbdt = DATEADD(HOUR, +5, wtbdt) WHERE timezone = ''HAWAIIAN''';
	EXECUTE SP_ExecuteSQL @UpdateStmt;
	SET @UpdateStmt = 'UPDATE #UnmessagedJobs SET wtbdt = DATEADD(HOUR, +3, wtbdt) WHERE timezone = ''ALASKAN''';
	EXECUTE SP_ExecuteSQL @UpdateStmt;
	SET @UpdateStmt = 'UPDATE #UnmessagedJobs SET wtbdt = DATEADD(HOUR, +2, wtbdt) WHERE timezone = ''PACIFIC''';
	EXECUTE SP_ExecuteSQL @UpdateStmt;
	SET @UpdateStmt = 'UPDATE #UnmessagedJobs SET wtbdt = DATEADD(HOUR, +1, wtbdt) WHERE timezone = ''MOUNTAIN''';
	EXECUTE SP_ExecuteSQL @UpdateStmt;	
	SET @UpdateStmt = 'UPDATE #UnmessagedJobs SET wtbdt = DATEADD(HOUR, -1, wtbdt) WHERE timezone = ''EASTERN''';
	EXECUTE SP_ExecuteSQL @UpdateStmt;
	
	SET @InsertStmt = 'INSERT INTO #AlertJobs SELECT DISTINCT jobid FROM #UnmessagedJobs ';
	SET @InsertStmt = @InsertStmt + '
		WHERE DATEDIFF(SS, GETDATE(), wtbdt) >= 0 
		AND DATEDIFF(SS, GETDATE(), wtbdt) <= ';

	SET @insertStmt = @insertStmt + CAST(@intervalSeconds AS VARCHAR) + ' ';
	
	EXECUTE SP_ExecuteSQL @InsertStmt;
	
	SET @selectStmt = 'SELECT DISTINCT jobid FROM #AlertJobs ';
	EXECUTE SP_ExecuteSQL @selectStmt;

END