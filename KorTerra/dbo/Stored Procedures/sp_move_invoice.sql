﻿-- =============================================
-- Description:	Move the Invoice collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_invoice]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		EXEC [dbo].[sp_move_table] @tableName = 'invoice', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ledger', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'ledgrem', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'deptbill', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		EXEC [dbo].[sp_move_table] @tableName = 'bbillstatus', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''InvoiceCollection'' FAILED Stored Procedure: ''sp_move_invoice''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END