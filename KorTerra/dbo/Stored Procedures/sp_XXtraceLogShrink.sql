﻿ 
CREATE PROCEDURE [dbo].[sp_XXtraceLogShrink]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @daysToRetain BIGINT;
	DECLARE @recordsToRetain BIGINT;
	DECLARE @maxDiskSpace BIGINT;
	
	DECLARE @usedDiskSpace FLOAT;
	DECLARE @usedRows BIGINT;

	DECLARE @rowsKeep BIGINT;

	--------------------
	-- Load the cutoffs from the config table
	--------------------

	-- using coalesce around a subselect allows us to set a default, if the
	-- record doesn't exist
	SELECT @daysToRetain = COALESCE(
		(SELECT CONVERT(BIGINT, value) FROM XXconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'DAYSTORETAIN'), 30)
	;
	PRINT 'daysToRetain = ' + CONVERT(VARCHAR, @daysToRetain)
		
	SELECT @maxDiskSpace = COALESCE(
		(SELECT CONVERT(BIGINT, value) FROM XXconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'MAXDISKSPACE'), 128)
	;
	PRINT 'maxDiskSpace = ' + CONVERT(VARCHAR, @maxDiskSpace)
		
	SELECT @recordsToRetain = COALESCE(
		(SELECT CONVERT(BIGINT, value) FROM XXconfig
		WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
		AND name = 'RECORDSTORETAIN'), 100000)
	;
	PRINT 'recordsToRetain = ' + CONVERT(VARCHAR, @recordsToRetain)

	--------------------
	-- delete if there are too many records
	--------------------
	
	DELETE FROM XXtracelog
	WHERE logid < (
		SELECT MAX(logid) - @recordsToRetain
		FROM XXtracelog
	)
	;

	--------------------
	-- delete if there are too old records
	--------------------
	
	DELETE FROM XXtracelog
	WHERE logdt <= DATEADD(DAY, - @daysToRetain, CURRENT_TIMESTAMP)
	;

	--------------------
	-- delete if we're using too much disk space
	--------------------
	
	-- sp_spaceused doesn't return it's data in a way that is accessible from
	-- within a stored procedure, so we pull it's data into temp table
	CREATE TABLE #tblResults
	(
		name NVARCHAR(20),
		rows BIGINT,
		reserved VARCHAR(18),
		reserved_int BIGINT DEFAULT(0),
		data VARCHAR(18),
		data_int BIGINT DEFAULT(0),
		index_size VARCHAR(18),
		index_size_int BIGINT DEFAULT(0),
		unused VARCHAR(18),
		unused_int BIGINT DEFAULT(0)
	)
	;

	INSERT INTO #tblResults
	(name, rows, reserved, data, index_size, unused)
	EXEC sp_spaceused 'XXtracelog'
	;

	-- We populate the _int fields by stripping off the trailing ' KB'
	UPDATE #tblResults 
	SET reserved_int = CAST(SUBSTRING(reserved, 1, CHARINDEX(' ', reserved)) AS BIGINT),
		data_int = CAST(SUBSTRING(data, 1, CHARINDEX(' ', data)) AS BIGINT),
		index_size_int = CAST(SUBSTRING(index_size, 1, CHARINDEX(' ', index_size)) AS BIGINT),
		unused_int = CAST(SUBSTRING(unused, 1, CHARINDEX(' ', unused)) AS BIGINT)
	;

	SELECT @usedRows = rows,  @usedDiskSpace = data_int / 1024.0
	FROM #tblResults
	;
	PRINT 'usedRows = ' + CONVERT(VARCHAR, @usedRows)
	PRINT 'usedDiskSpace = ' + CONVERT(VARCHAR, @usedDiskSpace)

	SELECT @rowsKeep = (@maxDiskSpace / ISNULL(NULLIF(@usedDiskSpace,0),1) ) * @usedRows
	;
	PRINT 'rowsKeep = ' + CONVERT(VARCHAR, @rowsKeep)

	DELETE FROM XXtracelog
	WHERE logid < (
		SELECT MAX(logid) - @rowsKeep
		FROM XXtracelog
	)
	;

END