﻿ 
CREATE PROCEDURE [dbo].[sp_ticketInflow_stats]
(
	@fromDate DATETIME2,
	@todate DATETIME2
)
AS
BEGIN
	SET NOCOUNT ON;
	WITH
	usher_end (customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, usherenddt)
	AS
	(
		SELECT customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, MAX(eventdt) AS usherenddt
		FROM appprofile
		WHERE workflow = 'Processing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		AND customerid IS NOT NULL
		AND eventdt BETWEEN @fromDate AND @todate
		GROUP BY customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier
	),
	usher_start (guid, identifier, usherstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS usherstartdt
		FROM appprofile
		WHERE workflow = 'Processing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	email_start (guid, identifier, emailstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS emailstartdt
		FROM appprofile
		WHERE workflow = 'Email Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	email_end (guid, identifier, emailenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS emailenddt
		FROM appprofile
		WHERE workflow = 'Email Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	ftp_start (guid, identifier, ftpstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS ftpstartdt
		FROM appprofile
		WHERE workflow = 'ftp Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	ftp_end (guid, identifier, ftpenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS ftpenddt
		FROM appprofile
		WHERE workflow = 'Ftp Receiver'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	occr_start (guid, identifier, occrstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS occrstartdt
		FROM appprofile
		WHERE workflow = 'Retrieving Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	occr_end (guid, identifier, occrenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS occrenddt
		FROM appprofile
		WHERE workflow = 'Retrieving Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	occp_start (guid, identifier, occpstartdt)
	AS
	(
		SELECT guid, identifier, MIN(eventdt) AS occpstartdt
		FROM appprofile
		WHERE workflow = 'Parsing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'START'
		GROUP BY guid, identifier
	),
	occp_end (guid, identifier, occpenddt)
	AS
	(
		SELECT guid, identifier, MAX(eventdt) AS occpenddt
		FROM appprofile
		WHERE workflow = 'Parsing Ticket'
		AND eventname = 'WORKFLOW'
		AND action = 'END'
		GROUP BY guid, identifier
	),
	start_ends (customerid, jobid, korterrapriority, customerpriority, occid, guid, identifier, 
			emailstartdt, emailenddt, ftpstartdt, ftpenddt, 
			occrstartdt, occrenddt, occpstartdt, occpenddt, usherstartdt, usherenddt)
	AS
	(
		SELECT ue.customerid, ue.jobid, ue.korterrapriority, ue.customerpriority, ue.occid, ue.guid, ue.identifier, 
			es.emailstartdt, ee.emailenddt, fs.ftpstartdt, fe.ftpenddt, 
			rs.occrstartdt, re.occrenddt, ps.occpstartdt, pe.occpenddt, us.usherstartdt, ue.usherenddt
		FROM usher_end ue
		LEFT OUTER JOIN usher_start us ON us.identifier = ue.identifier
		LEFT OUTER JOIN email_start es ON es.identifier = ue.identifier
		LEFT OUTER JOIN email_end ee ON ee.identifier = ue.identifier
		LEFT OUTER JOIN ftp_start fs ON fs.identifier = ue.identifier
		LEFT OUTER JOIN ftp_end fe ON fe.identifier = ue.identifier
		LEFT OUTER JOIN occr_start rs ON rs.identifier = ue.identifier
		LEFT OUTER JOIN occr_end re ON re.identifier = ue.identifier
		LEFT OUTER JOIN occp_start ps ON ps.identifier = ue.identifier
		LEFT OUTER JOIN occp_end pe ON pe.identifier = ue.identifier
	),
	elapsed(customerid, korterrapriority, customerpriority, occid, usherDate,
		email_elapsed_ms, ftp_elapsed_ms, occrq_elapsed_ms,
		occr_elapsed_ms, occpq_elapsed_ms, occp_elapsed_ms, 
		usherq_elapsed_ms, usher_elapsed_ms, total_elapsed_ms)
	AS
	(
		SELECT customerid, korterrapriority, customerpriority, occid,
			CAST(usherenddt AS DATE) AS usherDate,
			dbo.fn_datediff_milliseconds(emailstartdt, emailenddt) AS email_elapsed_ms,
			dbo.fn_datediff_milliseconds(ftpstartdt, ftpenddt) AS ftp_elapsed_ms,
			dbo.fn_datediff_milliseconds(COALESCE(emailenddt, ftpenddt), occrstartdt) AS occrq_elapsed_ms,
			dbo.fn_datediff_milliseconds(occrstartdt, occrenddt) AS occr_elapsed_ms,
			dbo.fn_datediff_milliseconds(occrenddt, occpstartdt) AS occpq_elapsed_ms,
			dbo.fn_datediff_milliseconds(occpstartdt, occpenddt) AS occp_elapsed_ms,
			dbo.fn_datediff_milliseconds(occpenddt, usherstartdt) AS usherq_elapsed_ms,
			dbo.fn_datediff_milliseconds(usherstartdt, usherenddt) AS usher_elapsed_ms,
			dbo.fn_datediff_milliseconds(COALESCE(emailstartdt, ftpstartdt), usherenddt) AS total_elapsed_ms
		FROM start_ends
	),
	stats(customerid, occid, korterrapriority, usherDate,
			email_min, email_avg, email_max,
			ftp_min, ftp_avg, ftp_max, 
			occrq_min, occrq_avg, occrq_max,
			occr_min, occr_avg, occr_max,
			occpq_min, occpq_avg, occpq_max, 
			occp_min, occp_avg, occp_max, 
			usherq_min, usherq_avg, usherq_max,
			usher_min, usher_avg, usher_max,
			total_min, total_avg, total_max, 
			count)
	AS
	(
		SELECT customerid, occid, korterrapriority, usherDate,
			MIN(email_elapsed_ms) AS email_min, AVG(email_elapsed_ms) AS email_avg, MAX(email_elapsed_ms) AS email_max,
			MIN(ftp_elapsed_ms) AS ftp_min, AVG(ftp_elapsed_ms) AS ftp_avg, MAX(ftp_elapsed_ms) AS ftp_max,
			MIN(occrq_elapsed_ms) AS occrq_min, AVG(occrq_elapsed_ms) AS occrq_avg, MAX(occrq_elapsed_ms) AS occrq_max,
			MIN(occr_elapsed_ms) AS occr_min, AVG(occr_elapsed_ms) AS occr_avg, MAX(occr_elapsed_ms) AS occr_max,
			MIN(occpq_elapsed_ms) AS occpq_min, AVG(occpq_elapsed_ms) AS occpq_avg, MAX(occpq_elapsed_ms) AS occpq_max,
			MIN(occp_elapsed_ms) AS occp_min, AVG(occp_elapsed_ms) AS occp_avg, MAX(occp_elapsed_ms) AS occp_max,
			MIN(usherq_elapsed_ms) AS usherq_min, AVG(usherq_elapsed_ms) AS usherq_avg, MAX(usherq_elapsed_ms) AS usherq_max,
			MIN(usher_elapsed_ms) AS usher_min, AVG(usher_elapsed_ms) AS usher_avg, MAX(usher_elapsed_ms) AS usher_max,
			MIN(total_elapsed_ms) AS total_min, AVG(total_elapsed_ms) AS total_avg, MAX(total_elapsed_ms) AS total_max,
			COUNT(*) AS count
		FROM elapsed
		GROUP BY customerid, occid, korterrapriority, usherDate
	)
	SELECT customerid, occid, korterrapriority, usherDate,
		[dbo].[fn_format_ms](email_min) AS [On Mail Server Min],
		[dbo].[fn_format_ms](email_avg) AS [On Mail Server Avg],
		[dbo].[fn_format_ms](email_max) AS [On Mail Server Max],
		[dbo].[fn_format_ms](ftp_min) AS [On Ftp Server Min],
		[dbo].[fn_format_ms](ftp_avg) AS [On Ftp Server Avg],
		[dbo].[fn_format_ms](ftp_max) AS [On Ftp Server Max],
		[dbo].[fn_format_ms](occrq_min) AS [In OCCR Queue Min],
		[dbo].[fn_format_ms](occrq_avg) AS [In OCCR Queue Avg],
		[dbo].[fn_format_ms](occrq_max) AS [In OCCR Queue Max],
		[dbo].[fn_format_ms](occr_min) AS [OCCR Processing Min],
		[dbo].[fn_format_ms](occr_avg) AS [OCCR Processing Avg],
		[dbo].[fn_format_ms](occr_max) AS [OCCR Processing Max],
		[dbo].[fn_format_ms](occpq_min) AS [In OCCP Queue Min],
		[dbo].[fn_format_ms](occpq_avg) AS [In OCCP Queue Avg],
		[dbo].[fn_format_ms](occpq_max) AS [In OCCP Queue Max],
		[dbo].[fn_format_ms](occp_min) AS [OCCP Processing Min],
		[dbo].[fn_format_ms](occp_avg) AS [OCCP Processing Avg],
		[dbo].[fn_format_ms](occp_max) AS [OCCP Processing Max],
		[dbo].[fn_format_ms](usherq_min) AS [In Usher Queue Min],
		[dbo].[fn_format_ms](usherq_avg) AS [In Usher Queue Avg],
		[dbo].[fn_format_ms](usherq_max) AS [In Usher Queue Max],
		[dbo].[fn_format_ms](usher_min) AS [Usher Processing Min],
		[dbo].[fn_format_ms](usher_avg) AS [Usher Processing Avg],
		[dbo].[fn_format_ms](usher_max) AS [Usher Processing Max],
		[dbo].[fn_format_ms](total_min) AS [Total Min],
		[dbo].[fn_format_ms](total_avg) AS [Total Avg],
		[dbo].[fn_format_ms](total_max) AS [Total Max],
		count
	FROM stats;
END