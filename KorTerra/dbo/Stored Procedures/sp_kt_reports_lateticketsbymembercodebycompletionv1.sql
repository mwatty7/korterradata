﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymembercodebycompletionv1]
	@customerid varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16),
	@clientDt datetime,
	@outputOptions varchar(32),
	@summary bit = 0,
	@detail bit = 1,
	@offset int = 0
AS
DECLARE @customer varchar(32) = @customerid
DECLARE @member varchar(16) = @memberid
DECLARE @memcode varchar(16) = @membercode
DECLARE @date varchar(16) = @dateType
DECLARE @from varchar(30) = @fromDate
DECLARE @through varchar(30) = @throughDate
DECLARE @emergencies varchar(16) = @includeEmergencies
DECLARE @projects varchar(16) = @includeProjects
DECLARE @client datetime = @clientDt
DECLARE @output varchar(32) = @outputOptions
DECLARE @_detail bit = @detail
DECLARE @_summary bit = @summary
DECLARE @nowUtc DATETIME = SYSUTCDATETIME()

BEGIN
	IF @date = '1' 
		SET @through = dateAdd(dy, 1, @throughDate)
	Else 
	BEGIN
		SET @from = dbo.DateOnly(dateAdd(MINUTE, @offset,  @from))
		SET @through = dbo.DateOnly(dateAdd(MINUTE, @offset, @through))
	END
END

if @_detail = 1
	BEGIN
	if @output = 'summary'
		BEGIN
			SELECT 0;
			return;
		END;
	END;

if @_summary = 1
	BEGIN
	if @output = 'detailed'
		BEGIN
			SELECT 0;
			return;
		END
	END;

WITH CompletedLate (
	customerid,
	jobid,
	mobileid,
	worktype,
	Transmit,
	Wtb,
	duedate,
	origpriority,
	address,
	street,
	city,
	county,
	state,
	isproject,
	status,
	membercode, 
	memberid, 
	description,
	lateHours,
	lateMinutes,
	completiondateutc,
	CompletedLate,
	OpenLate,
	AllLate,
	operatorid,
	RequestType
) AS
(
	SELECT TOP 100000 
		vc.customerid,
		vc.jobid,
		vc.mobileid,
		j.worktype,
		CONVERT(VARCHAR, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
		CONVERT(VARCHAR, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
		dbo.GetUTC(vc.duedateutc, o.timezone, 1) as duedate,
		j.origpriority,
		j.address, 
		j.street,
		j.city, 
		j.county, 
		j.state,
		j.isproject,
		j.status,
		vc.membercode, 
		m.memberid,
		mob.description,
		(DATEDIFF(minute, vc.duedateutc, ISNULL(vc.completiondateutc, @nowUtc)) / 60) as lateHours,
		(DATEDIFF(minute, vc.duedateutc, ISNULL(vc.completiondateutc, @nowUtc)) % 60) AS lateMinutes,
		dbo.GetUTC(completiondateutc, o.timezone, 1),
		1 as CompletedLate,
		0 as OpenLate,
		1 as AllLate,
		vc.operatorid,
		vc.requesttype
	FROM visitcommon vc
		JOIN job j on j.customerid = vc.customerid AND j.jobid = vc.jobid
		JOIN jobstatus js on js.customerid = vc.customerid and js.jobid = vc.jobid
		JOIN ticketpriority tp on js.ticketpriorityid = tp.ticketpriorityid 
		JOIN memcode m on m.customerid = vc.customerid AND m.membercode = vc.membercode
		JOIN occ o on o.customerid = j.customerid AND j.latestxmitsource = o.occid
		JOIN mobile mob on mob.customerid = vc.customerid AND mob.mobileid = vc.mobileid
	WHERE
		-- Filter by customerid always
		vc.customerid = @customer AND
		vc.islate = 1 AND
		-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
		((@date = '1' AND vc.duedateutc BETWEEN @from AND @through) OR
		(@date = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))) AND
		-- Rule G: Filter out all cancelled / nlr tickets
		j.status NOT IN ('R', 'X') AND  
		vc.completiondateutc > vc.duedateutc
			
		-- Filter Emergencies and Projects by user criteria
		AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
		AND (@projects = 'Yes' OR j.isproject = '0')

		--Filter By membercode and memberid by user criteria
		AND (@member = '0' OR m.memberid = @member)
		AND (@memcode = '0' OR m.membercode = @memcode)
	),
	OpenLate (
	customerid,
	jobid,
	mobileid,
	worktype,
	Transmit,
	Wtb,
	duedate,
	origpriority,
	address,
	street,
	city,
	county,
	state,
	isproject,
	status,
	membercode, 
	memberid, 
	description,
	lateHours,
	lateMinutes,
	completiondateutc,
	CompletedLate,
	OpenLate,
	AllLate,
	operatorid,
	RequestType
) AS
(
	SELECT TOP 100000 
		vr.customerid,
		vr.jobid,
		j.mobileid,
		j.worktype,
		CONVERT(VARCHAR, j.latestxmitdtdate + j.latestxmitdttime, 120) AS Transmit, 
		CONVERT(VARCHAR, j.wtbdtdate + j.wtbdttime, 120) AS Wtb, 
		dbo.GetUTC(j.duedateutc, o.timezone, 1) as duedate,
		j.origpriority,
		j.address, 
		j.street,
		j.city, 
		j.county, 
		j.state,
		j.isproject,
		j.status,
		vr.membercode,
		m.memberid,
		mob.description,
		(DATEDIFF(minute, j.duedateutc, @nowUtc) / 60) as lateHours,
		(DATEDIFF(minute, j.duedateutc, @nowUtc) % 60) AS lateMinutes,
		null,
		0 as CompletedLate,
		1 as OpenLate,
		1 as AllLate,
		'',
		vr.requesttype
	FROM visitreq vr
		JOIN job j on j.customerid = vr.customerid AND j.jobid = vr.jobid
		JOIN jobstatus js on js.customerid = vr.customerid and js.jobid = vr.jobid
		JOIN ticketpriority tp on js.ticketpriorityid = tp.ticketpriorityid 
		JOIN memcode m on m.customerid = vr.customerid AND m.membercode = vr.membercode
		JOIN occ o on o.customerid = j.customerid AND j.latestxmitsource = o.occid
		JOIN mobile mob on mob.customerid = j.customerid AND mob.mobileid = j.mobileid
	WHERE
		-- Filter by customerid always
		vr.customerid = @customer AND
		vr.lastcompletiondtdate IS NULL AND
		-- Rule A: I can report by WTB or Xmit date ranges (eliminate completion date)
		((@date = '1' AND j.duedateutc BETWEEN @from AND @through) OR
		(@date = '2' AND (j.latestxmitdtdate BETWEEN @from AND @through))) AND
		-- Rule G: Filter out all cancelled / nlr tickets
		j.status NOT IN ('R', 'X') AND  
		@nowUtc >j.duedateutc
			
		-- Filter Emergencies and Projects by user criteria
		AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
		AND (@projects = 'Yes' OR j.isproject = '0')
		--Filter By membercode and memberid by user criteria
		AND (@member = '0' OR m.memberid = @member)
		AND (@memcode = '0' OR m.membercode = @memcode)
	),
	AllLate AS (
		SELECT * FROM CompletedLate
		UNION ALL
		SELECT * FROM OpenLate
	)

SELECT * INTO #TEMPDetails FROM AllLate;

if @_detail = 1
BEGIN
	SELECT * FROM #TEMPDetails;
END

if @_summary = 1
BEGIN
	SELECT 
	COUNT_BIG(d.jobid) as LateCount,
	SUM(d.CompletedLate) as CompletedLate,
	SUM(d.OpenLate) as OpenLate,
	(SUM(d.lateHours) / COUNT(d.jobid)) as avgHrs,
	(SUM(d.lateMinutes) / COUNT(d.jobid)) as avgMins,
	d.membercode as membercode,
	d.description as fulldescription,
	d.RequestType,
	(select COUNT_BIG(*) from visitreq vr
			join job jb on vr.customerid = jb.customerid AND vr.jobid = jb.jobid
			join jobstatus js on
			jb.customerid = js.customerid and
			jb.jobid = js.jobid
			join ticketpriority tp on
			js.ticketpriorityid = tp.ticketpriorityid 
			join request rq on
			rq.jobid = jb.jobid and
			rq.customerid = jb.customerid
			left join memcode mc on
			rq.membercode = mc.membercode and
			rq.customerid = mc.customerid
			where 
				jb.customerid = @customer AND
				jb.status NOT IN ('R', 'X') AND
				((@date = '1' AND jb.duedateutc BETWEEN @from AND @through) OR (@date = '2' AND jb.latestxmitdtdate BETWEEN @from AND @through))
				AND (@member = '0' OR mc.memberid = @member)
				AND (mc.membercode = d.membercode)
				AND (@emergencies = 'Yes' OR tp.prioritytype <> 'EMERGENCY')
				AND (@projects = 'Yes' OR jb.isproject = '0')
	) as TotalTicketCount
	FROM
		#TEMPDetails as d
	GROUP BY d.membercode, d.description, d.RequestType;
END

DROP TABLE #TEMPDetails;

SET ANSI_NULLS ON