﻿CREATE PROCEDURE [dbo].[sp_kt_reports_completionsbymembercodebymonth]
	@customerId varchar(32),
	@startdate date,
	@enddate date
AS
BEGIN
declare 
@edatetime as datetime = CAST(@enddate as datetime) + cast('23:59:59' as datetime);
select m.regionid as reg, m.districtid  as dist , vc.membercode, vc.mobileid + ' - ' + m.description as mobileid, vc.compstatus, DATEPART(yy, vc.completiondt) as year, DATEPART(mm, vc.completiondt) as monthnum,  DATENAME(month, vc.completiondt) as monthname , count(*) as completions
 from visitcommon vc  
join mobile m on
m.customerid = vc.customerid and 
m.mobileid = vc.mobileid
join district d on 
d.customerid = m.customerid and 
d.districtid = m.districtid and
d.regionid = m.regionid
join region r on
r.customerid = m.customerid and
r.regionid = m.regionid
where vc.customerid = @customerId and vc.completiondt between @startdate and @edatetime 
group by m.regionid, m.districtid, vc.membercode,  vc.mobileid + ' - ' + m.description,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
order by m.regionid, m.districtid,  vc.membercode, vc.mobileid + ' - ' + m.description,  vc.compstatus,  DATEPART(yy, vc.completiondt), DATEPART(mm, vc.completiondt), DATENAME(month, vc.completiondt)
END