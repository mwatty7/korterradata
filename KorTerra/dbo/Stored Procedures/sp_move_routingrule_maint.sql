﻿-- =============================================
-- Description:	Move the routing rule maintenance tables from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_routingrule_maint]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @errMsg NVARCHAR(MAX);
		
		EXEC [dbo].[sp_move_table] @tableName = 'routingruleversion', @customerId = @customerId, @sourceServer = @sourceServer, @sourceDatabase = @sourceDatabase, @targetServer = @targetServer, @targetDatabase = @targetDatabase, @includeAudit = @includeAudit;
		
				
        SET @SQLCommand = N' 
			INSERT INTO [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingruletype 
				SELECT 
					s1.rrtypeid, 
					s1.rrversionid, 
					s1.match,
					s1.nomatch,
					s1.method,
					s1.field
				FROM 
					[' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingruletype s1
					JOIN [' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.routingruleversion s2 ON s1.rrversionid = s2.rrversionid
					AND s2.customerid = ''' + @customerId + '''
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
		
		
        SET @SQLCommand = N' 
			INSERT INTO [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingrulevalue 
				SELECT 
					s1.rrvalueid,
					s1.rrtypeid, 
					s1.value,
					s1.zoneid,
					s1.minx,
					s1.maxx,
					s1.miny,
					s1.maxy,
					s1.starttime,
					s1.endtime,
					s1.startdate,
					s1.enddate
				FROM 
					[' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingrulevalue s1
					JOIN [' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingruletype s2 on s1.rrtypeid = s2.rrtypeid
	                JOIN [' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingruleversion s3 on s2.rrversionid = s3.rrversionid
	                AND s3.customerid = ''' + @customerId + '''
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END		
		
		SET @SQLCommand = N' 
			INSERT INTO [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingrulepoints 
				SELECT 
					s1.rrpointid,
					s1.rrvalueid,
					s1.seqno,
					s1.x,
					s1.y
				FROM 
					[' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingrulepoints s1
                    JOIN [' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingrulevalue s2 on s1.rrvalueid = s2.rrvalueid
	                JOIN [' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingruletype s3 on s2.rrtypeid = s3.rrtypeid
	                JOIN [' + @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.routingruleversion s4 on s4.rrversionid = s3.rrversionid 
	                AND s4.customerid = ''' + @customerId + '''
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
		
		IF (@includeAudit = 1)
		BEGIN
			DECLARE @ParmDefinition nvarchar(500);
			DECLARE @CountSQLQuery varchar(30);
			EXEC sp_audit_moved_table 'routingruleversion', @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
			SET @SQLCommand = N'
				SELECT @result = ABS(
					(SELECT 
						COUNT(*) 
					FROM 
						[' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingruleversion AS RRV
						JOIN [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingruletype AS RRT ON RRT.rrversionid = RRV.rrversionid
						JOIN [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingrulevalue AS RRA ON RRA.rrtypeid = RRT.rrtypeid
						JOIN [' + @targetServer + ']' + '.' + @targetDatabase + '.dbo.routingrulepoints AS RRP ON RRP.rrvalueid = RRA.rrvalueid
					WHERE 
						RRV.customerid = ''' + @customerId + ''') - 
					(SELECT 
						COUNT(*) 
					FROM 
						[' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.routingruleversion AS RRV
						JOIN [' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.routingruletype AS RRT ON RRT.rrversionid = RRV.rrversionid
						JOIN [' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.routingrulevalue AS RRA ON RRA.rrtypeid = RRT.rrtypeid
						JOIN [' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.routingrulepoints AS RRP on RRP.rrvalueid = RRA.rrvalueid
					WHERE 
						RRV.customerid = ''' + @customerId + ''')
					
			)';
			SET @ParmDefinition = N'@result varchar(30) OUTPUT';
			EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
			SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
			IF @CountSQLQuery <> 0
			BEGIN
				PRINT @SQLCommand + ' FAILED!'
				SET @errMsg = 'TABLE: routingrulepoints FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
				RAISERROR (@errMsg, 15, 10)
			END
			ELSE IF @CountSQLQuery = 0
			BEGIN 
				PRINT 'routingrulepoints Records Match'
			END
		END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''Routing Rule Maint'' FAILED Stored Procedure: ''sp_move_routingrule_maint''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END