﻿-- =============================================
-- Description:	Move the First Alert Maintenance from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_firstalertmaint_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @return Int;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @errMsg NVARCHAR(MAX);
		DECLARE @expectedCount int = 19;
		DECLARE @tableName VARCHAR(50) = 'FirstAlertPricingSchedule';
		DECLARE @expectedColumnCount int = 19;
		if (@includeAudit = 1)
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedColumnCount
			
		SET @SQLCommand = ' 
			INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.FirstAlertPricingSchedule(
				customerid, 
				schedulename, 
				description, 
				locateprice, 
				locateotprice, 
				onecallprice, 
				onecallotprice, 
				gdprice, 
				gdotprice, 
				reportresearchprice, 
				onecallplacementprice, 
				kilometersabprice, 
				kilometersotherprice, 
				suppliesprice, 
				subsistenceprice, 
				drugtestingprice, 
				lodgingpercent, 
				taxpercent, 
				isactive
			)
			SELECT 
				customerid, 
				schedulename, 
				description, 
				locateprice, 
				locateotprice, 
				onecallprice, 
				onecallotprice, 
				gdprice, 
				gdotprice, 
				reportresearchprice, 
				onecallplacementprice, 
				kilometersabprice, 
				kilometersotherprice, 
				suppliesprice, 
				subsistenceprice, 
				drugtestingprice, 
				lodgingpercent, 
				taxpercent, 
				isactive
			FROM 
				['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.FirstAlertPricingSchedule 
			WHERE 
				customerid = ''' + @customerid + '''';
        EXEC @return = sp_executesql @SQLCommand OUTPUT
        IF @return <> 0 BEGIN
            SET @errMsg = @SQLCommand + ' FAILED!'
            RAISERROR (@errMsg, 15, 10);
        END
		IF (@includeAudit = 1)
			EXEC sp_audit_moved_table @tableName, @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''FirstAlertMaint'' FAILED Stored Procedure: ''sp_move_firstalertmaint_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END