﻿CREATE PROCEDURE [dbo].[sp_HeraldMessenger_nearlate]
	@customerid VARCHAR(32),
	@heraldmessengerid VARCHAR(16),
	@sp_param1 VARCHAR(64),
	@sp_param2 VARCHAR(64),
	@sp_param3 VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @intervalSeconds INT;
	SET @intervalSeconds = 60 * CAST(@sp_param1 AS INT);
	CREATE TABLE #UnmessagedJobs
	(
		jobid varchar(16) NOT NULL,
		duedtutc DATETIME2 NULL
	);
	
	CREATE TABLE #AlertJobs
	(
		jobid varchar(16) NOT NULL
	);
	
	DECLARE @customerClause VARCHAR(80);
	SET @customerClause = '';
	IF (@customerid <> 'ALL' AND @customerid <> '' AND @customerid <> ' ')
		SET @customerClause = ' AND j.customerid = ''' + @customerid + '''';
		
	DECLARE @selectStmt NVARCHAR(MAX);
	SET @selectStmt = '
	SELECT DISTINCT j.jobid, j.duedateutc
		FROM job j WITH (NOLOCK)
	LEFT OUTER JOIN occ o WITH (NOLOCK)
		ON o.customerid = j.customerid
		AND o.occid = j.latestxmitsource
	LEFT OUTER JOIN visitreq v WITH (NOLOCK)
		ON v.customerid = j.customerid
		AND v.jobid = j.jobid
	LEFT OUTER JOIN visitcommon vc WITH (NOLOCK)
		ON vc.customerid = v.customerid
		AND vc.jobid = v.jobid
		AND vc.membercode = v.membercode
		AND vc.requesttype = v.requesttype
	LEFT OUTER JOIN messagedjob m WITH (NOLOCK)
		ON m.customerid = j.customerid
		AND m.jobid = j.jobid
		AND m.heraldmessengerid = ''' + @heraldmessengerid + '''';
		
	SET @selectStmt = @selectStmt + ' WHERE j.status in (''A'', ''N'')';
	SET @selectStmt = @selectStmt + @customerClause;
	SET @selectStmt = @selectStmt + '
		AND vc.membercode IS NULL
		AND m.jobid IS NULL';
	DECLARE @InsertStmt NVARCHAR(MAX)	
	SET @InsertStmt = 'INSERT INTO #UnmessagedJobs ';
	SET @InsertStmt = @InsertStmt + @selectStmt;
	EXECUTE SP_ExecuteSQL @InsertStmt;
	
	SET @InsertStmt = 'INSERT INTO #AlertJobs SELECT DISTINCT jobid FROM #UnmessagedJobs ';
	SET @InsertStmt = @InsertStmt + '
		WHERE DATEDIFF(SS, GETUTCDATE(), duedtutc) >= 0 
		AND DATEDIFF(SS, GETUTCDATE(), duedtutc) <= ';
	SET @insertStmt = @insertStmt + CAST(@intervalSeconds AS VARCHAR) + ' ';
	
	EXECUTE SP_ExecuteSQL @InsertStmt;
	
	SET @selectStmt = 'SELECT DISTINCT jobid FROM #AlertJobs ';
	EXECUTE SP_ExecuteSQL @selectStmt;
END