﻿-- =============================================
-- Description:	Move the Screening Collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_misctablecollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		if (@includeAudit = 1)
			BEGIN
				EXEC sp_audit_column_count 'mapannotation', @sourceServer, @sourceDatabase, 8
				EXEC sp_audit_column_count 'mapannotationpoint', @sourceServer, @sourceDatabase, 4
			END
		    --POPULATE mapannotation and mapannotationpoint
    DECLARE @sqlMapNot NVARCHAR(MAX);
    SET @sqlMapNot= '
IF EXISTS (SELECT *
FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''')
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mapannotation(customerid, label, shapetype, color, comment, operatorid, isactive, lastmodifieddt)
SELECT customerid, label, shapetype, color, comment, operatorid, isactive, lastmodifieddt FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid =''' + @customerid + ''';
CREATE TABLE #mpann (shapeid_old INT, shapeid_new INT, customerid VARCHAR(32), label VARCHAR(64), color VARCHAR(32), operatorid VARCHAR(16), isactive smallint, lastmodifieddt datetime, lrn INT IDENTITY NOT NULL);
INSERT INTO #mpann (shapeid_old, customerid, label, color, operatorid, isactive, lastmodifieddt) SELECT shapeid, customerid, label, color, operatorid, isactive, lastmodifieddt FROM ['+ @sourceServer + ']
' + '.' + @sourceDatabase + '.dbo.mapannotation WHERE customerid = ''' + @customerid + ''';
UPDATE #mpann
SET shapeid_new = m.shapeid
FROM #mpann
    JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mapannotation m on #mpann.customerid = m.customerid 
    and #mpann.operatorid = m.operatorid 
    and #mpann.color = m.color and #mpann.label = m.label 
    and #mpann.lastmodifieddt = m.lastmodifieddt
INSERT INTO ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.mapannotationpoint
SELECT #mpann.shapeid_new, m.sequence, m.latitude, m.longitude
FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.mapannotationpoint m JOIN #mpann on #mpann.shapeid_old = m.shapeid;
DROP TABLE #mpann
'
	EXECUTE @return = sp_executesql @sqlMapNot OUTPUT
	IF @return <> 0 BEGIN
		DECLARE @errorMsg VARCHAR(MAX) = NULL;
		SET @errorMsg = @sqlMapNot + 'FAILED!'
		RAISERROR (@errorMsg, 15, 10);
	END
		
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''Misc Table Collection AutoIncremented'' FAILED Stored Procedure: ''sp_move_misctablecollection_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END