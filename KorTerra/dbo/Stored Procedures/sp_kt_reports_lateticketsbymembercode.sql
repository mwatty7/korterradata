﻿CREATE PROCEDURE [dbo].[sp_kt_reports_lateticketsbymembercode]
	@customerId varchar(32),
	@memberid varchar(16),
	@membercode varchar(16),
	@dateType varchar(16),
	@fromDate varchar(30),
	@throughDate varchar(30),
	@includeNLR varchar(16),
	@includeEmergencies varchar(16),
	@includeProjects varchar(16)
AS
BEGIN
	DECLARE @Command NVARCHAR(MAX) 
	Set @Command = '
				SELECT 
				job.customerid,
				job.jobid,
				job.mobileid,
				job.worktype,
				CONVERT(VARCHAR, job.latestxmitdtdate + job.latestxmitdttime, 120) AS Transmit, 
				CONVERT(VARCHAR, job.wtbdtdate + job.wtbdttime, 120) AS Wtb, 
				job.origpriority,
				job.address, 
				job.street,
				job.city, 
				job.county, 
				job.state,
				job.isproject,
				job.status,
				mobile.description,
				request.membercode, 
				memcode.memberid,
				request.firstcompletiondt,
				request.firstcompletiondtdate,
				visitcommon.operatorid
			FROM 
				job
				OUTER APPLY 
				(
					SELECT
						request.membercode,
						MIN(request.firstcompletiondtdate) as firstcompletiondtdate,
						CONVERT(VARCHAR, MIN(request.firstcompletiondtdate) + MIN(request.firstcompletiondttime), 120) AS firstcompletiondt
					FROM 
						request
					WHERE 
						request.customerid = job.customerid 
						AND request.jobid = job.jobid
					GROUP BY request.customerid, request.membercode, request.jobid
				) request
				OUTER APPLY
				(
					SELECT
						memcode.membercode, 
						memcode.memberid
					FROM 
						memcode
					WHERE 
						memcode.customerid = job.customerid 
						AND memcode.membercode = request.membercode
				) memcode
				OUTER APPLY
				(
					SELECT
						mobile.mobileid,
						mobile.description
					FROM 
						mobile
					WHERE 
						mobile.customerid = job.customerid 
						AND mobile.mobileid = job.mobileid
				) mobile
				OUTER APPLY
				(
					SELECT TOP 1 
						*
					FROM 
						visitcommon
					WHERE 
						visitcommon.customerid = job.customerid 
						AND visitcommon.jobid = job.jobid
						AND visitcommon.membercode = request.membercode
						AND visitcommon.completiondt = request.firstcompletiondt
				) visitcommon
				WHERE 
					job.customerid = ''' + @customerId + '''
	'
	-- Report Options
	If( @includeNLR = 'No')
		Set @Command = @Command + ' AND job.status NOT IN (''R'', ''X'', ''U'')'
	If( @includeEmergencies = 'No')
		Set @Command = @Command + '  AND job.priority <> ''EMERGENCY'''
	if( @includeProjects = 'No' )
		Set @Command = @Command + '  AND job.isproject = 0'
	  
	-- End Report Options
	-- Date Options
	If( @dateType = '0' )
		Set @Command = @Command + ' AND (CAST(request.firstcompletiondtdate as date) >= ''' + convert(varchar(30), @fromDate, 101) + ''') AND (CAST(request.firstcompletiondtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '1' )
		Set @Command = @Command + ' AND (CAST(job.wtbdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.wtbdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	If( @dateType = '2' )
	    Set @Command = @Command + ' AND (CAST(job.latestxmitdtdate as date) >= ''' + convert(varchar(30), @fromDate, 101)  + ''') AND (CAST(job.latestxmitdtdate as date) <= ''' + convert(varchar(30), @throughDate, 101) + ''')'
	-- End Date Options
	-- Member / Member Code
	If( @memberid <> '0' )
	   Set @Command = @Command + ' AND (memcode.memberid = ''' + @memberid + ''') '
	If( @membercode <> '0' )
	   Set @Command = @Command + ' AND (memcode.membercode = ''' + @membercode + ''') '
	-- End Member / Member Code
	Set @Command = @Command + ' ORDER BY memcode.memberid, memcode.membercode, job.jobid, request.firstcompletiondt'
	Execute SP_ExecuteSQL  @Command
END