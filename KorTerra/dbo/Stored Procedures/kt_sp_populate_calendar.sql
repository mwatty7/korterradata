﻿CREATE PROCEDURE kt_sp_populate_calendar 
	@customerid CHAR(32), 
	@occid CHAR(8)
AS
BEGIN
	-- @todt is a timestamp - it will have a time part other than midnight
	DECLARE @todt DATETIME;

	-- @fromdate and @todate are dates - they will have time parts equal to midnight
	DECLARE @fromdate DATETIME;
	DECLARE @todate DATETIME;

	SET @fromdate = '2012-01-01';

	SET @todt = DATEADD(year, 1, CURRENT_TIMESTAMP);
	-- truncate to midnight
	SET @todate = DATEADD(DD, 0, DATEDIFF(dd, 0, @todt));

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- DELETE any existing records
	DELETE FROM calendar
		WHERE customerid = @customerid
		AND occid = @occid
		AND thedate BETWEEN @fromdate AND @todate
	;

	-- http://msdn.microsoft.com/en-us/library/ms177399(v=sql.105).aspx
	--
	-- Local temporary tables have a single number sign (#) as the first 
	-- character of their names; they are visible only to the current connection 
	-- for the user, and they are deleted when the user disconnects from the 
	-- instance of SQL Server.
	--
	-- This kludgy way of creating bunch of records will create at most 99,999 dates
	-- which is > 273 years.
	--
	CREATE TABLE #ints (i INT NOT NULL PRIMARY KEY);
	INSERT INTO #ints VALUES (0);
	INSERT INTO #ints VALUES (1);
	INSERT INTO #ints VALUES (2);
	INSERT INTO #ints VALUES (3);
	INSERT INTO #ints VALUES (4);
	INSERT INTO #ints VALUES (5);
	INSERT INTO #ints VALUES (6);
	INSERT INTO #ints VALUES (7);
	INSERT INTO #ints VALUES (8);
	INSERT INTO #ints VALUES (9);

	INSERT INTO calendar (customerid, occid, thedate)
		SELECT @customerid, @occid, 
			DATEADD(DD, (a.i*10000 + b.i*1000 + c.i*100 + d.i*10 + e.i), @fromdate)
		FROM #ints a CROSS JOIN #ints b CROSS JOIN #ints c CROSS JOIN #ints d CROSS JOIN #ints e
		WHERE DATEADD(DD, (a.i*10000 + b.i*1000 + c.i*100 + d.i*10 + e.i), @fromdate) <= @todate
	;

	DROP TABLE #ints;

	-- set the parts and the isweekdays
	UPDATE calendar
	SET
		theyear = DATEPART(YEAR, thedate),
		themonth = DATEPART(MONTH, thedate),
		thedayofmonth = DATEPART(DAY, thedate),
		
		thequarter = DATEPART(QUARTER, thedate),
		theweekofyear = DATEPART(WEEK, thedate),
		thedayofweek = DATEPART(WEEKDAY, thedate),
		
		themonthname = DATENAME(MONTH, thedate),
		thedayname = DATENAME(WEEKDAY, thedate),
		isweekday = CASE WHEN DATEPART(WEEKDAY, thedate) IN (1,7) THEN 0 ELSE 1 END
	WHERE customerid = @customerid
	AND occid = @occid
	AND thedate BETWEEN @fromdate AND @todate
	;

	-- set the holidays
	UPDATE calendar
	SET isholiday = ISNULL((
		SELECT 1
		FROM holiday
		WHERE holiday.customerid = calendar.customerid
		AND holiday.occid = calendar.occid
		AND holiday.holidaydate = calendar.thedate
		AND holiday.iseveryyear = 0
	), 0),
	holidaydescr = ISNULL((
		SELECT description
		FROM holiday
		WHERE holiday.customerid = calendar.customerid 
		AND holiday.occid = calendar.occid
		AND holiday.holidaydate = calendar.thedate
		AND holiday.iseveryyear = 0
	), '')
	WHERE customerid = @customerid
	AND occid = @occid
	AND thedate BETWEEN @fromdate AND @todate
	;

	-- we could have multiple iseveryyear holiday records for any given month and day
	-- if we have several, we pick the first description, alphabetically
	UPDATE calendar
	SET isholiday = ISNULL((
		SELECT MAX(1)
		FROM holiday
		WHERE calendar.customerid = holiday.customerid
		AND calendar.occid = holiday.occid
		AND DATEPART(MONTH, holiday.holidaydate) = DATEPART(MONTH, calendar.thedate)
		AND DATEPART(DAY, holiday.holidaydate) = DATEPART(DAY, calendar.thedate)
		AND holiday.iseveryyear = 1
	), 0),
	holidaydescr = ISNULL((
		SELECT MIN(description)
		FROM holiday
		WHERE calendar.customerid = holiday.customerid
		AND calendar.occid = holiday.occid
		AND DATEPART(MONTH, holiday.holidaydate) = DATEPART(MONTH, calendar.thedate)
		AND DATEPART(DAY, holiday.holidaydate) = DATEPART(DAY, calendar.thedate)
		AND holiday.iseveryyear = 1
	), '')
	WHERE customerid = @customerid
	AND occid = @occid
	AND thedate BETWEEN @fromdate AND @todate
	AND isholiday = 0
	;

	-- Clear the daterank field
	UPDATE calendar
	SET daterank = NULL
	WHERE customerid = @customerid
	AND occid = @occid
	;
	
	-- Set the non-holiday weekdays
	WITH rankeddates AS
		(
			SELECT *, ROW_NUMBER() OVER (ORDER BY thedate) therank
			FROM calendar
			WHERE customerid = @customerid
			AND occid = @occid
			AND isweekday = 1
			AND isholiday = 0
		)
	UPDATE rankeddates
	SET daterank = therank
	;
	
	-- Set the holidays and weekends
	UPDATE calendar
	SET daterank = (
		SELECT MIN(daterank)
		FROM calendar c2
		WHERE customerid = @customerid
		AND occid = @occid
		AND c2.daterank IS NOT NULL
		AND c2.thedate > calendar.thedate
	)
	WHERE customerid = @customerid
	AND occid = @occid
	AND daterank IS NULL
	;
	
END