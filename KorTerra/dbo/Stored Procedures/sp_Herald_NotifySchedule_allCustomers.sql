﻿
CREATE PROCEDURE [dbo].[sp_Herald_NotifySchedule_allCustomers]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @theCustomerId VARCHAR(32);

	DECLARE cur CURSOR FOR
	SELECT customerid FROM customer;

	OPEN cur;

	FETCH NEXT FROM cur INTO @theCustomerId;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXECUTE sp_Herald_NotifySchedule_onecustomer @customerid = @theCustomerId;

		FETCH NEXT FROM cur INTO @theCustomerId;
	END

	CLOSE cur;
	DEALLOCATE cur;
END