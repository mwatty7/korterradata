﻿-- =============================================
-- Description:	Move the Screening Collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_screeningdecisionscollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		if (@includeAudit = 1)
			BEGIN
				EXEC sp_audit_column_count 'screeningdecisioncommon', @sourceServer, @sourceDatabase, 11
				EXEC sp_audit_column_count 'screeningdecisiondata', @sourceServer, @sourceDatabase, 3
			END
        --POPULATE screeningdecision common
        DECLARE @sqlPopulateScreening NVARCHAR(MAX);
        SET @sqlPopulateScreening = '
			CREATE TABLE #screeningdecisioncommon
			 ([screeningid] [int] NOT NULL,[customerid] [varchar](32) NOT NULL,[jobid] [varchar](16) NOT NULL,[membercode] [varchar](16) NOT NULL,
			  [requesttype] [varchar](9) NOT NULL,[creationdateutc] [datetime2](7) NOT NULL,	[templateid] [varchar](64) NOT NULL,
			  [templateversionnumber] [int] NOT NULL,	[operatorid] [varchar](16) NOT NULL,[decisionreason] [varchar](64) NULL,
			  [decision] [varchar](16) NOT NULL,[remarks] [varchar](max) NULL,[newid] [int]
			);
			INSERT INTO #screeningdecisioncommon SELECT screeningdecisioncommon.*, 0 FROM [' + @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.screeningdecisioncommon WHERE customerid = ''' + @customerid + ''';
			INSERT INTO  ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.screeningdecisioncommon(customerid,jobid,membercode,requesttype,creationdateutc,templateid,templateversionnumber,operatorid,decisionreason,decision,remarks) 
					SELECT customerid,jobid,membercode,requesttype,creationdateutc,templateid,templateversionnumber,operatorid,decisionreason,decision,remarks FROM ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.screeningdecisioncommon WHERE customerid = ''' + @customerid + ''';
			UPDATE sdt
					SET newid = sdc.screeningid
					FROM #screeningdecisioncommon sdt
					INNER JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.screeningdecisioncommon sdc ON
					sdt.customerid = sdc.customerid AND
					sdt.jobid = sdc.jobid AND
					sdt.membercode = sdc.membercode AND
					sdt.requesttype = sdc.requesttype AND
					sdt.creationdateutc = sdc.creationdateutc AND
					sdt.templateid = sdc.templateid AND
					sdt.templateversionnumber = sdc.templateversionnumber;
			INSERT into  ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.screeningdecisiondata 
					select newid, fieldid, value 
					from  ['+ @SourceServer + ']' + '.' + @SourceDatabase + '.dbo.screeningdecisiondata sdd
					inner join #screeningdecisioncommon  on 
					sdd.screeningid = #screeningdecisioncommon.screeningid;
					drop table #screeningdecisioncommon;'
        EXECUTE @return = sp_executesql @sqlPopulateScreening OUTPUT
        IF @return <> 0 BEGIN
			DECLARE @errorMsg VARCHAR(MAX) = NULL;
            SET @errorMsg = @sqlPopulateScreening + 'FAILED!'
			RAISERROR (@errorMsg, 15, 10);
        END
		IF @includeAudit = 1
			BEGIN
				DECLARE @errMsg VARCHAR(MAX) = NULL;
				DECLARE @ParmDefinition nvarchar(500);
				DECLARE @CountSQLQuery varchar(30);
				EXEC sp_audit_moved_table 'screeningdecisioncommon', @customerId, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
				SET @SQLCommand = N'
					SELECT @result = ABS(
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.screeningdecisioncommon AS SC
							JOIN ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.screeningdecisiondata AS SD ON sc.screeningid = SD.screeningid
							WHERE customerid = ''' + @customerId + ''') -
						(SELECT 
							COUNT(*) 
						FROM 
							['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.screeningdecisioncommon AS SC
							JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.screeningdecisiondata AS SD ON sc.screeningid = SD.screeningid
						WHERE customerid = ''' + @customerId + ''')
					
					)';
				SET @ParmDefinition = N'@result varchar(30) OUTPUT';
				EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
				SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
				IF @CountSQLQuery <> 0
				BEGIN
					PRINT @SQLCommand + ' FAILED!'
					SET @errMsg = 'TABLE: screeningdecisiondata FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
					RAISERROR (@errMsg, 15, 10)
				END
				ELSE IF @CountSQLQuery = 0
				BEGIN 
					PRINT 'screeningdecisiondata Records Match'
				END
			END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''ScreeningDecisionsCollection'' FAILED Stored Procedure: ''sp_move_screeningdecisionscollection_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END