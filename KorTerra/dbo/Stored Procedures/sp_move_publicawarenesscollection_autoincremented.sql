﻿-- =============================================
-- Description:	Move the Public Awareness Collection from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_publicawarenesscollection_autoincremented]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		DECLARE @expectedCount int;
		DECLARE @tableName VARCHAR(50);
        DECLARE @sqlPopulatePublicAwareness NVARCHAR(MAX);
		if (@includeAudit = 1) BEGIN
			SET @expectedCount = 4;
			SET @tableName = 'PAaudittrail';
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @expectedCount = 20;
			SET @tableName = 'PAcontact';
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @expectedCount = 3;
			SET @tableName = 'PAcontactdata';
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @expectedCount = 5;
			SET @tableName = 'PAmaint';
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
			SET @expectedCount = 5;
			SET @tableName = 'PAtemplate';
			EXEC sp_audit_column_count @tableName, @sourceServer, @sourceDatabase, @expectedCount
		END
        SET @sqlPopulatePublicAwareness = '
				INSERT INTO ['+ @targetServer + ']
				' + '.' + @targetDatabase + '.dbo.PAtemplate
				SELECT *
				FROM ['+ @sourceServer + ']
				' + '.' + @sourceDatabase + '.dbo.PAtemplate WHERE customerid =  '''+@customerId +''';
				CREATE TABLE #paMaint
				(
					maintid_old INT,
					maintid_new INT,
					customerid VARCHAR(32),
					type VARCHAR(32),
					description VARCHAR(256),
					isactive SMALLINT,
					sequence INT
				);
				CREATE TABLE #PAcontact
				(
					contactid_old int NOT NULL,
					customerid varchar(32) NOT NULL,
					contactdate date NOT NULL,
					firstname varchar(32) NULL,
					lastname varchar(32) NULL,
					contacttypeid int NULL,
					companyname varchar(64) NULL,
					origregionid varchar(16) NULL,
					origdistrictid varchar(16) NULL,
					mobileid varchar(16) NULL,
					operatorid varchar(16) NULL,
					address1 varchar(64) NULL,
					address2 varchar(64) NULL,
					city varchar(64) NULL,
					state varchar(3) NULL,
					zip varchar(12) NULL,
					email varchar(254) NULL,
					phone varchar(21) NULL,
					comments varchar(max) NULL,
					templatename varchar(32) NULL,
					templateversion smallint NULL,
					contactid_new int NOT NULL
				);
				INSERT INTO #paMaint
					(maintid_old, customerid, type, description, isactive, sequence)
				SELECT *
				FROM ['+ @sourceServer + ']
				' + '.' + @sourceDatabase + '.dbo.PAmaint
				WHERE  customerid = '''+@customerId +''';
				INSERT INTO ['+ @targetServer + ']
				' + '.' + @targetDatabase + '.dbo.PAmaint
				(customerid, type, description, isactive, sequence)
				SELECT customerid, type, description, isactive, sequence
				FROM ['+ @sourceServer + ']
				' + '.' + @sourceDatabase + '.dbo.PAmaint WHERE customerid = '''+@customerId +''';
				UPDATE #paMaint
				SET maintid_new = a.maintid
				FROM ['+ @targetServer + ']
				' + '.' + @targetDatabase + '.dbo.PAmaint a
				JOIN #paMaint b on b.customerid = a.customerid 
				AND b.type = a.type
				AND b.description = a.description
				AND b.isactive = a.isactive
				AND b.sequence = a.sequence;
				INSERT INTO #PAcontact
				SELECT PAcontact.*, 0
				FROM ['+ @sourceServer + ']
				' + '.' + @sourceDatabase + '.dbo.PAcontact  WHERE customerid = '''+@customerId +''';
				UPDATE #PAcontact 
				set contacttypeid = maintid_new 
				from #PAcontact
				join #PAmaint on 
				contacttypeid = maintid_old;
				INSERT INTO  ['+ @targetServer + ']
				' + '.' + @targetDatabase + '.dbo.PAcontact
				(customerid,contactdate,firstname,lastname,contacttypeid,companyname,origregionid,origdistrictid,
					mobileid,operatorid,address1,address2,city,state,zip,email,phone,comments,templatename,templateversion)
				SELECT customerid, contactdate, firstname, lastname, contacttypeid, companyname, origregionid, origdistrictid, mobileid, operatorid, address1, address2, city, state, zip, email, phone, comments, templatename, templateversion
				FROM #PAcontact;
				
				UPDATE #PAcontact 
				SET contactid_new = contactid
				FROM #PAcontact p1
					join ['+ @targetServer + '] ' + '.' + @targetDatabase + '.dbo.PAcontact p2 on 
				p1.customerid = p2.customerid and
						p1.contactdate = p2.contactdate and
						p1.origregionid = p2.origregionid and
						p1.origdistrictid = p2.origdistrictid and
						p1.mobileid = p2.mobileid and
						p1.operatorid = p2.operatorid and
						p1.firstname = p2.firstname and
						p1.lastname = p2.lastname;
				INSERT INTO  ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAcontactdata (contactid, maintid, value)
				SELECT contactid_new, maintid_new, value FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.dbo.PAcontactdata p1 
				JOIN #PAcontact p2 on p2.contactid_old = p1.contactid
				JOIN #PAmaint on p1.maintid = #PAMaint.maintid_old;
				CREATE TABLE #PAaudit
				(
					audittrailid_old INT,
					audittrailid_new INT,
					contactid_old INT,
					action VARCHAR(MAX),
					actiondt DATETIME2(7),
					operatorid VARCHAR(32)
				);
				INSERT INTO #PAaudit (audittrailid_old, contactid_old, action, actiondt, operatorid) 
				SELECT * FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.PAaudittrail 
				WHERE contactid IN (SELECT contactid FROM ['+ @sourceServer + '] ' + '.' + @sourceDatabase + '.dbo.PAcontact WHERE customerid =  '''+@customerId +''')
				INSERT INTO  ['+ @targetServer + ']' + '.' + @targetDatabase + '.dbo.PAaudittrail (contactid, action, actiondt, operatorid) 
				SELECT p.contactid_new, p1.action, p1.actiondt, p1.operatorid 
				FROM #PAcontact p
						JOIN #PAaudit p1 on p1.contactid_old = p.contactid_old
						WHERE p.customerid = '''+@customerId +''';
				drop table #PAmaint;
				drop table #PAcontact;
				drop table #PAaudit;'
		
        EXECUTE @return = sp_executesql @sqlPopulatePublicAwareness OUTPUT
        IF @return <> 0 BEGIN
			DECLARE @errorMsg VARCHAR(MAX) = NULL;
            SET @errorMsg = @sqlPopulatePublicAwareness + 'FAILED!'
			RAISERROR (@errorMsg, 15, 10);
        END
		if (@includeAudit = 1) BEGIN
			EXEC sp_audit_move_publicawarenesscollection_autoincremented @customerid, @sourceServer, @sourceDatabase, @targetServer, @targetDatabase
		END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''PublicAwarenessCollection'' FAILED Stored Procedure: ''sp_move_publicawarenesscollection_autoincremented''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END