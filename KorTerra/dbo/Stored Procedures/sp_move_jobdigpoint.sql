﻿-- =============================================
-- Description:	Move the Job Digpoint Table from one server to another
-- =============================================
CREATE PROCEDURE [dbo].[sp_move_jobdigpoint]
    @customerId VARCHAR(32) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
    @targetServer VARCHAR(50) = NULL,
    @targetDatabase VARCHAR(50) = NULL,
	@includeAudit BIT = 0
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;
		DECLARE @SQLCommand NVARCHAR(MAX);
		DECLARE @return Int;
		IF @includeAudit = 1
			EXEC sp_audit_column_count 'jobdigpoint', @sourceServer, @sourceDatabase, 3
		SET @SQLCommand = '
			INSERT INTO ['+ @targetServer + '].' + @targetDatabase + '.dbo.jobdigpoint
			SELECT customerid, jobid, jobgeometry=geometry::STGeomFromText(jobgeometry,0)
			FROM OPENQUERY(['+ @sourceServer + '], ''
				select 
					customerid, jobid, jobgeometry=jobgeometry.STAsText() 
				from ' + @sourceDatabase + '.dbo.jobdigpoint
				WHERE customerid = ''''' + @customerId + '''''
			'')
		';
		EXEC @return = sp_executesql @SQLCommand OUTPUT
		IF @return <> 0 BEGIN
			DECLARE @errMsg NVARCHAR(MAX);
			SET @errMsg = @SQLCommand + ' FAILED!'
			RAISERROR (@errMsg, 15, 10);
		END
		IF @includeAudit = 1
		BEGIN
			DECLARE @ParmDefinition nvarchar(500);
			DECLARE @CountSQLQuery varchar(30);
			SET @SQLCommand = N'SELECT @result = ABS(
				(
					SELECT
						COUNT(*)
					FROM [' + @targetServer +'].' + @targetDatabase + '.dbo.jobdigpoint
					WHERE customerid =  ''' + @customerid + '''
				) -
				(
					SELECT count(*)
					FROM OPENQUERY(['+ @sourceServer + '], ''
						select 
							customerid, jobid, jobgeometry=jobgeometry.STAsText() 
						from ' + @sourceDatabase + '.dbo.jobdigpoint
						WHERE customerid = ''''' + @customerId + '''''
					'')
				))';
			SET @ParmDefinition = N'@result varchar(30) OUTPUT';
			EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
			SELECT CAST(@CountSQLQuery as int) [Record Count Difference];
			IF @CountSQLQuery <> 0
			BEGIN
				PRINT @SQLCommand + ' FAILED!'
				SET @errMsg = 'TABLE: jobdigpoint FAILED AUDIT CHECK, RECORD COUNT DOES NOT MATCH!'
				RAISERROR (@errMsg, 15, 10)
			END
			ELSE IF @CountSQLQuery = 0
			BEGIN 
				PRINT 'jobdigpoint Records Match'
			END
		END
	END TRY
	BEGIN CATCH
		PRINT('MOVE ''jobdigpoint'' FAILED Stored Procedure: ''sp_move_jobdigpoint''');
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END