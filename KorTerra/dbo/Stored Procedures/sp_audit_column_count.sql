﻿CREATE PROCEDURE [dbo].[sp_audit_column_count]
    @tableName VARCHAR(50) = NULL,
    @sourceServer VARCHAR(50) = NULL,
    @sourceDatabase VARCHAR(50) = NULL,
	@expectedColumnCount int
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @SQLCommand NVARCHAR(MAX);
    DECLARE @ParmDefinition nvarchar(500);
	DECLARE @CountSQLQuery varchar(30);
    SET @SQLCommand = N'SELECT @result = COUNT(*) FROM ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.sys.columns c
		JOIN ['+ @sourceServer + ']' + '.' + @sourceDatabase + '.sys.tables t on c.object_id = t.object_id
        WHERE t.name = ''' + @tableName + '''
		AND is_identity <> 1';
	SET @ParmDefinition = N'@result varchar(30) OUTPUT';
	BEGIN TRY
		EXEC sp_executesql @SQLCommand, @ParmDefinition, @result=@CountSQLQuery OUTPUT
		SELECT CAST(@CountSQLQuery as int) [Column Count Match];
		IF @CountSQLQuery <> @expectedColumnCount
		BEGIN
			PRINT @SQLCommand + ' FAILED!'
			DECLARE @errMsg VARCHAR(200) = 'TABLE: ' + @tableName + ' FAILED AUDIT CHECK, COLUMN COUNT DOES NOT MATCH!'
			RAISERROR (@errMsg, 15, 10)
		END
		ELSE IF @CountSQLQuery = @expectedColumnCount
		BEGIN 
			PRINT @tableName + ' Columns Match'
		END
		   
	END TRY
    
	BEGIN CATCH 
		PRINT('COLUMN AUDIT MOVED TABLE FAILED: ' + @tableName);
		declare @error int, @message varchar(4000), @xstate int;
        select @error = ERROR_NUMBER(),
        @message = ERROR_MESSAGE(), 
		@xstate = XACT_STATE();
		RAISERROR (@message, 15, 10);
	END CATCH
END;