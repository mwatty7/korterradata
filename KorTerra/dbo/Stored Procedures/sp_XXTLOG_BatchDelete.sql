﻿ 
CREATE PROCEDURE dbo.sp_XXTLOG_BatchDelete
    @increment BIGINT

AS
     
    BEGIN TRY
     
    -- Declare local variables
    DECLARE @sqlstr NVARCHAR(2000);
    DECLARE @rowcount BIGINT;
    DECLARE @loopcount BIGINT;
    DECLARE @ParmDefinition nvarchar(500);
	DECLARE @recordsToRetain BIGINT;
	DECLARE @tablename SYSNAME; 


	SET @increment = 500;
	SET @tablename = 'XXtracelog'


     
    -- Set the parameters for the sp_executesql statement
    SET @ParmDefinition = N'@rowcountOUT BIGINT OUTPUT';
    -- Initialize the loop counter
    SET @loopcount = 1;
     
    -- Build the dynamic SQL string to return the row count
    -- Note that the input parameters are concatenated into the string, while the output parameter is contained in the string
    -- Also note that running a COUNT(*) on a large table can take a long time


	SELECT @recordsToRetain = ISNULL(
	(SELECT CONVERT(BIGINT, value) FROM XXconfig
	WHERE customerid = 'DEFAULT' AND sectionid = 'TRACELOG'
	AND name = 'RECORDSTORETAIN'), 100000)
	;
	PRINT 'recordsToRetain = ' + CONVERT(VARCHAR, @recordsToRetain)
 
	SELECT @rowcount =  COUNT(*) FROM XXtracelog WHERE logid < (
		SELECT MAX(logid) - @recordsToRetain
		FROM XXtracelog)

	PRINT 'rowCount = ' + CONVERT(VARCHAR, @rowcount)
      
    -- Perform the loop while there are rows to delete
    WHILE @loopcount < @rowcount
    BEGIN
          
         -- Build a dynamic SQL string to delete rows
SET @sqlstr = 'DELETE TOP ('+CAST(@increment AS VARCHAR(10))+') FROM XXtracelog WHERE logid < (SELECT MAX(logid) -'+CONVERT(VARCHAR,@recordsToRetain)+' FROM XXtracelog)';          
         -- Execute the dynamic SQL string to delete a batch of rows
         EXEC(@sqlstr);
          
         -- Add the @increment value to @loopcount
         SET @loopcount = @loopcount + @increment;
          
         PRINT CAST(@increment AS VARCHAR(10)) + ' rows deleted.'
          
    END
     
    END TRY
     
    BEGIN CATCH
         
        SELECT
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;
 
    END CATCH