﻿CREATE VIEW workexp AS
SELECT	paymastr.creationdtdate, paymastr.creationdttime, 
	   	paymastr.operatorid, 0 as cost,
	   	workdetl.workduration as duration, workdetl.workcode as workcode,
	   	0 as expcode 
FROM 	paymastr, workdetl, operator
WHERE 	paymastr.operatorid = operator.operatorid AND 
		paymastr.customerid = operator.customerid AND
		operator.inclpayroll = 1 AND
		paymastr.operatorid = workdetl.operatorid AND
		paymastr.customerid = workdetl.customerid AND
		paymastr.creationdtdate = workdetl.creationdtdate AND
		paymastr.creationdttime = workdetl.creationdttime 
UNION 
SELECT	paymastr.creationdtdate, paymastr.creationdttime,
		paymastr.operatorid, expdetl.expcost as cost, 
		0 as duration, 0 as workcode, expdetl.expcode as expcode
FROM	paymastr, expdetl, operator
WHERE	paymastr.operatorid = operator.operatorid AND
	paymastr.customerid = operator.customerid AND
	operator.inclpayroll = 1 AND
	paymastr.operatorid = expdetl.operatorid AND
	paymastr.customerid = expdetl.customerid AND
	paymastr.creationdtdate = expdetl.creationdtdate AND
	paymastr.creationdttime = expdetl.creationdttime;