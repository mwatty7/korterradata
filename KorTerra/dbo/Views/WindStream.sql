﻿CREATE VIEW dbo.WindStream
AS
SELECT        j.jobid AS 'Ticket ID', j.latestxmitdtdate AS 'Received Date', j.latestxmitdttime AS 'Received Time', j.origdtdate AS 'Taken Date', j.origdttime AS 'Taken Time', j.wtbdtdate AS 'Response Due Date', 
                         j.wtbdttime AS 'Response Due Time', j.meetdtdate AS 'Meet Date', j.meetdttime AS 'Meet Time', j.isexplosive AS 'Explosive', v.reasonid AS 'Code', j.priority AS 'Ticket Type', j.company AS 'Excavator Name', 
                         j.callstate AS 'Excavator State', j.callcity AS 'Excavator City', j.callzip AS 'Excavator Zip Code', j.subdivision AS 'Subdivision', j.donefor AS 'Done for', j.worktype AS 'Work Type', j.state AS 'State', 
                         j.county AS 'County', j.city AS 'Place', j.address AS 'Address', j.street AS 'Street', j.contact AS 'Contact Name', j.phone AS 'Contact Phone', j.callfax AS 'Contact Fax', j.callemail AS 'Contact Email', 
                         j.status AS 'Ticket Status', j.lattitude, j.longitude,
                             (SELECT        TOP (1) reasonid
                               FROM            dbo.visitcommon
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt) AS Expr1,
                             (SELECT        TOP (1) completedby
                               FROM            dbo.visitcommon AS visitcommon_5
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt) AS Expr2,
                             (SELECT        TOP (1) completiondt
                               FROM            dbo.visitcommon AS visitcommon_4
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt) AS Expr3,
                             (SELECT        TOP (1) reasonid
                               FROM            dbo.visitcommon AS visitcommon_3
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt DESC) AS Expr4,
                             (SELECT        TOP (1) completedby
                               FROM            dbo.visitcommon AS visitcommon_2
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt DESC) AS Expr5,
                             (SELECT        TOP (1) completiondt
                               FROM            dbo.visitcommon AS visitcommon_1
                               WHERE        (jobid = j.jobid) AND (customerid = j.customerid)
                               ORDER BY completiondt DESC) AS Expr6
FROM            dbo.visitcommon AS v INNER JOIN
                         dbo.job AS j ON v.customerid = j.customerid AND v.jobid = j.jobid
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "j"
            Begin Extent = 
               Top = 6
               Left = 292
               Bottom = 136
               Right = 495
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'WindStream';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'WindStream';

