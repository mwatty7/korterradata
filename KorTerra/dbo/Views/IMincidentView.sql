﻿CREATE VIEW [dbo].[IMincidentView]
AS
SELECT
	i.createdbyoperatorid,
	i.createddtutc,
	c.company,
	ic.customerid,
	ic.incidentnumber,
	ic.revisionnumber,
	ic.revisedby,
	ic.reviseddtutc,
	ic.incidentstatus,
	ic.priority,
	ic.incidentassignedto,
	ic.isvalidticket,
	ic.ticket,
	ic.emergencyticket,
	ic.ticketdueutc,
	ic.memberid,
	ic.membercode,
	ic.ishighprofile,
	ic.callername,
	ic.callerphone,
	ic.reportedstreetnumber,
	ic.reportedaddress1,
	ic.reportedaddress2,
	ic.reportedcity,
	ic.reportedcounty,
	ic.reportedstate,
	ic.reportedzip,
	ic.reportedcountry,
	ic.reportednearinter,
	ic.reportedlatitude,
	ic.reportedlongitude,
	ic.reporteddtutc,
	ic.reportednotes,
	ic.investigationstatus,
	ic.investigationassignedto,
	ic.locatoroperatorid,
	ic.actualstreetnumber,
	ic.actualaddress1,
	ic.actualaddress2,
	ic.actualcity,
	ic.actualcounty,
	ic.actualstate,
	ic.actualzip,
	ic.actualcountry,
	ic.actualnearinter,
	ic.actuallatitude,
	ic.actuallongitude,
	ic.descriptionofmarks,
	ic.ismarkedcorrectly,
	ic.ismarksvisible,
	ic.isfacilityabandoned,
	ic.isproject,
	ic.isrow,
	ic.isonecallmember,
	ic.isjointtrench,
	ic.isexcavatordowntime,
	ic.excavatordowntimeamount,
	ic.contractorid,
	ic.subcontractorid,
	ic.damagecause,
	ic.rootcause,
	ic.extentofdamage,
	ic.isdamaged,
	ic.isserviceinterruption,
	ic.serviceinterruptionduration,
	ic.serviceinterruptioncost,
	ic.customersaffected,
	ic.injuries,
	ic.fatalities,
	ic.regionid,
	ic.districtid,
	ic.investigatorphone,
	ic.duration,
	ic.dpsexperience,
	ic.investigationdtutc,
	ic.ticketstreetnumber,
	ic.ticketaddress1,
	ic.ticketaddress2,
	ic.ticketcity,
	ic.ticketcounty,
	ic.ticketstate,
	ic.ticketzip,
	ic.ticketcountry,
	ic.ticketnearinter,
	ic.ticketlatitude,
	ic.ticketlongitude,
	ic.facilitytypeid,
	ic.facilitysizeid,
	ic.materialid,
	ic.damagecodeid,
	ic.damagereasonid,
	ic.deviceid,
	ic.devicefrequencyid,
	ic.excavatortypeid,
	ic.excavationequipmentid,
	ic.excavationtypeid,
	ic.conditionofmarksid,
	ic.polygonpoints,
	ic.intrefnum,
	ic.InvoiceNumber,
	ic.InvoiceDate,
	ic.ReceivedDate,
	ic.ProcessedDate,
	ic.InvoiceStatus,
	ic.AuthorizedByOperatorId,
	ic.islocateourresponsibility,
	ic.onecallcenterid,
	ic.iscallcenternotified,
	ic.rightofwaytypeid,
	ic.notifieddtutc,
	facilitytype.codeid AS facilitytype,
	facilitysize.codeid AS facilitysize,
	material.codeid AS material,
	damagecode.codeid AS damagecode,
	damagereason.codeid AS damagereason,
	device.codeid AS device,
	devicefrequency.codeid AS devicefrequency,
	excavatortype.codeid AS excavatortype,
	excavationequipment.codeid AS excavationequipment,
	excavationtype.codeid AS excavationtype,
	conditionofmarks.codeid AS conditionofmarks,
	onecallcenter.codeid AS onecallcenter,
	rightofwaytype.codeid AS rightofwaytype 
FROM
	dbo.IMincident AS i 
	LEFT OUTER JOIN
		dbo.IMincidentContent AS ic 
		ON ic.customerid = i.customerid 
		AND ic.incidentnumber = i.incidentnumber 
		AND ic.revisionnumber = i.currentrevisionnumber 
	LEFT OUTER JOIN
		dbo.IMcontractor AS c 
		ON ic.customerid = c.customerid 
		AND ic.contractorid = c.contractorid 
	LEFT OUTER JOIN
		dbo.IMcode AS facilitytype 
		ON facilitytype.customerid = ic.customerid 
		AND facilitytype.codetype = 'FACILITYTYPE' 
		AND facilitytype.id = ic.facilitytypeid 
	LEFT OUTER JOIN
		dbo.IMcode AS facilitysize 
		ON facilitysize.customerid = ic.customerid 
		AND facilitysize.codetype = 'FACILITYSIZE' 
		AND facilitysize.id = ic.facilitysizeid 
	LEFT OUTER JOIN
		dbo.IMcode AS material 
		ON material.customerid = ic.customerid 
		AND material.codetype = 'MATERIAL' 
		AND material.id = ic.materialid 
	LEFT OUTER JOIN
		dbo.IMcode AS damagecode 
		ON damagecode.customerid = ic.customerid 
		AND damagecode.codetype = 'DAMAGECODE' 
		AND damagecode.id = ic.damagecodeid 
	LEFT OUTER JOIN
		dbo.IMcode AS damagereason 
		ON damagereason.customerid = ic.customerid 
		AND damagereason.codetype = 'DAMAGEREASON' 
		AND damagereason.id = ic.damagereasonid 
	LEFT OUTER JOIN
		dbo.IMcode AS device 
		ON device.customerid = ic.customerid 
		AND device.codetype = 'DEVICE' 
		AND device.id = ic.deviceid 
	LEFT OUTER JOIN
		dbo.IMcode AS devicefrequency 
		ON devicefrequency.customerid = ic.customerid 
		AND devicefrequency.codetype = 'DEVICEFREQUENCY' 
		AND devicefrequency.id = ic.devicefrequencyid 
	LEFT OUTER JOIN
		dbo.IMcode AS excavatortype 
		ON excavatortype.customerid = ic.customerid 
		AND excavatortype.codetype = 'EXCAVATORTYPE' 
		AND excavatortype.id = ic.excavatortypeid 
	LEFT OUTER JOIN
		dbo.IMcode AS excavationequipment 
		ON excavationequipment.customerid = ic.customerid 
		AND excavationequipment.codetype = 'EXCAVATIONEQUIPMENT' 
		AND excavationequipment.id = ic.excavationequipmentid 
	LEFT OUTER JOIN
		dbo.IMcode AS excavationtype 
		ON excavationtype.customerid = ic.customerid 
		AND excavationtype.codetype = 'EXCAVATIONTYPE' 
		AND excavationtype.id = ic.excavationtypeid 
	LEFT OUTER JOIN
		dbo.IMcode AS conditionofmarks 
		ON conditionofmarks.customerid = ic.customerid 
		AND conditionofmarks.codetype = 'CONDITIONOFMARKS' 
		AND conditionofmarks.id = ic.conditionofmarksid 
	LEFT OUTER JOIN
		dbo.IMcode AS onecallcenter 
		ON onecallcenter.customerid = ic.customerid 
		AND onecallcenter.codetype = 'ONECALLCENTER' 
		AND onecallcenter.id = ic.onecallcenterid 
	LEFT OUTER JOIN
		dbo.IMcode AS rightofwaytype 
		ON rightofwaytype.customerid = ic.customerid 
		AND rightofwaytype.codetype = 'RIGHTOFWAY_TYPE' 
		AND rightofwaytype.id = ic.rightofwaytypeid
;