﻿CREATE VIEW timeexpsum AS
SELECT	  paymastr.creationdtdate, paymastr.creationdttime, paymastr.operatorid, paymastr.status, operator.regionid, operator.districtid, 0 as cost, SUM(workdetl.workduration) as duration, paymastr.workdate 
FROM	  paymastr, workdetl, operator
WHERE	  paymastr.operatorid = operator.operatorid AND
	  operator.inclpayroll = 1 AND
	  paymastr.operatorid = workdetl.operatorid AND
	  paymastr.creationdtdate = workdetl.creationdtdate AND
	  paymastr.creationdttime = workdetl.creationdttime
GROUP BY  paymastr.creationdtdate, paymastr.creationdttime, paymastr.operatorid, paymastr.status, operator.regionid, operator.districtid, paymastr.workdate
UNION SELECT paymastr.creationdtdate, paymastr.creationdttime, paymastr.operatorid, paymastr.status, operator.regionid, operator.districtid, SUM(expdetl.expcost) as cost, 0 as duration, paymastr.workdate
FROM	  paymastr, expdetl, operator
WHERE 	  paymastr.operatorid = operator.operatorid AND
	  operator.inclpayroll = 1 AND
	  paymastr.operatorid = expdetl.operatorid AND
	  paymastr.creationdtdate = expdetl.creationdtdate AND
	  paymastr.creationdttime = expdetl.creationdttime
GROUP BY  paymastr.creationdtdate, paymastr.creationdttime, paymastr.operatorid, paymastr.status, operator.regionid, operator.districtid, paymastr.workdate;