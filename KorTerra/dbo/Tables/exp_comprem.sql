﻿CREATE TABLE [dbo].[exp_comprem] (
    [k_customerid]     VARCHAR (16)  NOT NULL,
    [e_localid]        VARCHAR (255) NOT NULL,
    [k_creationdtdate] DATETIME      NOT NULL,
    [k_creationdttime] VARCHAR (12)  NOT NULL,
    [seqno]            SMALLINT      NOT NULL,
    [remarks]          VARCHAR (80)  NULL,
    CONSTRAINT [exp_compremPrimaryKey] PRIMARY KEY CLUSTERED ([k_customerid] ASC, [e_localid] ASC, [k_creationdtdate] ASC, [k_creationdttime] ASC, [seqno] ASC)
);

