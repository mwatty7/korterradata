﻿CREATE TABLE [dbo].[kt_request] (
    [requestGuid]    UNIQUEIDENTIFIER NOT NULL,
    [ticketGuid]     UNIQUEIDENTIFIER NOT NULL,
    [membercodeGuid] UNIQUEIDENTIFIER NOT NULL,
    [mobileGuid]     UNIQUEIDENTIFIER NULL,
    [operatorGuid]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_kt_request] PRIMARY KEY CLUSTERED ([requestGuid] ASC),
    CONSTRAINT [FK_kt_request_ticketGuid_kt_ticket_ticketGuid] FOREIGN KEY ([ticketGuid]) REFERENCES [dbo].[kt_ticket] ([ticketGuid])
);

