﻿CREATE TABLE [dbo].[__vmdetail3] (
    [customerid]          VARCHAR (32)  NOT NULL,
    [requesttype]         VARCHAR (9)   NOT NULL,
    [sectionid]           VARCHAR (32)  NOT NULL,
    [fieldid]             VARCHAR (32)  NOT NULL,
    [fielddescription]    VARCHAR (100) NULL,
    [sequence]            SMALLINT      NULL,
    [korterrareportfield] VARCHAR (100) NULL,
    [html]                VARCHAR (MAX) NULL,
    [validation]          VARCHAR (MAX) NULL,
    [visitcommonfield]    VARCHAR (128) NULL,
    CONSTRAINT [PK_vmdetail] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC, [sectionid] ASC, [fieldid] ASC),
    CONSTRAINT [FK_vmsection_vmdetail] FOREIGN KEY ([customerid], [requesttype], [sectionid]) REFERENCES [dbo].[vmsection] ([customerid], [requesttype], [sectionid])
);

