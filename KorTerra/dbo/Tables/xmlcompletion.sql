﻿CREATE TABLE [dbo].[xmlcompletion] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [membercode]     VARCHAR (16)  NOT NULL,
    [requesttype]    VARCHAR (9)   NOT NULL,
    [creationdtdate] DATETIME      NOT NULL,
    [creationdttime] VARCHAR (12)  NOT NULL,
    [ackcode]        VARCHAR (10)  NULL,
    [description]    VARCHAR (255) NULL,
    [ackdtdate]      DATETIME      NULL,
    [ackdttime]      VARCHAR (12)  NULL,
    [status]         CHAR (1)      NULL,
    [versiondtdate]  DATETIME      NULL,
    [versiondttime]  VARCHAR (12)  NULL,
    [xmlfile]        VARCHAR (128) NULL,
    [completionid]   VARCHAR (128) NULL,
    CONSTRAINT [xmlcompletionPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [xmlcompletionxmlfileindex]
    ON [dbo].[xmlcompletion]([xmlfile] ASC);

