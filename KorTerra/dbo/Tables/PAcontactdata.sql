﻿CREATE TABLE [dbo].[PAcontactdata] (
    [contactdataid] INT           IDENTITY (1, 1) NOT NULL,
    [contactid]     INT           NOT NULL,
    [maintid]       INT           NOT NULL,
    [value]         VARCHAR (MAX) NULL,
    CONSTRAINT [PK_PAcontactdata] PRIMARY KEY CLUSTERED ([contactdataid] ASC),
    CONSTRAINT [FK_PAcontactdataContactId] FOREIGN KEY ([contactid]) REFERENCES [dbo].[PAcontact] ([contactid]),
    CONSTRAINT [FK_PAcontactdataMaintId] FOREIGN KEY ([maintid]) REFERENCES [dbo].[PAmaint] ([maintid])
);

