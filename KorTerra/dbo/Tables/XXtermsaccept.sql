﻿CREATE TABLE [dbo].[XXtermsaccept] (
    [customerid]    NVARCHAR (32) NOT NULL,
    [applicationid] NVARCHAR (32) NOT NULL,
    [operatorid]    NVARCHAR (16) NULL,
    [acceptdt]      DATETIME2 (7) NOT NULL,
    [checksum]      NVARCHAR (32) NULL,
    CONSTRAINT [XXtermsacceptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [applicationid] ASC)
);

