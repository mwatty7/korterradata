﻿CREATE TABLE [dbo].[XXgenericnotes] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [operatorid]    VARCHAR (16)  NOT NULL,
    [applicationid] VARCHAR (16)  NOT NULL,
    [type]          VARCHAR (64)  NOT NULL,
    [name]          VARCHAR (64)  NOT NULL,
    [createddt]     DATETIME      NOT NULL,
    [isactive]      SMALLINT      NULL,
    [content]       VARCHAR (MAX) NULL,
    CONSTRAINT [PK_XXgenericnotes] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [applicationid] ASC, [type] ASC, [name] ASC, [createddt] ASC)
);

