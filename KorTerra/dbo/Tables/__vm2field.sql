﻿CREATE TABLE [dbo].[__vm2field] (
    [customerid]         CHAR (32)      NOT NULL,
    [requesttype]        CHAR (9)       NOT NULL,
    [sectionid]          NVARCHAR (32)  NOT NULL,
    [fieldid]            NVARCHAR (32)  NOT NULL,
    [description]        NVARCHAR (255) NULL,
    [spanishdescription] NVARCHAR (255) NULL,
    [fieldtype]          NVARCHAR (32)  NULL,
    [fieldorder]         INT            NULL,
    [value]              NVARCHAR (MAX) NULL,
    [width]              NVARCHAR (255) NULL,
    CONSTRAINT [vm2fieldPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC, [sectionid] ASC, [fieldid] ASC)
);

