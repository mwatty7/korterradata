﻿CREATE TABLE [dbo].[reminderdata] (
    [reminderdataid] UNIQUEIDENTIFIER NOT NULL,
    [reminderid]     UNIQUEIDENTIFIER NOT NULL,
    [fieldid]        VARCHAR (32)     NOT NULL,
    [value]          VARCHAR (MAX)    NOT NULL,
    CONSTRAINT [reminderdataPrimaryKey] PRIMARY KEY NONCLUSTERED ([reminderdataid] ASC),
    CONSTRAINT [FK_reminderdata_reminderid] FOREIGN KEY ([reminderid]) REFERENCES [dbo].[remindercommon] ([reminderid])
);

