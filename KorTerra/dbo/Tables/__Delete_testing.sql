﻿CREATE TABLE [dbo].[__Delete_testing] (
    [id]   INT           IDENTITY (1, 1) NOT NULL,
    [text] VARCHAR (MAX) NULL,
    CONSTRAINT [testing_PK] PRIMARY KEY CLUSTERED ([id] ASC)
);

