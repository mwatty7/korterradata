﻿CREATE TABLE [dbo].[autosendrule] (
    [customerid]          VARCHAR (32)  NOT NULL,
    [seqno]               SMALLINT      NOT NULL,
    [description]         VARCHAR (128) NULL,
    [occid]               VARCHAR (8)   NULL,
    [priority]            VARCHAR (36)  NULL,
    [origpriority]        VARCHAR (80)  NULL,
    [ruleorder]           SMALLINT      NULL,
    [regionid]            VARCHAR (16)  NULL,
    [districtid]          VARCHAR (16)  NULL,
    [mobileid]            VARCHAR (16)  NULL,
    [starttime]           VARCHAR (12)  NULL,
    [endtime]             VARCHAR (12)  NULL,
    [delaybyminutes]      SMALLINT      NULL,
    [workdays]            SMALLINT      NULL,
    [holidaysandweekends] SMALLINT      NULL,
    CONSTRAINT [PK_autosendrule] PRIMARY KEY CLUSTERED ([customerid] ASC, [seqno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [autosend_rdmindex]
    ON [dbo].[autosendrule]([customerid] ASC, [regionid] ASC, [districtid] ASC, [mobileid] ASC);

