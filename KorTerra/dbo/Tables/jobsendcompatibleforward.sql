﻿CREATE TABLE [dbo].[jobsendcompatibleforward] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [customerid]  VARCHAR (32)  NOT NULL,
    [jobid]       VARCHAR (16)  NOT NULL,
    [membercode]  VARCHAR (16)  NULL,
    [mobileid]    VARCHAR (16)  NULL,
    [status]      VARCHAR (16)  NOT NULL,
    [createddt]   DATETIME2 (7) NULL,
    [attemptdt]   DATETIME2 (7) NULL,
    [numattempts] INT           NULL,
    [failreason]  VARCHAR (MAX) NULL,
    CONSTRAINT [PK_jobsendcompatibleforward] PRIMARY KEY CLUSTERED ([id] ASC)
);

