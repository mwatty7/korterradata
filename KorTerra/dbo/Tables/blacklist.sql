﻿CREATE TABLE [dbo].[blacklist] (
    [id]              INT           IDENTITY (1, 1) NOT NULL,
    [customerid]      VARCHAR (32)  NOT NULL,
    [phone]           VARCHAR (21)  NULL,
    [username]        VARCHAR (80)  NULL,
    [domain]          VARCHAR (80)  NULL,
    [listtype]        CHAR (1)      NULL,
    [type]            VARCHAR (16)  NULL,
    [creationdateutc] DATETIME2 (7) NULL,
    [createdby]       VARCHAR (16)  NULL,
    [comments]        VARCHAR (255) NULL,
    CONSTRAINT [PK_blacklist] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [blacklisttypeindex]
    ON [dbo].[blacklist]([type] ASC, [phone] ASC, [username] ASC, [domain] ASC);

