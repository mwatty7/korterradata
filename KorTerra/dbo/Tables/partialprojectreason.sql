﻿CREATE TABLE [dbo].[partialprojectreason] (
    [pprid]       UNIQUEIDENTIFIER NOT NULL,
    [customerid]  VARCHAR (32)     NOT NULL,
    [code]        VARCHAR (32)     NOT NULL,
    [description] VARCHAR (80)     NOT NULL,
    [isproject]   BIT              NOT NULL,
    [isactive]    BIT              NOT NULL,
    CONSTRAINT [_partialprojectreason] PRIMARY KEY NONCLUSTERED ([pprid] ASC),
    CONSTRAINT [uq_customerid_code_isproject] UNIQUE NONCLUSTERED ([customerid] ASC, [code] ASC, [isproject] ASC)
);

