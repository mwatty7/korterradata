﻿CREATE TABLE [dbo].[damageweb] (
    [damageid]                VARCHAR (16)    NOT NULL,
    [actualaddress]           VARCHAR (32)    NULL,
    [actualstreet]            VARCHAR (32)    NULL,
    [actualcity]              VARCHAR (64)    NULL,
    [actualcounty]            VARCHAR (32)    NULL,
    [actualstate]             VARCHAR (3)     NULL,
    [actualzipcode]           VARCHAR (11)    NULL,
    [facilitytypeid]          INT             NULL,
    [facilitysizeid]          INT             NULL,
    [materialtypeid]          INT             NULL,
    [memberid]                VARCHAR (16)    NULL,
    [membercode]              VARCHAR (16)    NULL,
    [utilityclaimsinvstgtr]   VARCHAR (32)    NULL,
    [contractorid]            INT             NULL,
    [subcontractor]           VARCHAR (32)    NULL,
    [isourlocate]             SMALLINT        NULL,
    [conditionmarks]          CHAR (1)        NULL,
    [issafezone]              SMALLINT        NULL,
    [isphotos]                SMALLINT        NULL,
    [damagecodeid]            INT             NULL,
    [damagereasonid]          INT             NULL,
    [invstgtrpayrollnbr]      VARCHAR (16)    NULL,
    [invstgnreportdtdate]     DATETIME        NULL,
    [invstgnreportdttime]     VARCHAR (12)    NULL,
    [invduration]             DECIMAL (15, 2) NULL,
    [investigation_status_id] INT             NULL,
    [statuschangedtdate]      DATETIME        NULL,
    [rptreceiveddtdate]       DATETIME        NULL,
    [rptreceiveddttime]       VARCHAR (12)    NULL,
    [actualnearinter]         VARCHAR (32)    NULL,
    [ishighprofile]           SMALLINT        NULL,
    CONSTRAINT [damagewebPrimaryKey] PRIMARY KEY CLUSTERED ([damageid] ASC)
);

