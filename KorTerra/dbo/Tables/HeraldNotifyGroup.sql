﻿CREATE TABLE [dbo].[HeraldNotifyGroup] (
    [hng_id]     INT          IDENTITY (1, 1) NOT NULL,
    [customerid] VARCHAR (32) NOT NULL,
    [msggroupid] VARCHAR (16) NOT NULL,
    [msgtype]    VARCHAR (32) NOT NULL,
    CONSTRAINT [PK_HeraldNotifyGroup] PRIMARY KEY CLUSTERED ([hng_id] ASC),
    CONSTRAINT [FK_HeraldNotifyCustomer] FOREIGN KEY ([customerid]) REFERENCES [dbo].[customer] ([customerid]),
    CONSTRAINT [FK_HeraldNotifyMsgGroup] FOREIGN KEY ([customerid], [msggroupid]) REFERENCES [dbo].[msggroup] ([customerid], [msggroupid]),
    CONSTRAINT [FK_HeraldNotifyMsgType] FOREIGN KEY ([msgtype]) REFERENCES [dbo].[msgtype] ([msgtype])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [HeraldNotifyGroupIndex]
    ON [dbo].[HeraldNotifyGroup]([customerid] ASC, [msggroupid] ASC, [msgtype] ASC);

