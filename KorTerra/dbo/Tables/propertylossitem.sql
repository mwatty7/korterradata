﻿CREATE TABLE [dbo].[propertylossitem] (
    [itemid]      VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [propertylossitemPrimaryKey] PRIMARY KEY CLUSTERED ([itemid] ASC)
);

