﻿CREATE TABLE [dbo].[savedrpttemplateline] (
    [customerid] VARCHAR (32)  NOT NULL,
    [reportid]   VARCHAR (32)  NOT NULL,
    [templateid] VARCHAR (32)  NOT NULL,
    [checksum]   VARCHAR (32)  NOT NULL,
    [seqno]      SMALLINT      NOT NULL,
    [line]       VARCHAR (255) NULL,
    CONSTRAINT [savedrpttemplatelinePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [templateid] ASC, [checksum] ASC, [seqno] ASC)
);

