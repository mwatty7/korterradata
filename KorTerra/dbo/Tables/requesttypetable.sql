﻿CREATE TABLE [dbo].[requesttypetable] (
    [customerid]  VARCHAR (32) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [tablename]   VARCHAR (16) NULL,
    CONSTRAINT [requesttypetablePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC)
);

