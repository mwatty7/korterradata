﻿CREATE TABLE [dbo].[menu] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [menuid]        VARCHAR (16)  NOT NULL,
    [applicationid] VARCHAR (16)  NOT NULL,
    [menuname]      VARCHAR (128) NULL,
    [menuitem]      VARCHAR (128) NULL,
    CONSTRAINT [menuPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [menuid] ASC, [applicationid] ASC)
);

