﻿CREATE TABLE [dbo].[worktype] (
    [customerid]     VARCHAR (32)    NOT NULL,
    [workcode]       INT             NOT NULL,
    [workdesc]       VARCHAR (40)    NULL,
    [suffix]         VARCHAR (16)    NULL,
    [affectovertime] SMALLINT        NULL,
    [st_minor]       VARCHAR (16)    NULL,
    [ot_minor]       VARCHAR (16)    NULL,
    [st_earncode]    VARCHAR (8)     NULL,
    [ot_earncode]    VARCHAR (8)     NULL,
    [onmobile]       SMALLINT        NULL,
    [requirerem]     SMALLINT        NULL,
    [isovertime]     SMALLINT        NULL,
    [minduration]    DECIMAL (15, 2) NULL,
    [maxduration]    DECIMAL (15, 2) NULL,
    [incrduration]   DECIMAL (15, 2) NULL,
    CONSTRAINT [worktypePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [workcode] ASC)
);

