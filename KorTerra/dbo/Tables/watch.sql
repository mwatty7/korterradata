﻿CREATE TABLE [dbo].[watch] (
    [customerid] VARCHAR (32) NOT NULL,
    [heraldid]   VARCHAR (32) NOT NULL,
    [msggroupid] VARCHAR (16) NOT NULL,
    CONSTRAINT [watchPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [heraldid] ASC, [msggroupid] ASC)
);

