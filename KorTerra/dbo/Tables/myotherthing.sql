﻿CREATE TABLE [dbo].[myotherthing] (
    [id]  INT          IDENTITY (1, 1) NOT NULL,
    [foo] VARCHAR (32) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

