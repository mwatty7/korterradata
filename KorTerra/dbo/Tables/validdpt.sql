﻿CREATE TABLE [dbo].[validdpt] (
    [customerid]      VARCHAR (32) NOT NULL,
    [membercode]      VARCHAR (16) NOT NULL,
    [requesttype]     VARCHAR (9)  NOT NULL,
    [departmentid]    VARCHAR (16) NOT NULL,
    [seqno]           SMALLINT     NOT NULL,
    [isdefault]       SMALLINT     NULL,
    [priority]        VARCHAR (16) NULL,
    [origpriority]    VARCHAR (80) NULL,
    [zoneid]          VARCHAR (16) NULL,
    [fromorigtime]    VARCHAR (12) NULL,
    [toorigtime]      VARCHAR (12) NULL,
    [days]            CHAR (1)     NULL,
    [displaylocation] CHAR (1)     NULL,
    CONSTRAINT [validdptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [requesttype] ASC, [departmentid] ASC, [seqno] ASC)
);

