﻿CREATE TABLE [dbo].[ScreeningRulevalue] (
    [rulevalueid] INT            IDENTITY (1, 1) NOT NULL,
    [ruleid]      INT            NOT NULL,
    [value]       VARCHAR (2048) NOT NULL,
    CONSTRAINT [PK_ScreeningRulevalue] PRIMARY KEY CLUSTERED ([rulevalueid] ASC),
    CONSTRAINT [FK_ScreeningRulevalueScreeningRule] FOREIGN KEY ([ruleid]) REFERENCES [dbo].[ScreeningRule] ([ruleid])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'One of the values that is matched for a rule, e.g., a county name if a rule matches a list of counties', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRulevalue';

