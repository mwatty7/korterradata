﻿CREATE TABLE [dbo].[msggroup] (
    [customerid] VARCHAR (32) NOT NULL,
    [msggroupid] VARCHAR (16) NOT NULL,
    CONSTRAINT [msggroupPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [msggroupid] ASC)
);

