﻿CREATE TABLE [dbo].[claimantcontactaudit] (
    [claimid]        VARCHAR (16) NOT NULL,
    [claimantid]     VARCHAR (32) NOT NULL,
    [creationdtdate] DATETIME     NOT NULL,
    [creationdttime] VARCHAR (12) NOT NULL,
    [contactname]    VARCHAR (32) NULL,
    [contactdtdate]  DATETIME     NULL,
    [contactdttime]  VARCHAR (12) NULL,
    [enteredby]      VARCHAR (16) NULL,
    [subject]        VARCHAR (80) NULL,
    CONSTRAINT [claimantcontactauditPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [claimantid] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);

