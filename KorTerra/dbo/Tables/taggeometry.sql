﻿CREATE TABLE [dbo].[taggeometry] (
    [taggeometryid] UNIQUEIDENTIFIER NOT NULL,
    [tagversionid]  UNIQUEIDENTIFIER NOT NULL,
    [description]   VARCHAR (255)    NOT NULL,
    [type]          VARCHAR (32)     NOT NULL,
    [geometry]      [sys].[geometry] NULL,
    CONSTRAINT [PK_taggeometry_taggeometryid] PRIMARY KEY NONCLUSTERED ([taggeometryid] ASC),
    FOREIGN KEY ([tagversionid]) REFERENCES [dbo].[tagversion] ([tagversionid])
);

