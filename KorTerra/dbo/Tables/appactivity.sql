﻿CREATE TABLE [dbo].[appactivity] (
    [application]     VARCHAR (32) NOT NULL,
    [activitytype]    VARCHAR (48) NULL,
    [activitydata]    VARCHAR (64) NULL,
    [activitydtdate]  DATETIME     NULL,
    [activitydttime]  VARCHAR (12) NULL,
    [lastcheckdtdate] DATETIME     NULL,
    [lastcheckdttime] VARCHAR (12) NULL,
    [startdtdate]     DATETIME     NULL,
    [startdttime]     VARCHAR (12) NULL,
    CONSTRAINT [appactivityPrimaryKey] PRIMARY KEY CLUSTERED ([application] ASC)
);

