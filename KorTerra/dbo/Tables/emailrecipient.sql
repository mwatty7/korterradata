﻿CREATE TABLE [dbo].[emailrecipient] (
    [emailrecipientid]      UNIQUEIDENTIFIER NOT NULL,
    [emailrecipientaddress] VARCHAR (255)    NULL,
    [customerid]            VARCHAR (32)     NOT NULL,
    CONSTRAINT [PK_emailrecipient_emailrecipientid] PRIMARY KEY NONCLUSTERED ([emailrecipientid] ASC)
);

