﻿CREATE TABLE [dbo].[XXemaildata] (
    [queueid]       UNIQUEIDENTIFIER NOT NULL,
    [customerid]    VARCHAR (32)     NOT NULL,
    [creationdtutc] DATETIME2 (7)    NOT NULL,
    [type]          VARCHAR (32)     NULL,
    [context]       VARCHAR (MAX)    NULL,
    [applicationid] VARCHAR (80)     NULL,
    [toaddress]     VARCHAR (256)    NULL,
    [fromaddress]   VARCHAR (256)    NULL,
    [subject]       VARCHAR (1024)   NULL,
    [emailbody]     VARCHAR (MAX)    NULL,
    [ishtml]        BIT              NULL,
    CONSTRAINT [PK_emaildata_id] PRIMARY KEY NONCLUSTERED ([queueid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_XXemaildata_customerid_queueid_include]
    ON [dbo].[XXemaildata]([customerid] ASC, [queueid] ASC)
    INCLUDE([creationdtutc], [type], [context], [applicationid], [toaddress], [fromaddress], [subject], [emailbody], [ishtml]);

