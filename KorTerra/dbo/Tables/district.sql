﻿CREATE TABLE [dbo].[district] (
    [customerid]        VARCHAR (32) NOT NULL,
    [regionid]          VARCHAR (16) NOT NULL,
    [districtid]        VARCHAR (16) NOT NULL,
    [description]       VARCHAR (32) NULL,
    [accntlocationcode] VARCHAR (3)  NULL,
    CONSTRAINT [districtPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [regionid] ASC, [districtid] ASC)
);

