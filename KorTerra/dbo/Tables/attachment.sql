﻿CREATE TABLE [dbo].[attachment] (
    [customerid]       VARCHAR (32)  NOT NULL,
    [jobid]            VARCHAR (16)  NOT NULL,
    [name]             VARCHAR (32)  NOT NULL,
    [forversiondtdate] DATETIME      NULL,
    [forversiondttime] VARCHAR (12)  NULL,
    [versiondtdate]    DATETIME      NULL,
    [versiondttime]    VARCHAR (12)  NULL,
    [versionauthor]    VARCHAR (16)  NULL,
    [senddtdate]       DATETIME      NULL,
    [senddttime]       VARCHAR (12)  NULL,
    [sendstatus]       CHAR (1)      NULL,
    [sendfailreason]   CHAR (1)      NULL,
    [sendackdtdate]    DATETIME      NULL,
    [sendackdttime]    VARCHAR (12)  NULL,
    [attachsize]       INT           NULL,
    [path]             VARCHAR (255) NULL,
    [forcesend]        SMALLINT      NULL,
    CONSTRAINT [attachmentPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [name] ASC)
);


GO
CREATE TRIGGER kt_tr_attachment_compatible
ON attachment
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_name VARCHAR(256);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_name VARCHAR(256);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_name=name, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_name=name
		FROM deleted;
	IF EXISTS (SELECT * FROM inserted) AND NOT EXISTS(SELECT * FROM deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS NULL)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		INSERT INTO attachmentbackward (customerid, jobid, name,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, name,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'FORWARD'
		FROM inserted;
	END
	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS (SELECT * FROM inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		INSERT INTO attachmentbackward (customerid, jobid, name,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, name,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'FORWARD'
		FROM deleted;
     END
	IF EXISTS(SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS NULL)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		-- We don't want to create a forward compatibility record for updates.
		-- The only time updates happen is when the receiver times out and resends.
		-- WRONG! Image Resizer is updating the records
	END
END