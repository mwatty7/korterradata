﻿CREATE TABLE [dbo].[IMwitness] (
    [customerid]     NVARCHAR (32)  NOT NULL,
    [incidentnumber] NVARCHAR (32)  CONSTRAINT [DF_IMwitness_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [witnessid]      INT            IDENTITY (1, 1) NOT NULL,
    [firstname]      NVARCHAR (32)  NULL,
    [lastname]       NVARCHAR (32)  NULL,
    [type]           NVARCHAR (32)  NULL,
    [createddtutc]   DATETIME       NULL,
    [phone]          NVARCHAR (32)  NULL,
    [altphone]       NVARCHAR (32)  NULL,
    [statement]      NVARCHAR (MAX) NULL,
    [company]        NVARCHAR (64)  NULL,
    [email]          NVARCHAR (256) NULL,
    CONSTRAINT [PK_IMwitness] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC, [witnessid] ASC),
    CONSTRAINT [FK_IMwitness_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

