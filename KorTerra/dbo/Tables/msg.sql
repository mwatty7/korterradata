﻿CREATE TABLE [dbo].[msg] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [msgtype]        VARCHAR (32)  NOT NULL,
    [msggroupid]     VARCHAR (16)  NOT NULL,
    [msgnumber]      INT           NOT NULL,
    [recvdtdate]     DATETIME      NULL,
    [recvdttime]     VARCHAR (12)  NULL,
    [msgtag]         VARCHAR (64)  NULL,
    [msgdescription] VARCHAR (128) NULL,
    [msgdata]        VARCHAR (128) NULL,
    [lockdtdate]     DATETIME      NULL,
    [lockdttime]     VARCHAR (12)  NULL,
    [lockedby]       VARCHAR (16)  NULL,
    [templock]       VARCHAR (16)  NULL,
    [msghandler]     VARCHAR (32)  NULL,
    CONSTRAINT [msgPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [msgtype] ASC, [msggroupid] ASC, [msgnumber] ASC)
);

