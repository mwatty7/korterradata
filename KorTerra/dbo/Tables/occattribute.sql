﻿CREATE TABLE [dbo].[occattribute] (
    [customerid] VARCHAR (32)  NOT NULL,
    [occid]      VARCHAR (8)   NOT NULL,
    [name]       VARCHAR (255) NOT NULL,
    [value]      VARCHAR (255) NULL,
    CONSTRAINT [occattributePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [name] ASC)
);

