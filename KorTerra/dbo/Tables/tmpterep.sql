﻿CREATE TABLE [dbo].[tmpterep] (
    [operatorid]    CHAR (16)       NULL,
    [startworkdate] DATETIME        NULL,
    [endworkdate]   DATETIME        NULL,
    [acctnum]       CHAR (20)       NULL,
    [earncode]      CHAR (9)        NULL,
    [description]   CHAR (40)       NULL,
    [duration]      NUMERIC (4, 2)  NULL,
    [major]         CHAR (9)        NULL,
    [cost]          NUMERIC (10, 2) NULL,
    [userid]        CHAR (16)       NULL
);

