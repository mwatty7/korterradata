﻿CREATE TABLE [dbo].[t_routingrulepoints] (
    [rrpointid] UNIQUEIDENTIFIER NOT NULL,
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [seqno]     INT              NOT NULL,
    [x]         FLOAT (53)       NULL,
    [y]         FLOAT (53)       NULL,
    CONSTRAINT [PK_t_routingrulepoint] PRIMARY KEY NONCLUSTERED ([rrpointid] ASC),
    CONSTRAINT [FK_t_routingrulevalue_rrvalueid] FOREIGN KEY ([rrvalueid]) REFERENCES [dbo].[t_routingrulevalue] ([rrvalueid])
);

