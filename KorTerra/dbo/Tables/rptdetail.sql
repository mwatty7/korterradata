﻿CREATE TABLE [dbo].[rptdetail] (
    [customerid]   VARCHAR (32) NOT NULL,
    [reportid]     VARCHAR (32) NOT NULL,
    [columnid]     VARCHAR (80) NOT NULL,
    [columndesc]   VARCHAR (80) NULL,
    [columnorder]  INT          NULL,
    [columnwidth]  INT          NULL,
    [iswordwrap]   SMALLINT     NULL,
    [isgroupby]    SMALLINT     NULL,
    [ordernum]     INT          NULL,
    [columnformat] VARCHAR (32) NULL,
    CONSTRAINT [rptdetailPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [columnid] ASC)
);

