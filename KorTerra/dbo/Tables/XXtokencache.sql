﻿CREATE TABLE [dbo].[XXtokencache] (
    [token]             VARCHAR (100) NOT NULL,
    [contents]          VARCHAR (MAX) NULL,
    [expirationMinutes] INT           NULL,
    [lastAccessed]      DATETIME      NULL,
    CONSTRAINT [PK_tokencache] PRIMARY KEY CLUSTERED ([token] ASC)
);

