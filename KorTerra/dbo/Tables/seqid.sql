﻿CREATE TABLE [dbo].[seqid] (
    [customerid] VARCHAR (32) NOT NULL,
    [name]       VARCHAR (16) NOT NULL,
    [maxseq]     SMALLINT     NULL,
    CONSTRAINT [seqidPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [name] ASC)
);

