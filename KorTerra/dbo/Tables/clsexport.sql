﻿CREATE TABLE [dbo].[clsexport] (
    [batchid]        VARCHAR (16) NOT NULL,
    [runsequence]    SMALLINT     NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [membercode]     VARCHAR (16) NOT NULL,
    [requesttype]    VARCHAR (9)  NOT NULL,
    [creationdtdate] DATETIME     NOT NULL,
    [creationdttime] VARCHAR (12) NOT NULL,
    [departmentid]   VARCHAR (16) NULL,
    CONSTRAINT [clsexportPrimaryKey] PRIMARY KEY CLUSTERED ([batchid] ASC, [runsequence] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [clsexportdeptindex]
    ON [dbo].[clsexport]([batchid] ASC, [departmentid] ASC);


GO
CREATE NONCLUSTERED INDEX [clsexportjobindex]
    ON [dbo].[clsexport]([jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC);

