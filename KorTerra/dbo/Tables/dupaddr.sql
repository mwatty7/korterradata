﻿CREATE TABLE [dbo].[dupaddr] (
    [customerid] VARCHAR (32) NOT NULL,
    [operatorid] VARCHAR (16) NOT NULL,
    [address]    VARCHAR (32) NOT NULL,
    [street]     VARCHAR (32) NOT NULL,
    [city]       VARCHAR (64) NOT NULL,
    [dupcount]   SMALLINT     NULL,
    CONSTRAINT [dupaddrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [address] ASC, [street] ASC, [city] ASC)
);

