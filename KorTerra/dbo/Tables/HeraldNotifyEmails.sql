﻿CREATE TABLE [dbo].[HeraldNotifyEmails] (
    [hnst_id]      INT           NOT NULL,
    [emailaddress] VARCHAR (320) NOT NULL,
    CONSTRAINT [PK_HeraldNotifyEmails] PRIMARY KEY CLUSTERED ([hnst_id] ASC, [emailaddress] ASC),
    CONSTRAINT [FK_HeraldNotifyEmails] FOREIGN KEY ([hnst_id]) REFERENCES [dbo].[HeraldNotifyScheduleTemplate] ([hnst_id])
);

