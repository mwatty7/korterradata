﻿CREATE TABLE [dbo].[exp_addmembers] (
    [k_customerid] VARCHAR (16) NOT NULL,
    [k_jobid]      VARCHAR (16) NOT NULL,
    [seqno]        SMALLINT     NOT NULL,
    [addmember]    VARCHAR (10) NULL,
    CONSTRAINT [exp_addmembersPrimaryKey] PRIMARY KEY CLUSTERED ([k_customerid] ASC, [k_jobid] ASC, [seqno] ASC)
);

