﻿CREATE TABLE [dbo].[visitdata] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [jobid]       VARCHAR (16)  NOT NULL,
    [membercode]  VARCHAR (16)  NOT NULL,
    [requesttype] VARCHAR (9)   NOT NULL,
    [creationdt]  DATETIME2 (7) NOT NULL,
    [fieldid]     VARCHAR (32)  NOT NULL,
    [value]       VARCHAR (MAX) NULL,
    CONSTRAINT [visitdataPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdt] ASC, [fieldid] ASC)
);

