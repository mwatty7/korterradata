﻿CREATE TABLE [dbo].[kt_facility] (
    [facilityGuid] UNIQUEIDENTIFIER NOT NULL,
    [name]         VARCHAR (32)     NOT NULL,
    [description]  VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_kt_facility] PRIMARY KEY CLUSTERED ([facilityGuid] ASC)
);

