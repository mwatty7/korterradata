﻿CREATE TABLE [dbo].[remarks] (
    [customerid] VARCHAR (32) NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    [type]       VARCHAR (16) NOT NULL,
    [seqno]      SMALLINT     NOT NULL,
    [remarks]    VARCHAR (80) NULL,
    CONSTRAINT [remarksPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [type] ASC, [seqno] ASC)
);

