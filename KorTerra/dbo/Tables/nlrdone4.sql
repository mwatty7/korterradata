﻿CREATE TABLE [dbo].[nlrdone4] (
    [customerid]  VARCHAR (32) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [company]     VARCHAR (32) NOT NULL,
    [donefor]     VARCHAR (32) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [isnlr]       SMALLINT     NULL,
    CONSTRAINT [nlrdone4PrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [company] ASC, [donefor] ASC, [requesttype] ASC)
);

