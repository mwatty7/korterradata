﻿CREATE TABLE [dbo].[prsonecalls] (
    [customerid]   VARCHAR (32) NOT NULL,
    [phoneline]    INT          NOT NULL,
    [buttonnumber] INT          NOT NULL,
    [textcode]     VARCHAR (10) NULL,
    [phonenumber]  VARCHAR (21) NULL,
    CONSTRAINT [prsonecallsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [phoneline] ASC, [buttonnumber] ASC)
);

