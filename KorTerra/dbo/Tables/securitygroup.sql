﻿CREATE TABLE [dbo].[securitygroup] (
    [customerid]  VARCHAR (32) NOT NULL,
    [groupid]     VARCHAR (16) NOT NULL,
    [description] VARCHAR (50) NULL,
    CONSTRAINT [securitygroupPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [groupid] ASC)
);

