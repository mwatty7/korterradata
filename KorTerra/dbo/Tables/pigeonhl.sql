﻿CREATE TABLE [dbo].[pigeonhl] (
    [customerid]      VARCHAR (32) NOT NULL,
    [msgtype]         VARCHAR (32) NOT NULL,
    [msggroupid]      VARCHAR (16) NOT NULL,
    [msgcount]        INT          NULL,
    [firstrecvdtdate] DATETIME     NULL,
    [firstrecvdttime] VARCHAR (12) NULL,
    [lastrecvdtdate]  DATETIME     NULL,
    [lastrecvdttime]  VARCHAR (12) NULL,
    [heraldid]        VARCHAR (32) NULL,
    [sitename]        VARCHAR (16) NULL,
    [accesscount]     INT          NULL,
    [reminderdelay]   INT          NULL,
    CONSTRAINT [pigeonhlPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [msgtype] ASC, [msggroupid] ASC)
);

