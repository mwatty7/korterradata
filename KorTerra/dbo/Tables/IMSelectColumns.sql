﻿CREATE TABLE [dbo].[IMSelectColumns] (
    [IMSelectColumnsId] INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]        NVARCHAR (32)  NOT NULL,
    [OperatorId]        NVARCHAR (32)  NOT NULL,
    [filterid]          INT            NULL,
    [fieldname]         NVARCHAR (128) NOT NULL,
    [orderno]           INT            NOT NULL,
    [displayname]       NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_IMSelectColumns] PRIMARY KEY CLUSTERED ([IMSelectColumnsId] ASC),
    CONSTRAINT [FK_IMSelectColumns_IMFilter] FOREIGN KEY ([filterid]) REFERENCES [dbo].[IMfilter] ([filterid]),
    CONSTRAINT [UI_IMSelectColumns] UNIQUE NONCLUSTERED ([CustomerId] ASC, [OperatorId] ASC, [filterid] ASC, [fieldname] ASC, [orderno] ASC)
);

