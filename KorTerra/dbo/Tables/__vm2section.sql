﻿CREATE TABLE [dbo].[__vm2section] (
    [customerid]         CHAR (32)      NOT NULL,
    [requesttype]        CHAR (9)       NOT NULL,
    [sectionid]          NVARCHAR (32)  NOT NULL,
    [description]        NVARCHAR (255) NULL,
    [spanishdescription] NVARCHAR (255) NULL,
    [sectionorder]       INT            NULL,
    [width]              NVARCHAR (255) NULL,
    CONSTRAINT [vm2sectionPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC, [sectionid] ASC)
);

