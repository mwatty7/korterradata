﻿CREATE TABLE [dbo].[valruledpt] (
    [customerid]   VARCHAR (32) NOT NULL,
    [departmentid] VARCHAR (16) NOT NULL,
    [validruleid]  VARCHAR (16) NOT NULL,
    CONSTRAINT [valruledptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [departmentid] ASC, [validruleid] ASC)
);

