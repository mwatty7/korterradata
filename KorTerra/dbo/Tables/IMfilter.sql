﻿CREATE TABLE [dbo].[IMfilter] (
    [filterid]            INT            IDENTITY (1, 1) NOT NULL,
    [customerid]          NVARCHAR (32)  NULL,
    [operatorid]          NVARCHAR (32)  NULL,
    [createdbyoperatorid] NVARCHAR (32)  NOT NULL,
    [createddtutc]        DATETIME       NOT NULL,
    [name]                NVARCHAR (32)  NOT NULL,
    [query]               NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_filter] PRIMARY KEY CLUSTERED ([filterid] ASC)
);

