﻿CREATE TABLE [dbo].[rptheader] (
    [customerid]   VARCHAR (32) NOT NULL,
    [reportid]     VARCHAR (32) NOT NULL,
    [description]  VARCHAR (80) NULL,
    [reportname]   VARCHAR (32) NULL,
    [pagewidth]    INT          NULL,
    [topmargin]    INT          NULL,
    [bottommargin] INT          NULL,
    [leftmargin]   INT          NULL,
    [rightmargin]  INT          NULL,
    [fontsize]     INT          NULL,
    [font]         VARCHAR (32) NULL,
    CONSTRAINT [rptheaderPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC)
);

