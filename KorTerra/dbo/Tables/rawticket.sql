﻿CREATE TABLE [dbo].[rawticket] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [jobid]         VARCHAR (16)  NOT NULL,
    [occid]         VARCHAR (8)   NOT NULL,
    [seqno]         SMALLINT      NOT NULL,
    [sendto]        VARCHAR (16)  NOT NULL,
    [xmitdtdate]    DATETIME      NOT NULL,
    [xmitdttime]    VARCHAR (12)  NOT NULL,
    [receivedtdate] DATETIME      NOT NULL,
    [receivedttime] VARCHAR (12)  NOT NULL,
    [linenum]       SMALLINT      NOT NULL,
    [contents]      VARCHAR (255) NULL,
    CONSTRAINT [rawticketPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [occid] ASC, [seqno] ASC, [sendto] ASC, [xmitdtdate] ASC, [xmitdttime] ASC, [receivedtdate] ASC, [receivedttime] ASC, [linenum] ASC)
);


GO
CREATE NONCLUSTERED INDEX [rawticketReceivedtIndex]
    ON [dbo].[rawticket]([jobid] ASC, [linenum] ASC, [receivedtdate] ASC, [receivedttime] ASC);

