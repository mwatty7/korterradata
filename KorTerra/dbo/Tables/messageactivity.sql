﻿CREATE TABLE [dbo].[messageactivity] (
    [customerid] VARCHAR (32)  NOT NULL,
    [operatorid] VARCHAR (32)  NOT NULL,
    [messageid]  INT           NOT NULL,
    [creationdt] DATETIME2 (7) NOT NULL,
    [action]     VARCHAR (32)  NULL,
    CONSTRAINT [PK_messageactivity] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [messageid] ASC, [creationdt] ASC),
    CONSTRAINT [FK_MessageDetailMessageActivity] FOREIGN KEY ([messageid]) REFERENCES [dbo].[messagedetail] ([messageid])
);

