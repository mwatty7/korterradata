﻿CREATE TABLE [dbo].[__vd2value] (
    [customerid]  NVARCHAR (32)  NOT NULL,
    [jobid]       NVARCHAR (16)  NOT NULL,
    [membercode]  NVARCHAR (16)  NOT NULL,
    [requesttype] NVARCHAR (9)   NOT NULL,
    [creationdt]  DATETIME       NOT NULL,
    [sectionid]   NVARCHAR (32)  NOT NULL,
    [fieldid]     NVARCHAR (32)  NOT NULL,
    [value]       NVARCHAR (MAX) NULL,
    CONSTRAINT [vd2valuePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdt] ASC, [sectionid] ASC, [fieldid] ASC)
);

