﻿CREATE TABLE [dbo].[membrem] (
    [customerid] VARCHAR (32) NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    [membercode] VARCHAR (16) NOT NULL,
    [remarks0]   VARCHAR (80) NULL,
    [remarks1]   VARCHAR (80) NULL,
    [remarks2]   VARCHAR (80) NULL,
    [remarks3]   VARCHAR (80) NULL,
    [remarks4]   VARCHAR (80) NULL,
    [remarks5]   VARCHAR (80) NULL,
    [remarks6]   VARCHAR (80) NULL,
    [remarks7]   VARCHAR (80) NULL,
    [remarks8]   VARCHAR (80) NULL,
    [remarks9]   VARCHAR (80) NULL,
    CONSTRAINT [membremPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC)
);

