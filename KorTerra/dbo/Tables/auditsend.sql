﻿CREATE TABLE [dbo].[auditsend] (
    [auditsendid] UNIQUEIDENTIFIER CONSTRAINT [DF_auditsend_auditsendid] DEFAULT (newsequentialid()) NOT NULL,
    [customerid]  VARCHAR (32)     NOT NULL,
    [jobid]       VARCHAR (16)     NOT NULL,
    [membercode]  VARCHAR (16)     NOT NULL,
    [seqno]       SMALLINT         NOT NULL,
    [mobileid]    VARCHAR (16)     NOT NULL,
    [transdt]     DATETIME2 (7)    NOT NULL,
    [transdate]   DATE             NOT NULL,
    [destination] VARCHAR (128)    NOT NULL,
    [status]      CHAR (1)         NULL,
    [occjobid]    VARCHAR (16)     NOT NULL,
    CONSTRAINT [PK_auditsend] PRIMARY KEY NONCLUSTERED ([auditsendid] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_auditsend_customerid_mobileid_destination_transdate_seqno]
    ON [dbo].[auditsend]([customerid] ASC, [mobileid] ASC, [destination] ASC, [transdate] ASC, [seqno] ASC);

