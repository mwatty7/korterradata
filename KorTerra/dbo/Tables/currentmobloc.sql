﻿CREATE TABLE [dbo].[currentmobloc] (
    [customerid] VARCHAR (32) NOT NULL,
    [mobileid]   VARCHAR (16) NOT NULL,
    [recordeddt] DATETIME     NOT NULL,
    [latitude]   VARCHAR (12) NOT NULL,
    [longitude]  VARCHAR (12) NOT NULL,
    CONSTRAINT [PK_currentmobloc] PRIMARY KEY CLUSTERED ([customerid] ASC, [mobileid] ASC, [recordeddt] ASC)
);

