﻿CREATE TABLE [dbo].[updatetype] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [occid]          VARCHAR (8)   NOT NULL,
    [updatetype]     VARCHAR (255) NOT NULL,
    [validationtype] SMALLINT      NULL,
    CONSTRAINT [updatetypePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [updatetype] ASC)
);

