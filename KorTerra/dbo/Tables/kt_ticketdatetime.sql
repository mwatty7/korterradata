﻿CREATE TABLE [dbo].[kt_ticketdatetime] (
    [ticketdatetimeGuid] UNIQUEIDENTIFIER NOT NULL,
    [ticketGuid]         UNIQUEIDENTIFIER NOT NULL,
    [field]              VARCHAR (32)     NOT NULL,
    [value]              DATETIME2 (7)    NULL,
    CONSTRAINT [PK_kt_ticketdatetime] PRIMARY KEY CLUSTERED ([ticketdatetimeGuid] ASC),
    CONSTRAINT [FK_kt_ticketdatetime_ticketGuid_kt_ticket_ticketGuid] FOREIGN KEY ([ticketGuid]) REFERENCES [dbo].[kt_ticket] ([ticketGuid])
);

