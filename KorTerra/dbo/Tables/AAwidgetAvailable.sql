﻿CREATE TABLE [dbo].[AAwidgetAvailable] (
    [customerid]    VARCHAR (32) NOT NULL,
    [widgetid]      VARCHAR (64) NOT NULL,
    [dashboardType] VARCHAR (16) NOT NULL,
    [isallowed]     BIT          NULL,
    CONSTRAINT [PK_AAwidgetAvailable] PRIMARY KEY CLUSTERED ([customerid] ASC, [widgetid] ASC, [dashboardType] ASC),
    CONSTRAINT [FK_AAwidgetAvailable_widgetid_AAwidget_widgetid] FOREIGN KEY ([widgetid]) REFERENCES [dbo].[AAwidget] ([widgetid])
);


GO
CREATE NONCLUSTERED INDEX [IX_AAwidgetAvailable_widgetid]
    ON [dbo].[AAwidgetAvailable]([widgetid] ASC);

