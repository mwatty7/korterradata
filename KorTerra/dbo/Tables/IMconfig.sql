﻿CREATE TABLE [dbo].[IMconfig] (
    [customerid] NVARCHAR (32)  NOT NULL,
    [sectionid]  NVARCHAR (32)  NOT NULL,
    [name]       NVARCHAR (256) NOT NULL,
    [type]       NVARCHAR (32)  NULL,
    [value]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_IMconfig] PRIMARY KEY CLUSTERED ([customerid] ASC, [sectionid] ASC, [name] ASC)
);

