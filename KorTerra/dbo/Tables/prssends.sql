﻿CREATE TABLE [dbo].[prssends] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [membercode]     VARCHAR (16)  NOT NULL,
    [requesttype]    VARCHAR (9)   NOT NULL,
    [creationdtdate] DATETIME      NOT NULL,
    [creationdttime] VARCHAR (12)  NOT NULL,
    [prssendtype]    VARCHAR (16)  NOT NULL,
    [senddtdate]     DATETIME      NOT NULL,
    [senddttime]     VARCHAR (12)  NOT NULL,
    [linenum]        SMALLINT      NOT NULL,
    [line]           VARCHAR (255) NULL,
    CONSTRAINT [prssendsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC, [prssendtype] ASC, [senddtdate] ASC, [senddttime] ASC, [linenum] ASC)
);

