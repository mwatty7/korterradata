﻿CREATE TABLE [dbo].[material] (
    [materialtypeid]   INT          NOT NULL,
    [materialtypecode] VARCHAR (8)  NULL,
    [materialtype]     VARCHAR (32) NULL,
    [facilitytypeid]   INT          NULL,
    CONSTRAINT [materialPrimaryKey] PRIMARY KEY CLUSTERED ([materialtypeid] ASC)
);

