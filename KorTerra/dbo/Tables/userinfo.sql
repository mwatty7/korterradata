﻿CREATE TABLE [dbo].[userinfo] (
    [customerid]    VARCHAR (32)   NOT NULL,
    [usercode]      VARCHAR (8)    NOT NULL,
    [companyid]     VARCHAR (32)   NULL,
    [description]   VARCHAR (32)   NULL,
    [address1]      VARCHAR (32)   NULL,
    [address2]      VARCHAR (32)   NULL,
    [city]          VARCHAR (64)   NULL,
    [state]         VARCHAR (3)    NULL,
    [zipcode]       VARCHAR (11)   NULL,
    [contact]       VARCHAR (32)   NULL,
    [phone1]        VARCHAR (21)   NULL,
    [phone2]        VARCHAR (21)   NULL,
    [fax]           VARCHAR (21)   NULL,
    [pager]         VARCHAR (21)   NULL,
    [mobile]        VARCHAR (21)   NULL,
    [email]         VARCHAR (80)   NULL,
    [besttime]      VARCHAR (80)   NULL,
    [callbacknotes] VARCHAR (2048) NULL,
    [fbcoverpage]   VARCHAR (32)   NULL,
    [fccoverpage]   VARCHAR (32)   NULL,
    [tplfilename]   VARCHAR (80)   NULL,
    [companytype]   VARCHAR (16)   NULL,
    [altcontact]    VARCHAR (32)   NULL,
    [altphone]      VARCHAR (21)   NULL,
    CONSTRAINT [userinfoPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [usercode] ASC)
);

