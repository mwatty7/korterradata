﻿CREATE TABLE [dbo].[HeraldNotifyScheduleTemplate] (
    [hnst_id]     INT           IDENTITY (1, 1) NOT NULL,
    [hng_id]      INT           NOT NULL,
    [cst_id]      INT           NOT NULL,
    [tplfilename] VARCHAR (255) NULL,
    CONSTRAINT [PK_HeraldNotifyScheduleTemplate] PRIMARY KEY CLUSTERED ([hnst_id] ASC),
    CONSTRAINT [FK_HeraldNotifyScheduleTemplatePerCustomer] FOREIGN KEY ([cst_id]) REFERENCES [dbo].[CustomerScheduleTemplates] ([cst_id]),
    CONSTRAINT [FK_HeraldNotifyScheduleTemplatePerGroup] FOREIGN KEY ([hng_id]) REFERENCES [dbo].[HeraldNotifyGroup] ([hng_id])
);

