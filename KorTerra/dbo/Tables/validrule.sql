﻿CREATE TABLE [dbo].[validrule] (
    [customerid]  VARCHAR (32) NOT NULL,
    [validruleid] VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [validrulePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [validruleid] ASC)
);

