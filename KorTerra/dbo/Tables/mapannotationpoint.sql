﻿CREATE TABLE [dbo].[mapannotationpoint] (
    [shapeid]   INT          NOT NULL,
    [sequence]  INT          NOT NULL,
    [latitude]  VARCHAR (32) NOT NULL,
    [longitude] VARCHAR (32) NOT NULL,
    CONSTRAINT [PK_mapannotationpoint] PRIMARY KEY CLUSTERED ([shapeid] ASC, [sequence] ASC),
    CONSTRAINT [FK_MapAnnotation_Point] FOREIGN KEY ([shapeid]) REFERENCES [dbo].[mapannotation] ([shapeid])
);

