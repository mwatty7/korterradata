﻿CREATE TABLE [dbo].[claimantcosttype] (
    [costtypeid]  VARCHAR (16) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [claimantcosttypePrimaryKey] PRIMARY KEY CLUSTERED ([costtypeid] ASC)
);

