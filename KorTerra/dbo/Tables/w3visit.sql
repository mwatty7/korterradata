﻿CREATE TABLE [dbo].[w3visit] (
    [customerid]         VARCHAR (32)    NOT NULL,
    [jobid]              VARCHAR (16)    NOT NULL,
    [membercode]         VARCHAR (16)    NOT NULL,
    [requesttype]        VARCHAR (9)     NOT NULL,
    [creationdtdate]     DATETIME        NOT NULL,
    [creationdttime]     VARCHAR (12)    NOT NULL,
    [versiondtdate]      DATETIME        NULL,
    [versiondttime]      VARCHAR (12)    NULL,
    [versionauthor]      VARCHAR (16)    NULL,
    [operatorid]         VARCHAR (16)    NULL,
    [mobileid]           VARCHAR (16)    NULL,
    [completiondtdate]   DATETIME        NULL,
    [completiondttime]   VARCHAR (12)    NULL,
    [billingtype]        CHAR (1)        NULL,
    [tmunits]            DECIMAL (15, 2) NULL,
    [tmunittype]         CHAR (1)        NULL,
    [completedby]        VARCHAR (10)    NULL,
    [completionlevel]    CHAR (1)        NULL,
    [departmentid]       VARCHAR (16)    NULL,
    [ismain]             SMALLINT        NULL,
    [isservices]         SMALLINT        NULL,
    [isstopbox]          SMALLINT        NULL,
    [ishydrant]          SMALLINT        NULL,
    [qtymain]            SMALLINT        NULL,
    [qtyservices]        SMALLINT        NULL,
    [qtystopbox]         SMALLINT        NULL,
    [qtyhydrant]         SMALLINT        NULL,
    [iscastiron]         SMALLINT        NULL,
    [isplastic]          SMALLINT        NULL,
    [issteel]            SMALLINT        NULL,
    [isconcrete]         SMALLINT        NULL,
    [iscopper]           SMALLINT        NULL,
    [ispaint]            SMALLINT        NULL,
    [isflag]             SMALLINT        NULL,
    [isstake]            SMALLINT        NULL,
    [isstakechaser]      SMALLINT        NULL,
    [isoffset]           SMALLINT        NULL,
    [qtyoffset]          DECIMAL (15, 1) NULL,
    [isclearexcavate]    SMALLINT        NULL,
    [isnounderground]    SMALLINT        NULL,
    [ishanddug]          SMALLINT        NULL,
    [ismapmeasure]       SMALLINT        NULL,
    [isnotcompleted]     SMALLINT        NULL,
    [isexcavatorpresent] SMALLINT        NULL,
    [excavatorpresent]   VARCHAR (32)    NULL,
    CONSTRAINT [w3visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [w3visitCompletionIndex]
    ON [dbo].[w3visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [w3visitDeptIndex]
    ON [dbo].[w3visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [w3visitMembercodeIndex]
    ON [dbo].[w3visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

