﻿CREATE TABLE [dbo].[schedule] (
    [scheduleid] UNIQUEIDENTIFIER NOT NULL,
    [customerid] VARCHAR (32)     NOT NULL,
    [isactive]   BIT              NOT NULL,
    CONSTRAINT [PK_schedule_scheduleid] PRIMARY KEY NONCLUSTERED ([scheduleid] ASC)
);

