﻿CREATE TABLE [dbo].[__Delete_auditsend_20190715] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [jobid]       VARCHAR (16)  NOT NULL,
    [seqno]       SMALLINT      NOT NULL,
    [mobileid]    VARCHAR (16)  NOT NULL,
    [transdtdate] DATETIME      NOT NULL,
    [transdttime] VARCHAR (12)  NOT NULL,
    [destination] VARCHAR (128) NOT NULL,
    [status]      CHAR (1)      NULL
);

