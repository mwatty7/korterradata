﻿CREATE TABLE [dbo].[DeadLockDetails] (
    [ProcessID]        NVARCHAR (50)  NULL,
    [HostName]         NVARCHAR (50)  NULL,
    [LoginName]        NVARCHAR (100) NULL,
    [ClientApp]        NVARCHAR (100) NULL,
    [Frame]            NVARCHAR (MAX) NULL,
    [TSQLString]       NVARCHAR (MAX) NULL,
    [DeadLockDateTime] DATETIME       NULL,
    [IsVictim]         TINYINT        NULL,
    [DeadLockNumber]   INT            NULL
);

