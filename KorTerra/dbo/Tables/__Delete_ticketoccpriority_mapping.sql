﻿CREATE TABLE [dbo].[__Delete_ticketoccpriority_mapping] (
    [occid]                       VARCHAR (8)      NULL,
    [kt_priority]                 VARCHAR (16)     NULL,
    [origpriority]                VARCHAR (55)     NULL,
    [customerid]                  VARCHAR (32)     NULL,
    [del_bad]                     VARCHAR (1)      NULL,
    [ticketpriorityid]            UNIQUEIDENTIFIER NULL,
    [fullsize_origpriority]       VARCHAR (80)     NULL,
    [was_origpriority_lengthened] VARCHAR (1)      NULL,
    [LRN]                         INT              IDENTITY (1, 1) NOT NULL
);

