﻿CREATE TABLE [dbo].[expcode] (
    [customerid] VARCHAR (32) NOT NULL,
    [expcode]    INT          NOT NULL,
    [expdesc]    VARCHAR (40) NULL,
    [suffix]     VARCHAR (16) NULL,
    [minor]      VARCHAR (16) NULL,
    [earncode]   VARCHAR (4)  NULL,
    [onmobile]   SMALLINT     NULL,
    [requirerem] SMALLINT     NULL,
    CONSTRAINT [expcodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [expcode] ASC)
);

