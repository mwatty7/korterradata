﻿CREATE TABLE [dbo].[damageremarks] (
    [damageid] VARCHAR (16)  NOT NULL,
    [type]     VARCHAR (16)  NOT NULL,
    [seqno]    SMALLINT      NOT NULL,
    [text]     VARCHAR (128) NULL,
    CONSTRAINT [damageremarksPrimaryKey] PRIMARY KEY CLUSTERED ([damageid] ASC, [type] ASC, [seqno] ASC)
);

