﻿CREATE TABLE [dbo].[screeningtemplate] (
    [templateid]            VARCHAR (64)  NOT NULL,
    [templateversionnumber] INT           NOT NULL,
    [html]                  VARCHAR (MAX) NULL,
    [json]                  VARCHAR (MAX) NULL,
    [js]                    VARCHAR (MAX) NULL,
    CONSTRAINT [PK_screeningtemplate] PRIMARY KEY CLUSTERED ([templateid] ASC, [templateversionnumber] ASC)
);

