﻿CREATE TABLE [dbo].[x2visit] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    [versionauthor]    VARCHAR (16)    NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      CHAR (1)        NULL,
    [tmunits]          DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [completedby]      VARCHAR (10)    NULL,
    [completionlevel]  CHAR (1)        NULL,
    [departmentid]     VARCHAR (16)    NULL,
    [platno]           VARCHAR (13)    NULL,
    [filmroll]         VARCHAR (13)    NULL,
    [serviceterritory] VARCHAR (16)    NULL,
    [reasonid]         VARCHAR (32)    NULL,
    [faccode1]         VARCHAR (16)    NULL,
    [ismarked1]        SMALLINT        NULL,
    [isclearexcavate1] SMALLINT        NULL,
    [actcode1]         VARCHAR (16)    NULL,
    [faccode2]         VARCHAR (16)    NULL,
    [ismarked2]        SMALLINT        NULL,
    [isclearexcavate2] SMALLINT        NULL,
    [actcode2]         VARCHAR (16)    NULL,
    [faccode3]         VARCHAR (16)    NULL,
    [ismarked3]        SMALLINT        NULL,
    [isclearexcavate3] SMALLINT        NULL,
    [actcode3]         VARCHAR (16)    NULL,
    [faccode4]         VARCHAR (16)    NULL,
    [ismarked4]        SMALLINT        NULL,
    [isclearexcavate4] SMALLINT        NULL,
    [actcode4]         VARCHAR (16)    NULL,
    [faccode5]         VARCHAR (16)    NULL,
    [ismarked5]        SMALLINT        NULL,
    [isclearexcavate5] SMALLINT        NULL,
    [actcode5]         VARCHAR (16)    NULL,
    [faccode6]         VARCHAR (16)    NULL,
    [ismarked6]        SMALLINT        NULL,
    [isclearexcavate6] SMALLINT        NULL,
    [actcode6]         VARCHAR (16)    NULL,
    [isnotsendprs]     SMALLINT        NULL,
    CONSTRAINT [x2visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x2visitCompletionIndex]
    ON [dbo].[x2visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x2visitDeptIndex]
    ON [dbo].[x2visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [x2visitMembercodeIndex]
    ON [dbo].[x2visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE TRIGGER kt_tr_x2visit_compatible
ON x2visit
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_membercode VARCHAR(16);
	DECLARE @new_requesttype VARCHAR(9);
	DECLARE @new_creationdtdate DATETIME;
	DECLARE @new_creationdttime VARCHAR(12);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_membercode VARCHAR(16);
	DECLARE @old_requesttype VARCHAR(9);
	DECLARE @old_creationdtdate DATETIME;
	DECLARE @old_creationdttime VARCHAR(12);
--	DECLARE @old_versionauthor VARCHAR(16);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_membercode=membercode, @new_requesttype=requesttype, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_membercode=membercode, @old_requesttype=requesttype
		FROM deleted;
	If exists (Select * from inserted) and not exists(Select * from deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'FORWARD'
		FROM inserted;
     END
	if exists(SELECT * from deleted) and not exists (SELECT * from inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'FORWARD'
		FROM deleted;
     END
	 -- Entity Framework prevents this from going into an infinite loop. Checking versionauthor broke edit completion where the visit was done in new korweb.
	if exists(SELECT * from inserted) and exists (SELECT * from deleted) -- AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'UPDATE', 'FORWARD'
		FROM inserted;
	END
END