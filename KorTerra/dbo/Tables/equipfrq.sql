﻿CREATE TABLE [dbo].[equipfrq] (
    [equipfreqid] INT          NOT NULL,
    [equipfreq]   VARCHAR (16) NULL,
    [equiptypeid] INT          NULL,
    CONSTRAINT [equipfrqPrimaryKey] PRIMARY KEY CLUSTERED ([equipfreqid] ASC)
);

