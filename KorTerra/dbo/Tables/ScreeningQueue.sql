﻿CREATE TABLE [dbo].[ScreeningQueue] (
    [customerid] VARCHAR (32)  NOT NULL,
    [jobid]      VARCHAR (16)  NOT NULL,
    [membercode] VARCHAR (16)  NOT NULL,
    [iscancel]   BIT           NOT NULL,
    [queuedt]    DATETIME2 (7) NOT NULL,
    CONSTRAINT [PK_ScreeningQueue] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC)
);

