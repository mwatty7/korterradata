﻿CREATE TABLE [dbo].[invoice] (
    [customerid]   VARCHAR (32) NOT NULL,
    [invoiceid]    VARCHAR (12) NOT NULL,
    [cutoffdate]   DATETIME     NULL,
    [masterdeptid] VARCHAR (16) NULL,
    CONSTRAINT [invoicePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [invoiceid] ASC)
);

