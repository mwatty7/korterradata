﻿CREATE TABLE [dbo].[msgtype] (
    [msgtype]     VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [msgtypePrimaryKey] PRIMARY KEY CLUSTERED ([msgtype] ASC)
);

