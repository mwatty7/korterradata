﻿CREATE TABLE [dbo].[remindernotification] (
    [remindernotificationid]   UNIQUEIDENTIFIER NOT NULL,
    [reminderid]               UNIQUEIDENTIFIER NOT NULL,
    [reminderdtutc]            DATETIME2 (7)    NOT NULL,
    [status]                   VARCHAR (MAX)    NULL,
    [remindernotificationtype] VARCHAR (80)     NOT NULL,
    CONSTRAINT [reminderidnotificationidPrimaryKey] PRIMARY KEY NONCLUSTERED ([remindernotificationid] ASC),
    CONSTRAINT [FK_reminderid_remindercommon] FOREIGN KEY ([reminderid]) REFERENCES [dbo].[remindercommon] ([reminderid])
);

