﻿CREATE TABLE [dbo].[ledgrem] (
    [customerid]      VARCHAR (32) NOT NULL,
    [invoiceid]       VARCHAR (12) NOT NULL,
    [departmentid]    VARCHAR (16) NOT NULL,
    [creationdtdate]  DATETIME     NOT NULL,
    [creationdttime]  VARCHAR (12) NOT NULL,
    [transactiontype] VARCHAR (16) NULL,
    [remarks0]        VARCHAR (80) NULL,
    [remarks1]        VARCHAR (80) NULL,
    [remarks2]        VARCHAR (80) NULL,
    [remarks3]        VARCHAR (80) NULL,
    [remarks4]        VARCHAR (80) NULL,
    [remarks5]        VARCHAR (80) NULL,
    [remarks6]        VARCHAR (80) NULL,
    [remarks7]        VARCHAR (80) NULL,
    [remarks8]        VARCHAR (80) NULL,
    [remarks9]        VARCHAR (80) NULL,
    CONSTRAINT [ledgremPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [invoiceid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);

