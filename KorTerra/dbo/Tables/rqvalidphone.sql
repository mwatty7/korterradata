﻿CREATE TABLE [dbo].[rqvalidphone] (
    [customerid] VARCHAR (32) NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    [membercode] VARCHAR (16) NOT NULL,
    [areacode]   VARCHAR (4)  NOT NULL,
    [wirecenter] VARCHAR (7)  NOT NULL,
    CONSTRAINT [rqvalidphonePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [areacode] ASC, [wirecenter] ASC)
);

