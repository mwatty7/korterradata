﻿CREATE TABLE [dbo].[imageremarks] (
    [claimid] VARCHAR (16)  NOT NULL,
    [imageid] VARCHAR (80)  NOT NULL,
    [type]    VARCHAR (16)  NOT NULL,
    [seqno]   SMALLINT      NOT NULL,
    [text]    VARCHAR (128) NULL,
    CONSTRAINT [imageremarksPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [imageid] ASC, [type] ASC, [seqno] ASC)
);

