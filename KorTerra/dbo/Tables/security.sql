﻿CREATE TABLE [dbo].[security] (
    [customerid]    VARCHAR (32) NOT NULL,
    [groupid]       VARCHAR (16) NOT NULL,
    [menuid]        VARCHAR (16) NOT NULL,
    [applicationid] VARCHAR (16) NOT NULL,
    CONSTRAINT [securityPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [groupid] ASC, [menuid] ASC, [applicationid] ASC)
);

