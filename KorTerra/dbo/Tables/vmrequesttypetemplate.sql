﻿CREATE TABLE [dbo].[vmrequesttypetemplate] (
    [templateid]            VARCHAR (64)  NOT NULL,
    [templateversionnumber] INT           NOT NULL,
    [description]           VARCHAR (255) NULL,
    [html]                  VARCHAR (MAX) NULL,
    [javascript]            VARCHAR (MAX) NULL,
    [css]                   VARCHAR (MAX) NULL,
    [jsontemplate]          VARCHAR (MAX) NULL,
    [htmltemplate]          VARCHAR (MAX) NULL,
    [jstemplate]            VARCHAR (MAX) NULL,
    CONSTRAINT [vmrequesttypetemplatePrimaryKey] PRIMARY KEY CLUSTERED ([templateid] ASC, [templateversionnumber] ASC)
);

