﻿CREATE TABLE [dbo].[archsettings] (
    [customerid]             VARCHAR (32) NOT NULL,
    [applicationid]          VARCHAR (21) NOT NULL,
    [starttime]              VARCHAR (12) NULL,
    [endtime]                VARCHAR (12) NULL,
    [isbydate]               SMALLINT     NULL,
    [cutoffdate]             DATETIME     NULL,
    [incompletecutoffdate]   DATETIME     NULL,
    [isbymonth]              SMALLINT     NULL,
    [onlinemonths]           SMALLINT     NULL,
    [incompleteonlinemonths] SMALLINT     NULL,
    CONSTRAINT [archsettingsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [applicationid] ASC)
);

