﻿CREATE TABLE [dbo].[jobmobilehistory] (
    [customerid]   VARCHAR (32)   NOT NULL,
    [jobid]        VARCHAR (16)   NOT NULL,
    [actiondt]     DATETIME2 (7)  NOT NULL,
    [frommobileid] VARCHAR (16)   NULL,
    [tomobileid]   VARCHAR (16)   NULL,
    [action]       VARCHAR (64)   NULL,
    [reason]       VARCHAR (1024) NULL,
    [actiondtutc]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_jobmobilehistory] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [actiondt] ASC),
    CONSTRAINT [FK_jobmobilehistory_customerid] FOREIGN KEY ([customerid]) REFERENCES [dbo].[customer] ([customerid]),
    CONSTRAINT [FK_jobmobilehistory_frommobile] FOREIGN KEY ([customerid], [frommobileid]) REFERENCES [dbo].[mobile] ([customerid], [mobileid]),
    CONSTRAINT [FK_jobmobilehistory_jobid] FOREIGN KEY ([customerid], [jobid]) REFERENCES [dbo].[job] ([customerid], [jobid]),
    CONSTRAINT [FK_jobmobilehistory_tomobile] FOREIGN KEY ([customerid], [tomobileid]) REFERENCES [dbo].[mobile] ([customerid], [mobileid])
);

