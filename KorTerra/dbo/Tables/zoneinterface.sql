﻿CREATE TABLE [dbo].[zoneinterface] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [suffixid]    VARCHAR (16)  NOT NULL,
    [zoneid]      VARCHAR (16)  NOT NULL,
    [interfaceid] VARCHAR (64)  NOT NULL,
    [name]        VARCHAR (128) NOT NULL,
    [value]       VARCHAR (255) NULL,
    CONSTRAINT [zoneinterfacePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC, [zoneid] ASC, [interfaceid] ASC, [name] ASC)
);

