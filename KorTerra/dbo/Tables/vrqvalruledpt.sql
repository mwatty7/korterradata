﻿CREATE TABLE [dbo].[vrqvalruledpt] (
    [customerid]   VARCHAR (32) NOT NULL,
    [jobid]        VARCHAR (16) NOT NULL,
    [departmentid] VARCHAR (16) NOT NULL,
    [validruleid]  VARCHAR (16) NOT NULL,
    CONSTRAINT [vrqvalruledptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [departmentid] ASC, [validruleid] ASC)
);

