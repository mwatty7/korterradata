﻿CREATE TABLE [dbo].[messagedjob] (
    [customerid]        VARCHAR (32) NOT NULL,
    [jobid]             VARCHAR (16) NOT NULL,
    [heraldmessengerid] VARCHAR (16) NOT NULL,
    [notifieddtdate]    DATETIME     NULL,
    [notifieddttime]    VARCHAR (12) NULL,
    CONSTRAINT [messagedjobPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [heraldmessengerid] ASC)
);

