﻿CREATE TABLE [dbo].[emailvalidation] (
    [username]       VARCHAR (80)  NULL,
    [domain]         VARCHAR (80)  NULL,
    [type]           CHAR (1)      NULL,
    [creationdtdate] DATETIME      NULL,
    [creationdttime] VARCHAR (12)  NULL,
    [operatorid]     VARCHAR (16)  NULL,
    [comments]       VARCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [validationindex]
    ON [dbo].[emailvalidation]([type] ASC, [domain] ASC, [username] ASC);

