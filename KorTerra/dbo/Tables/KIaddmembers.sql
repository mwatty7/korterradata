﻿CREATE TABLE [dbo].[KIaddmembers] (
    [customerid]      VARCHAR (32) NOT NULL,
    [casenumber]      VARCHAR (16) NOT NULL,
    [reportnumber]    BIGINT       NOT NULL,
    [membercode]      VARCHAR (16) NOT NULL,
    [description]     VARCHAR (80) NULL,
    [ismanuallyadded] BIT          NULL,
    [phone1]          VARCHAR (21) NULL,
    [phone2]          VARCHAR (21) NULL,
    [phone3]          VARCHAR (21) NULL,
    [utilitytype]     VARCHAR (32) NULL,
    CONSTRAINT [PK_KIaddmembers] PRIMARY KEY CLUSTERED ([customerid] ASC, [casenumber] ASC, [reportnumber] ASC, [membercode] ASC)
);

