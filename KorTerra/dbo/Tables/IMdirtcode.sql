﻿CREATE TABLE [dbo].[IMdirtcode] (
    [dirtcodetype]   NVARCHAR (32)  NOT NULL,
    [dirtid]         NVARCHAR (32)  NOT NULL,
    [abbrev]         NVARCHAR (32)  NULL,
    [description]    NVARCHAR (MAX) NULL,
    [state]          NVARCHAR (32)  NULL,
    [country]        NVARCHAR (32)  NULL,
    [parentCodetype] NVARCHAR (32)  NULL,
    [parentCodeid]   NVARCHAR (32)  NULL,
    CONSTRAINT [PK_dirtcode] PRIMARY KEY CLUSTERED ([dirtcodetype] ASC, [dirtid] ASC),
    CONSTRAINT [FK_IMdirtcode_IMdirtcode] FOREIGN KEY ([parentCodetype], [parentCodeid]) REFERENCES [dbo].[IMdirtcode] ([dirtcodetype], [dirtid])
);

