﻿CREATE TABLE [dbo].[tplformat] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [type]        VARCHAR (32)  NOT NULL,
    [tplname]     VARCHAR (32)  NOT NULL,
    [description] VARCHAR (255) NULL,
    [html]        VARCHAR (MAX) NULL,
    CONSTRAINT [tplformatPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [type] ASC, [tplname] ASC)
);

