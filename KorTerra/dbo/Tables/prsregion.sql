﻿CREATE TABLE [dbo].[prsregion] (
    [customerid]   VARCHAR (32) NOT NULL,
    [regionid]     VARCHAR (16) NOT NULL,
    [prssendtype]  VARCHAR (16) NOT NULL,
    [isenabled]    SMALLINT     NULL,
    [isclearonly]  SMALLINT     NULL,
    [ismarkedonly] SMALLINT     NULL,
    CONSTRAINT [prsregionPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [regionid] ASC, [prssendtype] ASC)
);

