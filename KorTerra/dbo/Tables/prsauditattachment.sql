﻿CREATE TABLE [dbo].[prsauditattachment] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [membercode]     VARCHAR (16)  NOT NULL,
    [requesttype]    VARCHAR (9)   NOT NULL,
    [creationdtdate] DATETIME      NOT NULL,
    [creationdttime] VARCHAR (12)  NOT NULL,
    [prssendtype]    VARCHAR (16)  NOT NULL,
    [attachmentname] VARCHAR (256) NOT NULL,
    [status]         CHAR (1)      NULL,
    [occfilename]    VARCHAR (255) NULL,
    [mimetype]       VARCHAR (256) NULL,
    [uploaddtdate]   DATETIME      NULL,
    [uploaddttime]   VARCHAR (12)  NULL,
    [issent]         SMALLINT      NULL,
    [failreason]     VARCHAR (255) NULL,
    [failurecount]   INT           NULL,
    CONSTRAINT [PK_prsauditattachment] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC, [prssendtype] ASC, [attachmentname] ASC)
);

