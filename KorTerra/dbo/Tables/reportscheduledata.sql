﻿CREATE TABLE [dbo].[reportscheduledata] (
    [reportscheduledataid]    UNIQUEIDENTIFIER CONSTRAINT [DF_reportscheduledata_reportscheduledataid] DEFAULT (newsequentialid()) NOT NULL,
    [reportscheduleversionid] UNIQUEIDENTIFIER NOT NULL,
    [param]                   VARCHAR (MAX)    NULL,
    [valuetype]               VARCHAR (32)     NULL,
    [value]                   VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_reportscheduledata] PRIMARY KEY CLUSTERED ([reportscheduledataid] ASC),
    CONSTRAINT [FK_reportscheduledata_reportscheduleversion] FOREIGN KEY ([reportscheduleversionid]) REFERENCES [dbo].[reportscheduleversion] ([reportscheduleversionid])
);


GO
CREATE NONCLUSTERED INDEX [IX_reportscheduledata_reportscheduleversionid_include]
    ON [dbo].[reportscheduledata]([reportscheduleversionid] ASC)
    INCLUDE([reportscheduledataid], [param], [valuetype], [value]);

