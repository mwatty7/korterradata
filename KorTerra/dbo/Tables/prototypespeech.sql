﻿CREATE TABLE [dbo].[prototypespeech] (
    [id]  INT           IDENTITY (1, 1) NOT NULL,
    [xml] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_prototypespeech] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Prototype Table For Speech Server Replacement', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'prototypespeech';

