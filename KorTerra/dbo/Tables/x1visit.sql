﻿CREATE TABLE [dbo].[x1visit] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    [versionauthor]    VARCHAR (16)    NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      CHAR (1)        NULL,
    [tmunits]          DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [completedby]      VARCHAR (10)    NULL,
    [completionlevel]  CHAR (1)        NULL,
    [departmentid]     VARCHAR (16)    NULL,
    [actcode1]         VARCHAR (16)    NULL,
    [actcode2]         VARCHAR (16)    NULL,
    [actcode3]         VARCHAR (16)    NULL,
    [actcode4]         VARCHAR (16)    NULL,
    [faccode1]         VARCHAR (16)    NULL,
    [faccode2]         VARCHAR (16)    NULL,
    [faccode3]         VARCHAR (16)    NULL,
    [faccode4]         VARCHAR (16)    NULL,
    [faccode5]         VARCHAR (16)    NULL,
    [faccode6]         VARCHAR (16)    NULL,
    [notcode]          VARCHAR (7)     NULL,
    [talkedto]         VARCHAR (32)    NULL,
    CONSTRAINT [x1visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x1visitCompletionIndex]
    ON [dbo].[x1visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x1visitDeptIndex]
    ON [dbo].[x1visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [x1visitMembercodeIndex]
    ON [dbo].[x1visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

