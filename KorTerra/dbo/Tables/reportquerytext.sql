﻿CREATE TABLE [dbo].[reportquerytext] (
    [queryname] VARCHAR (80)  NOT NULL,
    [linenum]   SMALLINT      NOT NULL,
    [text]      VARCHAR (255) NULL,
    CONSTRAINT [reportquerytextPrimaryKey] PRIMARY KEY CLUSTERED ([queryname] ASC, [linenum] ASC)
);

