﻿CREATE TABLE [dbo].[IMpayableclaim] (
    [customerid]          NVARCHAR (32)   NOT NULL,
    [incidentnumber]      NVARCHAR (32)   CONSTRAINT [DF_IMpayableclaim_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [payableclaimid]      INT             IDENTITY (1, 1) NOT NULL,
    [createdbyoperatorid] NVARCHAR (32)   NOT NULL,
    [createddtutc]        DATETIME        NOT NULL,
    [amount]              DECIMAL (18, 2) NOT NULL,
    [payableclaimstatus]  NVARCHAR (32)   CONSTRAINT [DF_payableclaim_payableclaimstatus] DEFAULT (N'NEW') NOT NULL,
    CONSTRAINT [PK_IMpayableclaim] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC, [payableclaimid] ASC),
    CONSTRAINT [FK_IMpayableclaim_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

