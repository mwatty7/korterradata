﻿CREATE TABLE [dbo].[c3visit] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    [versionauthor]    VARCHAR (16)    NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      CHAR (1)        NULL,
    [tmunits]          DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [completedby]      VARCHAR (10)    NULL,
    [completionlevel]  CHAR (1)        NULL,
    [iscable]          SMALLINT        NULL,
    [isdrop]           SMALLINT        NULL,
    [isfeeder]         SMALLINT        NULL,
    [istrunk]          SMALLINT        NULL,
    [isfiber]          SMALLINT        NULL,
    [qtycable]         SMALLINT        NULL,
    [qtydrop]          SMALLINT        NULL,
    [qtyfeeder]        SMALLINT        NULL,
    [qtytrunk]         SMALLINT        NULL,
    [qtyfiber]         SMALLINT        NULL,
    [ispaint]          SMALLINT        NULL,
    [isflag]           SMALLINT        NULL,
    [isstake]          SMALLINT        NULL,
    [isclearexcavate]  SMALLINT        NULL,
    [isnounderground]  SMALLINT        NULL,
    [isnotcompleted]   SMALLINT        NULL,
    [isafterhours]     SMALLINT        NULL,
    [isbadaddress]     SMALLINT        NULL,
    [isworkcompleted]  SMALLINT        NULL,
    CONSTRAINT [c3visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [c3visitCompletionIndex]
    ON [dbo].[c3visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [c3visitMembercodeIndex]
    ON [dbo].[c3visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

