﻿CREATE TABLE [dbo].[AAwidgetInUse] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [operatorid]    VARCHAR (16)  NOT NULL,
    [widgetid]      VARCHAR (64)  NOT NULL,
    [displayOrder]  INT           NULL,
    [settingsJSON]  VARCHAR (MAX) NULL,
    [instancenum]   INT           NOT NULL,
    [dashboardType] VARCHAR (16)  NOT NULL,
    CONSTRAINT [PK_AAwidgetInUse] PRIMARY KEY CLUSTERED ([customerid] ASC, [dashboardType] ASC, [operatorid] ASC, [widgetid] ASC, [instancenum] ASC),
    CONSTRAINT [FK_AAwidgetInUse_widgetid_AAwidget_widgetid] FOREIGN KEY ([widgetid]) REFERENCES [dbo].[AAwidget] ([widgetid])
);


GO
CREATE NONCLUSTERED INDEX [IX_AAwidgetInUse_widgetid]
    ON [dbo].[AAwidgetInUse]([widgetid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WidgetCustOperatorCombo_Index]
    ON [dbo].[AAwidgetInUse]([customerid] ASC, [widgetid] ASC)
    INCLUDE([operatorid], [instancenum], [dashboardType]);

