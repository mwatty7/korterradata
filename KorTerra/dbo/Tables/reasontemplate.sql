﻿CREATE TABLE [dbo].[reasontemplate] (
    [customerid]                  VARCHAR (32)  NOT NULL,
    [reasontemplateid]            VARCHAR (64)  NOT NULL,
    [reasontemplateversionnumber] INT           NOT NULL,
    [name]                        VARCHAR (32)  NULL,
    [description]                 VARCHAR (255) NULL,
    [html]                        VARCHAR (MAX) NULL,
    [json]                        VARCHAR (MAX) NULL,
    CONSTRAINT [PK_reasontemplate] PRIMARY KEY CLUSTERED ([customerid] ASC, [reasontemplateid] ASC, [reasontemplateversionnumber] ASC)
);

