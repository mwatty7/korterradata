﻿CREATE TABLE [dbo].[pt_occpriority] (
    [customerid]   VARCHAR (32) NOT NULL,
    [occid]        VARCHAR (8)  NOT NULL,
    [origpriority] VARCHAR (16) NOT NULL
);

