﻿CREATE TABLE [dbo].[citycounty] (
    [customerid] VARCHAR (32) NOT NULL,
    [city]       VARCHAR (64) NOT NULL,
    [county]     VARCHAR (64) NOT NULL,
    [state]      VARCHAR (3)  NOT NULL,
    CONSTRAINT [citycountyPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [city] ASC, [county] ASC, [state] ASC)
);

