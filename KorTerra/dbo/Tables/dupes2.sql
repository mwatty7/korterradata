﻿CREATE TABLE [dbo].[dupes2] (
    [origpriority]     VARCHAR (55)     NULL,
    [occid]            VARCHAR (55)     NULL,
    [real_priority]    VARCHAR (55)     NULL,
    [ticketpriorityid] UNIQUEIDENTIFIER NULL
);

