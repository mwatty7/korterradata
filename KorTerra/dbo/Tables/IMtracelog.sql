﻿CREATE TABLE [dbo].[IMtracelog] (
    [logid]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [applicationid] NVARCHAR (256) NULL,
    [customerid]    NVARCHAR (32)  NOT NULL,
    [operatorid]    NVARCHAR (32)  NOT NULL,
    [logdtutc]      DATETIME       NOT NULL,
    [type]          NVARCHAR (32)  NOT NULL,
    [message]       NVARCHAR (MAX) NOT NULL,
    [component]     NVARCHAR (256) NULL,
    [stacktrace]    NVARCHAR (MAX) NULL,
    [loginid]       NVARCHAR (64)  NULL,
    [controllerid]  NVARCHAR (64)  NULL,
    [url]           NVARCHAR (MAX) NULL,
    CONSTRAINT [tracelogPrimaryKey] PRIMARY KEY CLUSTERED ([logid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_IMtracelogLogDt]
    ON [dbo].[IMtracelog]([logdtutc] ASC);

