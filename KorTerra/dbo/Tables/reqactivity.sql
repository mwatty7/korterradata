﻿CREATE TABLE [dbo].[reqactivity] (
    [application]       VARCHAR (32) NOT NULL,
    [activitytype]      VARCHAR (48) NOT NULL,
    [starttime]         VARCHAR (12) NOT NULL,
    [endtime]           VARCHAR (12) NOT NULL,
    [isexcludeweekends] SMALLINT     NOT NULL,
    [requiredactivity]  SMALLINT     NULL,
    [msggroupid]        VARCHAR (16) NULL,
    CONSTRAINT [reqactivityPrimaryKey] PRIMARY KEY CLUSTERED ([application] ASC, [activitytype] ASC, [starttime] ASC, [endtime] ASC, [isexcludeweekends] ASC)
);

