﻿CREATE TABLE [dbo].[reportquerysecurity] (
    [queryname]  VARCHAR (80) NOT NULL,
    [customerid] VARCHAR (32) NOT NULL,
    [groupid]    VARCHAR (16) NOT NULL,
    CONSTRAINT [reportquerysecurityPrimaryKey] PRIMARY KEY CLUSTERED ([queryname] ASC, [customerid] ASC, [groupid] ASC)
);

