﻿CREATE TABLE [dbo].[e8visit] (
    [customerid]           VARCHAR (32)    NOT NULL,
    [jobid]                VARCHAR (16)    NOT NULL,
    [membercode]           VARCHAR (16)    NOT NULL,
    [requesttype]          VARCHAR (9)     NOT NULL,
    [creationdtdate]       DATETIME        NOT NULL,
    [creationdttime]       VARCHAR (12)    NOT NULL,
    [versiondtdate]        DATETIME        NULL,
    [versiondttime]        VARCHAR (12)    NULL,
    [versionauthor]        VARCHAR (16)    NULL,
    [operatorid]           VARCHAR (16)    NULL,
    [mobileid]             VARCHAR (16)    NULL,
    [completiondtdate]     DATETIME        NULL,
    [completiondttime]     VARCHAR (12)    NULL,
    [billingtype]          CHAR (1)        NULL,
    [tmunits]              DECIMAL (15, 2) NULL,
    [tmunittype]           CHAR (1)        NULL,
    [completedby]          VARCHAR (10)    NULL,
    [completionlevel]      CHAR (1)        NULL,
    [departmentid]         VARCHAR (16)    NULL,
    [platno]               VARCHAR (13)    NULL,
    [filmroll]             VARCHAR (13)    NULL,
    [isprimary]            SMALLINT        NULL,
    [issecondary]          SMALLINT        NULL,
    [isstreetlight]        SMALLINT        NULL,
    [isduct]               SMALLINT        NULL,
    [is1000mcm]            SMALLINT        NULL,
    [isnetwork]            SMALLINT        NULL,
    [ismarta]              SMALLINT        NULL,
    [istransmission]       SMALLINT        NULL,
    [issti]                SMALLINT        NULL,
    [qtyprimary]           SMALLINT        NULL,
    [qtysecondary]         SMALLINT        NULL,
    [qtystreetlight]       SMALLINT        NULL,
    [qtyduct]              SMALLINT        NULL,
    [qty1000mcm]           SMALLINT        NULL,
    [qtynetwork]           SMALLINT        NULL,
    [qtymarta]             SMALLINT        NULL,
    [qtytransmission]      SMALLINT        NULL,
    [qtysti]               SMALLINT        NULL,
    [ispaint]              SMALLINT        NULL,
    [isflag]               SMALLINT        NULL,
    [isstake]              SMALLINT        NULL,
    [isstakechaser]        SMALLINT        NULL,
    [isoffset]             SMALLINT        NULL,
    [qtyoffset]            DECIMAL (15, 1) NULL,
    [ishighprofile]        SMALLINT        NULL,
    [isprivatefacilities]  SMALLINT        NULL,
    [isongoingjob]         SMALLINT        NULL,
    [isclearexcavate]      SMALLINT        NULL,
    [isnounderground]      SMALLINT        NULL,
    [isoverheadfacilities] SMALLINT        NULL,
    [isnoconflictfacility] SMALLINT        NULL,
    [isnotcompleted]       SMALLINT        NULL,
    [unmarkedreason]       VARCHAR (32)    NULL,
    [markedreason]         VARCHAR (32)    NULL,
    [isnotsendprs]         SMALLINT        NULL,
    CONSTRAINT [e8visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [e8visitCompletionIndex]
    ON [dbo].[e8visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [e8visitDeptIndex]
    ON [dbo].[e8visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [e8visitMembercodeIndex]
    ON [dbo].[e8visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE TRIGGER kt_tr_e8visit_compatible
ON e8visit
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_membercode VARCHAR(16);
	DECLARE @new_requesttype VARCHAR(9);
	DECLARE @new_creationdtdate DATETIME;
	DECLARE @new_creationdttime VARCHAR(12);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_membercode VARCHAR(16);
	DECLARE @old_requesttype VARCHAR(9);
	DECLARE @old_creationdtdate DATETIME;
	DECLARE @old_creationdttime VARCHAR(12);
	DECLARE @old_versionauthor VARCHAR(16);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_membercode=membercode, @new_requesttype=requesttype, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_membercode=membercode, @old_requesttype=requesttype
		FROM deleted;
	If exists (Select * from inserted) and not exists(Select * from deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'FORWARD'
		FROM inserted;
	END
	if exists(SELECT * from deleted) and not exists (SELECT * from inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'FORWARD'
		FROM deleted;
	END
	 -- Entity Framework prevents this from going into an infinite loop. Checking versionauthor broke edit completion where the visit was done in new korweb.
	if exists(SELECT * from inserted) and exists (SELECT * from deleted) -- AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'UPDATE', 'FORWARD'
		FROM inserted;
	END
END