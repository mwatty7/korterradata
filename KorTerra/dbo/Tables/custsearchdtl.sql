﻿CREATE TABLE [dbo].[custsearchdtl] (
    [customerid]     VARCHAR (32) NOT NULL,
    [custsearchid]   INT          NOT NULL,
    [ordernum]       INT          NOT NULL,
    [applicationid]  VARCHAR (16) NOT NULL,
    [columnname]     VARCHAR (32) NULL,
    [columnid]       VARCHAR (32) NULL,
    [columndatatype] VARCHAR (32) NULL,
    [operatortype]   VARCHAR (16) NULL,
    [stringval]      VARCHAR (80) NULL,
    [logicalval]     VARCHAR (4)  NULL,
    [returnnulls]    SMALLINT     NULL,
    [labeltitle]     VARCHAR (33) NULL,
    CONSTRAINT [custsearchdtlPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [custsearchid] ASC, [ordernum] ASC, [applicationid] ASC)
);

