﻿CREATE TABLE [dbo].[XXpreference] (
    [customerid] VARCHAR (32)   NOT NULL,
    [operatorid] VARCHAR (15)   NOT NULL,
    [sectionid]  VARCHAR (32)   NOT NULL,
    [name]       VARCHAR (256)  NOT NULL,
    [type]       VARCHAR (32)   NULL,
    [value]      VARCHAR (8000) NULL,
    CONSTRAINT [XXpreferencePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [sectionid] ASC, [name] ASC)
);

