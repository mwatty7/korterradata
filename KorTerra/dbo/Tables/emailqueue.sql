﻿CREATE TABLE [dbo].[emailqueue] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [toaddress]   VARCHAR (254) NOT NULL,
    [fromaddress] VARCHAR (254) NULL,
    [subject]     VARCHAR (998) NULL,
    [emailbody]   VARCHAR (MAX) NULL,
    [ishtml]      BIT           NULL,
    [status]      VARCHAR (32)  NULL,
    [failreason]  VARCHAR (MAX) NULL,
    [attemptdt]   DATETIME2 (7) NULL,
    [numattempts] INT           NULL,
    CONSTRAINT [PK_emailqueue] PRIMARY KEY CLUSTERED ([id] ASC)
);

