﻿CREATE TABLE [dbo].[XXConfig] (
    [customerid] VARCHAR (32)  NOT NULL,
    [sectionid]  VARCHAR (32)  NOT NULL,
    [name]       VARCHAR (836) NOT NULL,
    [type]       VARCHAR (256) NULL,
    [value]      VARCHAR (MAX) NULL,
    CONSTRAINT [PK_XXConfig] PRIMARY KEY CLUSTERED ([customerid] ASC, [sectionid] ASC, [name] ASC)
);

