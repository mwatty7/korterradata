﻿CREATE TABLE [dbo].[kt_membercode] (
    [membercodeGuid] UNIQUEIDENTIFIER NOT NULL,
    [memberGuid]     UNIQUEIDENTIFIER NULL,
    [occGuid]        UNIQUEIDENTIFIER NOT NULL,
    [code]           VARCHAR (32)     NOT NULL,
    [description]    VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_kt_membercode] PRIMARY KEY CLUSTERED ([membercodeGuid] ASC)
);

