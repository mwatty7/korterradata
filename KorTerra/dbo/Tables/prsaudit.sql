﻿CREATE TABLE [dbo].[prsaudit] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [membercode]     VARCHAR (16)  NOT NULL,
    [requesttype]    VARCHAR (9)   NOT NULL,
    [creationdtdate] DATETIME      NOT NULL,
    [creationdttime] VARCHAR (12)  NOT NULL,
    [prssendtype]    VARCHAR (16)  NOT NULL,
    [occid]          VARCHAR (8)   NULL,
    [occmembercode]  VARCHAR (16)  NULL,
    [prsinfo]        VARCHAR (80)  NULL,
    [prssenddtdate]  DATETIME      NULL,
    [prssenddttime]  VARCHAR (12)  NULL,
    [status]         CHAR (1)      NULL,
    [responsetype]   VARCHAR (16)  NULL,
    [versiondtdate]  DATETIME      NULL,
    [versiondttime]  VARCHAR (12)  NULL,
    [addlinfo]       VARCHAR (80)  NULL,
    [operatorid]     VARCHAR (16)  NULL,
    [destination]    VARCHAR (255) NULL,
    CONSTRAINT [prsauditPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC, [prssendtype] ASC)
);


GO
CREATE NONCLUSTERED INDEX [prsauditaddlinfoindex]
    ON [dbo].[prsaudit]([customerid] ASC, [addlinfo] ASC);


GO
CREATE NONCLUSTERED INDEX [prssenddtindex]
    ON [dbo].[prsaudit]([prssenddtdate] ASC, [prssenddttime] ASC, [status] ASC);


GO
CREATE NONCLUSTERED INDEX [prssendtypeindex]
    ON [dbo].[prsaudit]([customerid] ASC, [prssendtype] ASC, [status] ASC);

