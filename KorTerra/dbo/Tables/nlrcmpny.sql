﻿CREATE TABLE [dbo].[nlrcmpny] (
    [customerid]  VARCHAR (32) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [company]     VARCHAR (32) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [isnlr]       SMALLINT     NULL,
    CONSTRAINT [nlrcmpnyPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [company] ASC, [requesttype] ASC)
);

