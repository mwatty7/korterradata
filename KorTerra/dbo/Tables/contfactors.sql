﻿CREATE TABLE [dbo].[contfactors] (
    [contfactorsid]   VARCHAR (16) NOT NULL,
    [contfactorsdesc] VARCHAR (32) NULL,
    CONSTRAINT [contfactorsPrimaryKey] PRIMARY KEY CLUSTERED ([contfactorsid] ASC)
);

