﻿CREATE TABLE [dbo].[equiptyp] (
    [equiptypeid]   INT          NOT NULL,
    [equiptypecode] VARCHAR (8)  NULL,
    [equiptype]     VARCHAR (32) NULL,
    CONSTRAINT [equiptypPrimaryKey] PRIMARY KEY CLUSTERED ([equiptypeid] ASC)
);

