﻿CREATE TABLE [dbo].[member] (
    [customerid]     VARCHAR (32) NOT NULL,
    [memberid]       VARCHAR (16) NOT NULL,
    [description]    VARCHAR (32) NULL,
    [isnlrdupvisits] SMALLINT     NULL,
    [isactive]       SMALLINT     NULL,
    CONSTRAINT [memberPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [memberid] ASC)
);

