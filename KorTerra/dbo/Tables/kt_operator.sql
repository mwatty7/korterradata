﻿CREATE TABLE [dbo].[kt_operator] (
    [operatorGuid] UNIQUEIDENTIFIER NOT NULL,
    [name]         VARCHAR (32)     NOT NULL,
    [description]  VARCHAR (80)     NOT NULL,
    [isactive]     BIT              NULL,
    CONSTRAINT [PK_kt_operator] PRIMARY KEY CLUSTERED ([operatorGuid] ASC)
);

