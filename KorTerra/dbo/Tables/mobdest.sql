﻿CREATE TABLE [dbo].[mobdest] (
    [customerid]             VARCHAR (32)  NOT NULL,
    [mobileid]               VARCHAR (16)  NOT NULL,
    [desttype]               VARCHAR (16)  NOT NULL,
    [specifictype]           VARCHAR (32)  NOT NULL,
    [address]                VARCHAR (256) NOT NULL,
    [queue]                  VARCHAR (64)  NULL,
    [sendraw]                SMALLINT      NULL,
    [issendaudit]            SMALLINT      NULL,
    [isactive]               SMALLINT      NULL,
    [mobdestid]              VARCHAR (16)  NULL,
    [ifpriority]             VARCHAR (16)  NULL,
    [templatefile]           VARCHAR (255) NULL,
    [templateid]             VARCHAR (32)  NULL,
    [issendeachmemcode]      SMALLINT      NULL,
    [issendcancelontransfer] SMALLINT      NULL,
    [username]               VARCHAR (255) NULL,
    [password]               VARCHAR (255) NULL,
    [token]                  VARCHAR (256) NULL,
    CONSTRAINT [mobdestPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [mobileid] ASC, [desttype] ASC, [specifictype] ASC, [address] ASC)
);

