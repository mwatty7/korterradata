﻿CREATE TABLE [dbo].[prsreasonlist] (
    [customerid]   VARCHAR (32) NOT NULL,
    [membercode]   VARCHAR (16) NOT NULL,
    [prssendtype]  VARCHAR (16) NOT NULL,
    [requesttype]  VARCHAR (9)  NOT NULL,
    [reasonlistid] VARCHAR (32) NULL,
    CONSTRAINT [prsreasonlistPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [prssendtype] ASC, [requesttype] ASC)
);

