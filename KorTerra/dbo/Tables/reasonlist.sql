﻿CREATE TABLE [dbo].[reasonlist] (
    [customerid]     VARCHAR (32) NOT NULL,
    [reasonlistid]   VARCHAR (32) NOT NULL,
    [description]    VARCHAR (80) NULL,
    [ismandatory]    SMALLINT     NULL,
    [prssendtype]    VARCHAR (16) NULL,
    [nlrresponse]    VARCHAR (80) NULL,
    [cancelresponse] VARCHAR (80) NULL,
    CONSTRAINT [reasonlistPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reasonlistid] ASC)
);

