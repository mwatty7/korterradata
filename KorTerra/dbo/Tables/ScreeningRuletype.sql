﻿CREATE TABLE [dbo].[ScreeningRuletype] (
    [ruletypeid]     INT            IDENTITY (1, 1) NOT NULL,
    [isactive]       BIT            NOT NULL,
    [ruletype]       VARCHAR (32)   NOT NULL,
    [version]        INT            NOT NULL,
    [category]       VARCHAR (32)   NOT NULL,
    [description]    VARCHAR (256)  NULL,
    [ruletypeconfig] VARCHAR (2048) NULL,
    CONSTRAINT [PK_ScreeningRuletype] PRIMARY KEY CLUSTERED ([ruletypeid] ASC),
    CONSTRAINT [UC_ScreeningRuletype_ruletype_version] UNIQUE NONCLUSTERED ([ruletype] ASC, [version] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'category is the common type for ruletype ex: VALUE/TIME/GEOSPATIAL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuletype', @level2type = N'COLUMN', @level2name = N'category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'description is the customer facing name for the ScreeningRuletype', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuletype', @level2type = N'COLUMN', @level2name = N'description';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ruletype is one of the choices defined in code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuletype', @level2type = N'COLUMN', @level2name = N'ruletype';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ruletype specific configuration definition', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuletype', @level2type = N'COLUMN', @level2name = N'ruletypeconfig';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A specific type of rule, parameterizes the capabilities defined in code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuletype';

