﻿CREATE TABLE [dbo].[visitprsqueue] (
    [visitprsqueueid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]      VARCHAR (32)     NOT NULL,
    [jobid]           VARCHAR (16)     NOT NULL,
    [membercode]      VARCHAR (16)     NOT NULL,
    [requesttype]     VARCHAR (9)      NOT NULL,
    [creationdt]      DATETIME2 (7)    NOT NULL,
    [prssendtype]     VARCHAR (16)     NOT NULL,
    [visitruleid]     VARCHAR (80)     NULL,
    [queueddt]        DATETIME2 (7)    NULL,
    [visitqueueid]    VARCHAR (80)     NULL,
    [templatedata]    VARCHAR (MAX)    NULL,
    [templatecontent] VARCHAR (MAX)    NULL,
    [actiontype]      VARCHAR (80)     NULL,
    [destination]     VARCHAR (MAX)    NULL,
    [seqno]           INT              NOT NULL,
    [prssenddt]       DATETIME2 (7)    NOT NULL,
    [status]          VARCHAR (32)     NULL,
    CONSTRAINT [PK_visitprsqueue] PRIMARY KEY CLUSTERED ([visitprsqueueid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [visitprsqueue_prsaudit_rule]
    ON [dbo].[visitprsqueue]([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdt] ASC, [prssendtype] ASC, [visitruleid] ASC);


GO
CREATE NONCLUSTERED INDEX [visitprsqueue_queueddtIndex]
    ON [dbo].[visitprsqueue]([queueddt] ASC);

