﻿CREATE TABLE [dbo].[kt_rawticket] (
    [rawticketGuid] UNIQUEIDENTIFIER NOT NULL,
    [ticketGuid]    UNIQUEIDENTIFIER NULL,
    [receiveddtutc] DATETIME2 (7)    NOT NULL,
    [text]          VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_kt_rawticket] PRIMARY KEY CLUSTERED ([rawticketGuid] ASC)
);

