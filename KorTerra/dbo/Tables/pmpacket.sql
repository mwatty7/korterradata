﻿CREATE TABLE [dbo].[pmpacket] (
    [customerid]          VARCHAR (32)  NOT NULL,
    [jobid]               VARCHAR (16)  NOT NULL,
    [sendidentifier]      VARCHAR (255) NOT NULL,
    [subpacketidentifier] VARCHAR (255) NOT NULL,
    [subpacketstatus]     CHAR (1)      NULL,
    [zpm_address]         VARCHAR (128) NULL,
    [zpm_returnAddress]   VARCHAR (128) NULL,
    [zpm_priority]        SMALLINT      NULL,
    [zpm_packetType]      SMALLINT      NULL,
    [zpm_initialSenddate] DATETIME      NULL,
    [zpm_initialSendtime] VARCHAR (12)  NULL,
    [zpm_initialId]       INT           NULL,
    [zpm_identifier]      VARCHAR (255) NULL,
    [zpm_versiondtdate]   DATETIME      NULL,
    [zpm_versiondttime]   VARCHAR (12)  NULL,
    [zpm_extensionType]   SMALLINT      NULL,
    [zpm_extensionSize]   INT           NULL,
    CONSTRAINT [pmpacketPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [sendidentifier] ASC, [subpacketidentifier] ASC)
);

