﻿CREATE TABLE [dbo].[excavation_type] (
    [excavation_type_id]   INT          NOT NULL,
    [excavation_type_code] VARCHAR (16) NULL,
    [excavation_type_desc] VARCHAR (32) NULL,
    CONSTRAINT [excavation_typePrimaryKey] PRIMARY KEY CLUSTERED ([excavation_type_id] ASC)
);

