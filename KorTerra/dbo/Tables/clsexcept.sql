﻿CREATE TABLE [dbo].[clsexcept] (
    [actiondtdate]   DATETIME      NULL,
    [actiondttime]   VARCHAR (12)  NULL,
    [batchid]        VARCHAR (16)  NULL,
    [runsequence]    SMALLINT      NULL,
    [jobid]          VARCHAR (16)  NULL,
    [membercode]     VARCHAR (16)  NULL,
    [requesttype]    VARCHAR (9)   NULL,
    [creationdtdate] DATETIME      NULL,
    [creationdttime] VARCHAR (12)  NULL,
    [failreason]     VARCHAR (255) NULL
);


GO
CREATE NONCLUSTERED INDEX [clsexceptbatchindex]
    ON [dbo].[clsexcept]([batchid] ASC, [runsequence] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [clsexceptjobindex]
    ON [dbo].[clsexcept]([jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC);

