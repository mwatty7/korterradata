﻿CREATE TABLE [dbo].[propertylosscause] (
    [causeid]     VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [propertylosscausePrimaryKey] PRIMARY KEY CLUSTERED ([causeid] ASC)
);

