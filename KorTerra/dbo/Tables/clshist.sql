﻿CREATE TABLE [dbo].[clshist] (
    [actiondtdate]            DATETIME      NULL,
    [actiondttime]            VARCHAR (12)  NULL,
    [completionfromdtdate]    DATETIME      NULL,
    [completionfromdttime]    VARCHAR (12)  NULL,
    [completionthroughdtdate] DATETIME      NULL,
    [completionthroughdttime] VARCHAR (12)  NULL,
    [batchid]                 VARCHAR (16)  NULL,
    [runsequence]             SMALLINT      NULL,
    [ticketfile]              VARCHAR (255) NULL,
    [workfile]                VARCHAR (255) NULL,
    [action]                  VARCHAR (16)  NULL,
    [departmentid]            VARCHAR (16)  NULL,
    [operatorid]              VARCHAR (16)  NULL
);

