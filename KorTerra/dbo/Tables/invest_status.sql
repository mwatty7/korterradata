﻿CREATE TABLE [dbo].[invest_status] (
    [investigation_status_id]   INT          NOT NULL,
    [investigation_status_desc] VARCHAR (32) NULL,
    CONSTRAINT [invest_statusPrimaryKey] PRIMARY KEY CLUSTERED ([investigation_status_id] ASC)
);

