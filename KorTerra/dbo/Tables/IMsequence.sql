﻿CREATE TABLE [dbo].[IMsequence] (
    [customerid]      NVARCHAR (32) NOT NULL,
    [sequence]        BIGINT        NOT NULL,
    [lastupdatedtutc] DATETIME      NOT NULL,
    CONSTRAINT [incidentPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [sequence] ASC)
);

