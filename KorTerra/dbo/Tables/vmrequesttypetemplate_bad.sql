﻿CREATE TABLE [dbo].[vmrequesttypetemplate_bad] (
    [templateid]            VARCHAR (64)  NOT NULL,
    [templateversionnumber] INT           NOT NULL,
    [description]           VARCHAR (255) NULL,
    [html]                  VARCHAR (MAX) NULL,
    [javascript]            VARCHAR (MAX) NULL,
    [css]                   VARCHAR (MAX) NULL,
    CONSTRAINT [vmrequesttypetemplatePrimaryKeybad] PRIMARY KEY CLUSTERED ([templateid] ASC, [templateversionnumber] ASC)
);

