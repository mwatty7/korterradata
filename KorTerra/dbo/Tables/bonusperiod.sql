﻿CREATE TABLE [dbo].[bonusperiod] (
    [customerid]  VARCHAR (32) NOT NULL,
    [reportid]    VARCHAR (16) NOT NULL,
    [operatorid]  VARCHAR (16) NOT NULL,
    [enddtdate]   DATETIME     NOT NULL,
    [enddttime]   VARCHAR (12) NOT NULL,
    [startdtdate] DATETIME     NULL,
    [startdttime] VARCHAR (12) NULL,
    [description] VARCHAR (80) NULL,
    [result]      VARCHAR (80) NULL,
    [tierchange]  VARCHAR (80) NULL,
    [status]      VARCHAR (16) NULL,
    [bonusrate]   FLOAT (53)   NULL,
    [locates]     FLOAT (53)   NULL,
    [amount]      FLOAT (53)   NULL,
    CONSTRAINT [bonusperiodPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [operatorid] ASC, [enddtdate] ASC, [enddttime] ASC)
);

