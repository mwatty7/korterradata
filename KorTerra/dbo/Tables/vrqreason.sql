﻿CREATE TABLE [dbo].[vrqreason] (
    [customerid]       VARCHAR (32)  NOT NULL,
    [jobid]            VARCHAR (16)  NOT NULL,
    [membercode]       VARCHAR (16)  NOT NULL,
    [requesttype]      VARCHAR (9)   NOT NULL,
    [reasonid]         VARCHAR (32)  NOT NULL,
    [description]      VARCHAR (80)  NULL,
    [seqno]            SMALLINT      NULL,
    [reasongroup]      CHAR (1)      NULL,
    [prsresponse]      VARCHAR (80)  NULL,
    [extresponse]      VARCHAR (255) NULL,
    [reasontemplateid] VARCHAR (64)  NULL,
    CONSTRAINT [vrqreasonPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [reasonid] ASC)
);

