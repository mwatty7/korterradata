﻿CREATE TABLE [dbo].[AAattachmentexif] (
    [customerid] VARCHAR (32)  NOT NULL,
    [jobid]      VARCHAR (16)  NOT NULL,
    [name]       VARCHAR (256) NOT NULL,
    [exiftag]    VARCHAR (256) NOT NULL,
    [exifvalue]  VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AAattachmentexif] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [name] ASC, [exiftag] ASC),
    CONSTRAINT [FK_AAattachmentexif_customerid_jobid_name_AAattachment_customerid_jobid_name] FOREIGN KEY ([customerid], [jobid], [name]) REFERENCES [dbo].[AAattachment] ([customerid], [jobid], [name])
);

