﻿CREATE TABLE [dbo].[reportschedulerecipient] (
    [reportschedulerecipientid] UNIQUEIDENTIFIER CONSTRAINT [DF__reportschedulerecipient_reprotschedulerecipientid] DEFAULT (newsequentialid()) NOT NULL,
    [reportscheduleversionid]   UNIQUEIDENTIFIER NOT NULL,
    [type]                      VARCHAR (32)     NOT NULL,
    [context]                   VARCHAR (MAX)    NOT NULL,
    [isactive]                  BIT              NULL,
    [createdby]                 VARCHAR (80)     NOT NULL,
    [creationdtutc]             DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_reportschedulerecipient] PRIMARY KEY CLUSTERED ([reportschedulerecipientid] ASC),
    CONSTRAINT [FK_reportschedulerecipient_reportscheduleversion] FOREIGN KEY ([reportscheduleversionid]) REFERENCES [dbo].[reportscheduleversion] ([reportscheduleversionid])
);


GO
CREATE NONCLUSTERED INDEX [IX_reportschedulerecpient_reportscheduleversionid_include]
    ON [dbo].[reportschedulerecipient]([reportscheduleversionid] ASC)
    INCLUDE([reportschedulerecipientid], [type], [context], [isactive], [createdby], [creationdtutc]);

