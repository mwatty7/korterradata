﻿CREATE TABLE [dbo].[facility] (
    [facilitytypeid]   INT          NOT NULL,
    [facilitytypecode] VARCHAR (8)  NULL,
    [facilitytype]     VARCHAR (32) NULL,
    CONSTRAINT [facilityPrimaryKey] PRIMARY KEY CLUSTERED ([facilitytypeid] ASC)
);

