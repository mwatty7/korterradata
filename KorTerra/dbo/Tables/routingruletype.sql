﻿CREATE TABLE [dbo].[routingruletype] (
    [rrtypeid]    UNIQUEIDENTIFIER NOT NULL,
    [rrversionid] UNIQUEIDENTIFIER NOT NULL,
    [match]       UNIQUEIDENTIFIER NULL,
    [nomatch]     UNIQUEIDENTIFIER NULL,
    [method]      VARCHAR (255)    NULL,
    [field]       VARCHAR (255)    NULL,
    CONSTRAINT [PK_routingruletype] PRIMARY KEY NONCLUSTERED ([rrtypeid] ASC),
    CONSTRAINT [FK_routingruletype_match_routingruletype_rrtypeid] FOREIGN KEY ([match]) REFERENCES [dbo].[routingruletype] ([rrtypeid]),
    CONSTRAINT [FK_routingruletype_nomatch_routingruletype_rrtypeid] FOREIGN KEY ([nomatch]) REFERENCES [dbo].[routingruletype] ([rrtypeid]),
    CONSTRAINT [FK_routingruletype_routingruleversionid] FOREIGN KEY ([rrversionid]) REFERENCES [dbo].[routingruleversion] ([rrversionid])
);


GO
CREATE NONCLUSTERED INDEX [IX_routingruletype_match]
    ON [dbo].[routingruletype]([match] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_routingruletype_nomatch]
    ON [dbo].[routingruletype]([nomatch] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_routingruletype_rrversionid]
    ON [dbo].[routingruletype]([rrversionid] ASC);

