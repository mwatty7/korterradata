﻿CREATE TABLE [dbo].[mything] (
    [id]  UNIQUEIDENTIFIER CONSTRAINT [DF_mything_id] DEFAULT (newsequentialid()) NOT NULL,
    [foo] VARCHAR (32)     NULL,
    CONSTRAINT [PK_mything] PRIMARY KEY CLUSTERED ([id] ASC)
);

