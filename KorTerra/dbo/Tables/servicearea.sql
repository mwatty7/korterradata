﻿CREATE TABLE [dbo].[servicearea] (
    [customerid]  VARCHAR (32) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [servicearea] VARCHAR (40) NOT NULL,
    CONSTRAINT [serviceareaPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [servicearea] ASC)
);

