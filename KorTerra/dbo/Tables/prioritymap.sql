﻿CREATE TABLE [dbo].[prioritymap] (
    [customerid]     VARCHAR (32) NOT NULL,
    [origpriority]   VARCHAR (80) NOT NULL,
    [occid]          VARCHAR (8)  NOT NULL,
    [priority]       VARCHAR (16) NULL,
    [custompriority] SMALLINT     NULL,
    [customorder]    VARCHAR (16) NULL,
    CONSTRAINT [prioritymapPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [origpriority] ASC, [occid] ASC)
);

