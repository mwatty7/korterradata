﻿CREATE TABLE [dbo].[savedrpttemplate] (
    [customerid]     VARCHAR (32) NOT NULL,
    [reportid]       VARCHAR (32) NOT NULL,
    [templateid]     VARCHAR (32) NOT NULL,
    [checksum]       VARCHAR (32) NOT NULL,
    [creationdtdate] DATETIME     NULL,
    [creationdttime] VARCHAR (12) NULL,
    CONSTRAINT [savedrpttemplatePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [templateid] ASC, [checksum] ASC)
);

