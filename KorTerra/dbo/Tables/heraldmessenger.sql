﻿CREATE TABLE [dbo].[heraldmessenger] (
    [customerid]                VARCHAR (32)  NOT NULL,
    [heraldmessengerid]         VARCHAR (16)  NOT NULL,
    [msghandlerparameter1]      VARCHAR (255) NULL,
    [msghandlerparameter2]      VARCHAR (255) NULL,
    [msghandler]                VARCHAR (32)  NULL,
    [msghandlerparameter3]      VARCHAR (255) NULL,
    [storedprocedure]           VARCHAR (255) NULL,
    [processingIntervalMinutes] INT           NULL,
    [isactive]                  SMALLINT      NULL,
    [sp_param1]                 VARCHAR (64)  NULL,
    [sp_param2]                 VARCHAR (64)  NULL,
    [sp_param3]                 VARCHAR (64)  NULL,
    [templateid]                VARCHAR (32)  NULL,
    CONSTRAINT [heraldmessengerPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [heraldmessengerid] ASC)
);

