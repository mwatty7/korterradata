﻿CREATE TABLE [dbo].[profileoperator] (
    [operatorid] VARCHAR (16) NOT NULL,
    [profileid]  VARCHAR (32) NOT NULL,
    CONSTRAINT [profileoperatorPrimaryKey] PRIMARY KEY CLUSTERED ([operatorid] ASC, [profileid] ASC)
);

