﻿CREATE TABLE [dbo].[occpriority] (
    [customerid]   VARCHAR (32) NOT NULL,
    [occid]        VARCHAR (8)  NOT NULL,
    [origpriority] VARCHAR (80) NOT NULL,
    CONSTRAINT [occpriorityPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [origpriority] ASC)
);

