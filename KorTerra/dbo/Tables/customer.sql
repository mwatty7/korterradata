﻿CREATE TABLE [dbo].[customer] (
    [customerid] VARCHAR (32) NOT NULL,
    CONSTRAINT [customerPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC)
);

