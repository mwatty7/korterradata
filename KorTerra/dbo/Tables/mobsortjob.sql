﻿CREATE TABLE [dbo].[mobsortjob] (
    [jobid]    VARCHAR (16) NOT NULL,
    [position] INT          NULL,
    CONSTRAINT [mobsortjobPrimaryKey] PRIMARY KEY CLUSTERED ([jobid] ASC)
);

