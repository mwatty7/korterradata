﻿CREATE TABLE [dbo].[reasonoccpriority] (
    [customerid]   VARCHAR (32) NOT NULL,
    [reasonlistid] VARCHAR (32) NOT NULL,
    [reasonid]     VARCHAR (32) NOT NULL,
    [occid]        VARCHAR (8)  NOT NULL,
    [origpriority] VARCHAR (80) NOT NULL,
    CONSTRAINT [PK_reasonoccpriority] PRIMARY KEY CLUSTERED ([customerid] ASC, [reasonlistid] ASC, [reasonid] ASC, [occid] ASC, [origpriority] ASC)
);

