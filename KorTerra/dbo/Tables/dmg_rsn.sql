﻿CREATE TABLE [dbo].[dmg_rsn] (
    [damagereasonid]   INT          NOT NULL,
    [damagereasoncode] VARCHAR (8)  NULL,
    [damagereason]     VARCHAR (32) NULL,
    [damagecodeid]     INT          NULL,
    [ishighlyvisible]  SMALLINT     NULL,
    [isvisible]        SMALLINT     NULL,
    [isfaded]          SMALLINT     NULL,
    [isnotpresent]     SMALLINT     NULL,
    CONSTRAINT [dmg_rsnPrimaryKey] PRIMARY KEY CLUSTERED ([damagereasonid] ASC)
);

