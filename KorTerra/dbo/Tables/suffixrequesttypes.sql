﻿CREATE TABLE [dbo].[suffixrequesttypes] (
    [customerid]  VARCHAR (32) NOT NULL,
    [suffixid]    VARCHAR (16) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    CONSTRAINT [suffixrequesttypesPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC, [membercode] ASC, [requesttype] ASC)
);


GO
CREATE NONCLUSTERED INDEX [sufreqtypememcodeindex]
    ON [dbo].[suffixrequesttypes]([customerid] ASC, [membercode] ASC, [requesttype] ASC);

