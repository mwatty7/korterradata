﻿CREATE TABLE [dbo].[PAmaint] (
    [maintid]     INT           IDENTITY (1, 1) NOT NULL,
    [customerid]  VARCHAR (32)  NOT NULL,
    [type]        VARCHAR (32)  NOT NULL,
    [description] VARCHAR (256) NOT NULL,
    [isactive]    SMALLINT      NULL,
    [sequence]    INT           NULL,
    CONSTRAINT [PK_PAmaint] PRIMARY KEY CLUSTERED ([maintid] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PAmaintDescIndex]
    ON [dbo].[PAmaint]([customerid] ASC, [type] ASC, [description] ASC);


GO
CREATE NONCLUSTERED INDEX [PAmaintIsactiveIndex]
    ON [dbo].[PAmaint]([customerid] ASC, [type] ASC, [isactive] ASC);

