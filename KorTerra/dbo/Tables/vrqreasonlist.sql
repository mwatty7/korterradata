﻿CREATE TABLE [dbo].[vrqreasonlist] (
    [customerid]     VARCHAR (32) NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [membercode]     VARCHAR (16) NOT NULL,
    [requesttype]    VARCHAR (9)  NOT NULL,
    [ismandatory]    SMALLINT     NULL,
    [prssendtype]    VARCHAR (16) NULL,
    [nlrresponse]    VARCHAR (80) NULL,
    [cancelresponse] VARCHAR (80) NULL,
    CONSTRAINT [vrqreasonlistPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC)
);

