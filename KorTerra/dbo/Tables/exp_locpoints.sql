﻿CREATE TABLE [dbo].[exp_locpoints] (
    [k_customerid] VARCHAR (16) NOT NULL,
    [k_jobid]      VARCHAR (16) NOT NULL,
    [k_type]       VARCHAR (16) NOT NULL,
    [k_seqno]      SMALLINT     NOT NULL,
    [field1]       VARCHAR (21) NULL,
    [field2]       VARCHAR (21) NULL,
    [field3]       VARCHAR (8)  NULL,
    [field4]       VARCHAR (8)  NULL,
    CONSTRAINT [exp_locpointsPrimaryKey] PRIMARY KEY CLUSTERED ([k_customerid] ASC, [k_jobid] ASC, [k_type] ASC, [k_seqno] ASC)
);

