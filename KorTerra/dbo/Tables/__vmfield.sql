﻿CREATE TABLE [dbo].[__vmfield] (
    [fieldid]        INT            IDENTITY (1, 1) NOT NULL,
    [completiontype] NVARCHAR (20)  NOT NULL,
    [sectionid]      NVARCHAR (20)  NOT NULL,
    [fieldtype]      NVARCHAR (20)  NOT NULL,
    [fieldorder]     INT            NOT NULL,
    [label]          NVARCHAR (255) NULL,
    [value]          NTEXT          NULL,
    [width]          NVARCHAR (255) NULL,
    CONSTRAINT [PK_vmfield] PRIMARY KEY CLUSTERED ([fieldid] ASC)
);

