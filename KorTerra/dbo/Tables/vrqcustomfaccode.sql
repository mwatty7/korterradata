﻿CREATE TABLE [dbo].[vrqcustomfaccode] (
    [customerid]  VARCHAR (32) NOT NULL,
    [jobid]       VARCHAR (16) NOT NULL,
    [faccode]     VARCHAR (16) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [suffixid]    VARCHAR (16) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [vrqcustomfaccodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [faccode] ASC, [membercode] ASC, [requesttype] ASC, [suffixid] ASC)
);

