﻿CREATE TABLE [dbo].[__visitdata3] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [requesttype]    VARCHAR (9)   NOT NULL,
    [membercode]     VARCHAR (16)  NOT NULL,
    [createdatetime] DATETIME      NOT NULL,
    [sectionid]      VARCHAR (32)  NOT NULL,
    [fieldid]        VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [data]           VARCHAR (MAX) NOT NULL,
    [html]           VARCHAR (MAX) NULL,
    CONSTRAINT [PK_vmdata] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC, [membercode] ASC, [sectionid] ASC, [fieldid] ASC, [createdatetime] ASC),
    CONSTRAINT [FK_vmcommon_vmdata] FOREIGN KEY ([customerid], [jobid], [membercode], [requesttype], [createdatetime]) REFERENCES [dbo].[__visitcommon3] ([customerid], [jobid], [membercode], [requesttype], [creationdt]),
    CONSTRAINT [FK_vmdetail_vmdata] FOREIGN KEY ([customerid], [requesttype], [sectionid], [fieldid]) REFERENCES [dbo].[__vmdetail3] ([customerid], [requesttype], [sectionid], [fieldid])
);

