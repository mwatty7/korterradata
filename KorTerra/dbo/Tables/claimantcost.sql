﻿CREATE TABLE [dbo].[claimantcost] (
    [claimid]     VARCHAR (16)    NOT NULL,
    [claimantid]  VARCHAR (32)    NOT NULL,
    [costtypeid]  VARCHAR (16)    NOT NULL,
    [seqno]       INT             NOT NULL,
    [description] VARCHAR (80)    NULL,
    [invoiceamt]  DECIMAL (15, 2) NULL,
    [estpayout]   DECIMAL (15, 2) NULL,
    [insrecovery] DECIMAL (15, 2) NULL,
    CONSTRAINT [claimantcostPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [claimantid] ASC, [costtypeid] ASC, [seqno] ASC)
);

