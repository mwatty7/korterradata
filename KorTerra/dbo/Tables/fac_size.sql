﻿CREATE TABLE [dbo].[fac_size] (
    [facilitysizeid]   INT          NOT NULL,
    [facilitysizecode] VARCHAR (8)  NULL,
    [facilitysize]     VARCHAR (32) NULL,
    [facilitytypeid]   INT          NULL,
    CONSTRAINT [fac_sizePrimaryKey] PRIMARY KEY CLUSTERED ([facilitysizeid] ASC)
);

