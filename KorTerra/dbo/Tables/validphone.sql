﻿CREATE TABLE [dbo].[validphone] (
    [customerid] VARCHAR (32) NOT NULL,
    [membercode] VARCHAR (16) NOT NULL,
    [areacode]   VARCHAR (4)  NOT NULL,
    [wirecenter] VARCHAR (7)  NOT NULL,
    CONSTRAINT [validphonePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [areacode] ASC, [wirecenter] ASC)
);

