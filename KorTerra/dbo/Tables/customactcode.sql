﻿CREATE TABLE [dbo].[customactcode] (
    [customerid]  VARCHAR (32) NOT NULL,
    [actcode]     VARCHAR (16) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [description] VARCHAR (32) NULL,
    [actiontype]  VARCHAR (80) NULL,
    CONSTRAINT [customactcodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [actcode] ASC, [membercode] ASC, [requesttype] ASC)
);

