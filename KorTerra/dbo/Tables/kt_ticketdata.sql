﻿CREATE TABLE [dbo].[kt_ticketdata] (
    [ticketdataGuid] UNIQUEIDENTIFIER NOT NULL,
    [ticketGuid]     UNIQUEIDENTIFIER NOT NULL,
    [field]          VARCHAR (32)     NOT NULL,
    [value]          VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_kt_ticketdata] PRIMARY KEY CLUSTERED ([ticketdataGuid] ASC),
    CONSTRAINT [FK_kt_ticketdata_ticketGuid_kt_ticket_ticketGuid] FOREIGN KEY ([ticketGuid]) REFERENCES [dbo].[kt_ticket] ([ticketGuid])
);

