﻿CREATE TABLE [dbo].[tagvalue] (
    [tagvalueid] UNIQUEIDENTIFIER NOT NULL,
    [tagfieldid] UNIQUEIDENTIFIER NOT NULL,
    [value]      VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_tagvalue_tagvalueid] PRIMARY KEY NONCLUSTERED ([tagvalueid] ASC),
    CONSTRAINT [FK_tagvalue_tagvfieldid_tagfield_tagfieldid] FOREIGN KEY ([tagfieldid]) REFERENCES [dbo].[tagfield] ([tagfieldid])
);

