﻿CREATE TABLE [dbo].[parent] (
    [parentid] UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [data]     VARCHAR (32)     NULL,
    CONSTRAINT [PK_parent] PRIMARY KEY CLUSTERED ([parentid] ASC)
);

