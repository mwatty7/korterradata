﻿CREATE TABLE [dbo].[ticketflagremarks] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [ticketflagid]   VARCHAR (32)  NOT NULL,
    [ticketflagtype] VARCHAR (32)  NOT NULL,
    [seqno]          SMALLINT      NOT NULL,
    [icon]           VARCHAR (255) NULL,
    [text]           VARCHAR (255) NULL,
    CONSTRAINT [ticketflagremarksPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [ticketflagid] ASC, [ticketflagtype] ASC, [seqno] ASC)
);

