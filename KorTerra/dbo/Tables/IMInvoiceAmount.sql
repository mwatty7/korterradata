﻿CREATE TABLE [dbo].[IMInvoiceAmount] (
    [InvoiceAmountId]     INT             IDENTITY (1, 1) NOT NULL,
    [CustomerId]          NVARCHAR (32)   NOT NULL,
    [IncidentNumber]      NVARCHAR (32)   NOT NULL,
    [Amount]              DECIMAL (18, 2) NOT NULL,
    [AmountDescription]   NVARCHAR (MAX)  NOT NULL,
    [CreatedByOperatorId] NVARCHAR (32)   NOT NULL,
    [CreatedDateUtc]      DATETIME        NOT NULL,
    CONSTRAINT [PK_IMInvoiceAmount] PRIMARY KEY CLUSTERED ([InvoiceAmountId] ASC),
    CONSTRAINT [FK_IMInvoiceAmount_IMincident] FOREIGN KEY ([CustomerId], [IncidentNumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

