﻿CREATE TABLE [dbo].[multipartyremarks] (
    [autolossid]   VARCHAR (16)  NOT NULL,
    [multipartyid] VARCHAR (32)  NOT NULL,
    [type]         VARCHAR (16)  NOT NULL,
    [seqno]        SMALLINT      NOT NULL,
    [text]         VARCHAR (128) NULL,
    CONSTRAINT [multipartyremarksPrimaryKey] PRIMARY KEY CLUSTERED ([autolossid] ASC, [multipartyid] ASC, [type] ASC, [seqno] ASC)
);

