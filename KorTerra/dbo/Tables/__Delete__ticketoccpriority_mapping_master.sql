﻿CREATE TABLE [dbo].[__Delete__ticketoccpriority_mapping_master] (
    [occid]                 VARCHAR (8)      NULL,
    [kt_priority]           VARCHAR (16)     NULL,
    [origpriority]          VARCHAR (25)     NULL,
    [fullsize_origpriority] VARCHAR (80)     NULL,
    [ticketpriorityid]      UNIQUEIDENTIFIER NULL
);

