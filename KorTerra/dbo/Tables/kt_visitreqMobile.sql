﻿CREATE TABLE [dbo].[kt_visitreqMobile] (
    [visitreqGuid] UNIQUEIDENTIFIER NOT NULL,
    [mobileGuid]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_kt_visitreqMobile] PRIMARY KEY CLUSTERED ([visitreqGuid] ASC, [mobileGuid] ASC),
    CONSTRAINT [FK_kt_visitreqMobile_mobileGuid_kt_mobile_mobileGuid] FOREIGN KEY ([mobileGuid]) REFERENCES [dbo].[kt_mobile] ([mobileGuid]),
    CONSTRAINT [FK_kt_visitreqMobile_visitreqGuid_kt_visitreq_visitreqGuid] FOREIGN KEY ([visitreqGuid]) REFERENCES [dbo].[kt_visitreq] ([visitreqGuid])
);

