﻿CREATE TABLE [dbo].[deptbill_xxx] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [invoiceid]        VARCHAR (12)    NOT NULL,
    [departmentid]     VARCHAR (16)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      VARCHAR (16)    NULL,
    [amount]           DECIMAL (15, 2) NULL,
    [quantity]         DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [workorder]        VARCHAR (16)    NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [billingstatus]    CHAR (1)        NULL,
    [pricetier]        SMALLINT        NULL,
    [bonusunits]       FLOAT (53)      NULL,
    [reportid]         VARCHAR (16)    NULL,
    [serviceterritory] VARCHAR (16)    NULL,
    [custcode]         VARCHAR (16)    NULL,
    [fee]              DECIMAL (15, 2) NULL,
    [zoneid]           VARCHAR (16)    NULL,
    CONSTRAINT [deptbill_xxxPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [invoiceid] ASC, [departmentid] ASC, [jobid] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC, [membercode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [deptbill_xxxcompletiondtindex]
    ON [dbo].[deptbill_xxx]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingstatus] ASC, [mobileid] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [deptbill_xxxCompletionIndex]
    ON [dbo].[deptbill_xxx]([customerid] ASC, [departmentid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [deptbill_xxxjobIndex]
    ON [dbo].[deptbill_xxx]([customerid] ASC, [jobid] ASC, [departmentid] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC);

