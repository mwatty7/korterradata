﻿CREATE TABLE [dbo].[screeningdecisiondata] (
    [screeningid] INT           NOT NULL,
    [fieldid]     VARCHAR (32)  NOT NULL,
    [value]       VARCHAR (MAX) NULL,
    CONSTRAINT [PK_screeningdecisiondata] PRIMARY KEY CLUSTERED ([screeningid] ASC, [fieldid] ASC),
    CONSTRAINT [FK_screeningdecisiondata_screeningid_screeningdecisioncommon_screeningid] FOREIGN KEY ([screeningid]) REFERENCES [dbo].[screeningdecisioncommon] ([screeningid])
);

