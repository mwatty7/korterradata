﻿CREATE TABLE [dbo].[e5visit] (
    [customerid]           VARCHAR (32)    NOT NULL,
    [jobid]                VARCHAR (16)    NOT NULL,
    [membercode]           VARCHAR (16)    NOT NULL,
    [requesttype]          VARCHAR (9)     NOT NULL,
    [creationdtdate]       DATETIME        NOT NULL,
    [creationdttime]       VARCHAR (12)    NOT NULL,
    [versiondtdate]        DATETIME        NULL,
    [versiondttime]        VARCHAR (12)    NULL,
    [versionauthor]        VARCHAR (16)    NULL,
    [operatorid]           VARCHAR (16)    NULL,
    [mobileid]             VARCHAR (16)    NULL,
    [completiondtdate]     DATETIME        NULL,
    [completiondttime]     VARCHAR (12)    NULL,
    [billingtype]          CHAR (1)        NULL,
    [tmunits]              DECIMAL (15, 2) NULL,
    [tmunittype]           CHAR (1)        NULL,
    [completedby]          VARCHAR (10)    NULL,
    [completionlevel]      CHAR (1)        NULL,
    [departmentid]         VARCHAR (16)    NULL,
    [platno]               VARCHAR (13)    NULL,
    [filmroll]             VARCHAR (13)    NULL,
    [isprimary]            SMALLINT        NULL,
    [issecondary]          SMALLINT        NULL,
    [isstreetlight]        SMALLINT        NULL,
    [isduct]               SMALLINT        NULL,
    [is1000mcm]            SMALLINT        NULL,
    [isnetwork]            SMALLINT        NULL,
    [ismarta]              SMALLINT        NULL,
    [istransmission]       SMALLINT        NULL,
    [issti]                SMALLINT        NULL,
    [qtyprimary]           SMALLINT        NULL,
    [qtysecondary]         SMALLINT        NULL,
    [qtystreetlight]       SMALLINT        NULL,
    [qtyduct]              SMALLINT        NULL,
    [qty1000mcm]           SMALLINT        NULL,
    [qtynetwork]           SMALLINT        NULL,
    [qtymarta]             SMALLINT        NULL,
    [qtytransmission]      SMALLINT        NULL,
    [qtysti]               SMALLINT        NULL,
    [ispaint]              SMALLINT        NULL,
    [isflag]               SMALLINT        NULL,
    [isstake]              SMALLINT        NULL,
    [isstakechaser]        SMALLINT        NULL,
    [isoffset]             SMALLINT        NULL,
    [qtyoffset]            DECIMAL (15, 1) NULL,
    [ishighprofile]        SMALLINT        NULL,
    [isprivatefacilities]  SMALLINT        NULL,
    [isongoingjob]         SMALLINT        NULL,
    [isclearexcavate]      SMALLINT        NULL,
    [isnounderground]      SMALLINT        NULL,
    [isoverheadfacilities] SMALLINT        NULL,
    [isnoconflictfacility] SMALLINT        NULL,
    [isnotcompleted]       SMALLINT        NULL,
    [unmarkedreason]       VARCHAR (32)    NULL,
    [isnotsendprs]         SMALLINT        NULL,
    CONSTRAINT [e5visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [e5visitCompletionIndex]
    ON [dbo].[e5visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [e5visitDeptIndex]
    ON [dbo].[e5visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [e5visitMembercodeIndex]
    ON [dbo].[e5visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

