﻿CREATE TABLE [dbo].[XXsessionduration] (
    [sessiondurationid] UNIQUEIDENTIFIER NOT NULL,
    [xxtoken]           UNIQUEIDENTIFIER NOT NULL,
    [application]       VARCHAR (256)    NULL,
    [customerid]        VARCHAR (32)     NOT NULL,
    [operatorid]        VARCHAR (256)    NOT NULL,
    [firstactivity]     DATETIME2 (7)    NULL,
    [lastactivity]      DATETIME2 (7)    NULL,
    [minutes]           DECIMAL (10, 2)  NULL,
    CONSTRAINT [XXsessiondurationIdPrimaryKey] PRIMARY KEY NONCLUSTERED ([sessiondurationid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_xxsessionduration_xxtoken_application_cust_operator]
    ON [dbo].[XXsessionduration]([xxtoken] ASC, [application] ASC, [customerid] ASC, [operatorid] ASC);


GO
CREATE NONCLUSTERED INDEX [XXsessiondurationCustomerIdOperatorId]
    ON [dbo].[XXsessionduration]([xxtoken] ASC, [customerid] ASC, [operatorid] ASC);

