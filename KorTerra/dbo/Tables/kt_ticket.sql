﻿CREATE TABLE [dbo].[kt_ticket] (
    [ticketGuid]     UNIQUEIDENTIFIER NOT NULL,
    [ticketNumber]   VARCHAR (16)     NOT NULL,
    [occGuid]        UNIQUEIDENTIFIER NULL,
    [prevTicketGuid] UNIQUEIDENTIFIER NULL,
    [nextTicketGuid] UNIQUEIDENTIFIER NULL,
    [excavatorGuid]  UNIQUEIDENTIFIER NULL,
    [locationGuid]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_kt_ticket] PRIMARY KEY CLUSTERED ([ticketGuid] ASC)
);

