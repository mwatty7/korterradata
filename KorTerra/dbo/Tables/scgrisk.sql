﻿CREATE TABLE [dbo].[scgrisk] (
    [customerid]        VARCHAR (32)  NOT NULL,
    [jobid]             VARCHAR (16)  NOT NULL,
    [riskscore]         FLOAT (53)    NULL,
    [XHPPipe]           BIT           NULL,
    [DigMethod]         VARCHAR (255) NULL,
    [XStationStructure] BIT           NULL,
    CONSTRAINT [PK_scgrisk] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC)
);

