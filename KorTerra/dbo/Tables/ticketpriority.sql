﻿CREATE TABLE [dbo].[ticketpriority] (
    [ticketpriorityid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]       VARCHAR (32)     NOT NULL,
    [name]             VARCHAR (64)     NOT NULL,
    [color]            VARCHAR (32)     NULL,
    [sortorder]        INT              NULL,
    [msgtype]          VARCHAR (32)     NULL,
    [prioritytype]     VARCHAR (32)     NULL,
    CONSTRAINT [PK_ticketpriority] PRIMARY KEY CLUSTERED ([ticketpriorityid] ASC)
);

