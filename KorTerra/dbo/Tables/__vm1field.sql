﻿CREATE TABLE [dbo].[__vm1field] (
    [fieldid]     INT            IDENTITY (1, 1) NOT NULL,
    [customerid]  CHAR (32)      NOT NULL,
    [requesttype] CHAR (9)       NOT NULL,
    [sectionid]   INT            NOT NULL,
    [fieldtype]   NVARCHAR (32)  NOT NULL,
    [fieldorder]  INT            NOT NULL,
    [label]       NVARCHAR (255) NULL,
    [value]       NVARCHAR (MAX) NULL,
    [width]       NVARCHAR (255) NULL,
    CONSTRAINT [PK_vm1field] PRIMARY KEY CLUSTERED ([fieldid] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [vm1fieldcustomeridrequesttypesectionidIndex]
    ON [dbo].[__vm1field]([customerid] ASC, [requesttype] ASC, [sectionid] ASC, [label] ASC);

