﻿CREATE TABLE [dbo].[holiday] (
    [customerid]  VARCHAR (32) NOT NULL,
    [holidaydate] DATETIME     NOT NULL,
    [occid]       VARCHAR (8)  NOT NULL,
    [description] VARCHAR (32) NULL,
    [iseveryyear] SMALLINT     NULL,
    CONSTRAINT [holidayPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [holidaydate] ASC, [occid] ASC)
);


GO


CREATE TRIGGER [dbo].[kt_tr_populate_calendar]
ON [dbo].[holiday] 
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_occid VARCHAR(8);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_occid VARCHAR(8);

	SELECT @new_customerid=customerid, @new_occid=occid
		FROM inserted;

	SELECT @old_customerid=customerid, @old_occid=occid
		FROM deleted;

	IF @new_customerid IS NOT NULL AND @new_occid IS NOT NULL
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_occid
		EXECUTE kt_sp_populate_calendar
			@customerid = @new_customerid,
			@occid = @new_occid
	END

	IF @old_customerid IS NOT NULL AND @old_occid IS NOT NULL
	BEGIN
		PRINT N'populating for ' + @old_customerid + ', ' + @old_occid
		EXECUTE kt_sp_populate_calendar
			@customerid = @old_customerid,
			@occid = @old_occid
	END;
END;