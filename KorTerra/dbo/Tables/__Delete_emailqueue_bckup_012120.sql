﻿CREATE TABLE [dbo].[__Delete_emailqueue_bckup_012120] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [toaddress]   VARCHAR (254) NOT NULL,
    [fromaddress] VARCHAR (254) NULL,
    [subject]     VARCHAR (998) NULL,
    [emailbody]   VARCHAR (MAX) NULL,
    [ishtml]      BIT           NULL,
    [status]      VARCHAR (32)  NULL,
    [failreason]  VARCHAR (MAX) NULL,
    [attemptdt]   DATETIME2 (7) NULL,
    [numattempts] INT           NULL
);

