﻿CREATE TABLE [dbo].[tagfield] (
    [tagfieldid]   UNIQUEIDENTIFIER NOT NULL,
    [tagversionid] UNIQUEIDENTIFIER NOT NULL,
    [fieldname]    VARCHAR (255)    NOT NULL,
    [equalitytype] VARCHAR (16)     NULL,
    CONSTRAINT [PK_tagfield_tagfieldid] PRIMARY KEY NONCLUSTERED ([tagfieldid] ASC),
    CONSTRAINT [FK_tagfield_tagversionid_tagversion_tagversionid] FOREIGN KEY ([tagversionid]) REFERENCES [dbo].[tagversion] ([tagversionid])
);

