﻿CREATE TABLE [dbo].[visitrule] (
    [visitruleid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]  VARCHAR (32)     NOT NULL,
    [membercode]  VARCHAR (16)     NOT NULL,
    [requesttype] VARCHAR (9)      NOT NULL,
    [prssendtype] VARCHAR (16)     NOT NULL,
    [description] VARCHAR (80)     NULL,
    [ruledef]     VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_visitrule] PRIMARY KEY CLUSTERED ([visitruleid] ASC),
    CONSTRAINT [FK_visitrule_prsmemcode] FOREIGN KEY ([customerid], [membercode], [prssendtype]) REFERENCES [dbo].[prsmemcode] ([customerid], [membercode], [prssendtype])
);

