﻿CREATE TABLE [dbo].[scheduledetail] (
    [scheduledetailid]  UNIQUEIDENTIFIER NOT NULL,
    [scheduleversionid] UNIQUEIDENTIFIER NOT NULL,
    [issunday]          BIT              NULL,
    [ismonday]          BIT              NULL,
    [istuesday]         BIT              NULL,
    [iswednesday]       BIT              NULL,
    [isthursday]        BIT              NULL,
    [isfriday]          BIT              NULL,
    [issaturday]        BIT              NULL,
    [isallday]          BIT              NULL,
    [isincludeholidays] BIT              NULL,
    [starttime]         TIME (7)         NULL,
    [endtime]           TIME (7)         NULL,
    CONSTRAINT [PK_scheduledetail_scheduledtailid] PRIMARY KEY NONCLUSTERED ([scheduledetailid] ASC),
    FOREIGN KEY ([scheduleversionid]) REFERENCES [dbo].[scheduleversion] ([scheduleversionid])
);

