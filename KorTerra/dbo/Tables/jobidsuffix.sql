﻿CREATE TABLE [dbo].[jobidsuffix] (
    [customerid]  VARCHAR (32) NOT NULL,
    [suffixid]    VARCHAR (16) NOT NULL,
    [suffixvalue] VARCHAR (16) NULL,
    CONSTRAINT [jobidsuffixPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC)
);

