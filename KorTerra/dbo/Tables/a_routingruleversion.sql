﻿CREATE TABLE [dbo].[a_routingruleversion] (
    [rrversionid]          UNIQUEIDENTIFIER NOT NULL,
    [customerid]           VARCHAR (32)     NOT NULL,
    [occid]                VARCHAR (8)      NOT NULL,
    [suffixid]             VARCHAR (16)     NOT NULL,
    [isactive]             BIT              NULL,
    [startingrule]         UNIQUEIDENTIFIER NULL,
    [publisheddtutc]       DATETIME2 (7)    NULL,
    [createddtutc]         DATETIME2 (7)    NULL,
    [createdby]            VARCHAR (80)     NULL,
    [lastmodifiedby]       VARCHAR (80)     NULL,
    [lastmodifieddtutc]    DATETIME2 (7)    NULL,
    [routingversionnumber] INT              NULL,
    CONSTRAINT [PK_a_routingruleversion] PRIMARY KEY NONCLUSTERED ([rrversionid] ASC)
);

