﻿CREATE TABLE [dbo].[revisedduedatereason] (
    [rvddrid]     UNIQUEIDENTIFIER NOT NULL,
    [customerid]  VARCHAR (32)     NOT NULL,
    [code]        VARCHAR (32)     NOT NULL,
    [description] VARCHAR (80)     NOT NULL,
    [isactive]    BIT              NOT NULL,
    CONSTRAINT [_revisedduedatereason] PRIMARY KEY NONCLUSTERED ([rvddrid] ASC),
    CONSTRAINT [uq_rrddate_customerid_code] UNIQUE NONCLUSTERED ([customerid] ASC, [code] ASC)
);

