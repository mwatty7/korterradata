﻿CREATE TABLE [dbo].[kt_mobile] (
    [mobileGuid]  UNIQUEIDENTIFIER NOT NULL,
    [name]        VARCHAR (32)     NOT NULL,
    [description] VARCHAR (80)     NOT NULL,
    [isidle]      BIT              NULL,
    [isactive]    BIT              NULL,
    CONSTRAINT [PK_kt_mobile] PRIMARY KEY CLUSTERED ([mobileGuid] ASC)
);

