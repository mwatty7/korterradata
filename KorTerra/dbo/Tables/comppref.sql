﻿CREATE TABLE [dbo].[comppref] (
    [customerid]  VARCHAR (32) NOT NULL,
    [operatorid]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [controlname] VARCHAR (32) NULL,
    [prettyname]  VARCHAR (32) NULL,
    CONSTRAINT [compprefPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [requesttype] ASC)
);

