﻿CREATE TABLE [dbo].[IMcode] (
    [customerid]   NVARCHAR (32)  NOT NULL,
    [codetype]     NVARCHAR (32)  NOT NULL,
    [codeid]       NVARCHAR (32)  NOT NULL,
    [description]  NVARCHAR (MAX) NULL,
    [dirtcodetype] NVARCHAR (32)  NULL,
    [dirtid]       NVARCHAR (32)  NULL,
    [id]           INT            IDENTITY (1, 1) NOT NULL,
    [parentid]     INT            NULL,
    [dirtid2]      VARCHAR (32)   NULL,
    CONSTRAINT [PK_code] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_IMcode_parentImCode] FOREIGN KEY ([parentid]) REFERENCES [dbo].[IMcode] ([id]),
    CONSTRAINT [UQ_codeID] UNIQUE NONCLUSTERED ([customerid] ASC, [codetype] ASC, [codeid] ASC, [parentid] ASC)
);

