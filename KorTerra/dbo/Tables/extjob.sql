﻿CREATE TABLE [dbo].[extjob] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [jobid]         VARCHAR (16)  NOT NULL,
    [field1]        VARCHAR (40)  NOT NULL,
    [field2]        VARCHAR (40)  NOT NULL,
    [field3]        VARCHAR (40)  NULL,
    [field4]        VARCHAR (40)  NULL,
    [field5]        VARCHAR (40)  NULL,
    [field6]        VARCHAR (40)  NULL,
    [field7]        VARCHAR (40)  NULL,
    [field8]        VARCHAR (128) NULL,
    [field9]        VARCHAR (128) NULL,
    [field10]       VARCHAR (128) NULL,
    [field11]       SMALLINT      NULL,
    [field12]       SMALLINT      NULL,
    [field13]       SMALLINT      NULL,
    [field14]       SMALLINT      NULL,
    [field15]       SMALLINT      NULL,
    [field16]       SMALLINT      NULL,
    [field17]       SMALLINT      NULL,
    [field18]       SMALLINT      NULL,
    [field19]       INT           NULL,
    [field20]       INT           NULL,
    [field21]       INT           NULL,
    [field22]       FLOAT (53)    NULL,
    [field23]       FLOAT (53)    NULL,
    [field24]       FLOAT (53)    NULL,
    [field25dtdate] DATETIME      NULL,
    [field25dttime] VARCHAR (12)  NULL,
    [field26dtdate] DATETIME      NULL,
    [field26dttime] VARCHAR (12)  NULL,
    [field27dtdate] DATETIME      NULL,
    [field27dttime] VARCHAR (12)  NULL,
    [field28dtdate] DATETIME      NULL,
    [field28dttime] VARCHAR (12)  NULL,
    [field29dtdate] DATETIME      NULL,
    [field29dttime] VARCHAR (12)  NULL,
    CONSTRAINT [extjobPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [field1] ASC, [field2] ASC)
);


GO
CREATE NONCLUSTERED INDEX [exjobCustomeridField1Index]
    ON [dbo].[extjob]([customerid] ASC, [field1] ASC);

