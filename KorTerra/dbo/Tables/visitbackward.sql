﻿CREATE TABLE [dbo].[visitbackward] (
    [id]             INT          IDENTITY (1, 1) NOT NULL,
    [customerid]     VARCHAR (32) NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [membercode]     VARCHAR (16) NOT NULL,
    [requesttype]    VARCHAR (9)  NOT NULL,
    [creationdtdate] DATETIME     NOT NULL,
    [creationdttime] VARCHAR (12) NOT NULL,
    [triggerdtdate]  DATETIME     NULL,
    [triggerdttime]  VARCHAR (12) NULL,
    [action]         VARCHAR (16) NULL,
    [direction]      VARCHAR (16) NULL,
    CONSTRAINT [PK_visitbackward] PRIMARY KEY CLUSTERED ([id] ASC)
);

