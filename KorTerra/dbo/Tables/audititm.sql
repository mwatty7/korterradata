﻿CREATE TABLE [dbo].[audititm] (
    [customerid] VARCHAR (32) NOT NULL,
    [auditdate]  DATETIME     NOT NULL,
    [membercode] VARCHAR (16) NOT NULL,
    [seqno]      SMALLINT     NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    [priority]   VARCHAR (16) NULL,
    [occstatus]  CHAR (1)     NULL,
    [isrepeated] SMALLINT     NULL,
    [occid]      VARCHAR (8)  NULL,
    CONSTRAINT [audititmPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [auditdate] ASC, [membercode] ASC, [seqno] ASC, [jobid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [audititmJobidIndex]
    ON [dbo].[audititm]([customerid] ASC, [jobid] ASC);

