﻿CREATE TABLE [dbo].[emergencygroup] (
    [customerid]       VARCHAR (32) NOT NULL,
    [emergencygroupid] VARCHAR (16) NOT NULL,
    [description]      VARCHAR (32) NULL,
    CONSTRAINT [emergencygroupPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [emergencygroupid] ASC)
);

