﻿CREATE TABLE [dbo].[msgqueue] (
    [id]             INT            IDENTITY (1, 1) NOT NULL,
    [status]         VARCHAR (1)    NOT NULL,
    [queueddt]       DATETIME2 (7)  CONSTRAINT [DF_msgqueue_queueddt] DEFAULT (getdate()) NULL,
    [processingdt]   DATETIME2 (7)  NULL,
    [failedreason]   VARCHAR (1024) NULL,
    [msgsource]      VARCHAR (1024) NULL,
    [customerid]     VARCHAR (32)   NULL,
    [msgtype]        VARCHAR (32)   NULL,
    [msggroupid]     VARCHAR (16)   NULL,
    [msgtag]         VARCHAR (64)   NULL,
    [msgdescription] VARCHAR (128)  NULL,
    [msghandler]     VARCHAR (32)   NULL,
    [msgdatatext]    VARCHAR (2048) NULL,
    CONSTRAINT [PK_msgqueue] PRIMARY KEY CLUSTERED ([id] ASC)
);

