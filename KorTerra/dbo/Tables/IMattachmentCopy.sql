﻿CREATE TABLE [dbo].[IMattachmentCopy] (
    [customerid]            NVARCHAR (32)    NOT NULL,
    [incidentnumber]        NVARCHAR (32)    NOT NULL,
    [name]                  NVARCHAR (256)   NOT NULL,
    [attacheddtutc]         DATETIME         NULL,
    [type]                  NVARCHAR (32)    NULL,
    [size]                  INT              NULL,
    [createdbyoperatorid]   NVARCHAR (32)    NULL,
    [path]                  NVARCHAR (256)   NULL,
    [notes]                 NVARCHAR (MAX)   NULL,
    [thumbnailimage]        NVARCHAR (256)   NULL,
    [reportimage]           NVARCHAR (256)   NULL,
    [exifcameramake]        NVARCHAR (32)    NULL,
    [exifcameramodel]       NVARCHAR (32)    NULL,
    [exifpicturetakendtutc] DATETIME         NULL,
    [exiflatitude]          NVARCHAR (32)    NULL,
    [exiflongitude]         NVARCHAR (32)    NULL,
    [thumbnailimagesize]    INT              NULL,
    [reportimagesize]       INT              NULL,
    [guid]                  UNIQUEIDENTIFIER NULL,
    [isactive]              BIT              NULL
);

