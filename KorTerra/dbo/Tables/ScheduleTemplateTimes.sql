﻿CREATE TABLE [dbo].[ScheduleTemplateTimes] (
    [stt_id]    INT          IDENTITY (1, 1) NOT NULL,
    [cst_id]    INT          NOT NULL,
    [day]       VARCHAR (32) NOT NULL,
    [starttime] VARCHAR (12) NOT NULL,
    [endtime]   VARCHAR (12) NOT NULL,
    CONSTRAINT [PK_ScheduleTemplateTimes] PRIMARY KEY CLUSTERED ([stt_id] ASC),
    CONSTRAINT [FK_ScheduleTemplateTimes] FOREIGN KEY ([cst_id]) REFERENCES [dbo].[CustomerScheduleTemplates] ([cst_id])
);

