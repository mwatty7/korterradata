﻿CREATE TABLE [dbo].[visitprsaudit] (
    [visitprsqueueid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]      VARCHAR (32)     NOT NULL,
    [jobid]           VARCHAR (16)     NOT NULL,
    [membercode]      VARCHAR (16)     NOT NULL,
    [requesttype]     VARCHAR (9)      NOT NULL,
    [creationdt]      DATETIME2 (7)    NOT NULL,
    [prssendtype]     VARCHAR (16)     NOT NULL,
    [visitruleid]     VARCHAR (80)     NULL,
    [queueddt]        DATETIME2 (7)    NULL,
    [visitqueueid]    VARCHAR (80)     NULL,
    [templatedata]    VARCHAR (MAX)    NULL,
    [templatecontent] VARCHAR (MAX)    NULL,
    [actiontype]      VARCHAR (80)     NULL,
    [destination]     VARCHAR (MAX)    NULL,
    [seqno]           INT              NOT NULL,
    [prssenddt]       DATETIME2 (7)    NOT NULL,
    [status]          VARCHAR (32)     NULL,
    [reason]          VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_visitprsaudit] PRIMARY KEY CLUSTERED ([visitprsqueueid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [visitprsaudit_queueddtIndex]
    ON [dbo].[visitprsaudit]([queueddt] ASC);

