﻿CREATE TABLE [dbo].[ticketupdate] (
    [customerid]      VARCHAR (32)  NOT NULL,
    [jobid]           VARCHAR (16)  NOT NULL,
    [occid]           VARCHAR (8)   NOT NULL,
    [seqno]           SMALLINT      NOT NULL,
    [ktoccjobid]      VARCHAR (16)  NULL,
    [occjobid]        VARCHAR (16)  NULL,
    [xmlfile]         VARCHAR (255) NULL,
    [username]        VARCHAR (51)  NULL,
    [companyid]       VARCHAR (32)  NULL,
    [phone]           VARCHAR (21)  NULL,
    [email]           VARCHAR (80)  NULL,
    [updatetype]      VARCHAR (255) NULL,
    [comments]        VARCHAR (255) NULL,
    [operatorid]      VARCHAR (16)  NULL,
    [submitteddtdate] DATETIME      NULL,
    [submitteddttime] VARCHAR (12)  NULL,
    CONSTRAINT [ticketupdatePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [occid] ASC, [seqno] ASC)
);

