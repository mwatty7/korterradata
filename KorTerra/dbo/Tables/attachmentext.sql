﻿CREATE TABLE [dbo].[attachmentext] (
    [customerid]      VARCHAR (32) NOT NULL,
    [jobid]           VARCHAR (16) NOT NULL,
    [name]            VARCHAR (32) NOT NULL,
    [receiveddtdate]  DATETIME     NULL,
    [receiveddttime]  VARCHAR (12) NULL,
    [movecheckdtdate] DATETIME     NULL,
    [movecheckdttime] VARCHAR (12) NULL,
    [filetype]        VARCHAR (16) NULL,
    [yyyymm]          VARCHAR (9)  NULL,
    [isresized]       SMALLINT     NULL,
    [ismoved]         SMALLINT     NULL,
    CONSTRAINT [attachmentextPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [name] ASC)
);

