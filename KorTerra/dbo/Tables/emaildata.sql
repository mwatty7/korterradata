﻿CREATE TABLE [dbo].[emaildata] (
    [emaildataid]   UNIQUEIDENTIFIER NOT NULL,
    [id]            INT              NOT NULL,
    [creationdtutc] DATETIME2 (7)    NOT NULL,
    [type]          VARCHAR (32)     NULL,
    [context]       VARCHAR (MAX)    NULL,
    [applicationid] VARCHAR (80)     NULL,
    [toaddress]     VARCHAR (254)    NULL,
    [fromaddress]   VARCHAR (254)    NULL,
    [subject]       VARCHAR (998)    NULL,
    [emailbody]     VARCHAR (MAX)    NULL,
    [ishtml]        BIT              NULL,
    CONSTRAINT [emaildataidPrimaryKey] PRIMARY KEY NONCLUSTERED ([emaildataid] ASC)
);

