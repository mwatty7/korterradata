﻿CREATE TABLE [dbo].[emailhistory] (
    [emailhistoryid]   UNIQUEIDENTIFIER NOT NULL,
    [emaildataid]      UNIQUEIDENTIFIER NOT NULL,
    [creationdtutc]    DATETIME2 (7)    NOT NULL,
    [status]           VARCHAR (32)     NULL,
    [failedreason]     VARCHAR (MAX)    NULL,
    [lastattemptdtutc] DATETIME2 (7)    NULL,
    CONSTRAINT [emailhistoryPrimaryKey] PRIMARY KEY NONCLUSTERED ([emailhistoryid] ASC),
    CONSTRAINT [FK_emailhistory_emaildataid] FOREIGN KEY ([emaildataid]) REFERENCES [dbo].[emaildata] ([emaildataid])
);

