﻿CREATE TABLE [dbo].[a_routingruletype] (
    [rrtypeid]    UNIQUEIDENTIFIER NOT NULL,
    [rrversionid] UNIQUEIDENTIFIER NOT NULL,
    [match]       UNIQUEIDENTIFIER NULL,
    [nomatch]     UNIQUEIDENTIFIER NULL,
    [method]      VARCHAR (255)    NULL,
    [field]       VARCHAR (255)    NULL,
    CONSTRAINT [PK_a_routingruletype] PRIMARY KEY NONCLUSTERED ([rrtypeid] ASC),
    CONSTRAINT [FK_a_routingruletype_a_routingruleversionid] FOREIGN KEY ([rrversionid]) REFERENCES [dbo].[a_routingruleversion] ([rrversionid]),
    CONSTRAINT [FK_a_routingruletype_match_a_routingruletype_rrtypeid] FOREIGN KEY ([match]) REFERENCES [dbo].[a_routingruletype] ([rrtypeid]),
    CONSTRAINT [FK_a_routingruletype_nomatch_a_routingruletype_rrtypeid] FOREIGN KEY ([nomatch]) REFERENCES [dbo].[a_routingruletype] ([rrtypeid])
);

