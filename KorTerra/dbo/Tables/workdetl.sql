﻿CREATE TABLE [dbo].[workdetl] (
    [customerid]     VARCHAR (32)    NOT NULL,
    [workdetlcode]   INT             NOT NULL,
    [creationdtdate] DATETIME        NOT NULL,
    [creationdttime] VARCHAR (12)    NOT NULL,
    [operatorid]     VARCHAR (16)    NOT NULL,
    [workdate]       DATETIME        NULL,
    [workcode]       INT             NULL,
    [workduration]   DECIMAL (15, 2) NULL,
    [regionid]       VARCHAR (16)    NULL,
    CONSTRAINT [workdetlPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [workdetlcode] ASC, [creationdtdate] ASC, [creationdttime] ASC, [operatorid] ASC)
);

