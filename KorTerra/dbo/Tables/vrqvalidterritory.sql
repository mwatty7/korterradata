﻿CREATE TABLE [dbo].[vrqvalidterritory] (
    [customerid]       VARCHAR (32) NOT NULL,
    [jobid]            VARCHAR (16) NOT NULL,
    [membercode]       VARCHAR (16) NOT NULL,
    [requesttype]      VARCHAR (9)  NOT NULL,
    [serviceterritory] VARCHAR (16) NOT NULL,
    [isdefault]        SMALLINT     NULL,
    CONSTRAINT [vrqvalidterritoryPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [serviceterritory] ASC)
);

