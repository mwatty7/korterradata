﻿CREATE TABLE [dbo].[jobgeometrydata] (
    [customerid]     VARCHAR (32)     NOT NULL,
    [jobid]          VARCHAR (16)     NOT NULL,
    [geometrydataid] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_jobgeometrydata_customerid_jobid_geometrydataid] PRIMARY KEY NONCLUSTERED ([customerid] ASC, [jobid] ASC, [geometrydataid] ASC),
    CONSTRAINT [FK_jobgeometrydata_geometrydata_geometrydataid] FOREIGN KEY ([geometrydataid]) REFERENCES [dbo].[geometrydata] ([geometrydataid]),
    CONSTRAINT [FK_jobgeometrydata_job_customerid_jobid] FOREIGN KEY ([customerid], [jobid]) REFERENCES [dbo].[job] ([customerid], [jobid])
);

