﻿CREATE TABLE [dbo].[cn_compls] (
    [printloc]             VARCHAR (3)  NOT NULL,
    [ticketnumber]         VARCHAR (16) NOT NULL,
    [versionnumber]        SMALLINT     NOT NULL,
    [conectivstatus]       CHAR (1)     NULL,
    [errormsg]             VARCHAR (80) NULL,
    [rmcontrolnumber]      VARCHAR (16) NULL,
    [operatorid]           VARCHAR (16) NULL,
    [mobileid]             VARCHAR (16) NULL,
    [completiondtdate]     DATETIME     NULL,
    [completiondttime]     VARCHAR (12) NULL,
    [actioncode1]          VARCHAR (5)  NULL,
    [actioncode2]          VARCHAR (5)  NULL,
    [actioncode3]          VARCHAR (5)  NULL,
    [actioncode4]          VARCHAR (5)  NULL,
    [facilitycode1]        VARCHAR (5)  NULL,
    [facilitycode2]        VARCHAR (5)  NULL,
    [facilitycode3]        VARCHAR (5)  NULL,
    [facilitycode4]        VARCHAR (5)  NULL,
    [facilitycode5]        VARCHAR (5)  NULL,
    [facilitycode6]        VARCHAR (5)  NULL,
    [facilitycomments]     VARCHAR (72) NULL,
    [notificationcode]     VARCHAR (7)  NULL,
    [talkedto]             VARCHAR (32) NULL,
    [notificationcomments] VARCHAR (72) NULL,
    [additionalcomments0]  VARCHAR (80) NULL,
    [additionalcomments1]  VARCHAR (80) NULL,
    [additionalcomments2]  VARCHAR (80) NULL,
    [additionalcomments3]  VARCHAR (80) NULL,
    [additionalcomments4]  VARCHAR (80) NULL,
    [additionalcomments5]  VARCHAR (80) NULL,
    [additionalcomments6]  VARCHAR (80) NULL,
    [additionalcomments7]  VARCHAR (80) NULL,
    CONSTRAINT [cn_complsPrimaryKey] PRIMARY KEY CLUSTERED ([printloc] ASC, [ticketnumber] ASC, [versionnumber] ASC)
);


GO
CREATE NONCLUSTERED INDEX [cn_compls_conectivstatus]
    ON [dbo].[cn_compls]([conectivstatus] ASC);

