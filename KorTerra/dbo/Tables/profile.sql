﻿CREATE TABLE [dbo].[profile] (
    [profileid]   VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [profilePrimaryKey] PRIMARY KEY CLUSTERED ([profileid] ASC)
);

