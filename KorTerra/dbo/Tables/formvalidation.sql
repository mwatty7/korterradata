﻿CREATE TABLE [dbo].[formvalidation] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [applicationid] VARCHAR (16)  NOT NULL,
    [windowname]    VARCHAR (32)  NOT NULL,
    [controlname]   VARCHAR (64)  NOT NULL,
    [fieldtype]     VARCHAR (128) NULL,
    [regexvalue]    VARCHAR (255) NULL,
    [isrequired]    SMALLINT      NULL,
    [custminlength] INT           NULL,
    [custmaxlength] INT           NULL,
    [sysmaxlength]  INT           NULL,
    [errormsg]      VARCHAR (255) NULL,
    [isprimarykey]  SMALLINT      NULL,
    [seqno]         INT           NULL,
    CONSTRAINT [formvalidationPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [applicationid] ASC, [windowname] ASC, [controlname] ASC)
);

