﻿CREATE TABLE [dbo].[PAaudittrail] (
    [audittrailid] INT           IDENTITY (1, 1) NOT NULL,
    [contactid]    INT           NOT NULL,
    [action]       VARCHAR (MAX) NOT NULL,
    [actiondt]     DATETIME2 (7) NOT NULL,
    [operatorid]   VARCHAR (32)  NULL,
    CONSTRAINT [PK_PAaudittrail] PRIMARY KEY CLUSTERED ([audittrailid] ASC),
    CONSTRAINT [FK_PAaudittrailContact] FOREIGN KEY ([contactid]) REFERENCES [dbo].[PAcontact] ([contactid])
);


GO
CREATE NONCLUSTERED INDEX [PAaudittrailDatetimeIndex]
    ON [dbo].[PAaudittrail]([contactid] ASC, [actiondt] ASC);


GO
CREATE NONCLUSTERED INDEX [PAaudittrailOperatorIndex]
    ON [dbo].[PAaudittrail]([contactid] ASC, [operatorid] ASC);

