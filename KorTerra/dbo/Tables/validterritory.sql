﻿CREATE TABLE [dbo].[validterritory] (
    [customerid]       VARCHAR (32) NOT NULL,
    [membercode]       VARCHAR (16) NOT NULL,
    [requesttype]      VARCHAR (9)  NOT NULL,
    [serviceterritory] VARCHAR (16) NOT NULL,
    [isdefault]        SMALLINT     NULL,
    CONSTRAINT [validterritoryPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [requesttype] ASC, [serviceterritory] ASC)
);

