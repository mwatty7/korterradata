﻿CREATE TABLE [dbo].[g5visit] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    [versionauthor]    VARCHAR (16)    NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      CHAR (1)        NULL,
    [tmunits]          DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [completedby]      VARCHAR (10)    NULL,
    [completionlevel]  CHAR (1)        NULL,
    [departmentid]     VARCHAR (16)    NULL,
    [platno]           VARCHAR (13)    NULL,
    [filmroll]         VARCHAR (13)    NULL,
    [serviceterritory] VARCHAR (16)    NULL,
    [ismain]           SMALLINT        NULL,
    [isservices]       SMALLINT        NULL,
    [qtymain]          SMALLINT        NULL,
    [qtyservices]      SMALLINT        NULL,
    [iscastiron]       SMALLINT        NULL,
    [isplastic]        SMALLINT        NULL,
    [iscopper]         SMALLINT        NULL,
    [issteel]          SMALLINT        NULL,
    [ispaint]          SMALLINT        NULL,
    [isflag]           SMALLINT        NULL,
    [isstake]          SMALLINT        NULL,
    [isstakechaser]    SMALLINT        NULL,
    [isoffset]         SMALLINT        NULL,
    [qtyoffset]        DECIMAL (15, 1) NULL,
    [isclearexcavate]  SMALLINT        NULL,
    [isnounderground]  SMALLINT        NULL,
    [isnotcompleted]   SMALLINT        NULL,
    [unmarkedreason]   VARCHAR (32)    NULL,
    [markedreason]     VARCHAR (32)    NULL,
    [ishighprofile]    SMALLINT        NULL,
    [isnotsendprs]     SMALLINT        NULL,
    CONSTRAINT [g5visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [g5visitCompletionIndex]
    ON [dbo].[g5visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [g5visitDeptIndex]
    ON [dbo].[g5visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [g5visitMembercodeIndex]
    ON [dbo].[g5visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

