﻿CREATE TABLE [dbo].[opertype] (
    [customerid]   VARCHAR (32) NOT NULL,
    [operatortype] VARCHAR (16) NOT NULL,
    [description]  VARCHAR (32) NULL,
    [inclpayroll]  SMALLINT     NULL,
    CONSTRAINT [opertypePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatortype] ASC)
);

