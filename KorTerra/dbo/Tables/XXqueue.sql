﻿CREATE TABLE [dbo].[XXqueue] (
    [queueid]       UNIQUEIDENTIFIER NOT NULL,
    [customerid]    VARCHAR (32)     NOT NULL,
    [context]       VARCHAR (MAX)    NOT NULL,
    [type]          VARCHAR (32)     NOT NULL,
    [creationdtutc] DATETIME2 (7)    NOT NULL,
    [workdtutc]     DATETIME2 (7)    NOT NULL,
    [status]        VARCHAR (32)     NOT NULL,
    CONSTRAINT [PK_XXqueue_queueid] PRIMARY KEY NONCLUSTERED ([queueid] ASC)
);

