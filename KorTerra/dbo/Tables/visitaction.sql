﻿CREATE TABLE [dbo].[visitaction] (
    [visitactionid]  UNIQUEIDENTIFIER NOT NULL,
    [customerid]     VARCHAR (80)     NOT NULL,
    [actiontype]     VARCHAR (64)     NULL,
    [actiontemplate] VARCHAR (MAX)    NULL,
    [templateid]     INT              NULL,
    [description]    VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_visitaction] PRIMARY KEY CLUSTERED ([visitactionid] ASC),
    CONSTRAINT [FK_visitaction_templateid_xxtemplate_template_id] FOREIGN KEY ([templateid]) REFERENCES [dbo].[XXtemplate] ([template_id])
);

