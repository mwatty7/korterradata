﻿CREATE TABLE [dbo].[childdept] (
    [customerid]    VARCHAR (32) NOT NULL,
    [departmentid]  VARCHAR (16) NOT NULL,
    [description]   VARCHAR (32) NULL,
    [masterdeptid]  VARCHAR (16) NULL,
    [cutoffdate]    DATETIME     NULL,
    [priceid1]      VARCHAR (16) NULL,
    [priceid2]      VARCHAR (16) NULL,
    [redothreshold] SMALLINT     NULL,
    [taxrate]       FLOAT (53)   NULL,
    [bonusunits]    FLOAT (53)   NULL,
    CONSTRAINT [childdeptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [departmentid] ASC)
);

