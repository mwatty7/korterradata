﻿CREATE TABLE [dbo].[reportschedule] (
    [reportscheduleid] UNIQUEIDENTIFIER CONSTRAINT [DF_reportschedule_reportscheduleid] DEFAULT (newsequentialid()) NOT NULL,
    [customerid]       VARCHAR (32)     NOT NULL,
    [type]             VARCHAR (32)     NOT NULL,
    [context]          VARCHAR (MAX)    NOT NULL,
    [isactive]         BIT              NOT NULL,
    CONSTRAINT [PK_reportschedule] PRIMARY KEY CLUSTERED ([reportscheduleid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_reportschedule_customerid_type_isactive_include]
    ON [dbo].[reportschedule]([isactive] ASC, [customerid] ASC, [type] ASC)
    INCLUDE([reportscheduleid], [context]);

