﻿CREATE TABLE [dbo].[expdetl] (
    [customerid]     VARCHAR (32)    NOT NULL,
    [expdetlcode]    INT             NOT NULL,
    [creationdtdate] DATETIME        NOT NULL,
    [creationdttime] VARCHAR (12)    NOT NULL,
    [operatorid]     VARCHAR (16)    NOT NULL,
    [workdate]       DATETIME        NULL,
    [expcode]        INT             NULL,
    [expcost]        DECIMAL (15, 2) NULL,
    [regionid]       VARCHAR (16)    NULL,
    CONSTRAINT [expdetlPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [expdetlcode] ASC, [creationdtdate] ASC, [creationdttime] ASC, [operatorid] ASC)
);

