﻿CREATE TABLE [dbo].[AAwidget] (
    [widgetid]            VARCHAR (64)  NOT NULL,
    [title]               VARCHAR (64)  NULL,
    [description]         VARCHAR (256) NULL,
    [chartDefinitionJSON] VARCHAR (MAX) NULL,
    [chartInitJs]         VARCHAR (MAX) NULL,
    [bootstrapWidth]      INT           NULL,
    [datasource]          VARCHAR (MAX) NULL,
    [settingsHtml]        VARCHAR (MAX) NULL,
    [settingsJs]          VARCHAR (MAX) NULL,
    [chartClickJs]        VARCHAR (MAX) NULL,
    [widgetHtml]          VARCHAR (MAX) NULL,
    [widgetJs]            VARCHAR (MAX) NULL,
    [allowmultiple]       BIT           NULL,
    [fakedata]            VARCHAR (MAX) NULL,
    CONSTRAINT [PK_AAwidget] PRIMARY KEY CLUSTERED ([widgetid] ASC)
);

