﻿CREATE TABLE [dbo].[t_routingrulevalue] (
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [rrtypeid]  UNIQUEIDENTIFIER NOT NULL,
    [value]     VARCHAR (MAX)    NULL,
    [zoneid]    VARCHAR (16)     NULL,
    [minx]      FLOAT (53)       NULL,
    [maxx]      FLOAT (53)       NULL,
    [miny]      FLOAT (53)       NULL,
    [maxy]      FLOAT (53)       NULL,
    [starttime] TIME (7)         NULL,
    [endtime]   TIME (7)         NULL,
    CONSTRAINT [PK_t_routingrulevalue] PRIMARY KEY NONCLUSTERED ([rrvalueid] ASC),
    CONSTRAINT [FK_t_routingrulevalue_rrtypeid_t_routingruletype_rrtypeid] FOREIGN KEY ([rrtypeid]) REFERENCES [dbo].[t_routingruletype] ([rrtypeid])
);

