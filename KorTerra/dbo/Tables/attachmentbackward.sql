﻿CREATE TABLE [dbo].[attachmentbackward] (
    [id]            INT           IDENTITY (1, 1) NOT NULL,
    [customerid]    VARCHAR (32)  NOT NULL,
    [jobid]         VARCHAR (16)  NOT NULL,
    [name]          VARCHAR (256) NOT NULL,
    [triggerdtdate] DATETIME      NULL,
    [triggerdttime] VARCHAR (12)  NULL,
    [action]        VARCHAR (16)  NULL,
    [direction]     VARCHAR (16)  NULL,
    CONSTRAINT [PK_attachmentbackward] PRIMARY KEY CLUSTERED ([id] ASC)
);

