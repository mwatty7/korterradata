﻿CREATE TABLE [dbo].[messagecustomer] (
    [customerid] VARCHAR (32) NOT NULL,
    [messageid]  INT          NOT NULL,
    CONSTRAINT [PK_messagecustomer] PRIMARY KEY CLUSTERED ([customerid] ASC, [messageid] ASC),
    CONSTRAINT [FK_MessageDetailMessageCustomer] FOREIGN KEY ([messageid]) REFERENCES [dbo].[messagedetail] ([messageid])
);

