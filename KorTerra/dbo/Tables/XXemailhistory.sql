﻿CREATE TABLE [dbo].[XXemailhistory] (
    [emailhistoryid] UNIQUEIDENTIFIER NOT NULL,
    [queueid]        UNIQUEIDENTIFIER NULL,
    [customerid]     VARCHAR (32)     NOT NULL,
    [status]         VARCHAR (32)     NOT NULL,
    [reason]         VARCHAR (MAX)    NULL,
    [attemptdtutc]   DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_emailhistory_emailhistoryid] PRIMARY KEY NONCLUSTERED ([emailhistoryid] ASC),
    CONSTRAINT [FK_XXemailhistory_queueid] FOREIGN KEY ([queueid]) REFERENCES [dbo].[XXemaildata] ([queueid])
);


GO
CREATE NONCLUSTERED INDEX [IX_XXemailhistory_customerid_queueid_include]
    ON [dbo].[XXemailhistory]([customerid] ASC, [queueid] ASC)
    INCLUDE([status], [attemptdtutc]);

