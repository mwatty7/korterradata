﻿CREATE TABLE [dbo].[ticketsort] (
    [customerid]        VARCHAR (32)  NOT NULL,
    [jobid]             VARCHAR (16)  NOT NULL,
    [mobileid]          VARCHAR (16)  NOT NULL,
    [sortorder]         INT           NOT NULL,
    [lastmodifiedby]    VARCHAR (80)  NULL,
    [lastmodifieddtutc] DATETIME2 (7) NULL,
    CONSTRAINT [ticketSortPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [mobileid] ASC),
    CONSTRAINT [IX_cust_job_mobile_sort] UNIQUE NONCLUSTERED ([customerid] ASC, [mobileid] ASC, [sortorder] ASC)
);

