﻿CREATE TABLE [dbo].[CustomerScheduleTemplates] (
    [cst_id]     INT          IDENTITY (1, 1) NOT NULL,
    [customerid] VARCHAR (32) NOT NULL,
    [name]       VARCHAR (64) NOT NULL,
    CONSTRAINT [PK_CustomerScheduleTemplates] PRIMARY KEY CLUSTERED ([cst_id] ASC),
    CONSTRAINT [FK_CustomerScheduleTemplates] FOREIGN KEY ([customerid]) REFERENCES [dbo].[customer] ([customerid])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [CustomerScheduleTemplatesIndex]
    ON [dbo].[CustomerScheduleTemplates]([customerid] ASC, [name] ASC);

