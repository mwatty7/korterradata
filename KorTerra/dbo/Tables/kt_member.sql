﻿CREATE TABLE [dbo].[kt_member] (
    [memberGuid]  UNIQUEIDENTIFIER NOT NULL,
    [name]        VARCHAR (32)     NOT NULL,
    [description] VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_kt_member] PRIMARY KEY CLUSTERED ([memberGuid] ASC)
);

