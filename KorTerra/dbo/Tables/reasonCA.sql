﻿CREATE TABLE [dbo].[reasonCA] (
    [reasonlistid] VARCHAR (32) NULL,
    [reasonid]     VARCHAR (32) NULL,
    [description]  VARCHAR (80) NULL,
    [seqno]        SMALLINT     NULL,
    [reasongroup]  CHAR (1)     NULL,
    [prsresponse]  VARCHAR (80) NULL
);

