﻿CREATE TABLE [dbo].[workcompinjury] (
    [injuryid]    VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [workcompinjuryPrimaryKey] PRIMARY KEY CLUSTERED ([injuryid] ASC)
);

