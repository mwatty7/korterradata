﻿CREATE TABLE [dbo].[vmsection] (
    [customerid]         VARCHAR (32)  NOT NULL,
    [requesttype]        VARCHAR (9)   NOT NULL,
    [sectionid]          VARCHAR (32)  NOT NULL,
    [sectiondescription] VARCHAR (100) NULL,
    [sequence]           SMALLINT      NULL,
    [html]               VARCHAR (MAX) NULL,
    CONSTRAINT [PK_vmsection] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC, [sectionid] ASC)
);

