﻿CREATE TABLE [dbo].[rdsecurity] (
    [customerid]  VARCHAR (32) NOT NULL,
    [groupid]     VARCHAR (16) NOT NULL,
    [regionid]    VARCHAR (16) NOT NULL,
    [districtid]  VARCHAR (16) NOT NULL,
    [accessstate] INT          NULL,
    CONSTRAINT [rdsecurityPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [groupid] ASC, [regionid] ASC, [districtid] ASC)
);

