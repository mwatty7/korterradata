﻿CREATE TABLE [dbo].[kt_visitreq] (
    [visitreqGuid] UNIQUEIDENTIFIER NOT NULL,
    [requestGuid]  UNIQUEIDENTIFIER NOT NULL,
    [templateGuid] UNIQUEIDENTIFIER NOT NULL,
    [facilitytype] VARCHAR (32)     NOT NULL,
    [mobileGuid]   UNIQUEIDENTIFIER NULL,
    [operatorGuid] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_kt_visitreq] PRIMARY KEY CLUSTERED ([visitreqGuid] ASC),
    CONSTRAINT [FK_kt_visitreq_requestGuid_kt_request_requestGuid] FOREIGN KEY ([requestGuid]) REFERENCES [dbo].[kt_request] ([requestGuid])
);

