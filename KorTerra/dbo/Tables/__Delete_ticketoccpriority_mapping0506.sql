﻿CREATE TABLE [dbo].[__Delete_ticketoccpriority_mapping0506] (
    [occid]                       VARCHAR (8)      NULL,
    [kt_priority]                 VARCHAR (16)     NULL,
    [origpriority]                VARCHAR (25)     NULL,
    [fullsize_origpriority]       VARCHAR (80)     NULL,
    [ticketpriorityid]            UNIQUEIDENTIFIER NULL,
    [was_origpriority_lengthened] VARCHAR (1)      NULL,
    [isduplicate]                 VARCHAR (1)      NULL,
    [needsreview]                 VARCHAR (1)      NULL,
    [lrn]                         INT              IDENTITY (1, 1) NOT NULL
);

