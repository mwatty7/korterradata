﻿CREATE TABLE [dbo].[j_sortedjob] (
    [customerid]           VARCHAR (32)  NOT NULL,
    [jobid]                VARCHAR (16)  NOT NULL,
    [versiondtdate]        DATETIME      NULL,
    [versiondttime]        VARCHAR (12)  NULL,
    [versionauthor]        VARCHAR (16)  NULL,
    [visitreqtot]          SMALLINT      NULL,
    [sendstatus]           CHAR (1)      NULL,
    [senddtdate]           DATETIME      NULL,
    [senddttime]           VARCHAR (12)  NULL,
    [printdtdate]          DATETIME      NULL,
    [printdttime]          VARCHAR (12)  NULL,
    [faxdtdate]            DATETIME      NULL,
    [faxdttime]            VARCHAR (12)  NULL,
    [sendfailreason]       CHAR (1)      NULL,
    [sendackdtdate]        DATETIME      NULL,
    [sendackdttime]        VARCHAR (12)  NULL,
    [batchdtdate]          DATETIME      NULL,
    [batchdttime]          VARCHAR (12)  NULL,
    [assigneddtdate]       DATETIME      NULL,
    [assigneddttime]       VARCHAR (12)  NULL,
    [latestseqno]          SMALLINT      NULL,
    [prevjobid]            VARCHAR (16)  NULL,
    [nextjobid]            VARCHAR (16)  NULL,
    [origpriority]         VARCHAR (80)  NOT NULL,
    [priority]             VARCHAR (16)  NULL,
    [latestoccstatus]      CHAR (1)      NULL,
    [mobileid]             VARCHAR (16)  NULL,
    [company]              VARCHAR (32)  NULL,
    [sendto]               VARCHAR (16)  NULL,
    [address]              VARCHAR (8)   NULL,
    [street]               VARCHAR (32)  NULL,
    [city]                 VARCHAR (64)  NULL,
    [zipcode]              VARCHAR (11)  NULL,
    [origmeetdtdate]       DATETIME      NULL,
    [origmeetdttime]       VARCHAR (12)  NULL,
    [meetdtdate]           DATETIME      NULL,
    [meetdttime]           VARCHAR (12)  NULL,
    [status]               CHAR (1)      NULL,
    [workstatus]           CHAR (1)      NULL,
    [mapnumber]            VARCHAR (16)  NULL,
    [mappage]              VARCHAR (16)  NULL,
    [mapgrid]              VARCHAR (16)  NULL,
    [county]               VARCHAR (64)  NULL,
    [state]                VARCHAR (3)   NULL,
    [nearinter]            VARCHAR (32)  NULL,
    [latestxmitsource]     VARCHAR (8)   NULL,
    [latestxmitdtdate]     DATETIME      NULL,
    [latestxmitdttime]     VARCHAR (12)  NULL,
    [origdtdate]           DATETIME      NULL,
    [origdttime]           VARCHAR (12)  NULL,
    [wtbdtdate]            DATETIME      NULL,
    [wtbdttime]            VARCHAR (12)  NULL,
    [utilsenddtdate]       DATETIME      NULL,
    [utilsenddttime]       VARCHAR (12)  NULL,
    [contractorrecvdtdate] DATETIME      NULL,
    [contractorrecvdttime] VARCHAR (12)  NULL,
    [contact]              VARCHAR (32)  NULL,
    [phone]                VARCHAR (21)  NULL,
    [altcontact]           VARCHAR (32)  NULL,
    [altphone]             VARCHAR (21)  NULL,
    [worktype]             VARCHAR (62)  NULL,
    [donefor]              VARCHAR (56)  NULL,
    [isexplosive]          SMALLINT      NULL,
    [duration]             VARCHAR (16)  NULL,
    [rightofway]           SMALLINT      NULL,
    [legalgiven]           SMALLINT      NULL,
    [addmembers]           VARCHAR (180) NULL,
    [lattitude]            VARCHAR (12)  NULL,
    [longitude]            VARCHAR (12)  NULL,
    [isproject]            SMALLINT      NULL,
    [projdurqty]           SMALLINT      NULL,
    [projdurtype]          SMALLINT      NULL,
    [projectdtdate]        DATETIME      NULL,
    [projectdttime]        VARCHAR (12)  NULL,
    [township1]            VARCHAR (8)   NULL,
    [range1]               VARCHAR (8)   NULL,
    [section1]             VARCHAR (30)  NULL,
    [township2]            VARCHAR (8)   NULL,
    [range2]               VARCHAR (8)   NULL,
    [section2]             VARCHAR (30)  NULL,
    [gisstatus]            CHAR (1)      NULL,
    [gismapid]             VARCHAR (16)  NULL,
    [zoneid]               VARCHAR (16)  NULL,
    [calladdr1]            VARCHAR (32)  NULL,
    [calladdr2]            VARCHAR (32)  NULL,
    [callcity]             VARCHAR (64)  NULL,
    [callstate]            VARCHAR (3)   NULL,
    [callzip]              VARCHAR (11)  NULL,
    [callfax]              VARCHAR (21)  NULL,
    [callpager]            VARCHAR (21)  NULL,
    [callmobile]           VARCHAR (21)  NULL,
    [callemail]            VARCHAR (80)  NULL,
    [callbesttime]         VARCHAR (80)  NULL,
    [companyphone]         VARCHAR (21)  NULL,
    [ticketflag]           VARCHAR (255) NULL,
    [address2]             VARCHAR (8)   NULL,
    [subdivision]          VARCHAR (32)  NULL,
    [revisionnumber]       VARCHAR (8)   NULL,
    [iswhitepaint]         SMALLINT      NULL,
    [isboring]             SMALLINT      NULL,
    [isoverhead]           SMALLINT      NULL,
    [isunderground]        SMALLINT      NULL,
    [isoffset]             SMALLINT      NULL,
    [permit]               VARCHAR (16)  NULL,
    [depth]                VARCHAR (16)  NULL,
    [nearinter2]           VARCHAR (32)  NULL,
    [iscrewonsite]         SMALLINT      NULL,
    [israilroad]           SMALLINT      NULL,
    [istrenching]          SMALLINT      NULL,
    [propertytype]         VARCHAR (32)  NULL,
    [maplink]              VARCHAR (255) NULL,
    [excavationequipment]  VARCHAR (32)  NULL,
    [frontrear]            CHAR (1)      NULL,
    [iseasement]           SMALLINT      NULL,
    [wostatus]             CHAR (1)      NULL,
    [source]               VARCHAR (32)  NULL,
    [submitdtdate]         DATETIME      NULL,
    [submitdttime]         VARCHAR (12)  NULL,
    [expirationdtdate]     DATETIME      NULL,
    [expirationdttime]     VARCHAR (12)  NULL,
    [eticketjobid]         VARCHAR (16)  NULL,
    [applicationid]        VARCHAR (16)  NULL,
    [windowname]           VARCHAR (32)  NULL,
    [operatorid]           VARCHAR (16)  NULL,
    [ticketgroup]          SMALLINT      NULL,
    [ticketorder]          INT           NULL,
    CONSTRAINT [j_sortedjobPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobMeetIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [meetdtdate] ASC, [meetdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobmobileidIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [mobileid] ASC, [status] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjoborigdtIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [origdtdate] ASC, [origdttime] ASC, [zoneid] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobProjectIndex]
    ON [dbo].[j_sortedjob]([projectdtdate] ASC, [projectdttime] ASC, [isproject] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobsendstatusIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [sendstatus] ASC, [priority] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobstatusIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [status] ASC, [priority] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobWtbIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [wtbdtdate] ASC, [wtbdttime] ASC, [status] ASC);


GO
CREATE NONCLUSTERED INDEX [j_sortedjobXmitIndex]
    ON [dbo].[j_sortedjob]([customerid] ASC, [latestxmitdtdate] ASC, [latestxmitdttime] ASC);

