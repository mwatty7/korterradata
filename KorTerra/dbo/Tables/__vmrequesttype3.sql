﻿CREATE TABLE [dbo].[__vmrequesttype3] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [requesttype] VARCHAR (9)   NOT NULL,
    [description] VARCHAR (255) NULL,
    [html]        VARCHAR (MAX) NULL
);

