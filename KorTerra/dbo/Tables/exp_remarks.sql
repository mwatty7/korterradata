﻿CREATE TABLE [dbo].[exp_remarks] (
    [k_customerid] VARCHAR (16) NOT NULL,
    [k_jobid]      VARCHAR (16) NOT NULL,
    [k_type]       VARCHAR (16) NOT NULL,
    [k_seqno]      SMALLINT     NOT NULL,
    [remarks]      VARCHAR (80) NULL,
    CONSTRAINT [exp_remarksPrimaryKey] PRIMARY KEY CLUSTERED ([k_customerid] ASC, [k_jobid] ASC, [k_type] ASC, [k_seqno] ASC)
);

