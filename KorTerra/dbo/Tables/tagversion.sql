﻿CREATE TABLE [dbo].[tagversion] (
    [tagversionid]  UNIQUEIDENTIFIER NOT NULL,
    [tagid]         UNIQUEIDENTIFIER NOT NULL,
    [customerid]    VARCHAR (32)     NOT NULL,
    [name]          VARCHAR (80)     NOT NULL,
    [description]   VARCHAR (255)    NULL,
    [isimportant]   BIT              NOT NULL,
    [creationdtutc] DATETIME2 (7)    NOT NULL,
    [createdby]     VARCHAR (80)     NOT NULL,
    [status]        VARCHAR (32)     NOT NULL,
    CONSTRAINT [PK_tagversion_tagversionid] PRIMARY KEY NONCLUSTERED ([tagversionid] ASC),
    FOREIGN KEY ([tagid]) REFERENCES [dbo].[tag] ([tagid])
);


GO
CREATE NONCLUSTERED INDEX [IX_tagid_status_name_isactive_desc]
    ON [dbo].[tagversion]([tagid] ASC, [status] ASC)
    INCLUDE([name], [description]);

