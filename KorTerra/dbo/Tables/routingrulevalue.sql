﻿CREATE TABLE [dbo].[routingrulevalue] (
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [rrtypeid]  UNIQUEIDENTIFIER NOT NULL,
    [value]     VARCHAR (MAX)    NULL,
    [zoneid]    VARCHAR (16)     NULL,
    [minx]      FLOAT (53)       NULL,
    [maxx]      FLOAT (53)       NULL,
    [miny]      FLOAT (53)       NULL,
    [maxy]      FLOAT (53)       NULL,
    [starttime] TIME (7)         NULL,
    [endtime]   TIME (7)         NULL,
    [startdate] DATETIME2 (7)    NULL,
    [enddate]   DATETIME2 (7)    NULL,
    CONSTRAINT [PK_routingrulevalue] PRIMARY KEY NONCLUSTERED ([rrvalueid] ASC),
    CONSTRAINT [FK_routingrulevalue_rrtypeid_routingruletype_rrtypeid] FOREIGN KEY ([rrtypeid]) REFERENCES [dbo].[routingruletype] ([rrtypeid])
);


GO
CREATE NONCLUSTERED INDEX [IX_routingruletype_rrversionid]
    ON [dbo].[routingrulevalue]([rrtypeid] ASC, [minx] ASC, [maxx] ASC, [miny] ASC, [maxy] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_routingrulevalue_datetime]
    ON [dbo].[routingrulevalue]([rrtypeid] ASC, [startdate] ASC, [enddate] ASC, [starttime] ASC, [endtime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_routingrulevalue_rrtypeid]
    ON [dbo].[routingrulevalue]([rrtypeid] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_routingrulevalue_zoneid]
    ON [dbo].[routingrulevalue]([rrtypeid] ASC, [zoneid] ASC);

