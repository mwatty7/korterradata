﻿CREATE TABLE [dbo].[rptsched_print] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [printerid]   VARCHAR (255) NOT NULL,
    [description] VARCHAR (255) NULL,
    CONSTRAINT [rptsched_printPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [printerid] ASC)
);

