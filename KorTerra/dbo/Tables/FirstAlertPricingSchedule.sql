﻿CREATE TABLE [dbo].[FirstAlertPricingSchedule] (
    [schedule_id]           INT             IDENTITY (1, 1) NOT NULL,
    [customerid]            VARCHAR (32)    NOT NULL,
    [schedulename]          VARCHAR (80)    NOT NULL,
    [description]           VARCHAR (255)   NULL,
    [locateprice]           DECIMAL (15, 2) NULL,
    [locateotprice]         DECIMAL (15, 2) NULL,
    [onecallprice]          DECIMAL (15, 2) NULL,
    [onecallotprice]        DECIMAL (15, 2) NULL,
    [gdprice]               DECIMAL (15, 2) NULL,
    [gdotprice]             DECIMAL (15, 2) NULL,
    [reportresearchprice]   DECIMAL (15, 2) NULL,
    [onecallplacementprice] DECIMAL (15, 2) NULL,
    [kilometersabprice]     DECIMAL (15, 2) NULL,
    [kilometersotherprice]  DECIMAL (15, 2) NULL,
    [suppliesprice]         DECIMAL (15, 2) NULL,
    [subsistenceprice]      DECIMAL (15, 2) NULL,
    [drugtestingprice]      DECIMAL (15, 2) NULL,
    [lodgingpercent]        DECIMAL (15, 2) NULL,
    [taxpercent]            DECIMAL (15, 2) NULL,
    [isactive]              BIT             NOT NULL,
    CONSTRAINT [PK_FirstAlertPricingSchedule] PRIMARY KEY CLUSTERED ([schedule_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Drug / Alcohol Test Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'drugtestingprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ground Disturbance Overtime Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'gdotprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ground Disturbance Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'gdprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'If schedule is active', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'isactive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service Truck Kilometers AB Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'kilometersabprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Service Truck Kilometers OTHER Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'kilometersotherprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Locator Overtime Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'locateotprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Locator Standard Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'locateprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lodging Percent Charged', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'lodgingpercent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'One Call Response Overtime Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'onecallotprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'One Call Placement Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'onecallplacementprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'One Call Response Hours Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'onecallprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Report and Research Package Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'reportresearchprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subsistence Man Nights Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'subsistenceprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Supply Bundles Price', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'suppliesprice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Tax Percent', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule', @level2type = N'COLUMN', @level2name = N'taxpercent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'CUSTOM DEV: A set of pricing schedules for First Alert Locating', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FirstAlertPricingSchedule';

