﻿CREATE TABLE [dbo].[messagedetail] (
    [messageid]        INT           IDENTITY (1, 1) NOT NULL,
    [internalname]     VARCHAR (255) NOT NULL,
    [displayname]      VARCHAR (255) NULL,
    [content]          VARCHAR (MAX) NULL,
    [link]             VARCHAR (MAX) NULL,
    [activefromdt]     DATETIME2 (7) NULL,
    [activethroughdt]  DATETIME2 (7) NULL,
    [reminderdelay]    INT           NULL,
    [messagetype]      VARCHAR (32)  NULL,
    [customerlisttype] VARCHAR (1)   NULL,
    [creationdt]       DATETIME2 (7) NULL,
    [isactive]         BIT           NULL,
    CONSTRAINT [PK_messagedetail] PRIMARY KEY CLUSTERED ([messageid] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'All Customers / Black List / White List', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'messagedetail', @level2type = N'COLUMN', @level2name = N'customerlisttype';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name displayed to the customer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'messagedetail', @level2type = N'COLUMN', @level2name = N'displayname';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Message type survey, motd, etc.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'messagedetail', @level2type = N'COLUMN', @level2name = N'messagetype';

