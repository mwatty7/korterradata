﻿CREATE TABLE [dbo].[mobmsg] (
    [stampdtdate] DATETIME     NOT NULL,
    [stampdttime] VARCHAR (12) NOT NULL,
    [jobid]       VARCHAR (16) NOT NULL,
    [msgtype]     CHAR (1)     NULL,
    [delayedack]  SMALLINT     NULL,
    [remarks0]    VARCHAR (80) NULL,
    [remarks1]    VARCHAR (80) NULL,
    [remarks2]    VARCHAR (80) NULL,
    [remarks3]    VARCHAR (80) NULL,
    [remarks4]    VARCHAR (80) NULL,
    [remarks5]    VARCHAR (80) NULL,
    [remarks6]    VARCHAR (80) NULL,
    [remarks7]    VARCHAR (80) NULL,
    [remarks8]    VARCHAR (80) NULL,
    [remarks9]    VARCHAR (80) NULL,
    CONSTRAINT [mobmsgPrimaryKey] PRIMARY KEY CLUSTERED ([stampdtdate] ASC, [stampdttime] ASC, [jobid] ASC)
);

