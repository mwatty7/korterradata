﻿CREATE TABLE [dbo].[areatypes] (
    [customerid]  VARCHAR (32) NOT NULL,
    [occid]       VARCHAR (8)  NOT NULL,
    [method]      VARCHAR (16) NOT NULL,
    [priority]    SMALLINT     NULL,
    [field1]      VARCHAR (25) NULL,
    [field2]      VARCHAR (25) NULL,
    [field3]      VARCHAR (25) NULL,
    [isscreening] SMALLINT     NOT NULL,
    CONSTRAINT [areatypesPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [method] ASC, [isscreening] ASC)
);

