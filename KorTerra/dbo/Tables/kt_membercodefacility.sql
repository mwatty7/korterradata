﻿CREATE TABLE [dbo].[kt_membercodefacility] (
    [membercodeGuid] UNIQUEIDENTIFIER NOT NULL,
    [facilityGuid]   UNIQUEIDENTIFIER NOT NULL,
    [templateGuid]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_kt_membercodefacility] PRIMARY KEY CLUSTERED ([membercodeGuid] ASC, [facilityGuid] ASC)
);

