﻿CREATE TABLE [dbo].[ScreeningRuleset] (
    [rulesetid]   INT           IDENTITY (1, 1) NOT NULL,
    [customerid]  VARCHAR (32)  NOT NULL,
    [name]        VARCHAR (64)  NOT NULL,
    [version]     INT           NOT NULL,
    [status]      VARCHAR (16)  NOT NULL,
    [description] VARCHAR (256) NULL,
    CONSTRAINT [PK_ScreeningRuleset] PRIMARY KEY CLUSTERED ([rulesetid] ASC),
    CONSTRAINT [UC_ScreeningRuleset_customerid_name_version] UNIQUE NONCLUSTERED ([customerid] ASC, [name] ASC, [version] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Status is TESTING|DRAFT|ACTIVE|INACTIVE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuleset', @level2type = N'COLUMN', @level2name = N'status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'A set of rules for ticket screening', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuleset';

