﻿CREATE TABLE [dbo].[dmg_code] (
    [damagecodeid]   INT          NOT NULL,
    [damagecode]     VARCHAR (8)  NULL,
    [damagecodedesc] VARCHAR (32) NULL,
    CONSTRAINT [dmg_codePrimaryKey] PRIMARY KEY CLUSTERED ([damagecodeid] ASC)
);

