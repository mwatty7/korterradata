﻿CREATE TABLE [dbo].[actionaudit] (
    [customerid]    VARCHAR (32) NOT NULL,
    [jobid]         VARCHAR (16) NOT NULL,
    [seqno]         INT          NOT NULL,
    [applicationid] VARCHAR (21) NULL,
    [operatorid]    VARCHAR (16) NULL,
    [action]        VARCHAR (48) NULL,
    [actionfrom]    VARCHAR (32) NULL,
    [actionto]      VARCHAR (16) NULL,
    [actiondtdate]  DATETIME     NULL,
    [actiondttime]  VARCHAR (12) NULL,
    CONSTRAINT [actionauditPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [seqno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [actionauditOperatorIndex]
    ON [dbo].[actionaudit]([customerid] ASC, [operatorid] ASC);

