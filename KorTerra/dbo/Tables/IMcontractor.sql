﻿CREATE TABLE [dbo].[IMcontractor] (
    [contractorid]   INT            IDENTITY (1, 1) NOT NULL,
    [customerid]     NVARCHAR (32)  NOT NULL,
    [createddtutc]   DATETIME       NOT NULL,
    [company]        NVARCHAR (64)  NULL,
    [email]          NVARCHAR (128) NULL,
    [phone]          NVARCHAR (32)  NULL,
    [altphone]       NVARCHAR (32)  NULL,
    [besttimetocall] NVARCHAR (80)  NULL,
    [streetnumber]   NVARCHAR (16)  NULL,
    [address1]       NVARCHAR (128) NULL,
    [address2]       NVARCHAR (128) NULL,
    [city]           NVARCHAR (64)  NULL,
    [county]         NVARCHAR (64)  NULL,
    [state]          NVARCHAR (16)  NULL,
    [zip]            NVARCHAR (16)  NULL,
    [country]        NVARCHAR (64)  NULL,
    [contact]        NVARCHAR (64)  NULL,
    [altcontact]     NVARCHAR (64)  NULL,
    CONSTRAINT [PK_contractor] PRIMARY KEY CLUSTERED ([contractorid] ASC, [customerid] ASC)
);

