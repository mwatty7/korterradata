﻿CREATE TABLE [dbo].[a_routingrulepoints] (
    [rrpointid] UNIQUEIDENTIFIER NOT NULL,
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [seqno]     INT              NOT NULL,
    [x]         FLOAT (53)       NULL,
    [y]         FLOAT (53)       NULL,
    CONSTRAINT [PK_a_routingrulepoint] PRIMARY KEY NONCLUSTERED ([rrpointid] ASC),
    CONSTRAINT [FK_a_routingrulevalue_rrvalueid] FOREIGN KEY ([rrvalueid]) REFERENCES [dbo].[a_routingrulevalue] ([rrvalueid])
);

