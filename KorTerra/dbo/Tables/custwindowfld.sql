﻿CREATE TABLE [dbo].[custwindowfld] (
    [customerid]    VARCHAR (32) NOT NULL,
    [windowname]    VARCHAR (32) NOT NULL,
    [operatorid]    VARCHAR (16) NOT NULL,
    [columnname]    VARCHAR (32) NOT NULL,
    [applicationid] VARCHAR (16) NOT NULL,
    [columnorder]   INT          NULL,
    [columnwidth]   INT          NULL,
    [columndesc]    VARCHAR (32) NULL,
    [sort]          INT          NULL,
    [sortorder]     SMALLINT     NULL,
    CONSTRAINT [custwindowfldPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [windowname] ASC, [operatorid] ASC, [columnname] ASC, [applicationid] ASC)
);

