﻿CREATE TABLE [dbo].[zsendlog] (
    [metacollectionid] VARCHAR (16)  NOT NULL,
    [collectionid]     VARCHAR (255) NOT NULL,
    [forversiondtdate] DATETIME      NULL,
    [forversiondttime] VARCHAR (12)  NULL,
    [versiondtdate]    DATETIME      NULL,
    [versiondttime]    VARCHAR (12)  NULL,
    [versionauthor]    VARCHAR (16)  NULL,
    [priority]         SMALLINT      NULL,
    [address]          VARCHAR (128) NOT NULL,
    [senddtdate]       DATETIME      NULL,
    [senddttime]       VARCHAR (12)  NULL,
    [sendstatus]       CHAR (1)      NULL,
    [sendfailreason]   CHAR (1)      NULL,
    [sendackdtdate]    DATETIME      NULL,
    [sendackdttime]    VARCHAR (12)  NULL,
    [expireddtdate]    DATETIME      NULL,
    [expireddttime]    VARCHAR (12)  NULL,
    CONSTRAINT [zsendlogPrimaryKey] PRIMARY KEY CLUSTERED ([metacollectionid] ASC, [collectionid] ASC, [address] ASC)
);


GO
CREATE NONCLUSTERED INDEX [zsendstatusIndex]
    ON [dbo].[zsendlog]([sendstatus] ASC, [sendackdtdate] ASC, [sendackdttime] ASC);

