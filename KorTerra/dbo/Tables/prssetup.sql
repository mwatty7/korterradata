﻿CREATE TABLE [dbo].[prssetup] (
    [customerid] VARCHAR (32) NOT NULL,
    [phoneline]  INT          NOT NULL,
    [linenumber] VARCHAR (20) NULL,
    CONSTRAINT [prssetupPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [phoneline] ASC)
);

