﻿CREATE TABLE [dbo].[mobilenotification] (
    [customerid]             VARCHAR (32)  NOT NULL,
    [mobileid]               VARCHAR (16)  NOT NULL,
    [seqno]                  SMALLINT      NOT NULL,
    [mobilenotificationtype] VARCHAR (255) NOT NULL,
    [emailaddress]           VARCHAR (80)  NULL,
    [isactive]               SMALLINT      NULL,
    CONSTRAINT [PK_mobilenotification] PRIMARY KEY CLUSTERED ([customerid] ASC, [mobileid] ASC, [seqno] ASC)
);

