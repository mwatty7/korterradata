﻿CREATE TABLE [dbo].[kt_occ] (
    [occGuid]     UNIQUEIDENTIFIER NOT NULL,
    [name]        VARCHAR (32)     NOT NULL,
    [description] VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_kt_occ] PRIMARY KEY CLUSTERED ([occGuid] ASC)
);

