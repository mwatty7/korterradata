﻿CREATE TABLE [dbo].[dupes] (
    [occid]                 VARCHAR (25)  NULL,
    [origpriority]          VARCHAR (55)  NULL,
    [kt_priority]           VARCHAR (55)  NULL,
    [fullsize_origpriority] VARCHAR (255) NULL,
    [isreal]                VARCHAR (255) NULL
);

