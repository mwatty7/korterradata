﻿CREATE TABLE [dbo].[damagecontfactors] (
    [damageid]      VARCHAR (16) NOT NULL,
    [contfactorsid] VARCHAR (16) NOT NULL,
    CONSTRAINT [damagecontfactorsPrimaryKey] PRIMARY KEY CLUSTERED ([damageid] ASC, [contfactorsid] ASC)
);

