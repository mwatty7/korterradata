﻿CREATE TABLE [dbo].[zone] (
    [customerid]        VARCHAR (32) NOT NULL,
    [suffixid]          VARCHAR (16) NOT NULL,
    [zoneid]            VARCHAR (16) NOT NULL,
    [description]       VARCHAR (32) NULL,
    [regionid]          VARCHAR (16) NULL,
    [districtid]        VARCHAR (16) NULL,
    [mobileid]          VARCHAR (16) NULL,
    [msggroupid]        VARCHAR (16) NULL,
    [screeningmobileid] VARCHAR (16) NULL,
    CONSTRAINT [zonePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC, [zoneid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [zonemobileIndex]
    ON [dbo].[zone]([customerid] ASC, [mobileid] ASC);

