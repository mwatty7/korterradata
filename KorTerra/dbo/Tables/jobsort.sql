﻿CREATE TABLE [dbo].[jobsort] (
    [customerid]     VARCHAR (32) NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [applicationid]  VARCHAR (16) NOT NULL,
    [windowname]     VARCHAR (32) NOT NULL,
    [operatorid]     VARCHAR (16) NOT NULL,
    [ticketgroup]    SMALLINT     NULL,
    [ticketorder]    INT          NULL,
    [ismanualsort]   SMALLINT     NULL,
    [origsenddtdate] DATETIME     NULL,
    [origsenddttime] VARCHAR (12) NULL,
    CONSTRAINT [jobsortPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [applicationid] ASC, [windowname] ASC, [operatorid] ASC)
);

