﻿CREATE TABLE [dbo].[filteraudititm] (
    [customerid]    VARCHAR (32) NOT NULL,
    [auditdate]     DATETIME     NOT NULL,
    [servicearea]   VARCHAR (40) NOT NULL,
    [seqno]         SMALLINT     NOT NULL,
    [filteredjobid] VARCHAR (40) NOT NULL,
    [membercode]    VARCHAR (16) NULL,
    [priority]      VARCHAR (16) NULL,
    [occstatus]     CHAR (1)     NULL,
    [isrepeated]    SMALLINT     NULL,
    CONSTRAINT [filteraudititmPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [auditdate] ASC, [servicearea] ASC, [seqno] ASC, [filteredjobid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [filteraudititmJobidIndex]
    ON [dbo].[filteraudititm]([customerid] ASC, [filteredjobid] ASC);

