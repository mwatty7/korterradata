﻿CREATE TABLE [dbo].[foreignkeys_dropcreate] (
    [id]                    INT            IDENTITY (1, 1) NOT NULL,
    [creationdt]            DATETIME       DEFAULT (getdate()) NOT NULL,
    [nocheck_script_script] NVARCHAR (MAX) NULL,
    [check_script_script]   NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

