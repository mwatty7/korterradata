﻿CREATE TABLE [dbo].[exca_equip] (
    [excavation_equipment_id]   INT          NOT NULL,
    [excavation_equipment_code] VARCHAR (16) NULL,
    [excavation_equipment_desc] VARCHAR (32) NULL,
    CONSTRAINT [exca_equipPrimaryKey] PRIMARY KEY CLUSTERED ([excavation_equipment_id] ASC)
);

