﻿CREATE TABLE [dbo].[__Delete_XXtracelog_bckup070319] (
    [logid]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [applicationid] VARCHAR (256) NULL,
    [customerid]    VARCHAR (32)  NOT NULL,
    [operatorid]    VARCHAR (256) NOT NULL,
    [logdt]         DATETIME2 (7) NOT NULL,
    [type]          VARCHAR (32)  NOT NULL,
    [message]       VARCHAR (MAX) NOT NULL,
    [component]     VARCHAR (256) NULL,
    [stacktrace]    VARCHAR (MAX) NULL,
    [loginid]       VARCHAR (64)  NULL,
    [controllerid]  VARCHAR (64)  NULL,
    [url]           VARCHAR (MAX) NULL,
    [errornumber]   INT           NULL,
    [application]   VARCHAR (32)  NULL,
    [requestid]     VARCHAR (32)  NULL
);

