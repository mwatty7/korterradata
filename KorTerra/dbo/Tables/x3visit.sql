﻿CREATE TABLE [dbo].[x3visit] (
    [customerid]          VARCHAR (32)    NOT NULL,
    [jobid]               VARCHAR (16)    NOT NULL,
    [membercode]          VARCHAR (16)    NOT NULL,
    [requesttype]         VARCHAR (9)     NOT NULL,
    [creationdtdate]      DATETIME        NOT NULL,
    [creationdttime]      VARCHAR (12)    NOT NULL,
    [versiondtdate]       DATETIME        NULL,
    [versiondttime]       VARCHAR (12)    NULL,
    [versionauthor]       VARCHAR (16)    NULL,
    [operatorid]          VARCHAR (16)    NULL,
    [mobileid]            VARCHAR (16)    NULL,
    [completiondtdate]    DATETIME        NULL,
    [completiondttime]    VARCHAR (12)    NULL,
    [billingtype]         CHAR (1)        NULL,
    [tmunits]             DECIMAL (15, 2) NULL,
    [tmunittype]          CHAR (1)        NULL,
    [completedby]         VARCHAR (10)    NULL,
    [completionlevel]     CHAR (1)        NULL,
    [departmentid]        VARCHAR (16)    NULL,
    [platno]              VARCHAR (13)    NULL,
    [filmroll]            VARCHAR (13)    NULL,
    [serviceterritory]    VARCHAR (16)    NULL,
    [ismain]              SMALLINT        NULL,
    [isservices]          SMALLINT        NULL,
    [qtymain]             SMALLINT        NULL,
    [qtyservices]         SMALLINT        NULL,
    [ishighprofile]       SMALLINT        NULL,
    [isprivatefacilities] SMALLINT        NULL,
    [isongoingjob]        SMALLINT        NULL,
    [ispaint]             SMALLINT        NULL,
    [isflag]              SMALLINT        NULL,
    [isstake]             SMALLINT        NULL,
    [isstakechaser]       SMALLINT        NULL,
    [isoffset]            SMALLINT        NULL,
    [qtyoffset]           DECIMAL (15, 1) NULL,
    [isclearexcavate]     SMALLINT        NULL,
    [isnounderground]     SMALLINT        NULL,
    [isnotcompleted]      SMALLINT        NULL,
    [facilitytype1]       VARCHAR (16)    NULL,
    [facilitytype2]       VARCHAR (16)    NULL,
    [markedreason]        VARCHAR (32)    NULL,
    [unmarkedreason]      VARCHAR (32)    NULL,
    [isnotsendprs]        SMALLINT        NULL,
    CONSTRAINT [x3visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x3visitCompletionIndex]
    ON [dbo].[x3visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x3visitDeptIndex]
    ON [dbo].[x3visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [x3visitMembercodeIndex]
    ON [dbo].[x3visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

