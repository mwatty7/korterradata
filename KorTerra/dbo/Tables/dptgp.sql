﻿CREATE TABLE [dbo].[dptgp] (
    [customerid]  VARCHAR (32) NOT NULL,
    [deptgroupid] VARCHAR (16) NOT NULL,
    [description] VARCHAR (32) NULL,
    [invruleid]   VARCHAR (12) NULL,
    CONSTRAINT [dptgpPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [deptgroupid] ASC)
);

