﻿CREATE TABLE [dbo].[activity] (
    [customerid]     VARCHAR (32) NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [creationdtdate] DATETIME     NOT NULL,
    [creationdttime] VARCHAR (12) NOT NULL,
    [versiondtdate]  DATETIME     NULL,
    [versiondttime]  VARCHAR (12) NULL,
    [versionauthor]  VARCHAR (16) NULL,
    [mobileid]       VARCHAR (16) NULL,
    [operatorid]     VARCHAR (16) NULL,
    [workstatus]     CHAR (1)     NULL,
    CONSTRAINT [activityPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [activityMobileIndex]
    ON [dbo].[activity]([customerid] ASC, [mobileid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [activityOperatorIndex]
    ON [dbo].[activity]([customerid] ASC, [operatorid] ASC, [creationdtdate] ASC, [creationdttime] ASC);

