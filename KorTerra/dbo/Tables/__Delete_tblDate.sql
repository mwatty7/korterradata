﻿CREATE TABLE [dbo].[__Delete_tblDate] (
    [DateKey]   INT          NOT NULL,
    [Date]      DATE         NOT NULL,
    [IsWeekend] TINYINT      NOT NULL,
    [IsHoliday] TINYINT      DEFAULT ((0)) NOT NULL,
    [Day]       TINYINT      NOT NULL,
    [DayName]   VARCHAR (10) NOT NULL,
    [Month]     TINYINT      NOT NULL,
    [MonthName] VARCHAR (10) NOT NULL,
    [Year]      INT          NOT NULL
);

