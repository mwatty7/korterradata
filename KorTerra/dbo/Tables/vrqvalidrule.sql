﻿CREATE TABLE [dbo].[vrqvalidrule] (
    [customerid]  VARCHAR (32) NOT NULL,
    [jobid]       VARCHAR (16) NOT NULL,
    [validruleid] VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [vrqvalidrulePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [validruleid] ASC)
);

