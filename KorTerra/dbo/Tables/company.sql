﻿CREATE TABLE [dbo].[company] (
    [customerid]  VARCHAR (32) NOT NULL,
    [companyid]   VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    [address1]    VARCHAR (32) NULL,
    [address2]    VARCHAR (32) NULL,
    [city]        VARCHAR (64) NULL,
    [state]       VARCHAR (3)  NULL,
    [zipcode]     VARCHAR (11) NULL,
    [contact]     VARCHAR (32) NULL,
    [phone1]      VARCHAR (21) NULL,
    [phone2]      VARCHAR (21) NULL,
    [fax]         VARCHAR (21) NULL,
    [pager]       VARCHAR (21) NULL,
    [mobile]      VARCHAR (21) NULL,
    [email]       VARCHAR (80) NULL,
    [besttime]    VARCHAR (80) NULL,
    CONSTRAINT [companyPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [companyid] ASC)
);

