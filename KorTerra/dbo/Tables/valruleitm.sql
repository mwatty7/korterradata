﻿CREATE TABLE [dbo].[valruleitm] (
    [customerid]    VARCHAR (32) NOT NULL,
    [validruleid]   VARCHAR (16) NOT NULL,
    [fieldtype]     CHAR (1)     NOT NULL,
    [fieldcontents] VARCHAR (32) NOT NULL,
    CONSTRAINT [valruleitmPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [validruleid] ASC, [fieldtype] ASC, [fieldcontents] ASC)
);

