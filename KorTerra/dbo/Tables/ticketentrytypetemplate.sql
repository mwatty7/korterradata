﻿CREATE TABLE [dbo].[ticketentrytypetemplate] (
    [entrytemplateid]        VARCHAR (64)  NOT NULL,
    [description]            VARCHAR (255) CONSTRAINT [DF_ticketentrytypetemplate_description] DEFAULT ('') NOT NULL,
    [html]                   VARCHAR (MAX) NULL,
    [javascript]             VARCHAR (MAX) NULL,
    [css]                    VARCHAR (MAX) NULL,
    [isgeneratedjobid]       BIT           NULL,
    [xml]                    VARCHAR (MAX) NULL,
    [webserviceurl]          VARCHAR (255) NULL,
    [drawzoomlevel]          INT           NULL,
    [digboxcoordinateformat] VARCHAR (8)   NULL,
    [xmldirectory]           VARCHAR (255) NULL,
    [xmlcopydirectory]       VARCHAR (255) NULL,
    CONSTRAINT [PK_ticketentrytypetemplate_1] PRIMARY KEY CLUSTERED ([entrytemplateid] ASC)
);

