﻿CREATE TABLE [dbo].[PAtemplate] (
    [customerid]      VARCHAR (32)  NOT NULL,
    [templatename]    VARCHAR (32)  NOT NULL,
    [templateversion] SMALLINT      NOT NULL,
    [html]            VARCHAR (MAX) NULL,
    [description]     VARCHAR (255) NULL,
    CONSTRAINT [PK_PAtemplate] PRIMARY KEY CLUSTERED ([customerid] ASC, [templatename] ASC, [templateversion] ASC)
);

