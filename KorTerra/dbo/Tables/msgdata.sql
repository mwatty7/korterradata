﻿CREATE TABLE [dbo].[msgdata] (
    [customerid] VARCHAR (32)  NOT NULL,
    [msgtype]    VARCHAR (32)  NOT NULL,
    [msggroupid] VARCHAR (16)  NOT NULL,
    [msgnumber]  INT           NOT NULL,
    [linenum]    SMALLINT      NOT NULL,
    [data]       VARCHAR (128) NULL,
    CONSTRAINT [msgdataPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [msgtype] ASC, [msggroupid] ASC, [msgnumber] ASC, [linenum] ASC)
);

