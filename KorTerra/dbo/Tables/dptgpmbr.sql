﻿CREATE TABLE [dbo].[dptgpmbr] (
    [customerid]   VARCHAR (32) NOT NULL,
    [deptgroupid]  VARCHAR (16) NOT NULL,
    [departmentid] VARCHAR (16) NOT NULL,
    CONSTRAINT [dptgpmbrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [deptgroupid] ASC, [departmentid] ASC)
);

