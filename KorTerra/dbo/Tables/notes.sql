﻿CREATE TABLE [dbo].[notes] (
    [customerid]          VARCHAR (32)  NOT NULL,
    [jobid]               VARCHAR (16)  NOT NULL,
    [type]                VARCHAR (16)  NOT NULL,
    [seqno]               SMALLINT      NOT NULL,
    [operatorid]          VARCHAR (16)  NOT NULL,
    [createddt]           DATETIME2 (7) NOT NULL,
    [includeInTicketSend] SMALLINT      NULL,
    [contents]            VARCHAR (MAX) NULL,
    CONSTRAINT [PK_notes] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [type] ASC, [seqno] ASC)
);

