﻿CREATE TABLE [dbo].[x4visit] (
    [customerid]            VARCHAR (32)    NOT NULL,
    [jobid]                 VARCHAR (16)    NOT NULL,
    [membercode]            VARCHAR (16)    NOT NULL,
    [requesttype]           VARCHAR (9)     NOT NULL,
    [creationdtdate]        DATETIME        NOT NULL,
    [creationdttime]        VARCHAR (12)    NOT NULL,
    [versiondtdate]         DATETIME        NULL,
    [versiondttime]         VARCHAR (12)    NULL,
    [versionauthor]         VARCHAR (16)    NULL,
    [operatorid]            VARCHAR (16)    NULL,
    [mobileid]              VARCHAR (16)    NULL,
    [completiondtdate]      DATETIME        NULL,
    [completiondttime]      VARCHAR (12)    NULL,
    [billingtype]           CHAR (1)        NULL,
    [tmunits]               DECIMAL (15, 2) NULL,
    [tmunittype]            CHAR (1)        NULL,
    [completedby]           VARCHAR (10)    NULL,
    [completionlevel]       CHAR (1)        NULL,
    [departmentid]          VARCHAR (16)    NULL,
    [markedreason]          VARCHAR (32)    NULL,
    [otherinfo]             VARCHAR (32)    NULL,
    [meternbr]              VARCHAR (16)    NULL,
    [isoutsidemeter]        SMALLINT        NULL,
    [isunabletoinspect]     SMALLINT        NULL,
    [unmarkedreason]        VARCHAR (32)    NULL,
    [iscorrinspcomp]        SMALLINT        NULL,
    [actcode1]              VARCHAR (16)    NULL,
    [corrosiondesc]         VARCHAR (80)    NULL,
    [isleaksurvcomp]        SMALLINT        NULL,
    [actcode2]              VARCHAR (16)    NULL,
    [lelreading]            DECIMAL (15, 2) NULL,
    [percentgas]            DECIMAL (15, 2) NULL,
    [utilityrepresentative] VARCHAR (32)    NULL,
    [iscorrosioncheck]      SMALLINT        NULL,
    [isreplacemeter]        SMALLINT        NULL,
    [israiselowermeter]     SMALLINT        NULL,
    [isrelocatemeter]       SMALLINT        NULL,
    [isrehangmeter]         SMALLINT        NULL,
    [isrepairmeter]         SMALLINT        NULL,
    [isreplaceregulator]    SMALLINT        NULL,
    [isreplaceert]          SMALLINT        NULL,
    [ispaintmeter]          SMALLINT        NULL,
    [isrepairindex]         SMALLINT        NULL,
    [isrepairvent]          SMALLINT        NULL,
    [ispeexposed]           SMALLINT        NULL,
    [isother]               SMALLINT        NULL,
    [otherdesc]             VARCHAR (32)    NULL,
    CONSTRAINT [x4visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x4visitCompletionIndex]
    ON [dbo].[x4visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x4visitDeptIndex]
    ON [dbo].[x4visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [x4visitMembercodeIndex]
    ON [dbo].[x4visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

