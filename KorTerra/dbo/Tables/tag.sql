﻿CREATE TABLE [dbo].[tag] (
    [tagid]      UNIQUEIDENTIFIER NOT NULL,
    [customerid] VARCHAR (32)     NOT NULL,
    [isactive]   BIT              NOT NULL,
    CONSTRAINT [PK_tag_tagid] PRIMARY KEY NONCLUSTERED ([tagid] ASC)
);

