﻿CREATE TABLE [dbo].[suffix] (
    [suffixid]    VARCHAR (16) NOT NULL,
    [description] VARCHAR (31) NULL,
    CONSTRAINT [suffixPrimaryKey] PRIMARY KEY CLUSTERED ([suffixid] ASC)
);

