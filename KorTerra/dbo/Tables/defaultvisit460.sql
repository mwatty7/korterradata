﻿CREATE TABLE [dbo].[defaultvisit460] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [membercode]  VARCHAR (16)  NOT NULL,
    [requesttype] VARCHAR (9)   NOT NULL,
    [fieldname]   VARCHAR (32)  NOT NULL,
    [value]       VARCHAR (128) NULL
);

