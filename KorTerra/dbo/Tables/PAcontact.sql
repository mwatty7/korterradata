﻿CREATE TABLE [dbo].[PAcontact] (
    [contactid]       INT           IDENTITY (1, 1) NOT NULL,
    [customerid]      VARCHAR (32)  NOT NULL,
    [contactdate]     DATE          NOT NULL,
    [firstname]       VARCHAR (32)  NULL,
    [lastname]        VARCHAR (32)  NULL,
    [contacttypeid]   INT           NULL,
    [companyname]     VARCHAR (64)  NULL,
    [origregionid]    VARCHAR (16)  NULL,
    [origdistrictid]  VARCHAR (16)  NULL,
    [mobileid]        VARCHAR (16)  NULL,
    [operatorid]      VARCHAR (16)  NULL,
    [address1]        VARCHAR (64)  NULL,
    [address2]        VARCHAR (64)  NULL,
    [city]            VARCHAR (64)  NULL,
    [state]           VARCHAR (3)   NULL,
    [zip]             VARCHAR (12)  NULL,
    [email]           VARCHAR (254) NULL,
    [phone]           VARCHAR (21)  NULL,
    [comments]        VARCHAR (MAX) NULL,
    [templatename]    VARCHAR (32)  NULL,
    [templateversion] SMALLINT      NULL,
    CONSTRAINT [PK_PAcontact] PRIMARY KEY CLUSTERED ([contactid] ASC),
    CONSTRAINT [FK_PAcontactDistrict] FOREIGN KEY ([customerid], [origregionid], [origdistrictid]) REFERENCES [dbo].[district] ([customerid], [regionid], [districtid]),
    CONSTRAINT [FK_PAcontactMobile] FOREIGN KEY ([customerid], [mobileid]) REFERENCES [dbo].[mobile] ([customerid], [mobileid]),
    CONSTRAINT [FK_PAcontactOperator] FOREIGN KEY ([customerid], [operatorid]) REFERENCES [dbo].[operator] ([customerid], [operatorid]),
    CONSTRAINT [FK_PAcontactRegion] FOREIGN KEY ([customerid], [origregionid]) REFERENCES [dbo].[region] ([customerid], [regionid]),
    CONSTRAINT [FK_PAcontactType] FOREIGN KEY ([contacttypeid]) REFERENCES [dbo].[PAmaint] ([maintid])
);


GO
CREATE NONCLUSTERED INDEX [PAcontactCompanyIndex]
    ON [dbo].[PAcontact]([customerid] ASC, [companyname] ASC);


GO
CREATE NONCLUSTERED INDEX [PAcontactDateIndex]
    ON [dbo].[PAcontact]([customerid] ASC, [contactdate] ASC);


GO
CREATE NONCLUSTERED INDEX [PAcontactLastnameIndex]
    ON [dbo].[PAcontact]([customerid] ASC, [lastname] ASC);


GO
CREATE NONCLUSTERED INDEX [PAcontactStateIndex]
    ON [dbo].[PAcontact]([customerid] ASC, [state] ASC);


GO
CREATE NONCLUSTERED INDEX [PAcontactTypeIndex]
    ON [dbo].[PAcontact]([customerid] ASC, [contacttypeid] ASC);

