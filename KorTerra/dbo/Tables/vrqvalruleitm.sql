﻿CREATE TABLE [dbo].[vrqvalruleitm] (
    [customerid]    VARCHAR (32) NOT NULL,
    [jobid]         VARCHAR (16) NOT NULL,
    [validruleid]   VARCHAR (16) NOT NULL,
    [fieldtype]     CHAR (1)     NOT NULL,
    [fieldcontents] VARCHAR (32) NOT NULL,
    CONSTRAINT [vrqvalruleitmPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [validruleid] ASC, [fieldtype] ASC, [fieldcontents] ASC)
);

