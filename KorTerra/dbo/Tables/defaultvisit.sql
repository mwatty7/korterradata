﻿CREATE TABLE [dbo].[defaultvisit] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [membercode]  VARCHAR (16)  NOT NULL,
    [requesttype] VARCHAR (9)   NOT NULL,
    [suffixid]    VARCHAR (16)  NOT NULL,
    [usetype]     CHAR (1)      NOT NULL,
    [fieldname]   VARCHAR (32)  NOT NULL,
    [value]       VARCHAR (128) NULL,
    CONSTRAINT [PK_defaultvisit] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [requesttype] ASC, [suffixid] ASC, [usetype] ASC, [fieldname] ASC)
);

