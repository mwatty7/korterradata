﻿CREATE TABLE [dbo].[opermember] (
    [customerid] VARCHAR (32) NOT NULL,
    [operatorid] VARCHAR (16) NOT NULL,
    [memberid]   VARCHAR (16) NOT NULL,
    CONSTRAINT [opermemberPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [memberid] ASC)
);

