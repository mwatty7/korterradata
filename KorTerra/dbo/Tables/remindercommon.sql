﻿CREATE TABLE [dbo].[remindercommon] (
    [reminderid]        UNIQUEIDENTIFIER NOT NULL,
    [customerid]        VARCHAR (32)     NOT NULL,
    [operatorid]        VARCHAR (16)     NOT NULL,
    [context]           VARCHAR (255)    NULL,
    [remindertype]      VARCHAR (80)     NULL,
    [template_id]       INT              NULL,
    [name]              VARCHAR (255)    NULL,
    [creationdtutc]     DATETIME2 (7)    NULL,
    [duedtutc]          DATETIME2 (7)    NOT NULL,
    [completiondtutc]   DATETIME2 (7)    NULL,
    [reminderlink]      VARCHAR (255)    NULL,
    [lastmodifieddtutc] DATETIME2 (7)    NULL,
    CONSTRAINT [reminderidPrimaryKey] PRIMARY KEY NONCLUSTERED ([reminderid] ASC)
);

