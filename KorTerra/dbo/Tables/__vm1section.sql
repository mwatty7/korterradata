﻿CREATE TABLE [dbo].[__vm1section] (
    [sectionid]    INT            IDENTITY (1, 1) NOT NULL,
    [customerid]   CHAR (32)      NOT NULL,
    [requesttype]  CHAR (9)       NOT NULL,
    [section]      NVARCHAR (32)  NOT NULL,
    [title]        NVARCHAR (255) NOT NULL,
    [sectionorder] INT            NOT NULL,
    [width]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_vm1section] PRIMARY KEY CLUSTERED ([sectionid] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [vm1sectioncustomeridrequesttypeIndex]
    ON [dbo].[__vm1section]([customerid] ASC, [requesttype] ASC, [section] ASC);

