﻿CREATE TABLE [dbo].[ticketentrytype] (
    [customerid]      VARCHAR (32)  NOT NULL,
    [entrytype]       VARCHAR (5)   NOT NULL,
    [description]     VARCHAR (255) NOT NULL,
    [entrytemplateid] VARCHAR (64)  NOT NULL,
    [occid]           VARCHAR (8)   NULL,
    CONSTRAINT [PK_ticketentrytype] PRIMARY KEY CLUSTERED ([customerid] ASC, [entrytemplateid] ASC)
);

