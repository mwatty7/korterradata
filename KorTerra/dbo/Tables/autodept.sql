﻿CREATE TABLE [dbo].[autodept] (
    [customerid]   VARCHAR (32) NOT NULL,
    [autodeptid]   VARCHAR (16) NOT NULL,
    [membercode]   VARCHAR (16) NULL,
    [requesttype]  VARCHAR (9)  NULL,
    [zoneid]       VARCHAR (16) NULL,
    [occpriority]  VARCHAR (80) NULL,
    [priority]     VARCHAR (80) NULL,
    [sitevisit]    SMALLINT     NULL,
    [departmentid] VARCHAR (16) NULL,
    [areacode]     VARCHAR (4)  NULL,
    [wirecenter]   VARCHAR (7)  NULL,
    CONSTRAINT [autodeptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [autodeptid] ASC)
);

