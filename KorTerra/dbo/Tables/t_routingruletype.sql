﻿CREATE TABLE [dbo].[t_routingruletype] (
    [rrtypeid]    UNIQUEIDENTIFIER NOT NULL,
    [rrversionid] UNIQUEIDENTIFIER NOT NULL,
    [match]       UNIQUEIDENTIFIER NULL,
    [nomatch]     UNIQUEIDENTIFIER NULL,
    [method]      VARCHAR (255)    NULL,
    [field]       VARCHAR (255)    NULL,
    CONSTRAINT [PK_t_routingruletype] PRIMARY KEY NONCLUSTERED ([rrtypeid] ASC),
    CONSTRAINT [FK_t_routingruletype_match_t_routingruletype_rrtypeid] FOREIGN KEY ([match]) REFERENCES [dbo].[t_routingruletype] ([rrtypeid]),
    CONSTRAINT [FK_t_routingruletype_nomatch_t_routingruletype_rrtypeid] FOREIGN KEY ([nomatch]) REFERENCES [dbo].[t_routingruletype] ([rrtypeid]),
    CONSTRAINT [FK_t_routingruletype_t_routingruleversionid] FOREIGN KEY ([rrversionid]) REFERENCES [dbo].[t_routingruleversion] ([rrversionid])
);

