﻿CREATE TABLE [dbo].[reason] (
    [customerid]       VARCHAR (32)  NOT NULL,
    [reasonlistid]     VARCHAR (32)  NOT NULL,
    [reasonid]         VARCHAR (32)  NOT NULL,
    [description]      VARCHAR (80)  NULL,
    [seqno]            SMALLINT      NULL,
    [reasongroup]      CHAR (1)      NULL,
    [prsresponse]      VARCHAR (80)  NULL,
    [extresponse]      VARCHAR (255) NULL,
    [reasontemplateid] VARCHAR (64)  NULL,
    CONSTRAINT [reasonPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reasonlistid] ASC, [reasonid] ASC)
);

