﻿CREATE TABLE [dbo].[AAattachment] (
    [customerid]         VARCHAR (32)  NOT NULL,
    [jobid]              VARCHAR (16)  NOT NULL,
    [name]               VARCHAR (256) NOT NULL,
    [attacheddt]         DATETIME2 (7) NULL,
    [mimetype]           VARCHAR (256) NULL,
    [createdby]          VARCHAR (32)  NULL,
    [notes]              VARCHAR (MAX) NULL,
    [fullpath]           VARCHAR (256) NULL,
    [fullsize]           INT           NULL,
    [thumbnailimagepath] VARCHAR (256) NULL,
    [thumbnailimagesize] INT           NULL,
    [reportimagepath]    VARCHAR (256) NULL,
    [reportimagesize]    INT           NULL,
    [exifcameramake]     VARCHAR (32)  NULL,
    [exifcameramodel]    VARCHAR (32)  NULL,
    [exifpicturetakendt] DATETIME2 (7) NULL,
    [exiflatitude]       VARCHAR (32)  NULL,
    [exiflongitude]      VARCHAR (32)  NULL,
    [receivededdt]       DATETIME2 (7) NULL,
    [movecheckdt]        DATETIME2 (7) NULL,
    [isresized]          BIT           NULL,
    [ismoved]            BIT           NULL,
    [source]             VARCHAR (32)  NULL,
    [versionauthor]      VARCHAR (16)  NULL,
    CONSTRAINT [PK_AAattachment] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [name] ASC)
);


GO
CREATE TRIGGER kt_tr_aaattachment_compatible
ON aaattachment
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_name VARCHAR(256);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_name VARCHAR(256);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_name=name, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_name=name
		FROM deleted;
	IF EXISTS (SELECT * FROM inserted) AND NOT EXISTS(SELECT * FROM deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS NULL)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		INSERT INTO attachmentbackward (customerid, jobid, name,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, name,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'BACKWARD'
		FROM inserted;
     END
	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS (SELECT * FROM inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		INSERT INTO attachmentbackward (customerid, jobid, name,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, name,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'BACKWARD'
		FROM deleted;
     END
	IF EXISTS(SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS NULL)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		INSERT INTO attachmentbackward (customerid, jobid, name,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, name,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'UPDATE', 'BACKWARD'
		FROM inserted;
	END
END