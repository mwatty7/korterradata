﻿CREATE TABLE [dbo].[workcompcause] (
    [causeid]     VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [workcompcausePrimaryKey] PRIMARY KEY CLUSTERED ([causeid] ASC)
);

