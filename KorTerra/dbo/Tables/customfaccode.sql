﻿CREATE TABLE [dbo].[customfaccode] (
    [customerid]  VARCHAR (32) NOT NULL,
    [faccode]     VARCHAR (16) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [suffixid]    VARCHAR (16) NOT NULL,
    [description] VARCHAR (32) NULL,
    [occfaccode]  VARCHAR (64) NULL,
    CONSTRAINT [customfaccodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [faccode] ASC, [membercode] ASC, [requesttype] ASC, [suffixid] ASC)
);

