﻿CREATE TABLE [dbo].[AAattachmentsize] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [databasename]     VARCHAR (64)    NOT NULL,
    [sizegb]           DECIMAL (15, 2) NOT NULL,
    [filecount]        INT             NOT NULL,
    [lastcalculateddt] DATETIME2 (7)   NOT NULL,
    CONSTRAINT [PK_AAattachmentsize] PRIMARY KEY CLUSTERED ([customerid] ASC, [databasename] ASC)
);

