﻿CREATE TABLE [dbo].[grandchild] (
    [grandchildid] UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [childid]      UNIQUEIDENTIFIER NULL,
    [data]         VARCHAR (32)     NULL,
    CONSTRAINT [PK_grandchild] PRIMARY KEY CLUSTERED ([grandchildid] ASC),
    CONSTRAINT [FK_child_grandchild] FOREIGN KEY ([childid]) REFERENCES [dbo].[child] ([childid])
);

