﻿CREATE TABLE [dbo].[cntrctr] (
    [contractorid]           INT          NOT NULL,
    [contractorname]         VARCHAR (32) NULL,
    [contractoraddress]      VARCHAR (32) NULL,
    [contractorcity]         VARCHAR (64) NULL,
    [contractorstate]        VARCHAR (3)  NULL,
    [contractorzipcode]      VARCHAR (11) NULL,
    [contractorphonenumber]  VARCHAR (21) NULL,
    [contractoraddress2]     VARCHAR (32) NULL,
    [contractoraltphone]     VARCHAR (21) NULL,
    [contractorcallfax]      VARCHAR (21) NULL,
    [contractorcallpager]    VARCHAR (21) NULL,
    [contractorcallmobile]   VARCHAR (21) NULL,
    [contractorcallemail]    VARCHAR (80) NULL,
    [contractorcallbesttime] VARCHAR (80) NULL,
    CONSTRAINT [cntrctrPrimaryKey] PRIMARY KEY CLUSTERED ([contractorid] ASC)
);

