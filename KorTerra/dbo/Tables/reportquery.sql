﻿CREATE TABLE [dbo].[reportquery] (
    [queryname]         VARCHAR (80)  NOT NULL,
    [description]       VARCHAR (255) NULL,
    [createdby]         VARCHAR (64)  NULL,
    [requestedby]       VARCHAR (64)  NULL,
    [requestedbydtdate] DATETIME      NULL,
    [requestedbydttime] VARCHAR (12)  NULL,
    CONSTRAINT [reportqueryPrimaryKey] PRIMARY KEY CLUSTERED ([queryname] ASC)
);

