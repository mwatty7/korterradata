﻿CREATE TABLE [dbo].[x5visit] (
    [customerid]        VARCHAR (32)    NOT NULL,
    [jobid]             VARCHAR (16)    NOT NULL,
    [membercode]        VARCHAR (16)    NOT NULL,
    [requesttype]       VARCHAR (9)     NOT NULL,
    [creationdtdate]    DATETIME        NOT NULL,
    [creationdttime]    VARCHAR (12)    NOT NULL,
    [versiondtdate]     DATETIME        NULL,
    [versiondttime]     VARCHAR (12)    NULL,
    [versionauthor]     VARCHAR (16)    NULL,
    [operatorid]        VARCHAR (16)    NULL,
    [mobileid]          VARCHAR (16)    NULL,
    [completiondtdate]  DATETIME        NULL,
    [completiondttime]  VARCHAR (12)    NULL,
    [billingtype]       CHAR (1)        NULL,
    [tmunits]           DECIMAL (15, 2) NULL,
    [tmunittype]        CHAR (1)        NULL,
    [completedby]       VARCHAR (10)    NULL,
    [completionlevel]   CHAR (1)        NULL,
    [isinconflict]      SMALLINT        NULL,
    [isstreet]          SMALLINT        NULL,
    [isstreetradio]     SMALLINT        NULL,
    [ishouse]           SMALLINT        NULL,
    [ishouseradio]      SMALLINT        NULL,
    [islocate]          SMALLINT        NULL,
    [islocateradio]     SMALLINT        NULL,
    [isexcavation]      SMALLINT        NULL,
    [isexcavationradio] SMALLINT        NULL,
    [isoffice]          SMALLINT        NULL,
    [isofficeradio]     SMALLINT        NULL,
    [conflictmethod]    VARCHAR (64)    NULL,
    [clearcriteria]     VARCHAR (64)    NULL,
    CONSTRAINT [x5visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x5visitCompletionIndex]
    ON [dbo].[x5visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x5visitMembercodeIndex]
    ON [dbo].[x5visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

