﻿CREATE TABLE [dbo].[visitqueue] (
    [visitqueueid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]   VARCHAR (32)     NOT NULL,
    [jobid]        VARCHAR (16)     NOT NULL,
    [membercode]   VARCHAR (16)     NOT NULL,
    [requesttype]  VARCHAR (9)      NOT NULL,
    [creationdt]   DATETIME2 (7)    NOT NULL,
    [queueddt]     DATETIME2 (7)    NULL,
    CONSTRAINT [PK_visitqueue] PRIMARY KEY CLUSTERED ([visitqueueid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [visitqueue_queueddtIndex]
    ON [dbo].[visitqueue]([queueddt] ASC);

