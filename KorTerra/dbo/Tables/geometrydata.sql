﻿CREATE TABLE [dbo].[geometrydata] (
    [geometrydataid] UNIQUEIDENTIFIER CONSTRAINT [DF_geometrydata_geometrydataid] DEFAULT (newsequentialid()) NOT NULL,
    [type]           VARCHAR (32)     NOT NULL,
    [coordinate]     [sys].[geometry] NULL,
    CONSTRAINT [PK_geometrydata_geometrydataid] PRIMARY KEY CLUSTERED ([geometrydataid] ASC)
);

