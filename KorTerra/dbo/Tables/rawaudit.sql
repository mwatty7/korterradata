﻿CREATE TABLE [dbo].[rawaudit] (
    [customerid] VARCHAR (32)  NOT NULL,
    [auditdate]  DATETIME      NOT NULL,
    [sendto]     VARCHAR (40)  NOT NULL,
    [type]       VARCHAR (16)  NOT NULL,
    [linenum]    SMALLINT      NOT NULL,
    [seqnum]     SMALLINT      NOT NULL,
    [occid]      VARCHAR (8)   NULL,
    [contents]   VARCHAR (255) NULL,
    CONSTRAINT [rawauditPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [auditdate] ASC, [sendto] ASC, [type] ASC, [linenum] ASC, [seqnum] ASC)
);


GO
CREATE NONCLUSTERED INDEX [rawAuditCompKey]
    ON [dbo].[rawaudit]([type] ASC, [linenum] ASC, [occid] ASC, [auditdate] ASC)
    INCLUDE([customerid], [sendto], [seqnum], [contents]);

