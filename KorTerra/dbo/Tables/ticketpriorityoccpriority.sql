﻿CREATE TABLE [dbo].[ticketpriorityoccpriority] (
    [ticketpriorityid] UNIQUEIDENTIFIER NOT NULL,
    [customerid]       VARCHAR (32)     NOT NULL,
    [occid]            VARCHAR (8)      NOT NULL,
    [origpriority]     VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_ticketpriorityoccpriority] PRIMARY KEY CLUSTERED ([ticketpriorityid] ASC, [customerid] ASC, [occid] ASC, [origpriority] ASC),
    CONSTRAINT [FK_ticketpriorityoccpriority_occpriority] FOREIGN KEY ([customerid], [occid], [origpriority]) REFERENCES [dbo].[occpriority] ([customerid], [occid], [origpriority]),
    CONSTRAINT [FK_ticketpriorityoccpriority_ticketpriority] FOREIGN KEY ([ticketpriorityid]) REFERENCES [dbo].[ticketpriority] ([ticketpriorityid])
);

