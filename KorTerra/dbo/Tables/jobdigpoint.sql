﻿CREATE TABLE [dbo].[jobdigpoint] (
    [id]          INT              IDENTITY (1, 1) NOT NULL,
    [customerid]  VARCHAR (32)     NOT NULL,
    [jobid]       VARCHAR (16)     NOT NULL,
    [jobgeometry] [sys].[geometry] NULL,
    CONSTRAINT [jobdigpointPrimaryKey] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [jobdigpointJobIndex]
    ON [dbo].[jobdigpoint]([customerid] ASC, [jobid] ASC);


GO
CREATE SPATIAL INDEX [jobdigpointGeometryIndex]
    ON [dbo].[jobdigpoint] ([jobgeometry])
    USING GEOMETRY_GRID
    WITH  (
            BOUNDING_BOX = (XMAX = -51.79, XMIN = -130.2, YMAX = 62.51, YMIN = 24.14),
            GRIDS = (LEVEL_1 = MEDIUM, LEVEL_2 = MEDIUM, LEVEL_3 = MEDIUM, LEVEL_4 = MEDIUM)
          );

