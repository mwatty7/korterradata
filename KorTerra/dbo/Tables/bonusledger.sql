﻿CREATE TABLE [dbo].[bonusledger] (
    [customerid]           VARCHAR (32) NOT NULL,
    [operatorid]           VARCHAR (16) NOT NULL,
    [creationdtdate]       DATETIME     NOT NULL,
    [creationdttime]       VARCHAR (12) NOT NULL,
    [bonuseligibilitydate] DATETIME     NULL,
    [tierchange]           SMALLINT     NULL,
    [bonusamount]          FLOAT (53)   NULL,
    [modifiedby]           VARCHAR (16) NULL,
    [reason]               VARCHAR (80) NULL,
    [reportid]             VARCHAR (16) NULL,
    CONSTRAINT [bonusledgerPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);

