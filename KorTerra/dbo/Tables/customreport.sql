﻿CREATE TABLE [dbo].[customreport] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [reportid]      VARCHAR (80)  NOT NULL,
    [description]   VARCHAR (255) NULL,
    [querytext]     VARCHAR (MAX) NULL,
    [createdby]     VARCHAR (64)  NULL,
    [requestedby]   VARCHAR (64)  NULL,
    [requestedbydt] DATETIME      NULL,
    [lastrundt]     DATETIME      NULL,
    [lastrunby]     VARCHAR (64)  NULL,
    CONSTRAINT [PK_customreport] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC)
);

