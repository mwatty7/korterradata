﻿CREATE TABLE [dbo].[estcost] (
    [facilitytypeid] INT             NOT NULL,
    [materialtypeid] INT             NOT NULL,
    [facilitysizeid] INT             NOT NULL,
    [estimatedcost]  DECIMAL (15, 2) NULL,
    CONSTRAINT [estcostPrimaryKey] PRIMARY KEY CLUSTERED ([facilitytypeid] ASC, [materialtypeid] ASC, [facilitysizeid] ASC)
);

