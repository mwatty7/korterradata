﻿CREATE TABLE [dbo].[mdsisends] (
    [customerid]    VARCHAR (32)  NOT NULL,
    [jobid]         VARCHAR (16)  NOT NULL,
    [ordernumber]   VARCHAR (20)  NOT NULL,
    [identifier]    VARCHAR (255) NULL,
    [versiondtdate] DATETIME      NULL,
    [versiondttime] VARCHAR (12)  NULL,
    [status]        CHAR (1)      NULL,
    [priority]      VARCHAR (16)  NULL,
    [wtbdtdate]     DATETIME      NULL,
    [wtbdttime]     VARCHAR (12)  NULL,
    [mobileid]      VARCHAR (16)  NULL,
    CONSTRAINT [mdsisendsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [ordernumber] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ordernumberIndex]
    ON [dbo].[mdsisends]([ordernumber] ASC);

