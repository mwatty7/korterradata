﻿CREATE TABLE [dbo].[manrequest] (
    [customerid]     VARCHAR (32) NOT NULL,
    [jobid]          VARCHAR (16) NOT NULL,
    [membercode]     VARCHAR (16) NOT NULL,
    [requesttype]    VARCHAR (9)  NOT NULL,
    [creationdtdate] DATETIME     NULL,
    [creationdttime] VARCHAR (12) NULL,
    [operatorid]     VARCHAR (16) NULL,
    CONSTRAINT [manrequestPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC)
);

