﻿CREATE TABLE [dbo].[x6visit] (
    [customerid]           VARCHAR (32)    NOT NULL,
    [jobid]                VARCHAR (16)    NOT NULL,
    [membercode]           VARCHAR (16)    NOT NULL,
    [requesttype]          VARCHAR (9)     NOT NULL,
    [creationdtdate]       DATETIME        NOT NULL,
    [creationdttime]       VARCHAR (12)    NOT NULL,
    [versiondtdate]        DATETIME        NULL,
    [versiondttime]        VARCHAR (12)    NULL,
    [versionauthor]        VARCHAR (16)    NULL,
    [operatorid]           VARCHAR (16)    NULL,
    [mobileid]             VARCHAR (16)    NULL,
    [completiondtdate]     DATETIME        NULL,
    [completiondttime]     VARCHAR (12)    NULL,
    [billingtype]          CHAR (1)        NULL,
    [tmunits]              DECIMAL (15, 2) NULL,
    [tmunittype]           CHAR (1)        NULL,
    [completedby]          VARCHAR (10)    NULL,
    [completionlevel]      CHAR (1)        NULL,
    [conflicttype]         VARCHAR (64)    NULL,
    [sewersize]            VARCHAR (32)    NULL,
    [location]             VARCHAR (64)    NULL,
    [facilitytype]         VARCHAR (64)    NULL,
    [gassize]              VARCHAR (32)    NULL,
    [facilitysize]         VARCHAR (32)    NULL,
    [houseelevation]       VARCHAR (64)    NULL,
    [soilconditions]       VARCHAR (64)    NULL,
    [observeddepth]        VARCHAR (64)    NULL,
    [materialtype]         VARCHAR (64)    NULL,
    [repairworkorder]      VARCHAR (64)    NULL,
    [yearinstalled]        VARCHAR (5)     NULL,
    [installedby]          VARCHAR (64)    NULL,
    [methodofinstallation] VARCHAR (64)    NULL,
    CONSTRAINT [x6visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [x6visitCompletionIndex]
    ON [dbo].[x6visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [x6visitMembercodeIndex]
    ON [dbo].[x6visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

