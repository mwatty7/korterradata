﻿CREATE TABLE [dbo].[statcols] (
    [customerid]  VARCHAR (32) NOT NULL,
    [columntitle] VARCHAR (32) NOT NULL,
    [fieldname]   VARCHAR (32) NULL,
    CONSTRAINT [statcolsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [columntitle] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [statcolsTitleIndex]
    ON [dbo].[statcols]([customerid] ASC, [fieldname] ASC);

