﻿CREATE TABLE [dbo].[kt_visitdata] (
    [visitdataGuid] UNIQUEIDENTIFIER NOT NULL,
    [visitGuid]     UNIQUEIDENTIFIER NOT NULL,
    [field]         VARCHAR (32)     NOT NULL,
    [value]         VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_kt_visitdata] PRIMARY KEY CLUSTERED ([visitdataGuid] ASC),
    CONSTRAINT [FK_kt_visitdata_visitGuid_kt_visit_visitGuid] FOREIGN KEY ([visitGuid]) REFERENCES [dbo].[kt_visit] ([visitGuid])
);

