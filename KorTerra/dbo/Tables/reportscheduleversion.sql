﻿CREATE TABLE [dbo].[reportscheduleversion] (
    [reportscheduleversionid] UNIQUEIDENTIFIER CONSTRAINT [DF_reportscheduleversion_reportscheduleversionid] DEFAULT (newsequentialid()) NOT NULL,
    [reportscheduleid]        UNIQUEIDENTIFIER NOT NULL,
    [name]                    VARCHAR (80)     NOT NULL,
    [description]             VARCHAR (255)    NOT NULL,
    [startdtutc]              DATETIME2 (7)    NOT NULL,
    [frequency]               VARCHAR (16)     NULL,
    [issunday]                BIT              CONSTRAINT [DF_reportscheduleversion_issunday] DEFAULT ((0)) NOT NULL,
    [ismonday]                BIT              CONSTRAINT [DF_reportscheduleversion_ismonday] DEFAULT ((0)) NOT NULL,
    [istuesday]               BIT              CONSTRAINT [DF_reportscheduleversion_istuesday] DEFAULT ((0)) NOT NULL,
    [iswednesday]             BIT              CONSTRAINT [DF_reportscheduleversion_iswednesday] DEFAULT ((0)) NOT NULL,
    [isthursday]              BIT              CONSTRAINT [DF_reportscheduleversion_isthursday] DEFAULT ((0)) NOT NULL,
    [isfriday]                BIT              CONSTRAINT [DF_reportscheduleversion_isfriday] DEFAULT ((0)) NOT NULL,
    [issaturday]              BIT              CONSTRAINT [DF_reportscheduleversion_issaturday] DEFAULT ((0)) NOT NULL,
    [exporttype]              VARCHAR (16)     NULL,
    [isactive]                BIT              NULL,
    [createdby]               VARCHAR (80)     NOT NULL,
    [creationdtutc]           DATETIME2 (7)    NOT NULL,
    [scheduleon]              VARCHAR (16)     NULL,
    [dayofmonth]              INT              NULL,
    CONSTRAINT [PK_reportscheduleversion] PRIMARY KEY CLUSTERED ([reportscheduleversionid] ASC),
    CONSTRAINT [FK_reportscheduleversion_reportschedule] FOREIGN KEY ([reportscheduleid]) REFERENCES [dbo].[reportschedule] ([reportscheduleid])
);


GO
CREATE NONCLUSTERED INDEX [IX_reportscheduleversion_reportscheduleid_isactive_include]
    ON [dbo].[reportscheduleversion]([reportscheduleid] ASC, [isactive] ASC)
    INCLUDE([reportscheduleversionid], [name], [description], [startdtutc], [frequency], [issunday], [ismonday], [istuesday], [iswednesday], [isthursday], [isfriday], [issaturday], [exporttype], [createdby], [creationdtutc], [scheduleon], [dayofmonth]);

