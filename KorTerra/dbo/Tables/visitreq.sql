﻿CREATE TABLE [dbo].[visitreq] (
    [customerid]           VARCHAR (32)  NOT NULL,
    [jobid]                VARCHAR (16)  NOT NULL,
    [membercode]           VARCHAR (16)  NOT NULL,
    [requesttype]          VARCHAR (9)   NOT NULL,
    [versiondtdate]        DATETIME      NULL,
    [versiondttime]        VARCHAR (12)  NULL,
    [versionauthor]        VARCHAR (16)  NULL,
    [visitstatus]          CHAR (1)      NULL,
    [lastcompletiondtdate] DATETIME      NULL,
    [lastcompletiondttime] VARCHAR (12)  NULL,
    [isscreened]           SMALLINT      NULL,
    [lastscreeningdateutc] DATETIME2 (7) NULL,
    CONSTRAINT [visitreqPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ndx_visitreq_customer_member_request]
    ON [dbo].[visitreq]([customerid] ASC, [membercode] ASC, [requesttype] ASC)
    INCLUDE([jobid]);

