﻿CREATE TABLE [dbo].[bbillstatus] (
    [customerid]      VARCHAR (32) NOT NULL,
    [deptgroupid]     VARCHAR (16) NOT NULL,
    [cutoffdate]      DATETIME     NOT NULL,
    [invoiceid]       VARCHAR (12) NOT NULL,
    [departmentid]    VARCHAR (16) NOT NULL,
    [operatorid]      VARCHAR (16) NULL,
    [processeddtdate] DATETIME     NULL,
    [processeddttime] VARCHAR (12) NULL,
    [status]          CHAR (1)     NULL,
    CONSTRAINT [bbillstatusPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [deptgroupid] ASC, [cutoffdate] ASC, [invoiceid] ASC, [departmentid] ASC)
);

