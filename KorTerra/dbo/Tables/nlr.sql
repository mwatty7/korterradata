﻿CREATE TABLE [dbo].[nlr] (
    [customerid] VARCHAR (32) NOT NULL,
    [auditdate]  DATETIME     NOT NULL,
    [membercode] VARCHAR (16) NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    CONSTRAINT [nlrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [auditdate] ASC, [membercode] ASC, [jobid] ASC)
);

