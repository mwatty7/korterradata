﻿CREATE TABLE [dbo].[visitruleaction] (
    [visitruleid]   UNIQUEIDENTIFIER NOT NULL,
    [visitactionid] UNIQUEIDENTIFIER NOT NULL,
    [seqno]         INT              NOT NULL,
    CONSTRAINT [PK_visitruleaction] PRIMARY KEY CLUSTERED ([visitruleid] ASC, [visitactionid] ASC, [seqno] ASC),
    CONSTRAINT [FK_visitruleaction_visitactionid_visitaction_visitactionid] FOREIGN KEY ([visitactionid]) REFERENCES [dbo].[visitaction] ([visitactionid]),
    CONSTRAINT [FK_visitruleaction_visitruleid_visitrule_visitruleid] FOREIGN KEY ([visitruleid]) REFERENCES [dbo].[visitrule] ([visitruleid])
);

