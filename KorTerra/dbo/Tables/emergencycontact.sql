﻿CREATE TABLE [dbo].[emergencycontact] (
    [customerid]       VARCHAR (32)  NOT NULL,
    [emergencygroupid] VARCHAR (16)  NOT NULL,
    [callorder]        INT           NOT NULL,
    [firstname]        VARCHAR (32)  NULL,
    [lastname]         VARCHAR (32)  NULL,
    [title]            VARCHAR (16)  NULL,
    [phone]            VARCHAR (21)  NULL,
    [mobile]           VARCHAR (21)  NULL,
    [altphone]         VARCHAR (21)  NULL,
    [email]            VARCHAR (80)  NULL,
    [address]          VARCHAR (64)  NULL,
    [city]             VARCHAR (64)  NULL,
    [zipcode]          VARCHAR (11)  NULL,
    [state]            VARCHAR (3)   NULL,
    [notes]            VARCHAR (255) NULL,
    CONSTRAINT [emergencycontactPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [emergencygroupid] ASC, [callorder] ASC)
);

