﻿CREATE TABLE [dbo].[membzone] (
    [customerid]  VARCHAR (32) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [zoneid]      VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [isnlr]       SMALLINT     NULL,
    CONSTRAINT [membzonePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [zoneid] ASC, [requesttype] ASC)
);

