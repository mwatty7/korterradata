﻿CREATE TABLE [dbo].[pay_status] (
    [pay_status_id]   INT          NOT NULL,
    [pay_status_desc] VARCHAR (32) NULL,
    CONSTRAINT [pay_statusPrimaryKey] PRIMARY KEY CLUSTERED ([pay_status_id] ASC)
);

