﻿CREATE TABLE [dbo].[addmembers] (
    [customerid]      VARCHAR (32) NOT NULL,
    [jobid]           VARCHAR (16) NOT NULL,
    [seqno]           SMALLINT     NOT NULL,
    [addmembers]      VARCHAR (80) NULL,
    [description]     VARCHAR (80) NULL,
    [ismanuallyadded] SMALLINT     NULL,
    [phone1]          VARCHAR (21) NULL,
    [phone2]          VARCHAR (21) NULL,
    [phone3]          VARCHAR (21) NULL,
    [utilitytype]     VARCHAR (32) NULL,
    CONSTRAINT [addmembersPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [seqno] ASC)
);

