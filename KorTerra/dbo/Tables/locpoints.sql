﻿CREATE TABLE [dbo].[locpoints] (
    [customerid] VARCHAR (32) NOT NULL,
    [jobid]      VARCHAR (16) NOT NULL,
    [type]       VARCHAR (16) NOT NULL,
    [seqno]      SMALLINT     NOT NULL,
    [field1]     VARCHAR (21) NULL,
    [field2]     VARCHAR (21) NULL,
    [field3]     VARCHAR (8)  NULL,
    [field4]     VARCHAR (8)  NULL,
    CONSTRAINT [locpointsPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [type] ASC, [seqno] ASC)
);

