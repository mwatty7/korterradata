﻿CREATE TABLE [dbo].[duedaterules] (
    [customerid]        VARCHAR (32) NOT NULL,
    [occid]             VARCHAR (8)  NOT NULL,
    [occpriority]       VARCHAR (80) NOT NULL,
    [offset]            FLOAT (53)   NOT NULL,
    [duedateoverride]   VARCHAR (64) NULL,
    [isincludeweekends] SMALLINT     NULL,
    [isincludeholidays] SMALLINT     NULL,
    CONSTRAINT [PK_duedaterules] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [occpriority] ASC)
);

