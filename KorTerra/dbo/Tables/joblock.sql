﻿CREATE TABLE [dbo].[joblock] (
    [customerid] VARCHAR (32)  NOT NULL,
    [jobid]      VARCHAR (16)  NOT NULL,
    [lockdtdate] DATETIME      NULL,
    [lockdttime] VARCHAR (12)  NULL,
    [operatorid] VARCHAR (16)  NOT NULL,
    [lockstatus] VARCHAR (16)  NOT NULL,
    [changes]    VARCHAR (256) NULL,
    CONSTRAINT [PK_joblock] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [operatorid] ASC, [lockstatus] ASC)
);

