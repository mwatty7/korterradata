﻿CREATE TABLE [dbo].[claimantremarks] (
    [claimid]        VARCHAR (16)  NOT NULL,
    [claimantid]     VARCHAR (32)  NOT NULL,
    [creationdtdate] DATETIME      NOT NULL,
    [creationdttime] VARCHAR (12)  NOT NULL,
    [seqno]          INT           NOT NULL,
    [text]           VARCHAR (128) NULL,
    CONSTRAINT [claimantremarksPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [claimantid] ASC, [creationdtdate] ASC, [creationdttime] ASC, [seqno] ASC)
);

