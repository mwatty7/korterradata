﻿CREATE TABLE [dbo].[IMreceivableclaim] (
    [customerid]            NVARCHAR (32)   NOT NULL,
    [incidentnumber]        NVARCHAR (32)   CONSTRAINT [DF_IMreceivableclaim_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [receivableclaimid]     INT             IDENTITY (1, 1) NOT NULL,
    [createdbyoperatorid]   NVARCHAR (32)   NOT NULL,
    [createddtutc]          DATETIME        NOT NULL,
    [amount]                DECIMAL (18, 2) NOT NULL,
    [receivableclaimstatus] NVARCHAR (32)   CONSTRAINT [DF_receivableclaim_receivableclaimstatus] DEFAULT (N'NEW') NOT NULL,
    CONSTRAINT [PK_receivableclaim] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC, [receivableclaimid] ASC),
    CONSTRAINT [FK_IMreceivableclaim_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

