﻿CREATE TABLE [dbo].[visitcommon] (
    [customerid]                   VARCHAR (32)    NOT NULL,
    [jobid]                        VARCHAR (16)    NOT NULL,
    [membercode]                   VARCHAR (16)    NOT NULL,
    [requesttype]                  VARCHAR (9)     NOT NULL,
    [creationdt]                   DATETIME2 (7)   NOT NULL,
    [versiondt]                    DATETIME2 (7)   NULL,
    [versionauthor]                VARCHAR (16)    NULL,
    [templateid]                   VARCHAR (64)    NULL,
    [templateversionnumber]        INT             NULL,
    [operatorid]                   VARCHAR (16)    NULL,
    [mobileid]                     VARCHAR (16)    NULL,
    [completiondt]                 DATETIME2 (7)   NULL,
    [billingtype]                  CHAR (1)        NULL,
    [tmunits]                      DECIMAL (15, 2) NULL,
    [tmunittype]                   CHAR (1)        NULL,
    [completedby]                  VARCHAR (10)    NULL,
    [completionlevel]              CHAR (1)        NULL,
    [departmentid]                 VARCHAR (16)    NULL,
    [platno]                       VARCHAR (13)    NULL,
    [filmroll]                     VARCHAR (13)    NULL,
    [serviceterritory]             VARCHAR (16)    NULL,
    [remarks]                      VARCHAR (MAX)   NULL,
    [compstatus]                   VARCHAR (32)    NULL,
    [reasonid]                     VARCHAR (32)    NULL,
    [isnotsendprs]                 SMALLINT        NULL,
    [internalremarks]              VARCHAR (MAX)   NULL,
    [latitude]                     VARCHAR (32)    NULL,
    [longitude]                    VARCHAR (32)    NULL,
    [billingtemplateid]            VARCHAR (64)    NULL,
    [billingtemplateversionnumber] INT             NULL,
    [prsdt]                        DATETIME        NULL,
    [completiondateutc]            DATETIME2 (7)   NULL,
    [reasontemplateid]             VARCHAR (64)    NULL,
    [reasontemplateversionnumber]  INT             NULL,
    [islate]                       SMALLINT        NULL,
    [duedateutc]                   DATETIME2 (7)   NULL,
    CONSTRAINT [visitcommonPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdt] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_visitcommon_cust_compldt]
    ON [dbo].[visitcommon]([customerid] ASC, [completiondt] ASC)
    INCLUDE([jobid], [operatorid]);


GO
CREATE NONCLUSTERED INDEX [ix_visitcommon_customerid_completiondt_includes]
    ON [dbo].[visitcommon]([customerid] ASC, [completiondt] ASC)
    INCLUDE([jobid], [operatorid]);


GO
CREATE NONCLUSTERED INDEX [ix_visitcommon_customerid_operatorid_completiondt_includes]
    ON [dbo].[visitcommon]([customerid] ASC, [operatorid] ASC, [completiondt] ASC)
    INCLUDE([jobid]);


GO
CREATE NONCLUSTERED INDEX [visitcommonCompletionIndex]
    ON [dbo].[visitcommon]([customerid] ASC, [completiondt] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [visitcommonDeptIndex]
    ON [dbo].[visitcommon]([customerid] ASC, [departmentid] ASC, [creationdt] ASC);


GO
CREATE NONCLUSTERED INDEX [visitcommonMembercodeIndex]
    ON [dbo].[visitcommon]([customerid] ASC, [membercode] ASC, [creationdt] ASC);


GO
CREATE TRIGGER kt_tr_visitcommon_compatible
ON visitcommon
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_membercode VARCHAR(16);
	DECLARE @new_requesttype VARCHAR(9);
--	DECLARE @new_creationdtdate DATETIME;
--	DECLARE @new_creationdttime VARCHAR(12);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_membercode VARCHAR(16);
	DECLARE @old_requesttype VARCHAR(9);
--	DECLARE @old_creationdtdate DATETIME;
--	DECLARE @old_creationdttime VARCHAR(12);
--	DECLARE @old_versionauthor VARCHAR(16);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_membercode=membercode, @new_requesttype=requesttype, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_membercode=membercode, @old_requesttype=requesttype
		FROM deleted;
	If exists (Select * from inserted) and not exists(Select * from deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, CONVERT(VARCHAR(10),creationdt,101), CONVERT(VARCHAR(12),creationdt,114),
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'BACKWARD'
		FROM inserted;
     END
	if exists(SELECT * from deleted) and not exists (SELECT * from inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, CONVERT(VARCHAR(10),creationdt,101), CONVERT(VARCHAR(12),creationdt,114),
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'BACKWARD'
		FROM deleted;
     END
	 -- Entity Framework prevents this from going into an infinite loop. Checking versionauthor broke edit completion where the visit was done in new korweb.
	if exists(SELECT * from inserted) and exists (SELECT * from deleted) -- AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, CONVERT(VARCHAR(10),creationdt,101), CONVERT(VARCHAR(12),creationdt,114),
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'UPDATE', 'BACKWARD'
		FROM inserted;
	END
END