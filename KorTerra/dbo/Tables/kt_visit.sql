﻿CREATE TABLE [dbo].[kt_visit] (
    [visitGuid]     UNIQUEIDENTIFIER NOT NULL,
    [visitreqGuid]  UNIQUEIDENTIFIER NOT NULL,
    [compstatus]    VARCHAR (32)     NOT NULL,
    [visitstatus]   VARCHAR (32)     NOT NULL,
    [creationUTC]   DATETIME2 (7)    NOT NULL,
    [completionUTC] DATETIME2 (7)    NOT NULL,
    [receivedUTC]   DATETIME2 (7)    NULL,
    CONSTRAINT [PK_kt_visit] PRIMARY KEY CLUSTERED ([visitGuid] ASC),
    CONSTRAINT [FK_kt_visit_visitreqGuid_kt_visitreq_visitreqGuid] FOREIGN KEY ([visitreqGuid]) REFERENCES [dbo].[kt_visitreq] ([visitreqGuid])
);

