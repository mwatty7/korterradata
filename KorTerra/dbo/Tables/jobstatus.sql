﻿CREATE TABLE [dbo].[jobstatus] (
    [customerid]       VARCHAR (32)     NOT NULL,
    [jobid]            VARCHAR (16)     NOT NULL,
    [ticketpriorityid] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_jobstatus] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC),
    CONSTRAINT [FK_jobstatus_job] FOREIGN KEY ([customerid], [jobid]) REFERENCES [dbo].[job] ([customerid], [jobid]),
    CONSTRAINT [FK_jobstatus_ticketpriority] FOREIGN KEY ([ticketpriorityid]) REFERENCES [dbo].[ticketpriority] ([ticketpriorityid])
);

