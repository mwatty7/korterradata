﻿CREATE TABLE [dbo].[customreportsecurity] (
    [customerid] VARCHAR (32) NOT NULL,
    [groupid]    VARCHAR (32) NOT NULL,
    [reportid]   VARCHAR (80) NOT NULL,
    CONSTRAINT [PK_customreportsecurity] PRIMARY KEY CLUSTERED ([customerid] ASC, [groupid] ASC, [reportid] ASC)
);

