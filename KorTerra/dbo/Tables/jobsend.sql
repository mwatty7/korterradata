﻿CREATE TABLE [dbo].[jobsend] (
    [customerid]   VARCHAR (32)  NOT NULL,
    [jobid]        VARCHAR (16)  NOT NULL,
    [sentdt]       DATETIME2 (7) NOT NULL,
    [operatorid]   VARCHAR (16)  NULL,
    [frommobileid] VARCHAR (16)  NULL,
    [tomobileid]   VARCHAR (16)  NOT NULL,
    [action]       VARCHAR (32)  NOT NULL,
    [reasonid]     VARCHAR (32)  NOT NULL,
    [isnotified]   SMALLINT      NOT NULL,
    CONSTRAINT [PK_jobsend] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [sentdt] ASC)
);

