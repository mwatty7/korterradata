﻿CREATE TABLE [dbo].[ledger] (
    [customerid]      VARCHAR (32)    NOT NULL,
    [invoiceid]       VARCHAR (12)    NOT NULL,
    [departmentid]    VARCHAR (16)    NOT NULL,
    [masterdeptid]    VARCHAR (16)    NULL,
    [creationdtdate]  DATETIME        NOT NULL,
    [creationdttime]  VARCHAR (12)    NOT NULL,
    [versionauthor]   VARCHAR (16)    NULL,
    [issystem]        SMALLINT        NULL,
    [transactiontype] VARCHAR (16)    NULL,
    [locatetot]       INT             NULL,
    [checknum]        VARCHAR (16)    NULL,
    [debit]           DECIMAL (15, 2) NULL,
    [credit]          DECIMAL (15, 2) NULL,
    [quantity]        DECIMAL (15, 2) NULL,
    CONSTRAINT [ledgerPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [invoiceid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);

