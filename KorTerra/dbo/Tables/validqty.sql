﻿CREATE TABLE [dbo].[validqty] (
    [customerid]   VARCHAR (32)    NOT NULL,
    [membercode]   VARCHAR (16)    NOT NULL,
    [requesttype]  VARCHAR (9)     NOT NULL,
    [departmentid] VARCHAR (16)    NOT NULL,
    [billingtype]  VARCHAR (2)     NOT NULL,
    [seqno]        SMALLINT        NOT NULL,
    [tmunittype]   CHAR (1)        NULL,
    [minvalue]     DECIMAL (15, 2) NULL,
    [maxvalue]     DECIMAL (15, 2) NULL,
    [incrvalue]    DECIMAL (15, 2) NULL,
    CONSTRAINT [validqtyPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC, [requesttype] ASC, [departmentid] ASC, [billingtype] ASC, [seqno] ASC)
);

