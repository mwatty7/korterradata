﻿CREATE TABLE [dbo].[invrule] (
    [customerid] VARCHAR (32) NOT NULL,
    [invruleid]  VARCHAR (12) NOT NULL,
    [prefix]     VARCHAR (12) NULL,
    [seqstart]   VARCHAR (8)  NULL,
    [seqend]     VARCHAR (8)  NULL,
    [suffix]     VARCHAR (12) NULL,
    [seqcur]     VARCHAR (8)  NULL,
    CONSTRAINT [invrulePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [invruleid] ASC)
);

