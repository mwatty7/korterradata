﻿CREATE TABLE [dbo].[jobsendcompatible] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [jobid]          VARCHAR (16)  NOT NULL,
    [actiontype]     VARCHAR (16)  NOT NULL,
    [senddtdate]     DATETIME      NOT NULL,
    [senddttime]     VARCHAR (12)  NOT NULL,
    [tomobileid]     VARCHAR (16)  NULL,
    [isreadonly]     SMALLINT      NOT NULL,
    [status]         VARCHAR (16)  NULL,
    [statusreason]   VARCHAR (128) NULL,
    [ackdtdate]      DATETIME      NULL,
    [ackdttime]      VARCHAR (12)  NULL,
    [frommobileid]   VARCHAR (16)  NULL,
    [originalstatus] CHAR (1)      NULL,
    [desttype]       VARCHAR (16)  NULL,
    [specifictype]   VARCHAR (32)  NULL,
    [address]        VARCHAR (256) NULL,
    [membercode]     VARCHAR (16)  NULL,
    CONSTRAINT [PK_jobsendcompatible_customerid_jobid_actiontype_senddtdate_senddttime] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [actiontype] ASC, [senddtdate] ASC, [senddttime] ASC)
);

