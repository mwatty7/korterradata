﻿CREATE TABLE [dbo].[paymastr] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [operatorid]       VARCHAR (16)    NOT NULL,
    [workdate]         DATETIME        NULL,
    [hadlunch]         DECIMAL (15, 2) NULL,
    [timestarted]      VARCHAR (12)    NULL,
    [timestopped]      VARCHAR (12)    NULL,
    [mileagebegin]     DECIMAL (15, 1) NULL,
    [mileageend]       DECIMAL (15, 1) NULL,
    [status]           VARCHAR (2)     NULL,
    [batch_num]        VARCHAR (16)    NULL,
    [batch_start_date] DATETIME        NULL,
    [batch_end_date]   DATETIME        NULL,
    [remarks]          VARCHAR (255)   NULL,
    [approvedby]       VARCHAR (16)    NULL,
    [approveddate]     DATETIME        NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    CONSTRAINT [paymastrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [creationdtdate] ASC, [creationdttime] ASC, [operatorid] ASC)
);

