﻿CREATE TABLE [dbo].[custsearchhdr] (
    [customerid]    VARCHAR (32) NOT NULL,
    [custsearchid]  INT          NOT NULL,
    [applicationid] VARCHAR (16) NOT NULL,
    [windowname]    VARCHAR (32) NULL,
    [operatorid]    VARCHAR (16) NULL,
    [searchname]    VARCHAR (32) NULL,
    CONSTRAINT [custsearchhdrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [custsearchid] ASC, [applicationid] ASC)
);

