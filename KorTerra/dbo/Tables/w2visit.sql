﻿CREATE TABLE [dbo].[w2visit] (
    [customerid]         VARCHAR (32)    NOT NULL,
    [jobid]              VARCHAR (16)    NOT NULL,
    [membercode]         VARCHAR (16)    NOT NULL,
    [requesttype]        VARCHAR (9)     NOT NULL,
    [creationdtdate]     DATETIME        NOT NULL,
    [creationdttime]     VARCHAR (12)    NOT NULL,
    [versiondtdate]      DATETIME        NULL,
    [versiondttime]      VARCHAR (12)    NULL,
    [versionauthor]      VARCHAR (16)    NULL,
    [operatorid]         VARCHAR (16)    NULL,
    [mobileid]           VARCHAR (16)    NULL,
    [completiondtdate]   DATETIME        NULL,
    [completiondttime]   VARCHAR (12)    NULL,
    [billingtype]        CHAR (1)        NULL,
    [tmunits]            DECIMAL (15, 2) NULL,
    [tmunittype]         CHAR (1)        NULL,
    [completedby]        VARCHAR (10)    NULL,
    [completionlevel]    CHAR (1)        NULL,
    [ismain]             SMALLINT        NULL,
    [isservices]         SMALLINT        NULL,
    [isstopbox]          SMALLINT        NULL,
    [isvalvebox]         SMALLINT        NULL,
    [isother]            SMALLINT        NULL,
    [qtymain]            SMALLINT        NULL,
    [qtyservices]        SMALLINT        NULL,
    [qtystopbox]         SMALLINT        NULL,
    [qtyvalvebox]        SMALLINT        NULL,
    [qtyother]           SMALLINT        NULL,
    [iscastiron]         SMALLINT        NULL,
    [islead]             SMALLINT        NULL,
    [issteel]            SMALLINT        NULL,
    [isconcrete]         SMALLINT        NULL,
    [iscopper]           SMALLINT        NULL,
    [isductileiron]      SMALLINT        NULL,
    [ispaint]            SMALLINT        NULL,
    [isflag]             SMALLINT        NULL,
    [isstake]            SMALLINT        NULL,
    [isstakechaser]      SMALLINT        NULL,
    [isoffset]           SMALLINT        NULL,
    [israin]             SMALLINT        NULL,
    [issand]             SMALLINT        NULL,
    [issnow]             SMALLINT        NULL,
    [isclearexcavate]    SMALLINT        NULL,
    [isnounderground]    SMALLINT        NULL,
    [iselectronic]       SMALLINT        NULL,
    [ismapmeasure]       SMALLINT        NULL,
    [isnotcompleted]     SMALLINT        NULL,
    [isexcavatorpresent] SMALLINT        NULL,
    [excavatorpresent]   VARCHAR (32)    NULL,
    [isothercompletion]  SMALLINT        NULL,
    [isnoshow]           SMALLINT        NULL,
    [isremark]           SMALLINT        NULL,
    CONSTRAINT [w2visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [w2visitCompletionIndex]
    ON [dbo].[w2visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [w2visitMembercodeIndex]
    ON [dbo].[w2visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);

