﻿CREATE TABLE [dbo].[profilewindowfld] (
    [windowname]    VARCHAR (32) NOT NULL,
    [profileid]     VARCHAR (32) NOT NULL,
    [columnname]    VARCHAR (32) NOT NULL,
    [applicationid] VARCHAR (16) NOT NULL,
    [columnorder]   INT          NULL,
    [columnwidth]   INT          NULL,
    [columndesc]    VARCHAR (32) NULL,
    CONSTRAINT [profilewindowfldPrimaryKey] PRIMARY KEY CLUSTERED ([windowname] ASC, [profileid] ASC, [columnname] ASC, [applicationid] ASC)
);

