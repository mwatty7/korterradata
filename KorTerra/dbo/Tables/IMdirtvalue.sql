﻿CREATE TABLE [dbo].[IMdirtvalue] (
    [dirtvaluetype] NVARCHAR (32)  NOT NULL,
    [value]         NVARCHAR (32)  NOT NULL,
    [description]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dirtvalue] PRIMARY KEY CLUSTERED ([dirtvaluetype] ASC, [value] ASC)
);

