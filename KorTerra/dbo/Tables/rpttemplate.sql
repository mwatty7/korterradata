﻿CREATE TABLE [dbo].[rpttemplate] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [reportid]       VARCHAR (32)  NOT NULL,
    [templateid]     VARCHAR (32)  NOT NULL,
    [description]    VARCHAR (32)  NULL,
    [path]           VARCHAR (255) NULL,
    [textequivalent] VARCHAR (32)  NULL,
    [outputtype]     VARCHAR (16)  NULL,
    CONSTRAINT [rpttemplatePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [templateid] ASC)
);

