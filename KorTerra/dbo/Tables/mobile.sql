﻿CREATE TABLE [dbo].[mobile] (
    [customerid]              VARCHAR (32) NOT NULL,
    [mobileid]                VARCHAR (16) NOT NULL,
    [description]             VARCHAR (32) NULL,
    [regionid]                VARCHAR (16) NULL,
    [districtid]              VARCHAR (16) NULL,
    [mobiletype]              CHAR (1)     NULL,
    [isidle]                  SMALLINT     NULL,
    [newemergencytot]         SMALLINT     NULL,
    [newemergencyvisitreqtot] SMALLINT     NULL,
    [newmeettot]              SMALLINT     NULL,
    [newmeetvisitreqtot]      SMALLINT     NULL,
    [newroutinetot]           SMALLINT     NULL,
    [newroutinevisitreqtot]   SMALLINT     NULL,
    [emergencytot]            SMALLINT     NULL,
    [emergencyvisitreqtot]    SMALLINT     NULL,
    [meettot]                 SMALLINT     NULL,
    [meetvisitreqtot]         SMALLINT     NULL,
    [routinetot]              SMALLINT     NULL,
    [routinevisitreqtot]      SMALLINT     NULL,
    [projecttot]              SMALLINT     NULL,
    [projectvisitreqtot]      SMALLINT     NULL,
    [isautosend]              SMALLINT     NULL,
    [isautosendnew]           SMALLINT     NULL,
    [schemaversion]           SMALLINT     NULL,
    [isdisableprs]            SMALLINT     NULL,
    CONSTRAINT [mobilePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [mobileid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_mobile_customerid_districtid]
    ON [dbo].[mobile]([customerid] ASC, [districtid] ASC);


GO
CREATE NONCLUSTERED INDEX [mobileregiondistrictindex]
    ON [dbo].[mobile]([customerid] ASC, [regionid] ASC, [districtid] ASC, [mobileid] ASC);

