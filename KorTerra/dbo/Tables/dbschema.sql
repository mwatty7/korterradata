﻿CREATE TABLE [dbo].[dbschema] (
    [id]            SMALLINT NOT NULL,
    [schemaversion] SMALLINT NULL,
    CONSTRAINT [dbschemaPrimaryKey] PRIMARY KEY CLUSTERED ([id] ASC)
);

