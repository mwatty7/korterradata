﻿CREATE TABLE [dbo].[tagemail] (
    [customerid]       VARCHAR (32)     NOT NULL,
    [tagversionid]     UNIQUEIDENTIFIER NOT NULL,
    [emailrecipientid] UNIQUEIDENTIFIER NOT NULL,
    [scheduleid]       UNIQUEIDENTIFIER NOT NULL,
    [template_id]      INT              NULL,
    [isemailexcavator] BIT              NULL,
    CONSTRAINT [PK_tagemail_tagversionid_emailrecipientid_template_id] PRIMARY KEY NONCLUSTERED ([tagversionid] ASC, [emailrecipientid] ASC, [scheduleid] ASC),
    CONSTRAINT [FK_tagemail_emailrecipientid_emailrecipient_emailrecipientid] FOREIGN KEY ([emailrecipientid]) REFERENCES [dbo].[emailrecipient] ([emailrecipientid]),
    CONSTRAINT [FK_tagemail_scheduleid_schedule_scheduleid] FOREIGN KEY ([scheduleid]) REFERENCES [dbo].[schedule] ([scheduleid]),
    CONSTRAINT [FK_tagemail_tagversionid_tagversion_tagversionid] FOREIGN KEY ([tagversionid]) REFERENCES [dbo].[tagversion] ([tagversionid]),
    CONSTRAINT [FK_tagemail_templateid_xxtemplate_template_id] FOREIGN KEY ([template_id]) REFERENCES [dbo].[XXtemplate] ([template_id])
);

