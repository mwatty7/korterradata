﻿CREATE TABLE [dbo].[emailtrigger] (
    [customerid]   VARCHAR (32)  NOT NULL,
    [occid]        VARCHAR (8)   NOT NULL,
    [triggerid]    VARCHAR (16)  NOT NULL,
    [regex]        VARCHAR (255) NULL,
    [templatefile] VARCHAR (255) NULL,
    [emailaddress] VARCHAR (255) NULL,
    CONSTRAINT [emailtriggerPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [triggerid] ASC)
);

