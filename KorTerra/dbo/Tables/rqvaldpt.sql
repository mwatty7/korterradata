﻿CREATE TABLE [dbo].[rqvaldpt] (
    [customerid]      VARCHAR (32) NOT NULL,
    [jobid]           VARCHAR (16) NOT NULL,
    [membercode]      VARCHAR (16) NOT NULL,
    [requesttype]     VARCHAR (9)  NOT NULL,
    [departmentid]    VARCHAR (16) NOT NULL,
    [seqno]           SMALLINT     NOT NULL,
    [isdefault]       SMALLINT     NULL,
    [description]     VARCHAR (32) NULL,
    [priority]        VARCHAR (16) NULL,
    [origpriority]    VARCHAR (80) NULL,
    [zoneid]          VARCHAR (16) NULL,
    [fromorigtime]    VARCHAR (12) NULL,
    [toorigtime]      VARCHAR (12) NULL,
    [days]            CHAR (1)     NULL,
    [displaylocation] CHAR (1)     NULL,
    CONSTRAINT [rqvaldptPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [departmentid] ASC, [seqno] ASC)
);

