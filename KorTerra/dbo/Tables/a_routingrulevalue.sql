﻿CREATE TABLE [dbo].[a_routingrulevalue] (
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [rrtypeid]  UNIQUEIDENTIFIER NOT NULL,
    [value]     VARCHAR (MAX)    NULL,
    [zoneid]    VARCHAR (16)     NULL,
    [minx]      FLOAT (53)       NULL,
    [maxx]      FLOAT (53)       NULL,
    [miny]      FLOAT (53)       NULL,
    [maxy]      FLOAT (53)       NULL,
    CONSTRAINT [PK_a_routingrulevalue] PRIMARY KEY NONCLUSTERED ([rrvalueid] ASC),
    CONSTRAINT [FK_a_routingrulevalue_rrtypeid_a_routingruletype_rrtypeid] FOREIGN KEY ([rrtypeid]) REFERENCES [dbo].[a_routingruletype] ([rrtypeid])
);

