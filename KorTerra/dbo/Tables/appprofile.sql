﻿CREATE TABLE [dbo].[appprofile] (
    [applicationid]    VARCHAR (32)     NOT NULL,
    [workflow]         VARCHAR (255)    NOT NULL,
    [guid]             UNIQUEIDENTIFIER NOT NULL,
    [eventseqno]       INT              NOT NULL,
    [identifier]       VARCHAR (255)    NULL,
    [eventname]        VARCHAR (255)    NULL,
    [action]           VARCHAR (32)     NULL,
    [eventdt]          DATETIME2 (7)    NULL,
    [customerid]       VARCHAR (32)     NULL,
    [jobid]            VARCHAR (16)     NULL,
    [occid]            VARCHAR (8)      NULL,
    [korterrapriority] VARCHAR (16)     NULL,
    [customerpriority] VARCHAR (32)     NULL,
    CONSTRAINT [PK_appprofile] PRIMARY KEY CLUSTERED ([applicationid] ASC, [workflow] ASC, [guid] ASC, [eventseqno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_appprofile_identifier_include]
    ON [dbo].[appprofile]([identifier] ASC)
    INCLUDE([guid], [eventseqno]);

