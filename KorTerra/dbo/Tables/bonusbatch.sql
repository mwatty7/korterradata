﻿CREATE TABLE [dbo].[bonusbatch] (
    [customerid]  VARCHAR (32) NOT NULL,
    [reportid]    VARCHAR (16) NOT NULL,
    [description] VARCHAR (32) NULL,
    [cutoffdate]  DATETIME     NOT NULL,
    CONSTRAINT [bonusbatchPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [reportid] ASC, [cutoffdate] ASC)
);

