﻿CREATE TABLE [dbo].[IMInvestigationRequiredFields] (
    [customerid] NVARCHAR (32)  NOT NULL,
    [fieldname]  NVARCHAR (128) NOT NULL,
    [labeltext]  NVARCHAR (128) NULL,
    [section]    NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_IMInvestigationRequiredFields] PRIMARY KEY CLUSTERED ([customerid] ASC, [fieldname] ASC)
);

