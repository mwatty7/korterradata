﻿CREATE TABLE [dbo].[IMincident] (
    [customerid]            NVARCHAR (32) NOT NULL,
    [incidentnumber]        NVARCHAR (32) CONSTRAINT [DF_IMincident_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [createdbyoperatorid]   NVARCHAR (32) NOT NULL,
    [createddtutc]          DATETIME      NOT NULL,
    [currentrevisionnumber] INT           NOT NULL,
    CONSTRAINT [PK_IMincident] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC)
);

