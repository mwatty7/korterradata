﻿CREATE TABLE [dbo].[__Delete__XXtemplateBckup081619] (
    [template_id]   INT           IDENTITY (1, 1) NOT NULL,
    [name]          VARCHAR (32)  NOT NULL,
    [description]   VARCHAR (255) NULL,
    [customerid]    VARCHAR (32)  NOT NULL,
    [template_type] VARCHAR (32)  NULL,
    [content]       VARCHAR (MAX) NULL,
    [creationdt]    DATETIME2 (7) NULL,
    [modifiedby]    VARCHAR (32)  NULL,
    [modifiedon]    DATETIME2 (7) NULL,
    [ishtml]        BIT           NULL
);

