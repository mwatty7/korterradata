﻿CREATE TABLE [dbo].[u1visit] (
    [customerid]       VARCHAR (32)    NOT NULL,
    [jobid]            VARCHAR (16)    NOT NULL,
    [membercode]       VARCHAR (16)    NOT NULL,
    [requesttype]      VARCHAR (9)     NOT NULL,
    [creationdtdate]   DATETIME        NOT NULL,
    [creationdttime]   VARCHAR (12)    NOT NULL,
    [versiondtdate]    DATETIME        NULL,
    [versiondttime]    VARCHAR (12)    NULL,
    [versionauthor]    VARCHAR (16)    NULL,
    [operatorid]       VARCHAR (16)    NULL,
    [mobileid]         VARCHAR (16)    NULL,
    [completiondtdate] DATETIME        NULL,
    [completiondttime] VARCHAR (12)    NULL,
    [billingtype]      CHAR (1)        NULL,
    [tmunits]          DECIMAL (15, 2) NULL,
    [tmunittype]       CHAR (1)        NULL,
    [completedby]      VARCHAR (10)    NULL,
    [completionlevel]  CHAR (1)        NULL,
    [checksum]         VARCHAR (32)    NULL,
    [departmentid]     VARCHAR (16)    NULL,
    [platno]           VARCHAR (13)    NULL,
    [filmroll]         VARCHAR (13)    NULL,
    [serviceterritory] VARCHAR (16)    NULL,
    [markedreason]     VARCHAR (32)    NULL,
    [unmarkedreason]   VARCHAR (32)    NULL,
    [isnotsendprs]     SMALLINT        NULL,
    [ismarked00]       SMALLINT        NULL,
    [ismarked01]       SMALLINT        NULL,
    [ismarked02]       SMALLINT        NULL,
    [ismarked03]       SMALLINT        NULL,
    [ismarked04]       SMALLINT        NULL,
    [ismarked05]       SMALLINT        NULL,
    [ismarked06]       SMALLINT        NULL,
    [ismarked07]       SMALLINT        NULL,
    [ismarked08]       SMALLINT        NULL,
    [ismarked09]       SMALLINT        NULL,
    [ismarked10]       SMALLINT        NULL,
    [ismarked11]       SMALLINT        NULL,
    [ismarked12]       SMALLINT        NULL,
    [ismarked13]       SMALLINT        NULL,
    [ismarked14]       SMALLINT        NULL,
    [ismarked15]       SMALLINT        NULL,
    [isnotmarked00]    SMALLINT        NULL,
    [isnotmarked01]    SMALLINT        NULL,
    [isnotmarked02]    SMALLINT        NULL,
    [isnotmarked03]    SMALLINT        NULL,
    [isnotmarked04]    SMALLINT        NULL,
    [isnotmarked05]    SMALLINT        NULL,
    [isnotmarked06]    SMALLINT        NULL,
    [isnotmarked07]    SMALLINT        NULL,
    [isnotmarked08]    SMALLINT        NULL,
    [isnotmarked09]    SMALLINT        NULL,
    [isnotmarked10]    SMALLINT        NULL,
    [isnotmarked11]    SMALLINT        NULL,
    [isnotmarked12]    SMALLINT        NULL,
    [isnotmarked13]    SMALLINT        NULL,
    [isnotmarked14]    SMALLINT        NULL,
    [isnotmarked15]    SMALLINT        NULL,
    [iscleared00]      SMALLINT        NULL,
    [iscleared01]      SMALLINT        NULL,
    [iscleared02]      SMALLINT        NULL,
    [iscleared03]      SMALLINT        NULL,
    [iscleared04]      SMALLINT        NULL,
    [iscleared05]      SMALLINT        NULL,
    [iscleared06]      SMALLINT        NULL,
    [iscleared07]      SMALLINT        NULL,
    [iscleared08]      SMALLINT        NULL,
    [iscleared09]      SMALLINT        NULL,
    [iscleared10]      SMALLINT        NULL,
    [iscleared11]      SMALLINT        NULL,
    [iscleared12]      SMALLINT        NULL,
    [iscleared13]      SMALLINT        NULL,
    [iscleared14]      SMALLINT        NULL,
    [iscleared15]      SMALLINT        NULL,
    [isother00]        SMALLINT        NULL,
    [isother01]        SMALLINT        NULL,
    [isother02]        SMALLINT        NULL,
    [isother03]        SMALLINT        NULL,
    [isother04]        SMALLINT        NULL,
    [isother05]        SMALLINT        NULL,
    [isother06]        SMALLINT        NULL,
    [isother07]        SMALLINT        NULL,
    [isother08]        SMALLINT        NULL,
    [isother09]        SMALLINT        NULL,
    [isother10]        SMALLINT        NULL,
    [isother11]        SMALLINT        NULL,
    [isother12]        SMALLINT        NULL,
    [isother13]        SMALLINT        NULL,
    [isother14]        SMALLINT        NULL,
    [isother15]        SMALLINT        NULL,
    [field00]          VARCHAR (32)    NULL,
    [field01]          VARCHAR (32)    NULL,
    [field02]          VARCHAR (32)    NULL,
    [field03]          VARCHAR (32)    NULL,
    [field04]          VARCHAR (32)    NULL,
    [field05]          VARCHAR (32)    NULL,
    [field06]          VARCHAR (32)    NULL,
    [field07]          VARCHAR (32)    NULL,
    [field08]          VARCHAR (32)    NULL,
    [field09]          VARCHAR (32)    NULL,
    [field10]          VARCHAR (32)    NULL,
    [field11]          VARCHAR (32)    NULL,
    [field12]          VARCHAR (32)    NULL,
    [field13]          VARCHAR (32)    NULL,
    [field14]          VARCHAR (32)    NULL,
    [field15]          VARCHAR (32)    NULL,
    [fixedfield0]      DECIMAL (15, 2) NULL,
    [fixedfield1]      DECIMAL (15, 2) NULL,
    [fixedfield2]      DECIMAL (15, 2) NULL,
    [fixedfield3]      DECIMAL (15, 2) NULL,
    [fixedfield4]      DECIMAL (15, 2) NULL,
    [fixedfield5]      DECIMAL (15, 2) NULL,
    [fixedfield6]      DECIMAL (15, 2) NULL,
    [fixedfield7]      DECIMAL (15, 2) NULL,
    [dblfield0]        FLOAT (53)      NULL,
    [dblfield1]        FLOAT (53)      NULL,
    [dblfield2]        FLOAT (53)      NULL,
    [dblfield3]        FLOAT (53)      NULL,
    [dblfield4]        FLOAT (53)      NULL,
    [dblfield5]        FLOAT (53)      NULL,
    [dblfield6]        FLOAT (53)      NULL,
    [dblfield7]        FLOAT (53)      NULL,
    [longfield0]       VARCHAR (255)   NULL,
    [longfield1]       VARCHAR (255)   NULL,
    [longfield2]       VARCHAR (255)   NULL,
    [longfield3]       VARCHAR (255)   NULL,
    [longfield4]       VARCHAR (255)   NULL,
    [longfield5]       VARCHAR (255)   NULL,
    [longfield6]       VARCHAR (255)   NULL,
    [longfield7]       VARCHAR (255)   NULL,
    CONSTRAINT [u1visitPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdtdate] ASC, [creationdttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [u1visitCompletionIndex]
    ON [dbo].[u1visit]([customerid] ASC, [completiondtdate] ASC, [completiondttime] ASC, [billingtype] ASC, [mobileid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [u1visitDeptIndex]
    ON [dbo].[u1visit]([customerid] ASC, [departmentid] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE NONCLUSTERED INDEX [u1visitMembercodeIndex]
    ON [dbo].[u1visit]([customerid] ASC, [membercode] ASC, [creationdtdate] ASC, [creationdttime] ASC);


GO
CREATE TRIGGER kt_tr_u1visit_compatible
ON u1visit
AFTER UPDATE, INSERT, DELETE AS
BEGIN
	DECLARE @new_customerid VARCHAR(32);
	DECLARE @new_jobid VARCHAR(16);
	DECLARE @new_membercode VARCHAR(16);
	DECLARE @new_requesttype VARCHAR(9);
	DECLARE @new_creationdtdate DATETIME;
	DECLARE @new_creationdttime VARCHAR(12);
	DECLARE @new_versionauthor VARCHAR(16);
	DECLARE @old_customerid VARCHAR(32);
	DECLARE @old_jobid VARCHAR(16);
	DECLARE @old_membercode VARCHAR(16);
	DECLARE @old_requesttype VARCHAR(9);
	DECLARE @old_creationdtdate DATETIME;
	DECLARE @old_creationdttime VARCHAR(12);
--	DECLARE @old_versionauthor VARCHAR(16);
	SELECT @new_customerid=customerid, @new_jobid=jobid, @new_membercode=membercode, @new_requesttype=requesttype, @new_versionauthor=versionauthor
		FROM inserted;
	SELECT @old_customerid=customerid, @old_jobid=jobid, @old_membercode=membercode, @old_requesttype=requesttype
		FROM deleted;
	If exists (Select * from inserted) and not exists(Select * from deleted) AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'populating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'INSERT', 'FORWARD'
		FROM inserted;
     END
	if exists(SELECT * from deleted) and not exists (SELECT * from inserted)
	BEGIN
		PRINT N'deleting for ' + @old_customerid + ', ' + @old_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'DELETE', 'FORWARD'
		FROM deleted;
     END
	 -- Entity Framework prevents this from going into an infinite loop. Checking versionauthor broke edit completion where the visit was done in new korweb.
	if exists(SELECT * from inserted) and exists (SELECT * from deleted) -- AND (@new_versionauthor != 'TRIGGER' OR @new_versionauthor IS null)
	BEGIN
		PRINT N'updating for ' + @new_customerid + ', ' + @new_jobid
		insert into visitbackward (customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						triggerdtdate, triggerdttime, action, direction)
		SELECT customerid, jobid, membercode, requesttype, creationdtdate, creationdttime,
						CONVERT(VARCHAR(10),GETUTCDATE(),101), CONVERT(VARCHAR(12),GETUTCDATE(),114), 'UPDATE', 'FORWARD'
		FROM inserted;
	END
END