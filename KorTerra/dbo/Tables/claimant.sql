﻿CREATE TABLE [dbo].[claimant] (
    [claimid]           VARCHAR (16)    NOT NULL,
    [claimantid]        VARCHAR (32)    NOT NULL,
    [name]              VARCHAR (32)    NULL,
    [address1]          VARCHAR (32)    NULL,
    [address2]          VARCHAR (32)    NULL,
    [city]              VARCHAR (64)    NULL,
    [state]             VARCHAR (3)     NULL,
    [zip]               VARCHAR (11)    NULL,
    [phone]             VARCHAR (21)    NULL,
    [fax]               VARCHAR (21)    NULL,
    [invoiceamt]        DECIMAL (15, 2) NULL,
    [estpayout]         DECIMAL (15, 2) NULL,
    [insrecovery]       DECIMAL (15, 2) NULL,
    [invnumber]         VARCHAR (32)    NULL,
    [invdtdate]         DATETIME        NULL,
    [estinvamt]         DECIMAL (15, 2) NULL,
    [pymntprocdt]       DATETIME        NULL,
    [authrzdpayrollnbr] VARCHAR (16)    NULL,
    [receiveddtdate]    DATETIME        NULL,
    CONSTRAINT [claimantPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [claimantid] ASC)
);

