﻿CREATE TABLE [dbo].[vrqcustomactcode] (
    [customerid]  VARCHAR (32) NOT NULL,
    [jobid]       VARCHAR (16) NOT NULL,
    [actcode]     VARCHAR (16) NOT NULL,
    [membercode]  VARCHAR (16) NOT NULL,
    [requesttype] VARCHAR (9)  NOT NULL,
    [description] VARCHAR (32) NULL,
    CONSTRAINT [vrqcustomactcodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [actcode] ASC, [membercode] ASC, [requesttype] ASC)
);

