﻿CREATE TABLE [dbo].[calendar] (
    [customerid]    CHAR (32)    NOT NULL,
    [occid]         CHAR (8)     NOT NULL,
    [thedate]       DATETIME     NOT NULL,
    [theyear]       INT          NULL,
    [themonth]      INT          NULL,
    [thedayofmonth] INT          NULL,
    [thequarter]    INT          NULL,
    [theweekofyear] INT          NULL,
    [thedayofweek]  INT          NULL,
    [themonthname]  CHAR (10)    NULL,
    [thedayname]    CHAR (10)    NULL,
    [isweekday]     SMALLINT     NULL,
    [isholiday]     SMALLINT     NULL,
    [holidaydescr]  VARCHAR (32) NULL,
    [daterank]      INT          NULL,
    CONSTRAINT [calendarPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [thedate] ASC)
);

