﻿CREATE TABLE [dbo].[occ] (
    [customerid]           VARCHAR (32)  NOT NULL,
    [occid]                VARCHAR (8)   NOT NULL,
    [description]          VARCHAR (32)  NULL,
    [msggroupid]           VARCHAR (16)  NULL,
    [jobprefix]            VARCHAR (4)   NULL,
    [nlrtruckname]         VARCHAR (16)  NULL,
    [citytruckname]        VARCHAR (16)  NULL,
    [isignoreupdatestatus] SMALLINT      NULL,
    [enablemessaging]      SMALLINT      NULL,
    [messageshorts]        SMALLINT      NULL,
    [shortcutoff]          SMALLINT      NULL,
    [excludeweekends]      SMALLINT      NULL,
    [messagecancellations] SMALLINT      NULL,
    [messagecorrections]   SMALLINT      NULL,
    [messageupdates]       SMALLINT      NULL,
    [messageduplicates]    SMALLINT      NULL,
    [timezone]             VARCHAR (128) NULL,
    [isupdatefollowmobile] SMALLINT      NULL,
    CONSTRAINT [occPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC)
);

