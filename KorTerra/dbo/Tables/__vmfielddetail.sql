﻿CREATE TABLE [dbo].[__vmfielddetail] (
    [detailid]  INT            IDENTITY (1, 1) NOT NULL,
    [fieldid]   NVARCHAR (20)  NOT NULL,
    [sectionid] NVARCHAR (20)  NOT NULL,
    [order]     INT            NOT NULL,
    [value]     NTEXT          NULL,
    [width]     NVARCHAR (255) NULL,
    CONSTRAINT [PK_vmfielddetail] PRIMARY KEY CLUSTERED ([detailid] ASC)
);

