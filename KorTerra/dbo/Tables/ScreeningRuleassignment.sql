﻿CREATE TABLE [dbo].[ScreeningRuleassignment] (
    [rulesetid]         INT           NOT NULL,
    [customerid]        VARCHAR (32)  NOT NULL,
    [memberid]          VARCHAR (16)  NOT NULL,
    [assignmentstartdt] DATETIME2 (7) NOT NULL,
    [assignmentenddt]   DATETIME2 (7) NULL,
    [status]            VARCHAR (32)  NOT NULL,
    CONSTRAINT [PK_ScreeningRuleassignment] PRIMARY KEY CLUSTERED ([rulesetid] ASC, [customerid] ASC, [memberid] ASC, [assignmentstartdt] ASC),
    CONSTRAINT [FK_ScreeningRuleassignmentScreeningRuleset] FOREIGN KEY ([rulesetid]) REFERENCES [dbo].[ScreeningRuleset] ([rulesetid])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'status is ACTIVE|TESTING|HISTORICAL - only one can be ACTIVE', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuleassignment', @level2type = N'COLUMN', @level2name = N'status';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'An intersection table between ScreeningRuleset and member', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRuleassignment';

