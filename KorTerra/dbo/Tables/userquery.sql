﻿CREATE TABLE [dbo].[userquery] (
    [customerid]  VARCHAR (32)  NOT NULL,
    [name]        VARCHAR (256) NOT NULL,
    [description] VARCHAR (256) NULL,
    [criteria]    VARCHAR (MAX) NULL,
    [createdby]   VARCHAR (16)  NULL,
    [createddt]   DATETIME2 (7) NULL,
    [editedby]    VARCHAR (16)  NULL,
    [editeddt]    DATETIME2 (7) NULL,
    [isshared]    SMALLINT      NULL,
    [groupid]     VARCHAR (16)  NULL,
    CONSTRAINT [PK_userquery] PRIMARY KEY CLUSTERED ([customerid] ASC, [name] ASC)
);

