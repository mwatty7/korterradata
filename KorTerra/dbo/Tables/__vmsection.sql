﻿CREATE TABLE [dbo].[__vmsection] (
    [sectionid]      INT            IDENTITY (1, 1) NOT NULL,
    [completiontype] NVARCHAR (20)  NOT NULL,
    [section]        NVARCHAR (20)  NOT NULL,
    [title]          NVARCHAR (255) NOT NULL,
    [sectionorder]   INT            NOT NULL,
    [width]          NVARCHAR (255) NULL,
    CONSTRAINT [PK_vmsection_bak] PRIMARY KEY CLUSTERED ([sectionid] ASC)
);

