﻿CREATE TABLE [dbo].[vmrequesttype] (
    [customerid]           VARCHAR (32)  NOT NULL,
    [requesttype]          VARCHAR (9)   NOT NULL,
    [description]          VARCHAR (255) NULL,
    [completiontemplateid] VARCHAR (64)  NULL,
    [billingtemplateid]    VARCHAR (64)  NULL,
    [screeningtemplateid]  VARCHAR (64)  NULL,
    CONSTRAINT [vmrequesttypePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [requesttype] ASC)
);

