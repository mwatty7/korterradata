﻿CREATE TABLE [dbo].[groupoperator] (
    [customerid] VARCHAR (32) NOT NULL,
    [operatorid] VARCHAR (16) NOT NULL,
    [groupid]    VARCHAR (16) NOT NULL,
    CONSTRAINT [groupoperatorPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC, [groupid] ASC)
);

