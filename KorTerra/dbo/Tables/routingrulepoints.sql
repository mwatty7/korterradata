﻿CREATE TABLE [dbo].[routingrulepoints] (
    [rrpointid] UNIQUEIDENTIFIER NOT NULL,
    [rrvalueid] UNIQUEIDENTIFIER NOT NULL,
    [seqno]     INT              NOT NULL,
    [x]         FLOAT (53)       NULL,
    [y]         FLOAT (53)       NULL,
    CONSTRAINT [PK_routingrulepoint] PRIMARY KEY NONCLUSTERED ([rrpointid] ASC),
    CONSTRAINT [FK_routingrulevalue_rrvalueid] FOREIGN KEY ([rrvalueid]) REFERENCES [dbo].[routingrulevalue] ([rrvalueid])
);


GO
CREATE NONCLUSTERED INDEX [IX_routingrulepoints_rrvalueid]
    ON [dbo].[routingrulepoints]([rrvalueid] ASC);

