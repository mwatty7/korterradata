﻿CREATE TABLE [dbo].[payrate] (
    [customerid] VARCHAR (32)    NOT NULL,
    [operatorid] VARCHAR (16)    NOT NULL,
    [st_rate]    DECIMAL (15, 2) NULL,
    [ot_rate]    DECIMAL (15, 2) NULL,
    CONSTRAINT [payratePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [operatorid] ASC)
);

