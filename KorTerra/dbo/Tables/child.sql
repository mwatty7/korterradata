﻿CREATE TABLE [dbo].[child] (
    [childid]  UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [parentid] UNIQUEIDENTIFIER NULL,
    [data]     VARCHAR (32)     NULL,
    CONSTRAINT [PK_child] PRIMARY KEY CLUSTERED ([childid] ASC),
    CONSTRAINT [FK_parent_child] FOREIGN KEY ([parentid]) REFERENCES [dbo].[parent] ([parentid])
);

