﻿CREATE TABLE [dbo].[kt_attachment] (
    [attachmentGuid] UNIQUEIDENTIFIER NOT NULL,
    [ticketGuid]     UNIQUEIDENTIFIER NULL,
    [visitGuid]      UNIQUEIDENTIFIER NULL,
    [name]           VARCHAR (32)     NOT NULL,
    [fullpath]       VARCHAR (256)    NULL,
    CONSTRAINT [PK_kt_attachment] PRIMARY KEY CLUSTERED ([attachmentGuid] ASC),
    CONSTRAINT [FK_kt_attachment_ticketGuid_kt_ticket_ticketGuid] FOREIGN KEY ([ticketGuid]) REFERENCES [dbo].[kt_ticket] ([ticketGuid])
);

