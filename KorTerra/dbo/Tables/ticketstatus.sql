﻿CREATE TABLE [dbo].[ticketstatus] (
    [customerid]           VARCHAR (32) NOT NULL,
    [jobid]                VARCHAR (16) NOT NULL,
    [occid]                VARCHAR (8)  NOT NULL,
    [seqno]                SMALLINT     NOT NULL,
    [sendto]               VARCHAR (16) NOT NULL,
    [xmitdtdate]           DATETIME     NOT NULL,
    [xmitdttime]           VARCHAR (12) NOT NULL,
    [receivedtdate]        DATETIME     NOT NULL,
    [receivedttime]        VARCHAR (12) NOT NULL,
    [mobileid]             VARCHAR (16) NULL,
    [origpriority]         VARCHAR (80) NULL,
    [revisionnumber]       VARCHAR (8)  NULL,
    [servicearea]          VARCHAR (40) NULL,
    [inetticketidentifier] VARCHAR (40) NULL,
    CONSTRAINT [ticketstatusPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [occid] ASC, [seqno] ASC, [sendto] ASC, [xmitdtdate] ASC, [xmitdttime] ASC, [receivedtdate] ASC, [receivedttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ticketstatusjobindex]
    ON [dbo].[ticketstatus]([customerid] ASC, [jobid] ASC);


GO
CREATE NONCLUSTERED INDEX [ticketstatussendtoIndex]
    ON [dbo].[ticketstatus]([customerid] ASC, [sendto] ASC, [xmitdtdate] ASC, [xmitdttime] ASC, [seqno] ASC);

