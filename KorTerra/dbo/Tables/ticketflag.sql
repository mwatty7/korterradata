﻿CREATE TABLE [dbo].[ticketflag] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [ticketflagid]   VARCHAR (32)  NOT NULL,
    [ticketflagtype] VARCHAR (32)  NOT NULL,
    [description]    VARCHAR (80)  NULL,
    [text]           VARCHAR (255) NULL,
    [icon]           VARCHAR (255) NULL,
    CONSTRAINT [ticketflagPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [ticketflagid] ASC, [ticketflagtype] ASC)
);

