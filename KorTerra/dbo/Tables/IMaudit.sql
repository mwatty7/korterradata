﻿CREATE TABLE [dbo].[IMaudit] (
    [customerid]          NVARCHAR (32)  NOT NULL,
    [incidentnumber]      NVARCHAR (32)  CONSTRAINT [DF_IMaudit_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [auditid]             INT            IDENTITY (1, 1) NOT NULL,
    [createdbyoperatorid] NVARCHAR (32)  NOT NULL,
    [createddtutc]        DATETIME       NOT NULL,
    [description]         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_IMaudit] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC, [auditid] ASC),
    CONSTRAINT [FK_IMaudit_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

