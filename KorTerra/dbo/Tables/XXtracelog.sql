﻿CREATE TABLE [dbo].[XXtracelog] (
    [logid]         BIGINT        IDENTITY (1, 1) NOT NULL,
    [applicationid] VARCHAR (256) NULL,
    [customerid]    VARCHAR (32)  NOT NULL,
    [operatorid]    VARCHAR (256) NOT NULL,
    [logdt]         DATETIME2 (7) NOT NULL,
    [type]          VARCHAR (32)  NOT NULL,
    [message]       VARCHAR (MAX) NOT NULL,
    [component]     VARCHAR (256) NULL,
    [stacktrace]    VARCHAR (MAX) NULL,
    [loginid]       VARCHAR (64)  NULL,
    [controllerid]  VARCHAR (64)  NULL,
    [url]           VARCHAR (MAX) NULL,
    [errornumber]   INT           NULL,
    [application]   VARCHAR (32)  NULL,
    [requestid]     VARCHAR (32)  NULL,
    CONSTRAINT [XXtracelogPrimaryKey] PRIMARY KEY CLUSTERED ([logid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [XXTracelogErrornumber]
    ON [dbo].[XXtracelog]([errornumber] ASC, [logdt] ASC);


GO
CREATE NONCLUSTERED INDEX [XXTracelogLogDt]
    ON [dbo].[XXtracelog]([logdt] ASC);


GO
CREATE NONCLUSTERED INDEX [XXTracelogLoginid]
    ON [dbo].[XXtracelog]([loginid] ASC, [logdt] ASC);


GO
CREATE NONCLUSTERED INDEX [XXTracelogOperatorid]
    ON [dbo].[XXtracelog]([customerid] ASC, [operatorid] ASC, [logdt] ASC);


GO
CREATE NONCLUSTERED INDEX [XXTracelogRequestid]
    ON [dbo].[XXtracelog]([requestid] ASC, [logdt] ASC);


GO
CREATE NONCLUSTERED INDEX [XXTracelogType]
    ON [dbo].[XXtracelog]([type] ASC, [logdt] ASC);

