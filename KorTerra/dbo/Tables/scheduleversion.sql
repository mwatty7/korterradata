﻿CREATE TABLE [dbo].[scheduleversion] (
    [scheduleversionid] UNIQUEIDENTIFIER NOT NULL,
    [scheduleid]        UNIQUEIDENTIFIER NOT NULL,
    [name]              VARCHAR (80)     NOT NULL,
    [description]       VARCHAR (255)    NULL,
    [status]            VARCHAR (32)     NOT NULL,
    [creationdtutc]     DATETIME2 (7)    NOT NULL,
    [createdby]         VARCHAR (80)     NOT NULL,
    CONSTRAINT [PK_scheduleversion_scheduleversionid] PRIMARY KEY NONCLUSTERED ([scheduleversionid] ASC),
    FOREIGN KEY ([scheduleid]) REFERENCES [dbo].[schedule] ([scheduleid])
);


GO
CREATE NONCLUSTERED INDEX [IX_name_status_creationdtutc_createdby]
    ON [dbo].[scheduleversion]([name] ASC, [status] ASC)
    INCLUDE([creationdtutc], [createdby]);

