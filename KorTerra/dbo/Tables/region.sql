﻿CREATE TABLE [dbo].[region] (
    [customerid]         VARCHAR (32) NOT NULL,
    [regionid]           VARCHAR (16) NOT NULL,
    [description]        VARCHAR (32) NULL,
    [accntmajorcode]     VARCHAR (4)  NULL,
    [msggroupid]         VARCHAR (16) NULL,
    [lastfiscalyear]     SMALLINT     NULL,
    [nextsequencenumber] SMALLINT     NULL,
    [usercode]           VARCHAR (8)  NULL,
    CONSTRAINT [regionPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [regionid] ASC)
);

