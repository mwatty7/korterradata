﻿CREATE TABLE [dbo].[areapoint] (
    [customerid] VARCHAR (32) NOT NULL,
    [suffixid]   VARCHAR (16) NOT NULL,
    [zoneid]     VARCHAR (16) NOT NULL,
    [areanumber] INT          NOT NULL,
    [seqno]      INT          NOT NULL,
    [x]          FLOAT (53)   NULL,
    [y]          FLOAT (53)   NULL,
    CONSTRAINT [areapointPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC, [zoneid] ASC, [areanumber] ASC, [seqno] ASC)
);

