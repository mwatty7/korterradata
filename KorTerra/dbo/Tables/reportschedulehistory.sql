﻿CREATE TABLE [dbo].[reportschedulehistory] (
    [reportschedulehistoryid] UNIQUEIDENTIFIER CONSTRAINT [DF_reportschedulehistory_reportschedulehistoryid] DEFAULT (newsequentialid()) NOT NULL,
    [reportscheduleversionid] UNIQUEIDENTIFIER NOT NULL,
    [status]                  VARCHAR (32)     NOT NULL,
    [creationdtutc]           DATETIME2 (7)    NOT NULL,
    [workdtutc]               DATETIME2 (7)    NOT NULL,
    [reportrunutc]            DATETIME2 (7)    NULL,
    [filename]                VARCHAR (256)    NULL,
    [reportpath]              VARCHAR (256)    NULL,
    [kilobyte]                INT              NOT NULL,
    [expirationdtutc]         DATETIME2 (7)    NOT NULL,
    CONSTRAINT [PK_reportschedulehistory] PRIMARY KEY CLUSTERED ([reportschedulehistoryid] ASC),
    CONSTRAINT [FK_reportschedulehistory_reportscheduleversion] FOREIGN KEY ([reportscheduleversionid]) REFERENCES [dbo].[reportscheduleversion] ([reportscheduleversionid])
);


GO
CREATE NONCLUSTERED INDEX [IX_reportschedulehistory_reportscheduleversionid_include]
    ON [dbo].[reportschedulehistory]([reportscheduleversionid] ASC)
    INCLUDE([reportschedulehistoryid], [status], [creationdtutc], [workdtutc], [reportrunutc], [filename], [reportpath], [kilobyte], [expirationdtutc]);

