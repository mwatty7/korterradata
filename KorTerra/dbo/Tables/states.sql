﻿CREATE TABLE [dbo].[states] (
    [statenbr]     INT          NOT NULL,
    [abbreviation] VARCHAR (3)  NULL,
    [statename]    VARCHAR (32) NULL,
    CONSTRAINT [statesPrimaryKey] PRIMARY KEY CLUSTERED ([statenbr] ASC)
);

