﻿CREATE TABLE [dbo].[screeningdecisioncommon] (
    [screeningid]           INT           IDENTITY (1, 1) NOT NULL,
    [customerid]            VARCHAR (32)  NOT NULL,
    [jobid]                 VARCHAR (16)  NOT NULL,
    [membercode]            VARCHAR (16)  NOT NULL,
    [requesttype]           VARCHAR (9)   NOT NULL,
    [creationdateutc]       DATETIME2 (7) NOT NULL,
    [templateid]            VARCHAR (64)  NOT NULL,
    [templateversionnumber] INT           NOT NULL,
    [operatorid]            VARCHAR (16)  NOT NULL,
    [decisionreason]        VARCHAR (64)  NULL,
    [decision]              VARCHAR (16)  NOT NULL,
    [remarks]               VARCHAR (MAX) NULL,
    CONSTRAINT [PK_screeningdecisioncommon] PRIMARY KEY CLUSTERED ([screeningid] ASC)
);

