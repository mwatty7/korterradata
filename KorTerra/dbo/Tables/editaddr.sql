﻿CREATE TABLE [dbo].[editaddr] (
    [customerid]  VARCHAR (32) NOT NULL,
    [jobid]       VARCHAR (16) NOT NULL,
    [editaddress] VARCHAR (39) NULL,
    CONSTRAINT [editaddrPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC)
);

