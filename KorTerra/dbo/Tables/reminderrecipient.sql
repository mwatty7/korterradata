﻿CREATE TABLE [dbo].[reminderrecipient] (
    [reminderrecipientid] UNIQUEIDENTIFIER NOT NULL,
    [reminderid]          UNIQUEIDENTIFIER NOT NULL,
    [recipient]           NVARCHAR (80)    NULL,
    CONSTRAINT [reminderrecipientPrimaryKey] PRIMARY KEY NONCLUSTERED ([reminderrecipientid] ASC),
    CONSTRAINT [FK_reminderid_remindercommon_notifs] FOREIGN KEY ([reminderid]) REFERENCES [dbo].[remindercommon] ([reminderid])
);

