﻿CREATE TABLE [dbo].[IMinvestigationremark] (
    [investigationremarkid] INT            IDENTITY (1, 1) NOT NULL,
    [customerid]            NVARCHAR (32)  NOT NULL,
    [incidentnumber]        NVARCHAR (32)  CONSTRAINT [DF_IMinvestigationremark_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [createdbyoperatorid]   NVARCHAR (32)  NOT NULL,
    [createddtutc]          DATETIME       NOT NULL,
    [description]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_IMinvestigationremark] PRIMARY KEY CLUSTERED ([investigationremarkid] ASC),
    CONSTRAINT [FK_IMinvestigationremark_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);

