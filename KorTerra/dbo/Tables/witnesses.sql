﻿CREATE TABLE [dbo].[witnesses] (
    [claimid]   VARCHAR (16) NOT NULL,
    [witnessid] VARCHAR (32) NOT NULL,
    [name]      VARCHAR (32) NULL,
    [address1]  VARCHAR (32) NULL,
    [address2]  VARCHAR (32) NULL,
    [city]      VARCHAR (64) NULL,
    [state]     VARCHAR (3)  NULL,
    [zip]       VARCHAR (11) NULL,
    [phone]     VARCHAR (21) NULL,
    [email]     VARCHAR (80) NULL,
    CONSTRAINT [witnessesPrimaryKey] PRIMARY KEY CLUSTERED ([claimid] ASC, [witnessid] ASC)
);

