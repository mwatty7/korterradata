﻿CREATE TABLE [dbo].[wojobid] (
    [customerid]       VARCHAR (32) NOT NULL,
    [jobidmain]        VARCHAR (16) NOT NULL,
    [maxjobidsequence] SMALLINT     NULL,
    CONSTRAINT [wojobidPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobidmain] ASC)
);

