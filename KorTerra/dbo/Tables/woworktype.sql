﻿CREATE TABLE [dbo].[woworktype] (
    [customerid]   VARCHAR (32) NOT NULL,
    [occid]        VARCHAR (8)  NOT NULL,
    [tickettypeid] VARCHAR (32) NOT NULL,
    [worktypeid]   VARCHAR (62) NOT NULL,
    CONSTRAINT [woworktypePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [occid] ASC, [tickettypeid] ASC, [worktypeid] ASC)
);

