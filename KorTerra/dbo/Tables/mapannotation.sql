﻿CREATE TABLE [dbo].[mapannotation] (
    [shapeid]        INT           IDENTITY (1, 1) NOT NULL,
    [customerid]     VARCHAR (32)  NOT NULL,
    [label]          VARCHAR (64)  NOT NULL,
    [shapetype]      VARCHAR (32)  NOT NULL,
    [color]          VARCHAR (32)  NOT NULL,
    [comment]        VARCHAR (MAX) NULL,
    [operatorid]     VARCHAR (16)  NOT NULL,
    [isactive]       SMALLINT      NULL,
    [lastmodifieddt] DATETIME      NULL,
    CONSTRAINT [PK_mapannotation] PRIMARY KEY CLUSTERED ([shapeid] ASC)
);

