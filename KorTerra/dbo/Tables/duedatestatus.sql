﻿CREATE TABLE [dbo].[duedatestatus] (
    [customerid]      VARCHAR (32) NOT NULL,
    [name]            VARCHAR (64) NOT NULL,
    [displayname]     VARCHAR (64) NOT NULL,
    [minutesuntildue] INT          NOT NULL,
    [icon]            VARCHAR (32) NOT NULL,
    [isproject]       SMALLINT     NOT NULL,
    CONSTRAINT [PK_duedatestatus] PRIMARY KEY CLUSTERED ([customerid] ASC, [name] ASC)
);

