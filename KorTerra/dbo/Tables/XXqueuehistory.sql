﻿CREATE TABLE [dbo].[XXqueuehistory] (
    [queuehistoryid] UNIQUEIDENTIFIER NOT NULL,
    [queueid]        UNIQUEIDENTIFIER NULL,
    [customerid]     VARCHAR (32)     NOT NULL,
    [context]        VARCHAR (MAX)    NOT NULL,
    [type]           VARCHAR (32)     NOT NULL,
    [creationdtutc]  DATETIME2 (7)    NOT NULL,
    [workdtutc]      DATETIME2 (7)    NOT NULL,
    [attemptdtutc]   DATETIME2 (7)    NOT NULL,
    [status]         VARCHAR (32)     NOT NULL,
    [reason]         VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_queuehistory_queuehistoryid] PRIMARY KEY NONCLUSTERED ([queuehistoryid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_XXqueuehistory_queueid_context_type]
    ON [dbo].[XXqueuehistory]([queueid] ASC, [customerid] ASC)
    INCLUDE([context], [type], [creationdtutc]);

