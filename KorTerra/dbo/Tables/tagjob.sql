﻿CREATE TABLE [dbo].[tagjob] (
    [customerid] VARCHAR (32)     NOT NULL,
    [jobid]      VARCHAR (16)     NOT NULL,
    [tagid]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_tagjob_customerid_jobid_tagid] PRIMARY KEY NONCLUSTERED ([customerid] ASC, [jobid] ASC, [tagid] ASC),
    FOREIGN KEY ([tagid]) REFERENCES [dbo].[tag] ([tagid])
);

