﻿CREATE TABLE [dbo].[IMincidentContributingFactor] (
    [customerid]         NVARCHAR (32) NOT NULL,
    [incidentnumber]     NVARCHAR (32) CONSTRAINT [DF_IMincidentContributingFactor_incidentnumber] DEFAULT (N'UNASSIGNED') NOT NULL,
    [codetype]           NVARCHAR (32) NOT NULL,
    [contributingFactor] NVARCHAR (32) NOT NULL,
    [codeid]             INT           NULL,
    CONSTRAINT [PK_IMincidentContributingFactor] PRIMARY KEY CLUSTERED ([customerid] ASC, [incidentnumber] ASC, [codetype] ASC, [contributingFactor] ASC),
    CONSTRAINT [FK_IMcontributingFactors_IMcode] FOREIGN KEY ([codeid]) REFERENCES [dbo].[IMcode] ([id]),
    CONSTRAINT [FK_IMincidentContributingFactor_IMincident] FOREIGN KEY ([customerid], [incidentnumber]) REFERENCES [dbo].[IMincident] ([customerid], [incidentnumber])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_IMincidentContributingFactor]
    ON [dbo].[IMincidentContributingFactor]([customerid] ASC, [incidentnumber] ASC, [codetype] ASC, [contributingFactor] ASC);

