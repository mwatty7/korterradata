﻿CREATE TABLE [dbo].[herald] (
    [customerid]  VARCHAR (32) NOT NULL,
    [heraldid]    VARCHAR (32) NOT NULL,
    [description] VARCHAR (32) NULL,
    [sitename]    VARCHAR (16) NULL,
    [accesscount] INT          NULL,
    CONSTRAINT [heraldPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [heraldid] ASC)
);

