﻿CREATE TABLE [dbo].[__vdvalue] (
    [customerid]  NVARCHAR (32)  NOT NULL,
    [jobid]       NVARCHAR (16)  NOT NULL,
    [membercode]  NVARCHAR (16)  NOT NULL,
    [requesttype] NVARCHAR (9)   NOT NULL,
    [creationdt]  DATETIME       NOT NULL,
    [sectionid]   INT            NOT NULL,
    [fieldid]     INT            NOT NULL,
    [value]       NVARCHAR (MAX) NULL,
    [label]       NVARCHAR (255) NULL,
    [fieldorder]  INT            NULL,
    CONSTRAINT [vdvaluePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC, [requesttype] ASC, [creationdt] ASC, [sectionid] ASC, [fieldid] ASC)
);

