﻿CREATE TABLE [dbo].[memcode] (
    [customerid]            VARCHAR (32)  NOT NULL,
    [membercode]            VARCHAR (16)  NOT NULL,
    [description]           VARCHAR (32)  NULL,
    [memberid]              VARCHAR (16)  NULL,
    [sendaddress]           VARCHAR (128) NULL,
    [byaddmembers]          SMALLINT      NULL,
    [autoauditdiscrepancy]  SMALLINT      NULL,
    [printocaudit]          SMALLINT      NULL,
    [completiontype1]       VARCHAR (9)   NULL,
    [completiontype2]       VARCHAR (9)   NULL,
    [completiontype3]       VARCHAR (9)   NULL,
    [completiontype4]       VARCHAR (9)   NULL,
    [completiontype5]       VARCHAR (9)   NULL,
    [completiontype6]       VARCHAR (9)   NULL,
    [invoicereportname]     VARCHAR (80)  NULL,
    [invoicereportinterval] CHAR (1)      NULL,
    [compflaggroup]         VARCHAR (16)  NULL,
    [isfiltered]            SMALLINT      NULL,
    [servicearea]           VARCHAR (40)  NULL,
    [occid]                 VARCHAR (8)   NULL,
    [originatingoccid]      VARCHAR (8)   NULL,
    [msggroupid]            VARCHAR (16)  NULL,
    [mapviewer]             VARCHAR (32)  NULL,
    [isautoconflict]        SMALLINT      NULL,
    CONSTRAINT [memcodePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [membercode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [membercodeOccIndex]
    ON [dbo].[memcode]([membercode] ASC, [occid] ASC);

