﻿CREATE TABLE [dbo].[area] (
    [customerid] VARCHAR (32)  NOT NULL,
    [suffixid]   VARCHAR (16)  NOT NULL,
    [zoneid]     VARCHAR (16)  NOT NULL,
    [areanumber] INT           NOT NULL,
    [occid]      VARCHAR (8)   NULL,
    [method]     VARCHAR (16)  NULL,
    [value]      VARCHAR (128) NULL,
    [minx]       FLOAT (53)    NULL,
    [maxx]       FLOAT (53)    NULL,
    [miny]       FLOAT (53)    NULL,
    [maxy]       FLOAT (53)    NULL,
    CONSTRAINT [areaPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [suffixid] ASC, [zoneid] ASC, [areanumber] ASC)
);


GO
CREATE NONCLUSTERED INDEX [areaCoords]
    ON [dbo].[area]([customerid] ASC, [suffixid] ASC, [method] ASC, [minx] ASC, [miny] ASC, [maxx] ASC, [maxy] ASC);


GO
CREATE NONCLUSTERED INDEX [areaIndex]
    ON [dbo].[area]([customerid] ASC, [occid] ASC, [method] ASC, [value] ASC, [suffixid] ASC);

