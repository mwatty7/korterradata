﻿CREATE TABLE [dbo].[ScreeningRule] (
    [ruleid]     INT          IDENTITY (1, 1) NOT NULL,
    [rulesetid]  INT          NOT NULL,
    [ruletypeid] INT          NOT NULL,
    [seqno]      INT          NOT NULL,
    [action]     VARCHAR (32) NOT NULL,
    [condition]  VARCHAR (32) NULL,
    [mobileid]   VARCHAR (16) NULL,
    CONSTRAINT [PK_ScreeningRule] PRIMARY KEY CLUSTERED ([ruleid] ASC),
    CONSTRAINT [FK_ScreeningRuleScreeningRuleset] FOREIGN KEY ([rulesetid]) REFERENCES [dbo].[ScreeningRuleset] ([rulesetid]),
    CONSTRAINT [FK_ScreeningRuleScreeningRuletype] FOREIGN KEY ([ruletypeid]) REFERENCES [dbo].[ScreeningRuletype] ([ruletypeid])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'action is NEEDS_LOCATE|CLEAR|SCREEN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRule', @level2type = N'COLUMN', @level2name = N'action';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'One of the rules in a set', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScreeningRule';

