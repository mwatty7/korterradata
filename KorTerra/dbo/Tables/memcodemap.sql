﻿CREATE TABLE [dbo].[memcodemap] (
    [occmembercode] VARCHAR (16) NOT NULL,
    [occid]         VARCHAR (8)  NOT NULL,
    [customerid]    VARCHAR (32) NULL,
    [membercode]    VARCHAR (16) NULL,
    CONSTRAINT [memcodemapPrimaryKey] PRIMARY KEY CLUSTERED ([occmembercode] ASC, [occid] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [memcodemapindex]
    ON [dbo].[memcodemap]([customerid] ASC, [membercode] ASC);

