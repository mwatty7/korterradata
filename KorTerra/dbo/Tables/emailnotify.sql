﻿CREATE TABLE [dbo].[emailnotify] (
    [customerid]    VARCHAR (32) NOT NULL,
    [emailnotifyid] SMALLINT     NOT NULL,
    [msggroupid]    VARCHAR (16) NOT NULL,
    [msgtype]       VARCHAR (32) NOT NULL,
    [days]          VARCHAR (32) NULL,
    [starttime]     VARCHAR (12) NULL,
    [endtime]       VARCHAR (12) NULL,
    [emailaddress1] VARCHAR (80) NULL,
    [emailaddress2] VARCHAR (80) NULL,
    [emailaddress3] VARCHAR (80) NULL,
    [tplfilename]   VARCHAR (80) NULL,
    CONSTRAINT [emailnotifyPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [emailnotifyid] ASC, [msggroupid] ASC, [msgtype] ASC)
);

