﻿CREATE TABLE [dbo].[exp_order] (
    [k_customerid]        VARCHAR (16)  NOT NULL,
    [k_jobid]             VARCHAR (16)  NOT NULL,
    [k_membercode]        VARCHAR (16)  NOT NULL,
    [k_requesttype]       VARCHAR (9)   NOT NULL,
    [k_mobileid]          VARCHAR (16)  NOT NULL,
    [k_senddtdate]        DATETIME      NOT NULL,
    [k_senddttime]        VARCHAR (12)  NOT NULL,
    [e_version]           VARCHAR (16)  NULL,
    [e_status]            CHAR (1)      NULL,
    [e_failreason]        VARCHAR (255) NULL,
    [e_localid]           VARCHAR (255) NULL,
    [t_occid]             VARCHAR (8)   NULL,
    [t_ticketnumber]      VARCHAR (40)  NULL,
    [t_sendto]            VARCHAR (10)  NULL,
    [t_servicearea]       VARCHAR (128) NULL,
    [z_address]           VARCHAR (128) NULL,
    [z_returnaddress]     VARCHAR (128) NULL,
    [z_priority]          SMALLINT      NULL,
    [z_packettype]        SMALLINT      NULL,
    [z_initialsenddtdate] DATETIME      NULL,
    [z_initialsenddttime] VARCHAR (12)  NULL,
    [z_initialid]         INT           NULL,
    [z_identifier]        VARCHAR (64)  NULL,
    [z_versiondtdate]     DATETIME      NULL,
    [z_versiondttime]     VARCHAR (12)  NULL,
    [z_extensiontype]     SMALLINT      NULL,
    [z_extensionsize]     INT           NULL,
    CONSTRAINT [exp_orderPrimaryKey] PRIMARY KEY CLUSTERED ([k_customerid] ASC, [k_jobid] ASC, [k_membercode] ASC, [k_requesttype] ASC, [k_mobileid] ASC, [k_senddtdate] ASC, [k_senddttime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [exp_order_localid_key]
    ON [dbo].[exp_order]([k_customerid] ASC, [e_localid] ASC);


GO
CREATE NONCLUSTERED INDEX [exp_order_senddt_key]
    ON [dbo].[exp_order]([k_customerid] ASC, [k_senddtdate] ASC, [k_senddttime] ASC);

