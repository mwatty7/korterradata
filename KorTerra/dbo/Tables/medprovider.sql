﻿CREATE TABLE [dbo].[medprovider] (
    [workcompid]  VARCHAR (16) NOT NULL,
    [medprovider] VARCHAR (32) NOT NULL,
    [doctor]      VARCHAR (32) NOT NULL,
    [address1]    VARCHAR (32) NULL,
    [address2]    VARCHAR (32) NULL,
    [city]        VARCHAR (64) NULL,
    [state]       VARCHAR (3)  NULL,
    [zip]         VARCHAR (11) NULL,
    [phone]       VARCHAR (21) NULL,
    CONSTRAINT [medproviderPrimaryKey] PRIMARY KEY CLUSTERED ([workcompid] ASC, [medprovider] ASC, [doctor] ASC)
);

