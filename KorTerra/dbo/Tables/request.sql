﻿CREATE TABLE [dbo].[request] (
    [customerid]             VARCHAR (32)  NOT NULL,
    [jobid]                  VARCHAR (16)  NOT NULL,
    [membercode]             VARCHAR (16)  NOT NULL,
    [versiondtdate]          DATETIME      NULL,
    [versiondttime]          VARCHAR (12)  NULL,
    [versionauthor]          VARCHAR (16)  NULL,
    [xmitsource]             VARCHAR (8)   NULL,
    [xmitdtdate]             DATETIME      NULL,
    [xmitdttime]             VARCHAR (12)  NULL,
    [seqno]                  SMALLINT      NULL,
    [finalstatus]            CHAR (1)      NULL,
    [firstcompletiondtdate]  DATETIME      NULL,
    [firstcompletiondttime]  VARCHAR (12)  NULL,
    [firstcompletiondateutc] DATETIME2 (7) NULL,
    CONSTRAINT [requestPrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [jobid] ASC, [membercode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [requestmembercodeIndex]
    ON [dbo].[request]([customerid] ASC, [membercode] ASC, [xmitdtdate] ASC, [xmitdttime] ASC, [finalstatus] ASC);


GO
CREATE NONCLUSTERED INDEX [requestxmitddtindex]
    ON [dbo].[request]([customerid] ASC, [xmitdtdate] ASC, [xmitdttime] ASC);

