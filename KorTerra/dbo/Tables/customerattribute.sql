﻿CREATE TABLE [dbo].[customerattribute] (
    [customerid]     VARCHAR (32)  NOT NULL,
    [attributename]  VARCHAR (255) NOT NULL,
    [attributevalue] VARCHAR (255) NULL,
    CONSTRAINT [customerattributePrimaryKey] PRIMARY KEY CLUSTERED ([customerid] ASC, [attributename] ASC)
);

