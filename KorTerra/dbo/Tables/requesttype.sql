﻿CREATE TABLE [dbo].[requesttype] (
    [requesttype]       VARCHAR (9)  NOT NULL,
    [customerid]        VARCHAR (32) NULL,
    [genericname]       VARCHAR (9)  NULL,
    [tablename]         VARCHAR (16) NULL,
    [htmltemplateid]    VARCHAR (32) NULL,
    [texttemplateid]    VARCHAR (32) NULL,
    [winformtemplateid] VARCHAR (32) NULL,
    [hascustomactcode]  SMALLINT     NULL,
    [hascustomfaccode]  SMALLINT     NULL,
    CONSTRAINT [requesttypePrimaryKey] PRIMARY KEY CLUSTERED ([requesttype] ASC)
);

